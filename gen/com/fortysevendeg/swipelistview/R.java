/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.fortysevendeg.swipelistview;

public final class R {
	public static final class attr {
		public static final int swipeActionLeft = 0x7f0100f0;
		public static final int swipeActionRight = 0x7f0100f1;
		public static final int swipeAnimationTime = 0x7f0100e9;
		public static final int swipeBackView = 0x7f0100ee;
		public static final int swipeCloseAllItemsWhenMoveList = 0x7f0100ec;
		public static final int swipeDrawableChecked = 0x7f0100f2;
		public static final int swipeDrawableUnchecked = 0x7f0100f3;
		public static final int swipeFrontView = 0x7f0100ed;
		public static final int swipeMode = 0x7f0100ef;
		public static final int swipeOffsetLeft = 0x7f0100ea;
		public static final int swipeOffsetRight = 0x7f0100eb;
		public static final int swipeOpenOnLongPress = 0x7f0100e8;
	}
	public static final class id {
		public static final int both = 0x7f060039;
		public static final int choice = 0x7f06003c;
		public static final int dismiss = 0x7f06003b;
		public static final int left = 0x7f06001b;
		public static final int none = 0x7f060010;
		public static final int reveal = 0x7f06003a;
		public static final int right = 0x7f06001c;
	}
	public static final class styleable {
		public static final int[] SwipeListView = { 0x7f0100e8, 0x7f0100e9, 0x7f0100ea, 0x7f0100eb, 0x7f0100ec, 0x7f0100ed, 0x7f0100ee, 0x7f0100ef, 0x7f0100f0, 0x7f0100f1, 0x7f0100f2, 0x7f0100f3 };
		public static final int SwipeListView_swipeActionLeft = 8;
		public static final int SwipeListView_swipeActionRight = 9;
		public static final int SwipeListView_swipeAnimationTime = 1;
		public static final int SwipeListView_swipeBackView = 6;
		public static final int SwipeListView_swipeCloseAllItemsWhenMoveList = 4;
		public static final int SwipeListView_swipeDrawableChecked = 10;
		public static final int SwipeListView_swipeDrawableUnchecked = 11;
		public static final int SwipeListView_swipeFrontView = 5;
		public static final int SwipeListView_swipeMode = 7;
		public static final int SwipeListView_swipeOffsetLeft = 2;
		public static final int SwipeListView_swipeOffsetRight = 3;
		public static final int SwipeListView_swipeOpenOnLongPress = 0;
	}
}
