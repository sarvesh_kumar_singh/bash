package com.bash.Activities;

import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.SignUpStepThreeActivity.CheckOutProcess;
import com.bash.Application.BashApplication;
import com.bash.GsonClasses.Login_Register_Class;
import com.bash.Managers.ActivityManager;
import com.bash.Managers.DataBaseManager;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.MyAsynTaskManager.LoadListener;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.ContactsProvider;
import com.bash.Utils.AppUrlList;
import com.google.gson.Gson;

public class SignUpStepThreeActivity extends Activity {
	
	Spinner dateSpinner, yearSpinner;
	Login_Register_Class responseForLogin;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup_stepthree);
		
		dateSpinner = (Spinner) findViewById(R.id.dateSpinner);
		yearSpinner = (Spinner) findViewById(R.id.yearSpinner);
		
		dateSpinner.setAdapter(BashApplication.daysAdapter);
		yearSpinner.setAdapter(BashApplication.yearsAdapter);
		
		((Button)findViewById(R.id.continueButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				makeFinalStep();
			}
		});
		
		((Button)findViewById(R.id.saveCardDetailsButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.e("time", dateSpinner.getSelectedItem().toString()+"/"+yearSpinner.getSelectedItem().toString());
				if(ValidateForm())
					saveCardNumberDetails();
			}
		});
		
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}
	
 	public boolean ValidateForm() {
		
		  if (TextUtils.isEmpty(((EditText) findViewById(R.id.cardNumberText)).getText().toString())){
			((EditText) findViewById(R.id.cardNumberText)).setError("Field is Empty");
			return false;
		  }else if(((EditText) findViewById(R.id.cardNumberText)).length() != 16){
			  Toast.makeText(getApplicationContext(), "Please Enter 16 digit valid card number!", Toast.LENGTH_SHORT).show();
			  return false;
		  }
		return true;
	}
	
public void makeFinalStep() {
		
		MyAsynTaskManager.setupParamsAndUrl(AppUrlList.ACTION_URL, new String[]{
				"module",
				"action",
				"iduser"
				},
			new String[]{
				"user",
				"finalstep",
				PreferenceManager.getInstance().getTempUserId()
				});

		new MyAsynTaskManager(SignUpStepThreeActivity.this, new LoadListener() {
			@Override
			public void onLoadComplete(final String jsonResponse) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						try {
							JSONObject rootObj = new JSONObject(jsonResponse);
							if(rootObj.getBoolean("result")){
								
								Gson gson = new Gson();
								
								responseForLogin = gson.fromJson(jsonResponse, Login_Register_Class.class);
									
								if(responseForLogin.result) {
									PreferenceManager.getInstance().setUserDetails
										(responseForLogin.iduser, 
										 responseForLogin.email_id,
										 responseForLogin.firstname,
										 responseForLogin.lastname,
										 responseForLogin.userimage, 
										 responseForLogin.PartnerCode,
										 responseForLogin.PartnerUsername,
										 responseForLogin.PartnerPassword,
										 responseForLogin.phone_no, 
										 responseForLogin.facebookid,
										 responseForLogin.pushnotify_on,
										 responseForLogin.credit,
										 responseForLogin.username
										 );
									BashApplication.isUserLoggedIn = true;
									new CheckOutProcess().execute();
								}
								 
							}else{
								DialogManager.showDialog(SignUpStepThreeActivity.this, rootObj.getString("msg"));
							}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
							DialogManager.showDialog(SignUpStepThreeActivity.this, "Server Error Occured! Try Again!");
						}
					}
				});
			}
			
			@Override
			public void onError(final String errorMessage) {
				runOnUiThread(new Runnable() {
					public void run() {
						DialogManager.showDialog(SignUpStepThreeActivity.this, errorMessage);		
					}
				});
				
			}
		}).execute();
	}
	 
class CheckOutProcess extends AsyncTask<Void, Void, Void> {
	ProgressDialog pd;
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		Log.e("Preexecute Called", "");
		pd = new ProgressDialog(SignUpStepThreeActivity.this);
		pd.setMessage("Loading CheckOut Process...");
		pd.show();
		super.onPreExecute();
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
		try {
			ContactsProvider.loadContacts(SignUpStepThreeActivity.this);
			DataBaseManager.getInstance().checkOutContactTable(responseForLogin.bashusers);	
		} catch (Exception e) {
			// TODO: handle exception
			pd.dismiss();
		}
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		pd.dismiss();
		BashApplication.isUserLoggedIn = true;
		ActivityManager.startActivity(SignUpStepThreeActivity.this, HomeActivity.class);
		finish();
	}
}

	public void saveCardNumberDetails() {
		
		//	01-06 15:22:01.710: E/Reponse :(24990): {"result":false,"msg":"false"}

		MyAsynTaskManager.setupParamsAndUrl(AppUrlList.ACTION_URL, new String[]{
				"module",
				"action",
				"iduser",
				"card_type",
				"card_number",
				"valid_upto",
				"default"
				},
			new String[]{
				"user",
				"addcarddetail",
				PreferenceManager.getInstance().getTempUserId(),
				(((RadioButton) findViewById(R.id.creditButton)).isChecked()) ? "credit" : "debit",
				((EditText) findViewById(R.id.cardNumberText)).getText().toString(),
				"01/"+dateSpinner.getSelectedItem().toString()+"/"+yearSpinner.getSelectedItem().toString(),
			 	(((CheckBox) findViewById(R.id.defaultCheckBox)).isChecked()) ? "1" : "0" });

		new MyAsynTaskManager(SignUpStepThreeActivity.this, new LoadListener() {
			@Override
			public void onLoadComplete(final String jsonResponse) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						try {
							JSONObject rootObj = new JSONObject(jsonResponse);
							if(rootObj.getBoolean("result")){
							 	((EditText) findViewById(R.id.cardNumberText)).setText("");
								((CheckBox) findViewById(R.id.defaultCheckBox)).setChecked(false);
								((Button) findViewById(R.id.saveCardDetailsButton)).setText("Add");
								DialogManager.showDialog(SignUpStepThreeActivity.this, "Card Details Added Successfully!");
							}else{
								DialogManager.showDialog(SignUpStepThreeActivity.this, "Error In Adding Card Details!");
							}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
							DialogManager.showDialog(SignUpStepThreeActivity.this, "Server Error Occured! Try Again!");
						}
					}
				});
			}
			
			@Override
			public void onError(final String errorMessage) {
				runOnUiThread(new Runnable() {
					public void run() {
						DialogManager.showDialog(SignUpStepThreeActivity.this, errorMessage);		
					}
				});
				
			}
		}).execute();
	}
	
	

	 
}

