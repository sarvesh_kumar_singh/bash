package com.bash.ListModels;

public class CashInOutGroup_Class {

	public String eventName, totalAmount, subTotalAmount;
	//boolean HaveYouPaid = false;
	int TypeOfPaid = 0;
	
	public CashInOutGroup_Class(String eventName, String totalAmount, String subTotalAmount, int TypeOfPaid){
		this.eventName = eventName;
		this.totalAmount = totalAmount;
		this.subTotalAmount = subTotalAmount;
		this.TypeOfPaid = TypeOfPaid;
	}
	
	public String geteventName(){
		return eventName;
	}
	
	public String gettotalAmount(){
		return totalAmount;
	}
	
	public String getsubTotalAmount(){
		return subTotalAmount;
	}
	
	public int getTypeOfPaid(){
		return TypeOfPaid;
	}
	
}

