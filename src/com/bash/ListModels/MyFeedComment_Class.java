package com.bash.ListModels;

public class MyFeedComment_Class {

	public String commentantName, commentLikeCount, commentText, timeofComment, commentantImage, commentId, isLiked;
	
	
	public MyFeedComment_Class(String commentantImage,String commentantNam, String commentLikeCount, String commentText, String timeofComment,
			String commentId, String isLiked){
		this.commentantImage = commentantImage;
		this.commentantName = commentantNam;
		this.commentLikeCount = commentLikeCount;
		this.commentText = commentText;
		this.timeofComment = timeofComment;
		this.commentId= commentId;
		this.isLiked = isLiked;
	}
	
	public String getisLiked(){
		return isLiked;
	}
	
	public void setisLiked(String isLiked){
		this.isLiked = isLiked; 
	}
	
	public String getcommentId(){
		return commentId;
	}
	public String getcommentantImage(){
		return commentantImage;
	}
	
	public String getcommentantName(){
		return commentantName;
	}
	
	public String getcommentLikeCount(){
		return commentLikeCount;
	}
	
	public void setcommentLikeCount(String commentLikeCount){
		this.commentLikeCount= commentLikeCount;
	}
	
	public String getcommentText(){
		return commentText;
	}
	
	public String gettimeofComment(){
		return timeofComment;
	}
	
	
	 
	 
}

