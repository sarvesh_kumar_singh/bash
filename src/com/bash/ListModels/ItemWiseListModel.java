package com.bash.ListModels;

import java.util.ArrayList;
import java.util.List;

public class ItemWiseListModel {
	
private int id ;
private String prsnName = "";
private ArrayList<TwoWayPersonModel> twowayPersonList = null;
private String itemAmount = "";
private boolean itemSelect;

public ItemWiseListModel(String prsnName, String itemAmount, ArrayList<TwoWayPersonModel> twowayPerson){
	
	this.prsnName = prsnName;
	this.itemAmount = itemAmount;
	this.twowayPersonList = twowayPerson;
}
public ItemWiseListModel(String prsnName, String itemAmount){
	
	this.prsnName = prsnName;
	this.itemAmount = itemAmount;
}
public ItemWiseListModel(){
	
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getPrsnName() {
	return prsnName;
}
public void setPrsnName(String prsnName) {
	this.prsnName = prsnName;
}
public ArrayList<TwoWayPersonModel> getTwowayPersonList() {
	return twowayPersonList;
}
public void setTwowayPersonList(ArrayList<TwoWayPersonModel> twowayPersonList) {
	this.twowayPersonList = twowayPersonList;
}

public String getItemAmount() {
	return itemAmount;
}
public void setItemAmount(String itemAmount) {
	this.itemAmount = itemAmount;
}
public boolean isItemSelect() {
	return itemSelect;
}
public void setItemSelect(boolean itemSelect) {
	this.itemSelect = itemSelect;
}


}
