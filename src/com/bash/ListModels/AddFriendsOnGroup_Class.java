package com.bash.ListModels;

public class AddFriendsOnGroup_Class {

	public String friendName;
	public int friendImageSource;
	
	public void setFieldValues(String friendName, int friendImageSource){
		this.friendName = friendName;
		this.friendImageSource = friendImageSource;
	}
	
	public AddFriendsOnGroup_Class(String friendName, int friendImageSource){
		this.friendName = friendName;
		this.friendImageSource = friendImageSource;
	}
	
	public String getfriendName(){
		return friendName;
	}
	
	public int getfriendImageSource(){
		return friendImageSource;
	}
}

