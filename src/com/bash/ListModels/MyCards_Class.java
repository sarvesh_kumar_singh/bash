package com.bash.ListModels;

public class MyCards_Class {

	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getCardVaildPeriod() {
		return cardVaildPeriod;
	}
	public void setCardVaildPeriod(String cardVaildPeriod) {
		this.cardVaildPeriod = cardVaildPeriod;
	}
	public Boolean getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}
	
	public void setCardId(String id) {
		this.id = id;
	}
	
	public String getCardId() {
		return id;
	}
	
	String cardNumber, cardType, cardVaildPeriod, id;
	
	public MyCards_Class(String cardId, String cardNumber, String cardType,
			String cardVaildPeriod, Boolean isDefault) {
		super();
		this.id = cardId;
		this.cardNumber = cardNumber;
		this.cardType = cardType;
		this.cardVaildPeriod = cardVaildPeriod;
		this.isDefault = isDefault;
	}


	Boolean isDefault;

	 
}
