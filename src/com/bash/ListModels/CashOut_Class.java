package com.bash.ListModels;

public class CashOut_Class {

	public int payerImage;
	public String payerName, payeeName, resonforPayment, paymentDate;
	
	public void setFieldValues(int payerImage, String payerName, String payeeName, String resonforPayment, String paymentDate){
		this.payerImage = payerImage;
		this.payerName = payerName;
		this.payeeName = payeeName;
		this.resonforPayment = resonforPayment;
		this.paymentDate = paymentDate;
	}
	
	public CashOut_Class(int payerImage, String payerName, String payeeName, String resonforPayment, String paymentDate){
		this.payerImage = payerImage;
		this.payerName = payerName;
		this.payeeName = payeeName;
		this.resonforPayment = resonforPayment;
		this.paymentDate = paymentDate;
	}
	
	public int getpayerImage(){
		return payerImage;
	}
	
	public String getpayerName(){
		return payerName;
	}
	
	public String getpayeeName(){
		return payeeName;
	}
	public String getresonFroPayment(){
		return resonforPayment;
	}
	public String getpaymentDate(){
		return paymentDate;
	}
	
}

