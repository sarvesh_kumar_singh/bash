package com.bash.ListModels;

public class TwoWayPersonModel {
private String Id = "";
private String userType = "";
private String personName = "";

public TwoWayPersonModel(String Id, String personName, String userType){
	this.Id = Id;
	this.personName = personName;
	this.userType = userType;
}

public String getId() {
	return Id;
}

public void setId(String id) {
	Id = id;
}

public String getPersonName() {
	return personName;
}

public void setPersonName(String personName) {
	this.personName = personName;
}

public String getUserType() {
	return userType;
}

public void setUserType(String userType) {
	this.userType = userType;
}

}
