package com.bash.ListModels;

public class CashInOutFriend_Class {

	public String eventName, totalAmount, subTotalAmount, date, location, imagepath, feed_comment, idtrans;
	boolean HaveYouPaid = false; 
	//int paidId = 0;
	
	public CashInOutFriend_Class(String eventName, String totalAmount, String subTotalAmount, boolean HaveYouPaid,
			String date, String location, String imagepath, String  feed_comment, String idtrans){
		this.idtrans = idtrans;
		this.date = date;
		this.feed_comment = feed_comment;
		this.location = location;
		this.imagepath = imagepath;
		this.eventName = eventName;
		this.totalAmount = totalAmount;
		this.subTotalAmount = subTotalAmount;
		this.HaveYouPaid = HaveYouPaid;
	}
	
	public String getIdTrans(){
		return idtrans;
	}
	
	public String getFeedComment(){
		return feed_comment;
	}
	
	public String getDate(){
		return date;
	}
	public String getLocation(){
		return location;
	}
	public String getImagepath(){
		return imagepath;
	}
	
	public String geteventName(){
		return eventName;
	}
	
	public String gettotalAmount(){
		return totalAmount;
	}
	
	public String getsubTotalAmount(){
		return subTotalAmount;
	}
	
	public boolean getHaveYouPaid(){
		return HaveYouPaid;
	}
	
}

