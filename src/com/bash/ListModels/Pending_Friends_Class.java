package com.bash.ListModels;

public class Pending_Friends_Class {

	public String groupName, groupAmount;
	
	public void setFieldValues(String groupName, String groupAmount){
		this.groupName = groupName;
		this.groupAmount = groupAmount;
	}
	
	public Pending_Friends_Class(String groupName, String groupAmount){
		this.groupName = groupName;
		this.groupAmount = groupAmount;
	}
	
	public String getgroupName(){
		return groupName;
	}
	
	public String getgroupAmount(){
		return groupAmount;
	}
	  
}

