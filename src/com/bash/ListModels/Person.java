package com.bash.ListModels;

import android.os.Parcel;
import android.os.Parcelable;


public class Person implements Parcelable {
	private String user_type = "";
    private String fullname = "";
    private String id = "";
    private String image_location = "";
    private boolean imagebool = false;
    private boolean chechbxbool = false;
    private boolean paidpersonbool = false;
    private boolean receivepersonbool = false;

	float paidamount = 0.0f;
    float receiveamount = 0.0f;

    public Person(String user_type, String id, String fullname, String image_location ) {
        this.user_type = user_type;
        this.id = id;
        this.fullname = fullname;
        this.image_location = image_location;
    }
    public Person(){
    	
    }
    
    public final boolean isPaidpersonbool() {
		return paidpersonbool;
	}
	public final void setPaidpersonbool(boolean paidpersonbool) {
		this.paidpersonbool = paidpersonbool;
	}
	public final boolean isReceivepersonbool() {
		return receivepersonbool;
	}
	public final void setReceivepersonbool(boolean receivepersonbool) {
		this.receivepersonbool = receivepersonbool;
	}
    
    
    public final float getPaidamount() {
		return paidamount;
	}
	public final void setPaidamount(float paidamount) {
		this.paidamount = paidamount;
	}
	public final float getReceiveamount() {
		return receiveamount;
	}
	public final void setReceiveamount(float receiveamount) {
		this.receiveamount = receiveamount;
	}
	public String getUserType() {
		return user_type;
	}
	public void setUserType(String user_type) {
		this.user_type = user_type;
	}
	public String getName() {
		return fullname;
	}
	public void setName(String fullname) {
		this.fullname = fullname;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getImage_location() {
		return image_location;
	}
	public void setImage_location(String image_location) {
		this.image_location = image_location;
	}
	public boolean isImagebool() {
		return imagebool;
	}
	public void setImagebool(boolean imagebool) {
		this.imagebool = imagebool;
	}

	public final boolean isChechbxbool() {
		return chechbxbool;
	}
	public final void setChechbxbool(boolean chechbxbool) {
		this.chechbxbool = chechbxbool;
	}
	@Override
    public String toString() { return fullname; }



	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}



	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(fullname);
		dest.writeString(user_type);
		dest.writeString(id);
		dest.writeString(image_location);
		dest.writeByte((byte) (imagebool ? 1 : 0));
		dest.writeByte((byte) (chechbxbool ? 1 : 0));
		dest.writeByte((byte) (paidpersonbool ? 1 : 0));
		dest.writeByte((byte) (receivepersonbool ? 1 : 0));
		dest.writeFloat(paidamount);
		dest.writeFloat(receiveamount);
	}
	public static final Parcelable.Creator<Person> CREATOR =
			new Parcelable.Creator<Person>() {

		@Override
		public Person createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			Person person = new Person();
			
			person.fullname = source.readString();
			person.user_type = source.readString();
			person.id = source.readString();
			person.image_location = source.readString();
			person.imagebool = source.readByte() != 0;
			person.chechbxbool = source.readByte() != 0;
			person.paidpersonbool = source.readByte() != 0;
			person.receivepersonbool = source.readByte() != 0;
			person.paidamount = source.readFloat();
			person.receiveamount = source.readFloat();
			
	
			
			return person;
		}

		@Override
		public Person[] newArray(int size) {
			// TODO Auto-generated method stub
			return new Person[size];  
		}
	};
}
