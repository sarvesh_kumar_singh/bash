package com.bash.ListModels;

public class MyFriends_Transactions_Class {

	public String transText, transLocation, transTime;
	
	public MyFriends_Transactions_Class(String transText, String transLocation, String transTime){
		this.transText = transText;
		this.transLocation = transLocation;
		this.transTime = transTime;
	}
	public String gettransText(){
		return transText;
	}
	
	public String gettransLocation(){
		return transLocation;
	}
	public String gettransTime(){
		return transTime;
	}
	
	public void settrasnText(String transText){
		this.transText = transText;
	}
	
	public void settransLocation(String transLocation){
		this.transLocation = transLocation;
	}
	
	public void settransTime(String transTime){
		this.transTime = transTime;
	}
	
}
