package com.bash.ListModels;

public class CurrencyModel {

	private String currencyCode = "";
	private String currencyText = "";
	
	public CurrencyModel(String currencyCode, String currencyText) {
		this.currencyCode = currencyCode;
		this.currencyText = currencyText;
	}
	public final String getCurrencyCode() {
		return currencyCode;
	}
	public final void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public final String getCurrencyText() {
		return currencyText;
	}
	public final void setCurrencyText(String currencyText) {
		this.currencyText = currencyText;
	}
	
	
}
