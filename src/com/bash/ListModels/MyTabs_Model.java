package com.bash.ListModels;


public class MyTabs_Model {

	public String firstname;
	public String imagepath;
	public String charge; 
	public String idpaid_by;
	public String idpaid_for;
	 
	public void setFieldValues(String firstname, String imagepath, String charge, String idpaid_by, String idpaid_for){
		this.idpaid_for = idpaid_for;
		this.idpaid_by = idpaid_by;
		this.firstname = firstname;
		this.imagepath = imagepath;
		this.charge = charge;
	}
	
	public MyTabs_Model(String firstname, String imagepath, String charge, String idpaid_by, String idpaid_for){
		this.idpaid_for = idpaid_for;
		this.idpaid_by = idpaid_by;
		this.firstname = firstname;
		this.imagepath = imagepath;
		this.charge = charge;
	}
	
	public String getidPaidFor(){
		return idpaid_for;
	}
	
	public String getIdPaidBy(){
		return idpaid_by;
	}
	
	public String getFristName(){
		return firstname;
	}
	public String getImagepath(){
		return imagepath;
	}
	
	public String getCharge(){
		return charge;
	}
		
}

