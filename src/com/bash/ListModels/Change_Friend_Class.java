package com.bash.ListModels;

public class Change_Friend_Class {
	
	String friendName, friendId, friendImagePath;
	boolean isSelected;
	
	public String getFriendName() {
		return friendName;
	}

	public void setFriendName(String friendName) {
		this.friendName = friendName;
	}

	public String getFriendId() {
		return friendId;
	}

	public void setFriendId(String friendId) {
		this.friendId = friendId;
	}

	public String getFriendImagePath() {
		return friendImagePath;
	}

	public void setFriendImagePath(String friendImagePath) {
		this.friendImagePath = friendImagePath;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	
	
	public Change_Friend_Class(String friendName, String friendId,
			String friendImagePath, boolean isSelected) {
		super();
		this.friendName = friendName;
		this.friendId = friendId;
		this.friendImagePath = friendImagePath;
		this.isSelected = isSelected;
	}


	 
	 
	
	 
}
