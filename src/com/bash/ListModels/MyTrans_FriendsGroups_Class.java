package com.bash.ListModels;

import com.google.gson.annotations.SerializedName;

public class MyTrans_FriendsGroups_Class {

	@SerializedName("firstname")
	public String firstname;
	
	@SerializedName("imagepath")
	public String imagepath;
	
	@SerializedName("charge")
	public String charge; 
	
	@SerializedName("idpaid_by")
	public String idpaid_by;
	
	@SerializedName("idpaid_for")
	public String idpaid_for;
	
	@SerializedName("groupname")
	public String groupname;
	
	@SerializedName("type")
	public String type;
	
	public void setFieldValues(String imagepath, String charge, 
			String idpaid_by, String groupname){
		this.idpaid_for = idpaid_for;
		this.groupname = groupname;
		this.imagepath = imagepath;
		this.charge = charge;
	}
	
	public MyTrans_FriendsGroups_Class(String imagepath, String charge, String idpaid_by, String groupname, String type){
		this.type = type;
		this.idpaid_for = idpaid_for;
		this.groupname = groupname;
		this.imagepath = imagepath;
		this.charge = charge;
	}
	
	public void setFieldValues(String firstname, String imagepath, String charge, String status, 
			String recorded_on, String feed_comment, String location, String idpaid_by, String idpaid_for){
		this.idpaid_for = idpaid_for;
		this.idpaid_by = idpaid_by;
		this.firstname = firstname;
		this.imagepath = imagepath;
		this.charge = charge;
	}
	
	public MyTrans_FriendsGroups_Class(String firstname, String imagepath, String charge, String idpaid_by, String idpaid_for, String type){
		this.type = type;
		this.idpaid_for = idpaid_for;
		this.idpaid_by = idpaid_by;
		this.firstname = firstname;
		this.imagepath = imagepath;
		this.charge = charge;
	}
	
	public String getType(){
		return type;
	}
	public void setType(String type){
		this.type = type;
	}
	
	public String getidPaidFor(){
		return idpaid_for;
	}
	
	public String getIdPaidBy(){
		return idpaid_by;
	}
	
	public String getFristName(){
		return firstname;
	}
	
	public String getGroupName(){
		return groupname;
	}
	
	public String getImagepath(){
		return imagepath;
	}
	
	public String getCharge(){
		return charge;
	}
	
	 
	
}

