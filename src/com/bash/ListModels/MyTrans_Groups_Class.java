package com.bash.ListModels;

import com.google.gson.annotations.SerializedName;

public class MyTrans_Groups_Class {

	@SerializedName("groupname")
	public String groupname;
	
	@SerializedName("imagepath")
	public String imagepath;
	
	@SerializedName("idpaid_for")
	public String idpaid_for;
	
	@SerializedName("charge")
	public String charge; 
	
	 
	public void setFieldValues(String imagepath, String charge, 
			String idpaid_by, String groupname){
		this.idpaid_for = idpaid_for;
		this.groupname = groupname;
		this.imagepath = imagepath;
		this.charge = charge;
	}
	
	public MyTrans_Groups_Class(String imagepath, String charge, 
			String idpaid_by, String groupname){
		this.idpaid_for = idpaid_for;
		this.groupname = groupname;
		this.imagepath = imagepath;
		this.charge = charge;
	}
	
	public String getidPaidFor(){
		return idpaid_for;
	}
	
	public String getFristName(){
		return groupname;
	}
	public String getImagepath(){
		return imagepath;
	}
	
	public String getCharge(){
		return charge;
	}
	
}

