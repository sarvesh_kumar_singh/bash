package com.bash.ListModels;

public class Transaction_Class {

	
	public String paidByName, paidForName, paymentComment, paymentDate, payerImage, transactionId, status, paymentLocation, amount;
	
	public void setFieldValues(String paidByName, String paidForName, String paymentComment, String paymentDate, 
			String payerImage, String transactionId, String status, String paymentLocation, String amount){
		this.paidByName = paidByName;
		this.paidForName = paidForName;
		this.amount = amount;
		this.paymentComment = paymentComment;
		this.paymentDate = paymentDate;
		this.payerImage = payerImage;
		this.transactionId = transactionId;
		this.status = status;
		this.paymentLocation = paymentLocation;
	}
	
	public Transaction_Class(String paidByName, String paidForName, String paymentComment, String paymentDate, 
			String payerImage, String transactionId, String status, String paymentLocation, String amount){
		this.paidByName = paidByName;
		this.amount = amount;
		this.paidForName = paidForName;
		this.paymentComment = paymentComment;
		this.paymentDate = paymentDate;
		this.payerImage = payerImage;
		this.transactionId = transactionId;
		this.status = status;
		this.paymentLocation = paymentLocation;
	}
	
	
	public String getAmount(){
		return amount;
	}
	public String getpaidByName(){
		return paidByName;
	}
	
	public String getpaidForName(){
		return paidForName;
	}
	
	public String getpaymentComment(){
		return paymentComment;
	}
	public String getpaymentDate(){
		return paymentDate;
	}
	public String getpayerImage(){
		return payerImage;
	}
	
	public String gettransactionId(){
		return transactionId;
	}
	public String getstatus(){
		return status;
	}
	public String getpaymentLocation(){
		return paymentLocation;
	}
	
}

