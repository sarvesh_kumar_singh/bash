package com.bash.BaseFragmentClasses;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.bash.R;
import com.bash.Activities.HomeActivity;

public class BaseFragment extends Fragment
{

	   public HomeActivity mActivity = (HomeActivity) this.getActivity();
	   protected View mRootView;
	   	    
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	    }
	    
	    public void replaceFragment(Fragment fragment, boolean addToBackStack) 
		{
			FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
			if (addToBackStack) {
				transaction.addToBackStack(fragment.getClass().getName());
			}
			transaction.replace(R.id.container_framelayout, fragment, fragment.getClass().getName());
			transaction.commit();
			getChildFragmentManager().executePendingTransactions();
		}
		 
		
	    public void addFragment(Fragment fragment, boolean addToBackStack) 
		{
			FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
			if (addToBackStack) {
				transaction.addToBackStack(fragment.getClass().getName());
			}
			transaction.add(R.id.container_framelayout, fragment, fragment.getClass().getName());
			transaction.commit();
			getChildFragmentManager().executePendingTransactions();
		}
		 
	    
	 	public boolean popFragment() 
		{
			boolean isPop = false;
			
			if (getChildFragmentManager().getBackStackEntryCount() > 0) {
				isPop = true;
				getChildFragmentManager().popBackStack();
			}
			return isPop; 
		}

	 
}
