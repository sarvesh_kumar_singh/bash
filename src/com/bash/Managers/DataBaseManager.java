package com.bash.Managers;

import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.bash.GsonClasses.Friends_Class;
import com.bash.GsonClasses.Pending_Class;
import com.bash.ListModels.BashUsers_Class;
import com.bash.ListModels.Contacts_Class;
import com.bash.ListModels.MyTrans_Friends_Class;
import com.bash.ListModels.MyTrans_Groups_Class;
import com.bash.Utils.AppConstants;

public class DataBaseManager extends SQLiteOpenHelper {
	
	private static DataBaseManager sDataBaseHelper = null;
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "Bashdb";

	//table
	public static final String TABLE_CONTACTS = "contacts_table";    //contact up
	public static final String TABLE_TRANSACTIONS = "transaction_table";    //transaction up
	//public static final String TABLE_BASHUSERS = "bash_users_table";    //contact up
 
	//contacts table
	public static final String CONTACTS_PHONE_EMAILID = "contact_emailid"; 
	public static final String CONTACTS_MOBILE_NUMBER = "contact_number";               
	public static final String CONTACTS_PHONE_NAME = "contact_phone_name";
	     
	public static final String CONTACTS_BASH_USERID = "bash_id";
	public static final String CONTACTS_BASH_NAME = "contact_bash_name";          
	public static final String CONTACTS_BASH_IMAGEPATH = "imagepath";
	public static final String CONTACTS_ISBASHUSER = "isbashuser";
	public static final String CONTACTS_ISREQUEST_SENT = "isrequest_sent";
	public static final String CONTACTS_ISREQUEST_RECEIVED = "isrequest_received";	 
	public static final String CONTACTS_IS_FRIEND = "is_friend";
	
	//bash users table
	public static final String BASH_USER_ID = "bashuser_id";               
	public static final String BASH_USER_NAME = "bashuser_name";
	public static final String BASH_USER_IMAGEPATH = "bashuser_imagepath";
	public static final String BASH_USER_ISFRIENDRSENT = "bashuser_isfriendsent";
	public static final String BASH_USER_ISFRIENDRRECEIVED = "bashuser_isfriendreceived";
	public static final String BASH_USER_MOBILENUMBER = "bashuser_number";
	
	private static final String CREATE_TABLE_CONTACTS = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_CONTACTS 
			+ "("
			+ CONTACTS_MOBILE_NUMBER + " TEXT, "
			+ CONTACTS_PHONE_NAME + " TEXT, "
			+ CONTACTS_BASH_USERID + " TEXT DEFAULT \'NY\', "
			+ CONTACTS_BASH_NAME + " TEXT DEFAULT \'NY\', "
			+ CONTACTS_BASH_IMAGEPATH + " TEXT DEFAULT \'NY\', "
			+ CONTACTS_ISBASHUSER + "  BOOLEAN DEFAULT FALSE, "
			+ CONTACTS_ISREQUEST_SENT + "  BOOLEAN DEFAULT FALSE, "
			+ CONTACTS_ISREQUEST_RECEIVED + "  BOOLEAN DEFAULT FALSE, "
			+ CONTACTS_IS_FRIEND + " BOOLEAN DEFAULT FALSE, "
			+ CONTACTS_PHONE_EMAILID + " TEXT"
			+ ")";
	
	
	/*private static final String CREATE_TABLE_BASH_USERS = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_BASHUSERS 
			+ "("
			+ BASH_USER_ID + " TEXT,"
			+ BASH_USER_NAME + " TEXT,"
			+ BASH_USER_MOBILENUMBER + " TEXT, "
			+ BASH_USER_IMAGEPATH + " TEXT, "
			+ BASH_USER_ISFRIENDRSENT + " TEXT, "
			+ BASH_USER_ISFRIENDRRECEIVED + " TEXT"
			+ ")";*/
	
	//transaction table
	
	public static final String TRANS_IDPAID_BY = "trans_idpaid_by";               
	public static final String TRANS_IDPAID_FOR = "trans_idpaid_for";
	public static final String TRANS_FIRST_NAME = "trans_first_name";
	public static final String TRANS_IMAGE_PATH = "trans_imagepath";
	public static final String TRANS_CHARGE = "trans_charge";
	public static final String TRANS_STATUS = "trans_status";
	public static final String TRANS_RECORDED_ON = "trans_recorded_on";
	public static final String TRANS_RECORDED_INLONG = "trans_recorded_in_long";
	public static final String TRANS_FEED_COMMENT = "trans_feedcomment";
	public static final String TRANS_LOCATION = "trans_location";
	public static final String TRANS_GROUPNAME = "trans_groupname";
	public static final String TRANS_HAVE_YOU_PAID = "trans_have_you_paid";
	public static final String TRANS_TYPE = "trans_type";
	public static final String TRANS_ISFRIENDTAB = "trans_isfriendtab";
	
	private static final String CREATE_TABLE_TRANSACTIONS = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_TRANSACTIONS 
			+ "("
			+ TRANS_IDPAID_FOR + " TEXT DEFAULT \'NY\', "
			+ TRANS_IDPAID_BY + " TEXT, "
			+ TRANS_FIRST_NAME + " TEXT, "
			+ TRANS_IMAGE_PATH + " TEXT DEFAULT \'NY\', "
			+ TRANS_CHARGE + " INTEGER DEFAULT 0, "
			+ TRANS_STATUS + " TEXT DEFAULT \'NY\', "
			+ TRANS_RECORDED_ON + " TEXT DEFAULT \'NY\', "
			+ TRANS_RECORDED_INLONG + " BIGINT DEFAULT 0, "
			+ TRANS_FEED_COMMENT + " TEXT DEFAULT \'NY\', "
			+ TRANS_LOCATION + " TEXT DEFAULT \'NY\', "
			+ TRANS_GROUPNAME + " TEXT DEFAULT \'NY\', "
			+ TRANS_TYPE + " TEXT DEFAULT \'TOTAL\', "
			+ TRANS_HAVE_YOU_PAID + " BOOLEAN DEFAULT FALSE,"
			+ TRANS_ISFRIENDTAB + " BOOLEAN DEFAULT FALSE"
			+ ")";
	
	
/*friend : 
    "imagepath": "",
    "firstname": "real device",
    "idpaid_by": "12",
    "charge": "100",
    "status": "pending",
    "recorded_on": "2014-11-13 18:55:42",
    "feed_comment": "birthday treat",
    "location": "chennai"

"group" 
    "imagepath": "",
    "firstname": "real device",
    "idpaid_by": "12",
    "groupname": "test",
    "charge": "100",
    "status": "pending",
    "recorded_on": "2014-11-18 12:15:13",
    "feed_comment": "test",
    "location": "Chennai"
 */
    	
 	public static void initInstance(Context context) {
		if(sDataBaseHelper==null)
			sDataBaseHelper = new DataBaseManager(context);
	}

	public static DataBaseManager getInstance(){
		return sDataBaseHelper;
	}

	public DataBaseManager(Context context) {
		super(context, AppConstants.ROOT_DIRECTORY + DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		//db.execSQL(CREATE_TABLE_SIGNUP);
		Log.e("CREATE_TABLE_CONTACTS", CREATE_TABLE_CONTACTS);
		Log.e("CREATE_TABLE_CONTACTS", CREATE_TABLE_TRANSACTIONS);
		db.execSQL(CREATE_TABLE_CONTACTS);
		db.execSQL(CREATE_TABLE_TRANSACTIONS);
		//db.execSQL(CREATE_TABLE_BASH_USERS);
	}
	
	/*//Bash Users Manipulations
	public void storeBashUserDetails(ArrayList<BashUsers_Class> bashUsersList) {
		SQLiteDatabase db = this.getWritableDatabase();
		try {
			ContentValues cv = new ContentValues();
			for(int i = 0; i < bashUsersList.size(); i++){
				cv.clear();
				cv.put(BASH_USER_ID, bashUsersList.get(i).idfriend);
				cv.put(BASH_USER_NAME, bashUsersList.get(i).name);
				cv.put(BASH_USER_IMAGEPATH, bashUsersList.get(i).imagepath);
				cv.put(BASH_USER_ISFRIENDRSENT, bashUsersList.get(i).is_send_request);
				cv.put(BASH_USER_ISFRIENDRRECEIVED, bashUsersList.get(i).is_request_received);
				cv.put(BASH_USER_MOBILENUMBER, bashUsersList.get(i).idfriend);
				db.insert(TABLE_BASHUSERS, null, cv);
			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			db.close();
		}
	}
	
	public int getBashUsersCount() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery("select * from "+TABLE_BASHUSERS+";", null);
		int count = cursor.getCount();
		cursor.close();
		db.close();
		return count;
	}
	
	public void resetBashUsersTable() {
		try {
			SQLiteDatabase db = this.getReadableDatabase();
			db.delete(TABLE_BASHUSERS, null, null);
			db.close();
		} catch (Exception e) {
		}
	}*/
	
	public void resetBashUsersTable() {
		try {
			SQLiteDatabase db = this.getReadableDatabase();
			db.delete(TABLE_CONTACTS, null, null);
			db.close();
		} catch (Exception e) {
		}
	}
	
	public void resetTransactionTable() {
		try {
			SQLiteDatabase db = this.getReadableDatabase();
			db.delete(TABLE_TRANSACTIONS, null, null);
			db.close();
		} catch (Exception e) {
		}
	}
	
	/*
	+ "("
	+ TRANS_IDPAID_BY + " TEXT, "
	+ TRANS_FIRST_NAME + " TEXT, "
	+ TRANS_IMAGE_PATH + " TEXT DEFAULT \'NY\', "
	+ TRANS_CHARGE + " TEXT DEFAULT \'NY\', "
	+ TRANS_STATUS + " TEXT DEFAULT \'NY\', "
	+ TRANS_RECORDED_ON + " TEXT DEFAULT \'NY\', "
	+ TRANS_FEED_COMMENT + " TEXT DEFAULT \'NY\', "
	+ TRANS_LOCATION + " TEXT DEFAULT \'NY\', "
	+ TRANS_GROUPNAME + " TEXT DEFAULT \'NY\', "
	+ TRANS_TYPE + " TEXT DEFAULT \'TOTAL\', "
	+ TRANS_HAVE_YOU_PAID + " BOOLEAN DEFAULT FALSE,"
	+ TRANS_ISFRIENDTAB + " BOOLEAN DEFAULT FALSE"
	+ ")";
	*/
	public void storeTransactionDetails(Pending_Class pendingClass){
		resetTransactionTable();
		SQLiteDatabase db = this.getWritableDatabase();
		try {
			ContentValues cv = new ContentValues();
			int i = 0;
			for(i = 0; i<pendingClass.total_bal.friend.size(); i++){
				cv.clear();
				cv.put(TRANS_IDPAID_BY, pendingClass.total_bal.friend.get(i).getIdPaidBy());
				cv.put(TRANS_FIRST_NAME, pendingClass.total_bal.friend.get(i).getFristName());
				cv.put(TRANS_IMAGE_PATH, pendingClass.total_bal.friend.get(i).getImagepath());
				cv.put(TRANS_CHARGE, Integer.parseInt(pendingClass.total_bal.friend.get(i).getCharge()));
				/*cv.put(TRANS_STATUS, pendingClass.total_bal.friend.get(i).getStatus());
				cv.put(TRANS_RECORDED_ON, pendingClass.total_bal.friend.get(i).getRecordedOn());
				cv.put(TRANS_RECORDED_INLONG,
						new Date().getTime() - AppConstants.dateformatforServer.parse(pendingClass.total_bal.friend.get(i).getRecordedOn()).getTime());
				cv.put(TRANS_FEED_COMMENT, pendingClass.total_bal.friend.get(i).getFeedComment());
				cv.put(TRANS_LOCATION, pendingClass.total_bal.friend.get(i).getLocation());*/
				cv.put(TRANS_GROUPNAME, "");
				cv.put(TRANS_TYPE, "TOTAL");
				cv.put(TRANS_HAVE_YOU_PAID,
						(PreferenceManager.getInstance().getUserId().equals(pendingClass.total_bal.friend.get(i).getIdPaidBy())) ? "TRUE" : "FALSE");
				cv.put(TRANS_ISFRIENDTAB, "TRUE");
				db.insert(TABLE_TRANSACTIONS, null, cv);
			}
			
			for(i = 0; i<pendingClass.you_owe.friend.size(); i++) {
				cv.clear();
				cv.put(TRANS_IDPAID_BY, pendingClass.you_owe.friend.get(i).getIdPaidBy());
				cv.put(TRANS_FIRST_NAME, pendingClass.you_owe.friend.get(i).getFristName());
				cv.put(TRANS_IMAGE_PATH, pendingClass.you_owe.friend.get(i).getImagepath());
				cv.put(TRANS_CHARGE, Integer.parseInt(pendingClass.you_owe.friend.get(i).getCharge()));
				/*cv.put(TRANS_STATUS, pendingClass.you_owe.friend.get(i).getStatus());
				cv.put(TRANS_RECORDED_ON, pendingClass.you_owe.friend.get(i).getRecordedOn());
				cv.put(TRANS_RECORDED_INLONG,
						new Date().getTime() - AppConstants.dateformatforServer.parse(pendingClass.you_owe.friend.get(i).getRecordedOn()).getTime());
				cv.put(TRANS_FEED_COMMENT, pendingClass.you_owe.friend.get(i).getFeedComment());
				cv.put(TRANS_LOCATION, pendingClass.you_owe.friend.get(i).getLocation());*/
				cv.put(TRANS_GROUPNAME, "");
				cv.put(TRANS_TYPE, "YOUOWE");
				cv.put(TRANS_HAVE_YOU_PAID,
						(PreferenceManager.getInstance().getUserId().equals(pendingClass.you_owe.friend.get(i).getIdPaidBy())) ? "TRUE" : "FALSE");
				cv.put(TRANS_ISFRIENDTAB, "TRUE");
				db.insert(TABLE_TRANSACTIONS, null, cv);
			}
			
			for(i = 0; i<pendingClass.you_are_owe.friend.size(); i++){
				cv.clear();
				cv.put(TRANS_IDPAID_BY, pendingClass.you_are_owe.friend.get(i).getIdPaidBy());
				cv.put(TRANS_FIRST_NAME, pendingClass.you_are_owe.friend.get(i).getFristName());
				cv.put(TRANS_IMAGE_PATH, pendingClass.you_are_owe.friend.get(i).getImagepath());
				cv.put(TRANS_CHARGE, Integer.parseInt(pendingClass.you_are_owe.friend.get(i).getCharge()));
				/*cv.put(TRANS_STATUS, pendingClass.you_are_owe.friend.get(i).getStatus());
				cv.put(TRANS_RECORDED_ON, pendingClass.you_are_owe.friend.get(i).getRecordedOn());*/
				/*cv.put(TRANS_RECORDED_INLONG,
						new Date().getTime() - AppConstants.dateformatforServer.parse(pendingClass.total_bal.friend.get(i).getRecordedOn()).getTime());
				cv.put(TRANS_FEED_COMMENT, pendingClass.you_are_owe.friend.get(i).getFeedComment());
				cv.put(TRANS_LOCATION, pendingClass.you_are_owe.friend.get(i).getLocation());*/
				cv.put(TRANS_GROUPNAME, "");
				cv.put(TRANS_TYPE, "YOUAREOWE");
				cv.put(TRANS_HAVE_YOU_PAID,
						(PreferenceManager.getInstance().getUserId().equals(pendingClass.you_are_owe.friend.get(i).getIdPaidBy())) ? "TRUE" : "FALSE");
				cv.put(TRANS_ISFRIENDTAB, "TRUE");
				db.insert(TABLE_TRANSACTIONS, null, cv);
			}
			
			
			for(i = 0; i<pendingClass.total_bal.group.size(); i++){
				cv.clear();
				cv.put(TRANS_IDPAID_FOR, pendingClass.total_bal.group.get(i).getidPaidFor());
				//cv.put(TRANS_IDPAID_BY, pendingClass.total_bal.group.get(i).getIdPaidBy());
				cv.put(TRANS_FIRST_NAME, pendingClass.total_bal.group.get(i).getFristName());
				cv.put(TRANS_IMAGE_PATH, pendingClass.total_bal.group.get(i).getImagepath());
				cv.put(TRANS_CHARGE, Integer.parseInt(pendingClass.total_bal.group.get(i).getCharge()));
				/*cv.put(TRANS_STATUS, pendingClass.total_bal.group.get(i).getStatus());
				cv.put(TRANS_RECORDED_ON, pendingClass.total_bal.group.get(i).getRecordedOn());*/
				/*cv.put(TRANS_RECORDED_INLONG,
						new Date().getTime() - AppConstants.dateformatforServer.parse(pendingClass.total_bal.group.get(i).getRecordedOn()).getTime());
				cv.put(TRANS_FEED_COMMENT, pendingClass.total_bal.group.get(i).getFeedComment());
				cv.put(TRANS_LOCATION, pendingClass.total_bal.group.get(i).getLocation());
				*/cv.put(TRANS_GROUPNAME, pendingClass.total_bal.group.get(i).getFristName());
				cv.put(TRANS_TYPE, "TOTAL");
				/*cv.put(TRANS_HAVE_YOU_PAID,
						(PreferenceManager.getInstance().getUserId().equals(pendingClass.total_bal.group.get(i).getIdPaidBy())) ? "TRUE" : "FALSE");*/
				cv.put(TRANS_ISFRIENDTAB, "FALSE");
				db.insert(TABLE_TRANSACTIONS, null, cv);
			}
			
			for(i = 0; i<pendingClass.you_owe.group.size(); i++){
				cv.clear();
				cv.put(TRANS_IDPAID_FOR, pendingClass.total_bal.group.get(i).getidPaidFor());
				//cv.put(TRANS_IDPAID_BY, pendingClass.you_owe.group.get(i).getIdPaidBy());
				cv.put(TRANS_FIRST_NAME, pendingClass.you_owe.group.get(i).getFristName());
				cv.put(TRANS_IMAGE_PATH, pendingClass.you_owe.group.get(i).getImagepath());
				cv.put(TRANS_CHARGE, Integer.parseInt(pendingClass.you_owe.group.get(i).getCharge()));
				/*cv.put(TRANS_STATUS, pendingClass.you_owe.group.get(i).getStatus());
				cv.put(TRANS_RECORDED_ON, pendingClass.you_owe.group.get(i).getRecordedOn());*/
				/*cv.put(TRANS_RECORDED_INLONG,
						new Date().getTime() - AppConstants.dateformatforServer.parse(pendingClass.you_owe.group.get(i).getRecordedOn()).getTime());
				cv.put(TRANS_FEED_COMMENT, pendingClass.you_owe.group.get(i).getFeedComment());
				cv.put(TRANS_LOCATION, pendingClass.you_owe.group.get(i).getLocation());*/
				cv.put(TRANS_GROUPNAME, pendingClass.you_owe.group.get(i).getFristName());
				cv.put(TRANS_TYPE, "YOUOWE");
				/*cv.put(TRANS_HAVE_YOU_PAID,
						(PreferenceManager.getInstance().getUserId().equals(pendingClass.you_owe.group.get(i).getIdPaidBy())) ? "TRUE" : "FALSE");*/
				cv.put(TRANS_ISFRIENDTAB, "FALSE");
				db.insert(TABLE_TRANSACTIONS, null, cv);
			}
			
			for(i = 0; i<pendingClass.you_are_owe.group.size(); i++){
				cv.clear();
				cv.put(TRANS_IDPAID_FOR, pendingClass.total_bal.group.get(i).getidPaidFor());
				//cv.put(TRANS_IDPAID_BY, pendingClass.you_are_owe.group.get(i).getIdPaidBy());
				cv.put(TRANS_FIRST_NAME, pendingClass.you_are_owe.group.get(i).getFristName());
				cv.put(TRANS_IMAGE_PATH, pendingClass.you_are_owe.group.get(i).getImagepath());
				cv.put(TRANS_CHARGE, Integer.parseInt(pendingClass.you_are_owe.group.get(i).getCharge()));
				/*cv.put(TRANS_STATUS, pendingClass.you_are_owe.group.get(i).getStatus());
				cv.put(TRANS_RECORDED_ON, pendingClass.you_are_owe.group.get(i).getRecordedOn());*/
				/*cv.put(TRANS_RECORDED_INLONG,
						new Date().getTime() - AppConstants.dateformatforServer.parse(pendingClass.you_are_owe.group.get(i).getRecordedOn()).getTime());
				cv.put(TRANS_FEED_COMMENT, pendingClass.you_are_owe.group.get(i).getFeedComment());
				cv.put(TRANS_LOCATION, pendingClass.you_are_owe.group.get(i).getLocation());*/
				cv.put(TRANS_GROUPNAME, pendingClass.you_are_owe.group.get(i).getFristName());
				cv.put(TRANS_TYPE, "YOUAREOWE");
				/*cv.put(TRANS_HAVE_YOU_PAID,
						(PreferenceManager.getInstance().getUserId().equals(pendingClass.you_are_owe.group.get(i).getIdPaidBy())) ? "TRUE" : "FALSE");*/
				cv.put(TRANS_ISFRIENDTAB, "FALSE");
				db.insert(TABLE_TRANSACTIONS, null, cv);
			}
			
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			db.close();
		}
	}
	
	
	public ArrayList<MyTrans_Friends_Class> getTransactionFriendsDetailsByQuery(String query){
		SQLiteDatabase db = this.getReadableDatabase();
		ArrayList<MyTrans_Friends_Class> friendsList = new ArrayList<MyTrans_Friends_Class>();
		MyTrans_Friends_Class item = null;
		Cursor cursor = db.rawQuery(query+";", null);
		if(cursor.moveToFirst()){
			do{
				item = new MyTrans_Friends_Class(
						cursor.getString(cursor.getColumnIndex(TRANS_FIRST_NAME)),
						cursor.getString(cursor.getColumnIndex(TRANS_IMAGE_PATH)), 
						cursor.getString(cursor.getColumnIndex(TRANS_CHARGE)),
						cursor.getString(cursor.getColumnIndex(TRANS_IDPAID_BY)),
						cursor.getString(cursor.getColumnIndex(TRANS_IDPAID_FOR))
						);
						
				friendsList.add(item);
			}while(cursor.moveToNext());
		}
		
		cursor.close();
		db.close();
		return friendsList;
	}
	
	public ArrayList<MyTrans_Groups_Class> getTransactionGroupsDetailsByQuery(String query){
		SQLiteDatabase db = this.getReadableDatabase();
		ArrayList<MyTrans_Groups_Class> groupList = new ArrayList<MyTrans_Groups_Class>();
		MyTrans_Groups_Class item = null;
		Cursor cursor = db.rawQuery(query+";", null);
		if(cursor.moveToFirst()){
			do{
				item = new MyTrans_Groups_Class(
						cursor.getString(cursor.getColumnIndex(TRANS_IMAGE_PATH)),
						cursor.getString(cursor.getColumnIndex(TRANS_CHARGE)), 
						cursor.getString(cursor.getColumnIndex(TRANS_IDPAID_BY)),
						cursor.getString(cursor.getColumnIndex(TRANS_GROUPNAME))
						);
				groupList.add(item);
			}while(cursor.moveToNext());
		}
		
		cursor.close();
		db.close();
		return groupList;
	}
	
	public void resetContactsTable() {
		try {
			SQLiteDatabase db = this.getReadableDatabase();
			db.delete(TABLE_CONTACTS, null, null);
			db.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//Cotacts Table Manipulations
	public void storeContactDetails(ArrayList<Contacts_Class> contactsList) {
		
		resetContactsTable();
		
		SQLiteDatabase db = this.getWritableDatabase();
		try {
			ContentValues cv = new ContentValues();
			for(int i = 0; i < contactsList.size(); i++){
				cv.clear();
				cv.put(CONTACTS_MOBILE_NUMBER, contactsList.get(i).getcontactNumber());
				cv.put(CONTACTS_PHONE_NAME, contactsList.get(i).getcontactName());
				cv.put(CONTACTS_PHONE_EMAILID, contactsList.get(i).getcontactEmailId());
				db.insert(TABLE_CONTACTS, null, cv);
			}
			db.close();
		} catch (Exception e) {
			
			// TODO: handle exception
			db.close();
		}
	}
	
	public int getContactsCount() {
		SQLiteDatabase db = this.getReadableDatabase();	
		Cursor cursor = db.rawQuery("select * from "+TABLE_CONTACTS+";", null);
		int count = cursor.getCount();
		cursor.close();
		db.close();
		return count;
	}
	
	
	
 	//Cotacts Table Manipulations
	public void checkOutContactTable(ArrayList<BashUsers_Class> bashUsersList) {
			SQLiteDatabase db = this.getWritableDatabase();
			try {
				
				ContentValues cv = new ContentValues();
				for(int i = 0; i < bashUsersList.size(); i++){
					if(CheckIsNumberInContactList(bashUsersList.get(i).getphone_no())) {
						Log.e("Bash User Contact", bashUsersList.get(i).getphone_no().substring(2, 
								bashUsersList.get(i).getphone_no().length()));
						if(db == null || !db.isOpen()){
							db = this.getWritableDatabase();
						}
						cv.clear();
						cv.put(CONTACTS_BASH_USERID, bashUsersList.get(i).getisFriend());
						cv.put(CONTACTS_BASH_NAME, bashUsersList.get(i).getname());
						cv.put(CONTACTS_BASH_IMAGEPATH, bashUsersList.get(i).getimagepath());
						cv.put(CONTACTS_ISBASHUSER, "TRUE");
						if(bashUsersList.get(i).is_send_request.equals("1")){
							cv.put(CONTACTS_ISREQUEST_SENT, "TRUE");
						}else{
							cv.put(CONTACTS_ISREQUEST_SENT, "FALSE");	
						}
						
						if(bashUsersList.get(i).is_request_received.equals("1")){
							cv.put(CONTACTS_ISREQUEST_RECEIVED, "TRUE");
						}else{
							cv.put(CONTACTS_ISREQUEST_RECEIVED, "FALSE");	
						}
						
						Log.e("id friend", bashUsersList.get(i).is_friend);
						
						if(bashUsersList.get(i).is_friend.equals("1")){
							cv.put(CONTACTS_IS_FRIEND, "TRUE");
						}else{
							cv.put(CONTACTS_IS_FRIEND, "FALSE");	
						}
						
						db.update(TABLE_CONTACTS, cv, CONTACTS_MOBILE_NUMBER +" LIKE \'%"+bashUsersList.get(i).getphone_no().substring(2, 
								bashUsersList.get(i).getphone_no().length())+"\'", null);
						
						
						/*db.update(TABLE_CONTACTS, cv, CONTACTS_MOBILE_NUMBER +" = "+ 
								bashUsersList.get(i).getphone_no().substring(2, 
										bashUsersList.get(i).getphone_no().length()), null);*/
					}
				}
				db.close();
			} catch (Exception e) {
				// TODO: handle exception
				db.close();
			}
	}
		
	
	public void storeFriendsList(ArrayList<Friends_Class.friendlist> friendsList) {
		SQLiteDatabase db = this.getWritableDatabase();
		try {
			
			ContentValues cv = new ContentValues();
			for(int i = 0; i < friendsList.size(); i++){
				//Log.e("My String", friendsList.get(i).getphone_no());
				if(db == null || !db.isOpen()){
					db = this.getWritableDatabase();
				}
				
				if(CheckIsNumberInContactList(friendsList.get(i).getphone_no())) {
					Log.e("Bash User Number", friendsList.get(i).getphone_no());
					if(db == null || !db.isOpen()){
						db = this.getWritableDatabase();
					}
					cv.clear();
					cv.put(CONTACTS_BASH_USERID, friendsList.get(i).getisFriend());
					cv.put(CONTACTS_BASH_NAME, friendsList.get(i).getname());
					cv.put(CONTACTS_BASH_IMAGEPATH, friendsList.get(i).getimagepath());
					cv.put(CONTACTS_MOBILE_NUMBER, friendsList.get(i).getphone_no());
					cv.put(CONTACTS_ISBASHUSER, "TRUE");
					cv.put(CONTACTS_IS_FRIEND, "TRUE");
					cv.put(CONTACTS_ISREQUEST_SENT, "FALSE");	
					cv.put(CONTACTS_ISREQUEST_RECEIVED, "FALSE");
					
					//db.insertWithOnConflict(TABLE_CONTACTS, CONTACTS_MOBILE_NUMBER +" = "+ friendsList.get(i).getphone_no(),cv, SQLiteDatabase.CONFLICT_REPLACE);
					db.update(TABLE_CONTACTS, cv, CONTACTS_MOBILE_NUMBER +" = "+ 
							friendsList.get(i).getphone_no(), null);
				}
				
				 
			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			db.close();
		}
	}
	
	public boolean CheckIsNumberInContactList(String mobileNumber) {
		Log.e("mobile String", mobileNumber);
		SQLiteDatabase datab = this.getReadableDatabase();
		try {
			if(mobileNumber.length() >= 3){
				Log.e("Query", 
						"SELECT * FROM "+TABLE_CONTACTS+ " WHERE "
								+ CONTACTS_MOBILE_NUMBER
								+" LIKE "
								+"%"
								+mobileNumber.substring(2, mobileNumber.length()
								));
				
				Cursor icursor = datab.rawQuery("SELECT * FROM "
						+TABLE_CONTACTS 
						+ " WHERE "
						+ CONTACTS_MOBILE_NUMBER
						+" LIKE "+"\'%"+mobileNumber.substring(2, mobileNumber.length())+"\'", null);
				if(icursor.moveToFirst()){
					datab.close();
					return true;
				} 
			}
			
			datab.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			datab.close();
		}
		return false;
	}
	

	public void updateCotactTableForFriendRequestReceived(String userId) {
				SQLiteDatabase db = this.getWritableDatabase();
				try {
					ContentValues cv = new ContentValues();
					cv.put(CONTACTS_ISREQUEST_RECEIVED, "TRUE");
					db.update(TABLE_CONTACTS, cv, CONTACTS_BASH_USERID +" = "+ userId, null);
					db.close();
					
				} catch (Exception e) {
					// TODO: handle exception
					db.close();
				}
		}
			
	public void makeMyFriend(String userId) {
		SQLiteDatabase db = this.getWritableDatabase();
		try {
			ContentValues cv = new ContentValues();
			cv.put(CONTACTS_IS_FRIEND, "TRUE");
			cv.put(CONTACTS_ISREQUEST_RECEIVED, "FALSE");
			cv.put(CONTACTS_ISREQUEST_SENT, "FALSE");
			cv.put(CONTACTS_ISBASHUSER, "TRUE");
			db.update(TABLE_CONTACTS, cv, CONTACTS_BASH_USERID +" = "+ userId, null);
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			db.close();
		}
	}
	
	public void unFriendwithUserId(String userId) {
		SQLiteDatabase db = this.getWritableDatabase();
		try {
			ContentValues cv = new ContentValues();
			cv.put(CONTACTS_IS_FRIEND, "FALSE");
			cv.put(CONTACTS_ISREQUEST_RECEIVED, "FALSE");
			cv.put(CONTACTS_ISREQUEST_SENT, "FALSE");
			cv.put(CONTACTS_ISBASHUSER, "TRUE");
			db.update(TABLE_CONTACTS, cv, CONTACTS_BASH_USERID +" = "+ userId, null);
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			db.close();
		}
	}
	
	/*public void updateCotactTableForFriendRequestReceived(String userId) {
		SQLiteDatabase db = this.getWritableDatabase();
				try {
					ContentValues cv = new ContentValues();
					cv.put(CONTACTS_ISREQUEST_RECEIVED, "TRUE");
					db.update(TABLE_CONTACTS, cv, CONTACTS_BASH_USERID +" = "+ userId, null);
					db.close();
					
				} catch (Exception e) {
					// TODO: handle exception
					db.close();
				}
		}*/
			
	
		
	/*	0+ CONTACTS_MOBILE_NUMBER + " TEXT, "
	1+ CONTACTS_PHONE_NAME + " TEXT, "
	2+ CONTACTS_BASH_USERID + " TEXT DEFAULT \'NY\', "
	3+ CONTACTS_BASH_NAME + " TEXT DEFAULT \'NY\', "
	4+ CONTACTS_BASH_IMAGEPATH + " TEXT DEFAULT \'NY\', "
	5+ CONTACTS_ISBASHUSER + "  BOOLEAN DEFAULT FALSE, "
	6+ CONTACTS_ISREQUEST_SENT + "  BOOLEAN DEFAULT FALSE, "
	7+ CONTACTS_ISREQUEST_RECEIVED + "  BOOLEAN DEFAULT FALSE, "
	8+ CONTACTS_IS_FRIEND + " BOOLEAN DEFAULT FALSE"
	new BashUsers_Class(idfriend, phone_no, name, imagepath, is_send_request, is_request_received)
 */
	
	public ArrayList<BashUsers_Class> getBashUsersList() {
		try {
			SQLiteDatabase db = this.getReadableDatabase();
			Cursor cursor = db.rawQuery("select * from "
										+TABLE_CONTACTS 
										+ " where "
										+ CONTACTS_ISBASHUSER +"= \'TRUE\'"
										+ " AND "
										+ CONTACTS_IS_FRIEND +"= \'FALSE\'"
										+ " AND "
										+ CONTACTS_MOBILE_NUMBER +" NOT LIKE \'%"
										   +PreferenceManager.getInstance().getUserMobileNumber().substring(2, PreferenceManager.getInstance().getUserMobileNumber().length())
										+"\'"
										+ " ORDER BY "
										+ CONTACTS_PHONE_NAME
										, null);
			
			Log.e("Phone", PreferenceManager.getInstance().getUserMobileNumber().substring(2, PreferenceManager.getInstance().getUserMobileNumber().length()));
					
			ArrayList<BashUsers_Class> usersList = new ArrayList<BashUsers_Class>();
			
			// 0        1     2          3               4         5            6               7                8
			//phoneno, name, bashid, contactbashname, imagepath, isbashuser, isrequestsent, isrequestreceived, isfriend
			
			if (cursor.moveToFirst()) {
				do {
					usersList.add(new BashUsers_Class(
							cursor.getString(2), 
							cursor.getString(0), 
							cursor.getString(1), 
							cursor.getString(4), 
							cursor.getString(6), 
							cursor.getString(7), 
							cursor.getString(8)));
				} while (cursor.moveToNext());
				
			}
			cursor.close();
			db.close();
			return usersList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	public ArrayList<BashUsers_Class> getNormalUsersList(){
		try {
			SQLiteDatabase db = this.getReadableDatabase();
			Cursor cursor = db.rawQuery("SELECT * FROM "
										+TABLE_CONTACTS 
										+ " WHERE "
										+ CONTACTS_ISBASHUSER + "= \'FALSE\'"
										+ " ORDER BY "
										+ CONTACTS_PHONE_NAME, null);
			/*+ CONTACTS_IS_FRIEND +" = false AND "*/
			ArrayList<BashUsers_Class> usersList = new ArrayList<BashUsers_Class>();
			if (cursor.moveToFirst()) {
				do {
					usersList.add(new BashUsers_Class(
							" ", 
							cursor.getString(0), 
							cursor.getString(1),  
							" ", 
							" ", 
							" ",
							cursor.getString(8),
							cursor.getString(9)
							));
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
			return usersList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	public ArrayList<BashUsers_Class> getFriendsList() {
		ArrayList<BashUsers_Class> usersList = new ArrayList<BashUsers_Class>();
		try {
			Log.e("*****************", "");
			
			SQLiteDatabase db = this.getReadableDatabase();
			Cursor cursor = db.rawQuery("SELECT * FROM "
										+TABLE_CONTACTS 
										+ " WHERE "
										+ CONTACTS_IS_FRIEND +" = \'TRUE\'", null);
				
			Log.e("*****************", "");
			
			if(cursor == null || cursor.getCount() == 0){
				Log.e("There is no value", "");
			}else{
				Log.e("There is something value", "");
			}
			
			if (cursor.moveToFirst()) {
				do {
					usersList.add(new BashUsers_Class(
							cursor.getString(2), 
							cursor.getString(0), 
							cursor.getString(3), 
							cursor.getString(4), 
							cursor.getString(6), 
							cursor.getString(7),
							cursor.getString(8)));
				} while (cursor.moveToNext());
			}

			db.close();
			return usersList;
		} catch (Exception e) {
			e.printStackTrace();

		}
		return usersList;
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS "+TABLE_CONTACTS);
	 	onCreate(db);
	}
}
