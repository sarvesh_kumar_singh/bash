package com.bash.Managers;

import com.bash.R;
import com.bash.Activities.Login_Bash_Activity;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DialogManager {

	static Typeface customFont;
	public static void showDialog(Activity parentActivity, String message)
    {
		
		
//    	AlertDialog.Builder builder = new AlertDialog.Builder(parentActivity);
//    	builder.setMessage(message)
//    		.setPositiveButton("Close", dialogClickListener)
//    	    .show();
		
		customFont = Typeface.createFromAsset(parentActivity.getAssets(), "myriad.ttf");
		final Dialog dialog = new Dialog(parentActivity,R.style.Dialog_No_Border);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_alert_dialog);
		LinearLayout topLayout=(LinearLayout)dialog.findViewById(R.id.topLayout);
		TextView textView1=(TextView)dialog.findViewById(R.id.textView1);
		textView1.setTypeface(customFont);
		TextView msgText=(TextView)dialog.findViewById(R.id.mobileNumberText);
		msgText.setTypeface(customFont);
		msgText.setText(message);
		Button btn_ok=(Button)dialog.findViewById(R.id.btn_ok);
		btn_ok.setTypeface(customFont);
		topLayout.setBackgroundColor(Color.TRANSPARENT);
		
		dialog.show();
		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				}
		});
	
    }
	
//	public static DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//	    @Override
//	    public void onClick(DialogInterface dialog, int which) {
//	        switch (which){
//	        case DialogInterface.BUTTON_NEUTRAL:
//	            //No button clicked
//	        	dialog.dismiss();
//	            break;
//	            
//	        }
//	    }
//	};

	
	
}
