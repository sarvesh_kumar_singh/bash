package com.bash.Managers;
import android.content.Context;
import com.bash.Application.BashApplication;
import com.bash.Utils.AppConstants;
public class PreferenceManager
{
	static PreferenceManager preferenceInstance;
	public static void initInstance(){
		if(preferenceInstance == null)
			preferenceInstance = new PreferenceManager(BashApplication.mContext);
	}
	
	private PreferenceManager(Context context){
		super();
	}
	
	public static PreferenceManager getInstance() {
		return preferenceInstance;
	}
	
	public void setPhoneConfiguration(int width, int height){
		BashApplication.getSharedPreferences().edit().putInt(AppConstants.PHONE_WIDTH, width).commit();
		int resourceId = BashApplication.mContext.getResources().getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0)	{
			height = height - BashApplication.mContext.getResources().getDimensionPixelSize(resourceId);
		}
		BashApplication.getSharedPreferences().edit().putInt(AppConstants.PHONE_HEIGHT, height).commit();
	}
	
	public boolean isPhoneConfigFixed(){
		if(getPhoneWidth() == 0){
			return false;
		}else{
			return true;
		}
	}
	
	public int getPhoneWidth() {
		return BashApplication.getSharedPreferences().getInt(AppConstants.PHONE_WIDTH, 0);
	}
	
	public int getPhoneHeight() {
		return BashApplication.getSharedPreferences().getInt(AppConstants.PHONE_HEIGHT, 0);
	}
	
	public int getPercentageFromWidth(int percentage) {
		 return (getPhoneWidth() * percentage) / 100;
	}
	    
	public int getPercentageFromHeight(int percentage) {
			 return (getPhoneHeight() * percentage) / 100;
	}
 
	
	public String getGCMRegistrationId() {
		return BashApplication.getSharedPreferences().getString(AppConstants.GCM_ID, null);
	}

	public void setGCMRegistrationId(String gcmId) {
		BashApplication.getSharedPreferences().edit().putString(AppConstants.GCM_ID, gcmId).commit();
	}
	
	
	public void setUserDetails(String userId, String userEmailId, String fullname,String password, String imagePath,
			String partnerCode, String partnerUsername, String partnerPassword, String phone_no,
			String facebookid, String pushnotify_on, String credit)
	{
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_ID, userId).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_EMAILID, userEmailId).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_FULLNAME, fullname).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_PASSWORD, phone_no).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_MOBILENUMBER, phone_no).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.PARTNER_CODE, partnerCode).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.PARTNER_USERNAME, partnerUsername).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.PARTNER_PASSWORD, partnerPassword).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_IMAGE_PATH, imagePath).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_FBACCESS_ID, facebookid).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_NOTIFICATION_STATUS, pushnotify_on).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_CREDIT, credit).commit();
	}
	
	public void resetUserDetails(){
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_ID, null).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_EMAILID, null).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_FULLNAME, null).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_PASSWORD, null).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_IMAGE_PATH, null).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_MOBILENUMBER, null).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.PARTNER_CODE, null).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.PARTNER_USERNAME, null).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.PARTNER_PASSWORD, null).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_FBACCESS_ID, null).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_NOTIFICATION_STATUS, null).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_CREDIT, null).commit();
	}
	public String getPinText(){
		return BashApplication.getSharedPreferences().getString(AppConstants.PIN_TEXT, null);
	}
	
	public void setPinText(String pinText){
		BashApplication.getSharedPreferences().edit().putString(AppConstants.PIN_TEXT, pinText).commit();
	}
	
	public String getUser_FullName()
	{
		return BashApplication.getSharedPreferences().getString(AppConstants.USER_FULLNAME, null);
	}
	
	public void setUser_FullName(String fullName){
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_FULLNAME, fullName).commit();
	}
	
	
	public String getUser_Password()
	{
		return BashApplication.getSharedPreferences().getString(AppConstants.USER_PASSWORD, null);
	}
	
	public void setUser_Password(String password){
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_PASSWORD, password).commit();
	}
	
	public String getUserEmailId(){
		return BashApplication.getSharedPreferences().getString(AppConstants.USER_EMAILID, null);
	}
	
	public void setUserMobileNumber(String mobileNumber){
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_MOBILENUMBER, mobileNumber).commit();
	}
	
	public void setUserId(String userId){
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_ID, userId).commit();
	}
	
	public String getUserCredits(){
		return BashApplication.getSharedPreferences().getString(AppConstants.USER_CREDIT, null);
	}
	
	public void setUserCredits(String credit){
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_CREDIT, credit).commit();
	}
	
	public String getUserNotificationSettings(){
		return BashApplication.getSharedPreferences().getString(AppConstants.USER_NOTIFICATION_STATUS, null);
	}
	
	public void setUserNotificationSettings(String notificationStatus){
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_FBACCESS_ID, notificationStatus).commit();
	}
	
	public String getPartnerCode(){
		return BashApplication.getSharedPreferences().getString(AppConstants.PARTNER_CODE, null);
	}
	
	
	
	public void setTempUserId(String facebookId){
		BashApplication.getSharedPreferences().edit().putString(AppConstants.TEMP_USER_ID, facebookId).commit();
	}
	
	
	public String getTempUserId(){
		return BashApplication.getSharedPreferences().getString(AppConstants.TEMP_USER_ID, null);
	}
	
	public void setUserFacebookId(String facebookId){
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_FBACCESS_ID, facebookId).commit();
	}
	
	public String getUserFacebookId(){
		return BashApplication.getSharedPreferences().getString(AppConstants.USER_FBACCESS_ID, null);
	}
	
	public String getPartnerUserName(){
		return BashApplication.getSharedPreferences().getString(AppConstants.PARTNER_USERNAME, null);
	}
	
	public String getPartnerPassword(){
		return BashApplication.getSharedPreferences().getString(AppConstants.PARTNER_PASSWORD, null);
	}
	
	public String getUserId(){
		return BashApplication.getSharedPreferences().getString(AppConstants.USER_ID, null);
	}
	
	public void setUpdatedUserDetails(String fullName, String imagepath)
	{
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_FULLNAME, fullName).commit();
		BashApplication.getSharedPreferences().edit().putString(AppConstants.USER_IMAGE_PATH, imagepath).commit();
	}
	
	public String getUserImagePath(){
		return BashApplication.getSharedPreferences().getString(AppConstants.USER_IMAGE_PATH, null);
	}
	
	public String getUserFullName(){
		return BashApplication.getSharedPreferences().getString(AppConstants.USER_FULLNAME, null);
	}
	public String getUserMobileNumber(){
		return BashApplication.getSharedPreferences().getString(AppConstants.USER_MOBILENUMBER, null);
	}
}

