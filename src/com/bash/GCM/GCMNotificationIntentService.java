package com.bash.GCM;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Managers.DataBaseManager;
import com.bash.Utils.AppConstants;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GCMNotificationIntentService extends IntentService {

	public static final int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;

	public GCMNotificationIntentService() {
		super("GcmIntentService");
	}

	public static final String TAG = "GCMNotificationIntentService";

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) {
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {
				sendNotification("Send error: " + extras.toString(), "");
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {
				sendNotification("Deleted messages on server: "	+ extras.toString(), "");
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType)) {

				for (int i = 0; i < 3; i++) {
					Log.i(TAG,
							"Working... " + (i + 1) + "/5 @ "
									+ SystemClock.elapsedRealtime());
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
					}

				}
				Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
				sendNotification("Message: " + extras.get(AppConstants.MESSAGE_KEY), ""+extras.get(AppConstants.IDUSER_KEY));
				
				Log.i(TAG, "Received: " + extras.toString());
			}
		}
		
		//GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void sendNotification(String msg, String idUser) {
		
		Log.e(TAG, "Preparing to send notification...: " + msg);
		mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
		
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				new Intent(this, HomeActivity.class), 0);

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this).setSmallIcon(R.drawable.bash_notification)
				.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo))
				.setContentTitle("GCM Notification")
				.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
				.setContentText(msg);

		mBuilder.setContentIntent(contentIntent);
		
		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
		Log.e(TAG, "Notification sent successfully.");
		Log.e("ID USER", idUser);
		
		
		/*if(msg.contains("invited")) {
			Log.e("Processing....", "*************");
			DataBaseManager.getInstance().updateCotactTableForFriendRequestReceived(idUser);
		}else if(msg.contains("accepted")) {
			DataBaseManager.getInstance().makeMyFriend(idUser);
		}*/
	}
}
