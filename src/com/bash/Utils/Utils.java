package com.bash.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import com.bash.R;
import com.bash.Application.BashApplication;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.EditText;

public class Utils {

	static long diff, diffSeconds, diffMinutes, diffDays, diffHours;
	
	public static void closeSoftInputBoard(EditText editTextInstance){
		BashApplication.softKeypadInstance.hideSoftInputFromWindow(editTextInstance.getWindowToken(), 0);
	}
	
	public static String getAppColorText(String stringText){
		return "<font color='#00BDCC' size='16'>"+stringText+"</font>";
	}
	
	public static String getFormattedTimerText(String timerText){
		try {
			//AppConstants.dateformatforServer.setTimeZone(TimeZone.getDefault());
			SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			//dateTimeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date newDate = dateTimeFormat.parse(timerText);
			
        	Log.e("converted Time ", String.valueOf(newDate.toGMTString()));
        	Log.e("System time", String.valueOf(new Date().toGMTString()));
        	
        	//new Date().getTime()
        	diff =  new Date().getTime() - newDate.getTime();
        	
        	System.out.println("Difference " + diff);
        	diffSeconds = diff / 1000 % 60;
        	
			diffMinutes = diff / (60 * 1000) % 60;
			
			diffHours = diff / (60 * 60 * 1000) % 24;
			
			System.out.println("Difference hours" + diffHours);
			
			diffDays = diff / (24 * 60 * 60 * 1000);
			
			System.out.println("Difference days" + diffDays);
			
			if(diffDays != 0){
				if(diffDays == 1)
					timerText = String.valueOf(diffDays+" day ago.");
				else
					timerText = String.valueOf(diffDays+" days ago.");
			}
			else if(diffHours != 0 && diffMinutes != 0)
				timerText = String.valueOf(diffHours+" h "+diffMinutes+" m ago.");
			else if (diffHours != 0)
				timerText = String.valueOf(diffHours+" h ago.");
			else if(diffMinutes != 0)
				timerText = String.valueOf(diffMinutes+" m ago.");
			else
				timerText = "just a moment ago.";
			return timerText;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
