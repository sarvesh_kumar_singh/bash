package com.bash.Utils;
import java.io.File;
import java.text.SimpleDateFormat;
import com.bash.Application.BashApplication;
import android.annotation.SuppressLint;
import android.text.Html;
import android.text.Spanned;
public class AppConstants {
	public static final String FILE_DIR = "BashDatas"; 
	public static final String ROOT_DIRECTORY = 
									BashApplication.getPrivateFilePath()
						            + File.separator;
	/*public static final String ROOT_DIRECTORY = 
									Environment.getExternalStorageDirectory()
						            + File.separator + FILE_DIR
						            + File.separator;*/
	public static final String HOME_FRAGMENT_TAG = "homefrgment";
	public static final String CASH_IN_OUT_FRAGMENT_TAG = "cashinandoutfragment";
	public static final String CASH_IN_FRAGMENT_TAG = "cashinfragment";
	public static final String CASH_OUT_FRAGMENT_TAG = "cashoutfragment";
	public static final String MY_PROFILE_FRAGMENT_TAG = "myprofilefragment";
	public static final String MY_WALLET_FRAGMENT_TAG = "mywalletfragment";
	public static final String MY_CARDS_FRAGMENT_TAG = "mycardsfragment";
	public static final String MY_FRIENDS_FRAGMENT_TAG = "myfriendsfragment";
	public static final String MY_FRIENDS_AND_GROUPSFRAGMENT_TAG = "myfriendsandgroupsfragment";
	public static final String MY_GROUPS_FRAGMENT_TAG = "mygroupsfragment";
	public static final String BASH_MOMENTS_FRAGMENT_TAG = "bashmomentsfragment";
	public static final String INVITEPEOPLE_FRAGMENT_TAG = "invitepeoplefragment";
	public static final String PRIVACY_FRAGMENT_TAG = "privacyfragment";
	public static final String SUPPORT_FRAGMENT_TAG = "supportfragment";
	
	//phone config
	public static final String PHONE_WIDTH = "phonewidth";
	public static final String PHONE_HEIGHT = "phoneheight";
	
	//tab contents
	public static final String TAB_FEED_TAG = "Feed";
	public static final String TAB_USER_TAG = "User";
	public static final String TAB_NEW_TAG = "New";
	
	//tab contents for Profile
	public static final String TAB_MYBASH_TAG = "Mybash";
	public static final String TAB_MYTRANSACTION_TAG = "Mytransaction";
	
		
	
	//sub contents of Feed Tabhost
	public static final String TAB_FEED_MYACTIVITY_TAG = "Feed_myfeed";
	public static final String TAB_FEED_TRENDING_TAG = "Feed_trending";
	public static final String TAB_FEED_LATEST_TAG = "Feed_latest";
	
	//Add Description Tabs
	public static final String ADD_DESCRIPTION_GROUP_TAG = "Group_Tag";
	public static final String ADD_DESCRIPTION_PAY_TAG = "Pay_Tag";
	public static final String ADD_DESCRIPTION_CHARGE_TAG = "Charge_Tag";
	
	
	//sub contents of My Transaction 
	public static final String TAB_HOME_TOTALBALANCE_TAG = "tab_home_totalbalance_tag";
	public static final String TAB_HOME_YOU_OWE_TAG = "tab_home_you_owe_tag";
	public static final String TAB_HOME_YOU_ARE_OWED_TAG = "tab_home_you_are_owed_tag";
	
	//sub contents of My AddTab 
		public static final String TAB_PAY_TAG = "tab_pay_tag";
		public static final String TAB_SPLIT_TAG = "tab_split_tag";
		public static final String TAB_RECEIVE_TAG = "tab_receive_tag";
		
		public static final String FRAGMENT_TAG = "Fragment_tag";
		
		//sub contents of My AddTabSplit 
				public static final String SPLIT_TAB_RATIO_TAG = "tab_ratio_tag";
				public static final String SPLIT_TAB_UNEQUAL_TAG = "tab_unequal_tag";
				public static final String SPLIT_TAB_EQUAL_TAG = "tab_equal_tag";
				public static final String SPLIT_TAB_PERCENT_TAG = "tab_percent_tag";
				public static final String SPLIT_TAB_ITEMWISE_TAG = "tab_itemwise_tag";
				
				public static final String SPLITFRAGMENT_TAG = "splitfragment_tag";
	
	//sub contents of total balance 
	public static final String TAB_HOME_TOTALBALANCE_FRIENDS_TAG = "tab_home_totalbalance_friends";
	public static final String TAB_HOME_TOTALBALANCE_GROUP_TAG = "tab_home_totalbalance_groups";
	
	//sub contents of yow owe balance 
	public static final String TAB_HOME_YOU_OWE_FRIENDS_TAG = "tab_home_you_owe_friends";
	public static final String TAB_HOME_YOU_OWE_GROUP_TAG = "tab_home_you_owe_groups";
	
	//sub contents of you are owed balance 
	public static final String TAB_HOME_YOU_ARE_OWED_FRIENDS_TAG = "tab_home_you_are_owe_friends";
	public static final String TAB_HOME_YOU_ARE_OWED_GROUP_TAG = "tab_home_you_are_owe_groups";
	
	
	
	//sub contents of My Transaction Part 1
	public static final String TAB_MYPROFILE_MYTRANSACTION_PENDING_TAG = "Myprofile_mytransaction_pending";
	public static final String TAB_MYPROFILE_HISTORY_PENDING_TAG = "Myprofile_hisotry_pending";
	
	//sub contents of My Wallent => Banking => saved, debit, credit Cards 
	public static final String TAB_MYWALLET_BANKING_SAVEDCARD = "mywallet_banking_savedcard";
	public static final String TAB_MYWALLET_BANKING_DEBITCARD = "mywallet_banking_debitcard";
	public static final String TAB_MYWALLET_BANKING_CREDITCARD = "mywallet_banking_creditcard";
		
	
	//sub contents of My Wallent => Banking => Save accounts, new Accounts 
	public static final String TAB_MYWALLET_BANKING_SAVEDACCOUNT = "mywallet_banking_savedaccount";
	public static final String TAB_MYWALLET_BANKING_NEWACCOUNT = "mywallet_banking_newaccount";
	
	
	
	//sub contents of My Wallent => Banking => saved, debit, credit Cards 
	public static final String TAB_MYWALLET_DEPOSIT_SAVEDCARD = "mywallet_deposit_savedcard";
	public static final String TAB_MYWALLET_DEPOSIT_DEBITCARD = "mywallet_deposit_debitcard";
	public static final String TAB_MYWALLET_DEPOSIT_CREDITCARD = "mywallet_deposit_creditcard";
	public static final String TAB_MYWALLET_DEPOSIT_NEWBANKING = "mywallet_deposit_creditcard";
			
		
	
	//sub contents of My Transaction --> Pending 
	public static final String TAB_PENDING_FRIENDS_TAG = "Pending_friends";
	public static final String TAB_PENDING_GROUPS_TAG = "Pending_groups";
		
	//sub contents of My Transaction --> History 
	public static final String TAB_HISTORY_FRIENDS_TAG = "History_friends";
	public static final String TAB_HISTORY_GROUPS_TAG = "History_groups";
	
	// Indian Raa symbol
	public static final Spanned RASYMBOL = Html.fromHtml("&#8377");
	
	//gcm constants
	public static final String GOOGLE_PROJECT_ID = "706125230629";
	public static final String MESSAGE_KEY = "message";
	public static final String IDUSER_KEY = "iduser";
	public static final String GCM_ID = "gcmId";
	
	//userDetails
	public static final String TEMP_USER_ID = "tempuserId";
	public static final String USER_ID = "userId";
	public static final String USER_EMAILID = "userEmailId";
	public static final String USER_MOBILENUMBER = "userMobileNumber";
	public static final String USER_FULLNAME = "userFullName";
	public static final String USER_PASSWORD = "userPassword";
	public static final String USER_IMAGE_PATH = "userImagepath";
	public static final String USER_FBACCESS_ID = "userFbAccessId";
	public static final String PARTNER_CODE = "partnerCode";
	public static final String PARTNER_USERNAME = "partnerUsername";
	public static final String PARTNER_PASSWORD = "partnerPassword";
	public static final String USER_NOTIFICATION_STATUS = "usernotficationstatus";
	public static final String USER_CREDIT = "userCredits";
	
	public static final String PIN_TEXT = "pintext";
	
	
	//Social Media Details
	
	
	// Facebook Id
	public static final String FB_ID = "1403423463232852";
	public static final String APP_NAMESPACE = "BashApplication";
	
	//Camera and Sd card IDS
	public static final int CODE_CAMERA = 111;
	public static final int CODE_GALLERY = 222;
	
	
	//DateFormatter SimpleDateFormat
	@SuppressLint("SimpleDateFormat")
	public static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
	
	
	//Payment Url
	public static final String URL_ZIP_CASH = "http://www.zipcash.in/InAPPPG_UAT/PayByZipcash.aspx";
	public static final SimpleDateFormat dateformatforZipCash = new SimpleDateFormat("ddMMyyyyhhmmss");
	public static final SimpleDateFormat dateformatforServer = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
 }

