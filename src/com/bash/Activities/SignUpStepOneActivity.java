package com.bash.Activities;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bash.R;
import com.bash.Managers.ActivityManager;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;

public class SignUpStepOneActivity extends Activity implements AsyncResponse{
	MyAsynTaskManager  myAsyncTask;
Typeface customFont;
	String facebookid;
	@Override
	public void onBackPressed() 
	{
		Intent intent = new Intent(SignUpStepOneActivity.this,BashLandingPage.class);
		startActivity(intent);
		overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
		finish();
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		// To hide Title Bar and Enable Full Screen
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	    //for Portrait mode only
	    setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    //To Hide Title Bar Only
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup_stepone);
		customFont = Typeface.createFromAsset(SignUpStepOneActivity.this.getAssets(), "myriad.ttf");
		((Button)findViewById(R.id.cancelButton)).setTypeface(customFont);
		((Button)findViewById(R.id.cancelButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(SignUpStepOneActivity.this,BashLandingPage.class);
				startActivity(intent);
				overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
				finish();
			}
		});
		((Button)findViewById(R.id.signUpButton)).setTypeface(customFont);
		((Button)findViewById(R.id.signUpButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(ValidateForm()){
					ProcessSignUpStepOneOpteration();
				}
			}
		});
		
		
//		((Button)findViewById(R.id.checkAvailButton)).setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				if (TextUtils.isEmpty(((EditText) findViewById(R.id.userNameText)).getText().toString())){
//					((EditText) findViewById(R.id.userNameText)).setError("Field is Empty");
//				}else{
//					checkAvailability(((EditText) findViewById(R.id.userNameText)).getText().toString());
//				}
//			}
//		});
		
		((EditText) findViewById(R.id.fullNameText)).setTypeface(customFont);
		((EditText) findViewById(R.id.emailIdText)).setTypeface(customFont);
		((EditText) findViewById(R.id.passwordText)).setTypeface(customFont);
		((EditText) findViewById(R.id.confirmpasswordText)).setTypeface(customFont);
	}
	
	/*@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(PreferenceManager.getInstance().getUserId() != null || BashApplication.isUserLoggedIn){
		//	ActivityManager.startActivity(SignUpActivity.this, HomeActivity.class);
			finish();
		}
	}*/
	 
	public boolean ValidateForm() {
		
		if(TextUtils.isEmpty(((EditText) findViewById(R.id.fullNameText)).getText().toString())){
			((EditText) findViewById(R.id.fullNameText)).setError("Field is Empty");
			return false;
		}
//		else if (TextUtils.isEmpty(((EditText) findViewById(R.id.lastNameText)).getText().toString())){
//			((EditText) findViewById(R.id.lastNameText)).setError("Field is Empty");
//			return false;
//		}
//		else if (TextUtils.isEmpty(((EditText) findViewById(R.id.userNameText)).getText().toString())){
//			((EditText) findViewById(R.id.userNameText)).setError("Field is Empty");
//			return false;
//		}
		else if (TextUtils.isEmpty(((EditText) findViewById(R.id.passwordText)).getText().toString())){
			((EditText) findViewById(R.id.passwordText)).setError("Field is Empty");
			return false;
		}  else if (TextUtils.isEmpty(((EditText) findViewById(R.id.confirmpasswordText)).getText().toString())){
			((EditText) findViewById(R.id.confirmpasswordText)).setError("Field is Empty");
			return false;
		} else if (TextUtils.isEmpty(((EditText) findViewById(R.id.emailIdText)).getText().toString())){
			((EditText) findViewById(R.id.emailIdText)).setError("Field is Empty");
			return false;
		}
		
		else if(!(((EditText) findViewById(R.id.passwordText)).getText().toString()).equals(
				((EditText) findViewById(R.id.confirmpasswordText)).getText().toString())) {
			Toast.makeText(getApplicationContext(), "Password and ReType Password doesn't match!", Toast.LENGTH_SHORT).show();
			return false;
		}
		
		else if(!emailValidator(((EditText) findViewById(R.id.emailIdText)).getText().toString())){
			((EditText) findViewById(R.id.emailIdText)).setError("Email Id is Not Valid!");
			return false;
		}
		return true;
	}
	
	public boolean emailValidator(String email) 
	{
	    Pattern pattern;
	    Matcher matcher;
	    final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	    pattern = Pattern.compile(EMAIL_PATTERN);
	    matcher = pattern.matcher(email);
	    return matcher.matches();
	}
	
public void checkAvailability(String userName) {
	myAsyncTask=new MyAsynTaskManager();
	myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("checkAvailability",SignUpStepOneActivity.this,AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"username"},
				
			new String[]{
				"user",
				"checkusername",
				userName});

		myAsyncTask.execute();
	}
	
	
	public void ProcessSignUpStepOneOpteration() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl
		("ProcessSignUpStepOneOpteration",SignUpStepOneActivity.this,AppUrlList.ACTION_URL, new String[]
				{
				"module",
				"action",
				"fullname",
				"email_id",
				"password",
				"device_id",
				"device_type",
				"facebookid"},
				
			new String[]{
				"user",
				"register",
				((EditText) findViewById(R.id.fullNameText)).getText().toString(),
				((EditText) findViewById(R.id.emailIdText)).getText().toString(),
			 	((EditText) findViewById(R.id.passwordText)).getText().toString(),
				PreferenceManager.getInstance().getGCMRegistrationId(),
				"0",
				(PreferenceManager.getInstance().getUserFacebookId() != null) ? 
						PreferenceManager.getInstance().getUserFacebookId() : ""});
		myAsyncTask.execute();
		
	}
	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		
		if(from.equalsIgnoreCase("checkAvailability"))
		{
			if(output!=null)
			{
				try {
					JSONObject rootObj = new JSONObject(output);
					if(rootObj.getBoolean("result")){
						Toast.makeText(getApplicationContext(), "UserName is Available!", Toast.LENGTH_SHORT).show();
						((EditText) findViewById(R.id.userNameText)).setError(null);
					}else{
						((EditText) findViewById(R.id.userNameText)).setError("Change any other username!");
					}

				} catch (Exception e) {
					e.printStackTrace();
					DialogManager.showDialog(SignUpStepOneActivity.this, "Server Error Occured! Try Again!");
				}

			}
			else
			{
				DialogManager.showDialog(SignUpStepOneActivity.this, "Server Error Occured! Try Again!");
			}
		}
		else if(from.equalsIgnoreCase("ProcessSignUpStepOneOpteration"))
		{
			if(output!=null)
			{
				try {
					JSONObject rootObj = new JSONObject(output);
					if(rootObj.getBoolean("result")){
						PreferenceManager.getInstance().setTempUserId(rootObj.getString("iduser"));
						PreferenceManager.getInstance().setUser_FullName(rootObj.getString("fullname"));
						PreferenceManager.getInstance().setUser_Password(rootObj.getString("password"));
						ActivityManager.startActivity(SignUpStepOneActivity.this, SignUpStepTwo_A_Activity.class);
						overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
						finish();
					}else{
						DialogManager.showDialog(SignUpStepOneActivity.this, rootObj.getString("msg"));
					}
				} catch (Exception e) {
					e.printStackTrace();
					DialogManager.showDialog(SignUpStepOneActivity.this, "Server Error Occured! Try Again!");
				}	
			}
			else
			{
				DialogManager.showDialog(SignUpStepOneActivity.this, "Server Error Occured! Try Again!");
			}

		}
		
	}
	
}

