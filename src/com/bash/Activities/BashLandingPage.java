package com.bash.Activities;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bash.R;
import com.bash.Application.BashApplication;
import com.bash.GsonClasses.Login_Register_Class;
import com.bash.Managers.ActivityManager;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DataBaseManager;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.ContactsProvider;
import com.bash.Utils.AppUrlList;
import com.google.gson.Gson;
import com.sromku.simple.fb.Permission.Type;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnLogoutListener;
import com.sromku.simple.fb.listeners.OnProfileListener;

public class BashLandingPage extends Activity implements OnClickListener,AsyncResponse{
	Typeface customFont;
	private SimpleFacebook mSimpleFacebook;
	public Dialog addPhoneDialog;
	public View addPhoneView;
	ViewPager viewpager;
	MyAsynTaskManager myAsyncTask;
	TextView signIn,signUp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		// To hide Title Bar and Enable Full Screen
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	    //for Portrait mode only
	    setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    //To Hide Title Bar Only
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.bash", 
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("BashKeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                }
        } catch (NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
		
		setContentView(R.layout.bash_landing_page);
		customFont = Typeface.createFromAsset(BashLandingPage.this.getAssets(), "myriad.ttf");
		viewpager=(ViewPager)findViewById(R.id.viewpager);
		signIn=(TextView)findViewById(R.id.signIn);
		signUp=(TextView)findViewById(R.id.signUp);
		signIn.setTypeface(customFont);
		signUp.setTypeface(customFont);
		signIn.setOnClickListener(this);
		signUp.setOnClickListener(this);
		SpannableString signInContent = new SpannableString("Sign in");
		signInContent.setSpan(new UnderlineSpan(), 0, signInContent.length(), 0);
		signIn.setText(signInContent);
		SpannableString signUpContent = new SpannableString("Sign up");
		signUpContent.setSpan(new UnderlineSpan(), 0, signUpContent.length(), 0);
		signUp.setText(signUpContent);
        ((Button)findViewById(R.id.fbSignIn)).setOnClickListener(this);
        mSimpleFacebook = SimpleFacebook.getInstance(this);
        // Loading contacts 
        if(DataBaseManager.getInstance().getContactsCount() == 0){
        	ContactsProvider.loadContacts(BashLandingPage.this);
        }
        
        if(PreferenceManager.getInstance().getUserId() != null){
			ActivityManager.startActivity(BashLandingPage.this, HomeActivity.class);
			overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
			finish();
		}else{
			BashApplication.isUserLoggedIn = false;
		}
        System.gc();
        initializeAlertView();
        viewpager.setAdapter(new SlideViewAdapter());
        viewpager.setCurrentItem(0);
	}
public class SlideViewAdapter extends PagerAdapter
{
	@Override
	public int getCount() {
		return 3;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((View) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		View itemView = inflater.inflate(R.layout.viewpager, container,false);
		ImageView mainrel = (ImageView) itemView.findViewById(R.id.mainrel);
		ImageView firstslideimage=(ImageView)itemView.findViewById(R.id.firstslideimage);
		ImageView secondslideimage=(ImageView)itemView.findViewById(R.id.secondslideimage);
		ImageView thirdslideimage=(ImageView)itemView.findViewById(R.id.thirdslideimage);
		//mainrel.setTag(R.id.mainrel,position);
		if(position==0)
		{
			mainrel.setImageResource(R.drawable.swipe_one);
			firstslideimage.setImageResource(R.drawable.circle_swipe_blue);
			secondslideimage.setImageResource(R.drawable.circle_swipe_grey1);
			thirdslideimage.setImageResource(R.drawable.circle_swipe_grey1);
		}
		else if(position==1)
		{
			mainrel.setImageResource(R.drawable.swipe_two);
			secondslideimage.setImageResource(R.drawable.circle_swipe_blue);
			firstslideimage.setImageResource(R.drawable.circle_swipe_grey1);
			thirdslideimage.setImageResource(R.drawable.circle_swipe_grey1);
		}
		else
		{
			mainrel.setImageResource(R.drawable.swipe_three);
			thirdslideimage.setImageResource(R.drawable.circle_swipe_blue);
			secondslideimage.setImageResource(R.drawable.circle_swipe_grey1);
			firstslideimage.setImageResource(R.drawable.circle_swipe_grey1);
		}
		((ViewPager) container).addView(itemView);

		return itemView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) 
	{
		((ViewPager) container).removeView((View) object);

	}

	@Override
	public void startUpdate(ViewGroup container) {
		// TODO Auto-generated method stub
		super.startUpdate(container);

	}

	@Override
	public Parcelable saveState() {
		return null;
	}
}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mSimpleFacebook = SimpleFacebook.getInstance(this);
		if(PreferenceManager.getInstance().getUserId() != null || BashApplication.isUserLoggedIn){
		//	ActivityManager.startActivity(SignInActivity.this, HomeActivity.class);
			finish();
		}else{
			PreferenceManager.getInstance().resetUserDetails();
			/*if(mSimpleFacebook.getInstance().isLogin())
				mSimpleFacebook.getInstance().logout(onLogoutListner);*/
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		mSimpleFacebook.onActivityResult(this, requestCode, resultCode, data);
	}
	
	@Override
	public void onClick(View v) {
		
		if(v.getId() == R.id.signIn)
		{
			ActivityManager.startActivity(BashLandingPage.this, Login_Bash_Activity.class);
			overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
			finish();
		}
			
		
			//ActivityManager.startActivity(SignInActivity.this, HomeActivity.class);
		else if(v.getId() == R.id.signUp)
		{
			ActivityManager.startActivity(BashLandingPage.this, SignUpStepOneActivity.class);
			overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
			finish();
		}
			
			
			//ActivityManager.startActivity(SignInActivity.this, HomeActivity.class);
		else if(v.getId() == R.id.fbSignIn){
			
//			if(SimpleFacebook.getInstance().isLogin()){
//	 			callFbPost();	
//	 		}else{
//	 			SimpleFacebook.getInstance().login(onLoginListener);		 			
//	 		}
			
			
			mSimpleFacebook.getInstance().login(new OnLoginListener() {
				@Override
				public void onFail(String reason) {
					Toast.makeText(getApplicationContext(),"Fb Login Failed!", Toast.LENGTH_SHORT).show();					
				}
				
				@Override
				public void onException(Throwable throwable) {
					Toast.makeText(getApplicationContext(),"Fb Login Failed!", Toast.LENGTH_SHORT).show();
					
				}
				
				@Override
				public void onThinking() {
					
				}
				
				@Override
				public void onNotAcceptingPermissions(Type type) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onLogin() {
					Toast.makeText(getApplicationContext(),"Redirecting...", Toast.LENGTH_SHORT).show();
					//SimpleFacebook.getInstance().get
					mSimpleFacebook.getProfile(onProfileListener);
				
				}
			});
		}
			//ActivityManager.startActivity(SignInActivity.this, HomeActivity.class);
	}
	
	Profile userProfile;
	
	OnLogoutListener onLogoutListner = new OnLogoutListener() {
		
		@Override
		public void onFail(String reason) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onException(Throwable throwable) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onThinking() {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onLogout() {
			// TODO Auto-generated method stub
			
		}
	};
	
	OnProfileListener onProfileListener = new OnProfileListener() {         
	    @Override
	    public void onComplete(Profile profile) {
	    //    Log.i(TAG, "My profile id = " + profile.getId());
	    	
	    	/*android.util.Log.e("profile Id", profile.getId());
	    	android.util.Log.e(" ", "My email id = " + profile.getEmail());
	    	android.util.Log.e(" ", "My birthday id = " + profile.getBirthday());
	    	android.util.Log.e(" ", "My name id = " + profile.getName());
	    	android.util.Log.e(" ", "My gender id = " + profile.getGender());*/
	    	
	    	userProfile = profile;
	    	Log.e("Id", userProfile.getId());
	    	PreferenceManager.getInstance().setUserFacebookId(userProfile.getId());
	    	CheckFacebookLogin();
	    	//addPhoneDialog.show();
	    	
	    	
	    }
	};
	
	private void initializeAlertView() {
		addPhoneDialog = new Dialog(this);
		addPhoneDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		addPhoneView = View.inflate(this, R.layout.alert_askphonenumber_dialog, null);
		addPhoneDialog.setContentView(addPhoneView);
		addPhoneDialog.setCancelable(false);
		((Button)addPhoneView.findViewById(R.id.submitPhoneNumber)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(((EditText) addPhoneView.findViewById(R.id.mobileNumberText)).getText() != null && 
						((EditText) addPhoneView.findViewById(R.id.mobileNumberText)).getText().toString().length() != 0){
					if(((EditText) addPhoneView.findViewById(R.id.mobileNumberText)).getText().length()>=10){
						ProcessFacebookLoginProcess();
						addPhoneDialog.dismiss();		
					}else{
						Toast.makeText(getApplicationContext(), "Please Enter the 10 digit mobile number!", Toast.LENGTH_SHORT).show();
					}
				}else{
					((EditText) addPhoneView.findViewById(R.id.mobileNumberText)).setError("Phone number is Empty!");
				}
			}
		});
	}
	
	
public void ProcessSignUpStepOneOpteration() {

	
	
	myAsyncTask=new MyAsynTaskManager();
	myAsyncTask.delegate=this;
	myAsyncTask.setupParamsAndUrl("ProcessSignUpStepOneOpteration",BashLandingPage.this,AppUrlList.ACTION_URL, new String[]{
				"module",
				"action",
				"firstname",
				"lastname",
				"email_id",
				"username",
				"password",
				"device_id",
				"device_type",
				"facebookid"},
				
			new String[]{
				"user",
				"register",
				userProfile.getFirstName(),
				userProfile.getLastName(),
				"",
				"",
			 	"",
				PreferenceManager.getInstance().getGCMRegistrationId(),
				"0",
				userProfile.getId()
				});
	myAsyncTask.execute();
		
	}
	

	
	public void CheckFacebookLogin() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("CheckFacebookLogin",BashLandingPage.this,AppUrlList.ACTION_URL, new String[]{
				"module",
				"action",
				"facebookid"
				},
				
			new String[]{
				"user",
				"facebook_login",
				userProfile.getId()
				});
		myAsyncTask.execute();
		
		
	}
	
	
public void ProcessFacebookLoginProcess() {
	myAsyncTask=new MyAsynTaskManager();
	myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("ProcessFacebookLoginProcess",BashLandingPage.this,AppUrlList.ACTION_URL, new String[]{
				"module",
				"action",
				"email_id",
				"password",
				"firstname",
				"lastname",
				"dob",
				"mobile_number",
				"username",
				"gender",
				"device_id",
				"device_type",
				"facebookid"
				},
			new String[]{
				"user",
				"register",
				"",
				"",
				(userProfile.getFirstName() == null || userProfile.getFirstName().length() == 0) ? "" : userProfile.getFirstName(),
				(userProfile.getLastName() == null || userProfile.getLastName().length() == 0) ? "" : userProfile.getLastName(),
				(userProfile.getBirthday() == null || userProfile.getBirthday().length() == 0) ? "" : userProfile.getBirthday(),
				"91"+((EditText) addPhoneView.findViewById(R.id.mobileNumberText)).getText().toString(),
				((EditText) addPhoneView.findViewById(R.id.mobileNumberText)).getText().toString(),
				(userProfile.getGender() != null && userProfile.getGender().equals("male")) ? "M" : "F",
				PreferenceManager.getInstance().getGCMRegistrationId(),
				"0",
				userProfile.getId()
				});
		myAsyncTask.execute();

	}
@Override
public void backgroundProcessFinish(String from, String output) {
	if(from.equalsIgnoreCase("ProcessSignUpStepOneOpteration"))
	{
		if(output!=null)
		{
			try {
				JSONObject rootObj = new JSONObject(output);
				if(rootObj.getBoolean("result")){
					PreferenceManager.getInstance().setTempUserId(rootObj.getString("iduser"));
					ActivityManager.startActivity(BashLandingPage.this, SignUpStepTwo_A_Activity.class);
					overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
					finish();
				}else{
					DialogManager.showDialog(BashLandingPage.this, rootObj.getString("msg"));
				}

			} catch (Exception e) {
				e.printStackTrace();
				DialogManager.showDialog(BashLandingPage.this, "Server Error Occured! Try Again!");
			}

		}
		else
		{
			DialogManager.showDialog(BashLandingPage.this, "Server Error Occured! Try Again!");
		}
	}
	else if(from.equalsIgnoreCase("CheckFacebookLogin"))
	{
		if(output!=null)
		{
			try {
				Gson gson = new Gson();
				Login_Register_Class responseForLogin = gson.fromJson(output, Login_Register_Class.class);
					Log.e("Response", output);
				if(responseForLogin.result){
					if(responseForLogin.msg.equals("newuser")){
						//addPhoneDialog.show();
						ProcessSignUpStepOneOpteration();
					}else{
					PreferenceManager.getInstance().setUserDetails
						(responseForLogin.iduser, 
						 responseForLogin.email_id,
						 responseForLogin.fullname,
						 responseForLogin.password,
						 responseForLogin.userimage,
						 responseForLogin.PartnerCode,
						 responseForLogin.PartnerUsername,
						 responseForLogin.PartnerPassword,
						 responseForLogin.phone_no,
						 responseForLogin.facebookid,
						 responseForLogin.pushnotify_on,
						 responseForLogin.credit	
						 );
					BashApplication.isUserLoggedIn = true;
					//ContactsProvider.loadContacts(SignUpActivity.this);
						ActivityManager.startActivity(BashLandingPage.this, HomeActivity.class);
						overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
						//new code
						finish();
					}
					
				}else if(responseForLogin.msg.equals("newuser")){
					//DialogManager.showDialog(SignInActivity.this, responseForLogin.msg);
					//addPhoneDialog.show();
					ProcessSignUpStepOneOpteration();
					
				}else if(responseForLogin.msg.equals("not zipuser")){
					PreferenceManager.getInstance().setTempUserId(responseForLogin.iduser);
					ActivityManager.startActivity(BashLandingPage.this, SignUpStepTwo_A_Activity.class);
					overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
					finish();
				}
				
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					DialogManager.showDialog(BashLandingPage.this, "Server Error Occured! Try Again!");
				}
		}
		else
		{
			DialogManager.showDialog(BashLandingPage.this, "Server Error Occured! Try Again!");
		}
	}
	else if(from.equalsIgnoreCase("ProcessFacebookLoginProcess"))
	{
		if(output!=null)
		{
			try {
				Gson gson = new Gson();
				Login_Register_Class responseForLogin = gson.fromJson(output, Login_Register_Class.class);

				if(responseForLogin.result){
					PreferenceManager.getInstance().setUserDetails
					(responseForLogin.iduser, 
							responseForLogin.email_id,
							responseForLogin.fullname,
							responseForLogin.password,
							responseForLogin.userimage,
							responseForLogin.PartnerCode,
							responseForLogin.PartnerUsername,
							responseForLogin.PartnerPassword,
							responseForLogin.phone_no,
							responseForLogin.facebookid,
							responseForLogin.pushnotify_on,
							responseForLogin.credit
							);
					BashApplication.isUserLoggedIn = true;
					//ContactsProvider.loadContacts(SignUpActivity.this);
					ActivityManager.startActivity(BashLandingPage.this, HomeActivity.class);
					overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
					finish();
				}else{
					//DialogManager.showDialog(SignInActivity.this, responseForLogin.msg);
					addPhoneDialog.show();
					Toast.makeText(getApplicationContext(), responseForLogin.msg, Toast.LENGTH_SHORT).show();
				}
			} catch (Exception e) {
				e.printStackTrace();
				addPhoneDialog.show();
				Toast.makeText(getApplicationContext(), "Sever Error Occured! Try Again!", Toast.LENGTH_SHORT).show();
			}

		}
		else
		{
			addPhoneDialog.show();
			Toast.makeText(getApplicationContext(), "Sever Error Occured! Try Again!", Toast.LENGTH_SHORT).show();
		}

	}

}
	
	
	
	
}
