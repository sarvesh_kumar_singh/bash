package com.bash.Activities;

import com.bash.Managers.ActivityManager;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.bash.R;

public class AddCardActivity extends Activity implements OnClickListener {
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addcard);
        ((ImageView)findViewById(R.id.imageView2)).setOnClickListener(this);
        ((ImageView)findViewById(R.id.imageView3)).setOnClickListener(this);
        ((ImageView)findViewById(R.id.imageView1)).setOnClickListener(this);
        ((ImageView)findViewById(R.id.imageView4)).setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		ActivityManager.startActivity(AddCardActivity.this, HomeActivity.class);
	}
}
