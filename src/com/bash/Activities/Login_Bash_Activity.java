package com.bash.Activities;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.BuildConfig;
import com.bash.R;
import com.bash.Application.BashApplication;
import com.bash.GsonClasses.Login_Register_Class;
import com.bash.Managers.ActivityManager;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DataBaseManager;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.ContactsProvider;
import com.bash.Utils.AppUrlList;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.google.gson.Gson;
public class Login_Bash_Activity extends Activity implements AsyncResponse
{
	String facebookemail="";
	String facebookimagepath="";
	String facebookname="";
	String facebookid="";
	List<String> permissions;
	private UiLifecycleHelper uiHelper;
	MyAsynTaskManager  myAsyncTask;
	Typeface customFont;
	Dialog progressDialog;
	Login_Register_Class responseForLogin;
	public Dialog forgotPasswordDialog;
	CheckOutProcess cp;
	@Override
	public void onBackPressed() 
	{
		Intent intent = new Intent(Login_Bash_Activity.this,BashLandingPage.class);
		startActivity(intent);
		overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
		finish();
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		
		// To hide Title Bar and Enable Full Screen
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		//for Portrait mode only
		setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		//To Hide Title Bar Only
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		progressDialog = new Dialog(this);
		permissions = new ArrayList<String>();
		 permissions.add("email");
		uiHelper = new UiLifecycleHelper(this, callback);
	    uiHelper.onCreate(savedInstanceState);
		//progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		progressDialog.getWindow().setBackgroundDrawable
		(new ColorDrawable(Color.TRANSPARENT));
		progressDialog.setContentView(R.layout.progress_dialog_view);
		progressDialog.setCancelable(false);
		progressDialog.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				Log.e("Progress Dialog Closed!", "Manually!");
				cp.cancel(true);

			}
		});
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		customFont = Typeface.createFromAsset(Login_Bash_Activity.this.getAssets(), "myriad.ttf");
		cp=new CheckOutProcess();

		initializeAlertView();


		((Button)findViewById(R.id.login_button)).setTypeface(customFont);
		((Button)findViewById(R.id.login_button)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(ValidateForm()){
					LoginOperation();
					
				}

			}
		});
		((Button)findViewById(R.id.back_button)).setTypeface(customFont);
		((Button)findViewById(R.id.back_button)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Login_Bash_Activity.this,BashLandingPage.class);
				startActivity(intent);
				overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
				finish();
			}
		});
		
		((TextView)findViewById(R.id.forgetPasswordText)).setTypeface(customFont);
		((TextView)findViewById(R.id.forgetPasswordText)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				forgotPasswordDialog.show();
			}
		});
		((EditText)findViewById(R.id.emailIdText)).setTypeface(customFont);
		((EditText)findViewById(R.id.passwordText)).setTypeface(customFont);
	}
	public boolean ValidateForm() {

		if(TextUtils.isEmpty(((EditText) findViewById(R.id.emailIdText)).getText().toString())){
			((EditText) findViewById(R.id.emailIdText)).setError("Field is Empty");
			return false;
		}
		else if (TextUtils.isEmpty(((EditText) findViewById(R.id.passwordText)).getText().toString())){
			((EditText) findViewById(R.id.passwordText)).setError("Field is Empty");
			return false;
		}  
		else if(!emailValidator(((EditText) findViewById(R.id.emailIdText)).getText().toString())){
			((EditText) findViewById(R.id.emailIdText)).setError("Email Id is Not Valid!");
			return false;
		}
		return true;
	}
	public boolean emailValidator(String email) 
	{
		Pattern pattern;
		Matcher matcher;
		final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(email);
		return matcher.matches();
	}

	private void initializeAlertView() {

		forgotPasswordDialog = new Dialog(this,R.style.Dialog_No_Border);
		forgotPasswordDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		forgotPasswordDialog.setContentView(R.layout.forgotdialog);
		//forgotPasswordView = View.inflate(this, R.layout.forgotdialog, null);
		//forgotPasswordDialog.setContentView(forgotPasswordView);
		//((LinearLayout))
		LinearLayout topLayout=(LinearLayout)forgotPasswordDialog.findViewById(R.id.topLayout);
		TextView textView1=(TextView)forgotPasswordDialog.findViewById(R.id.textView1);
		textView1.setTypeface(customFont);
		EditText et=(EditText)forgotPasswordDialog.findViewById(R.id.mobileNumberText);
		et.setTypeface(customFont);
		Button btn_cancel=(Button)forgotPasswordDialog.findViewById(R.id.btn_cancel);
		btn_cancel.setTypeface(customFont);
		Button btn_ok=(Button)forgotPasswordDialog.findViewById(R.id.btn_ok);
		btn_ok.setTypeface(customFont);
		topLayout.setBackgroundColor(Color.TRANSPARENT);
		btn_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				forgotPasswordDialog.dismiss();
			}
		});

		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(((EditText) forgotPasswordDialog.findViewById(R.id.mobileNumberText)).getText() != null && 
						((EditText) forgotPasswordDialog.findViewById(R.id.mobileNumberText)).getText().toString().length() != 0){
					if(((EditText) forgotPasswordDialog.findViewById(R.id.mobileNumberText)).getText().length()>=10){
						sendPasswordToMobileNumber();
						forgotPasswordDialog.dismiss();		
					}else{
						Toast.makeText(getApplicationContext(), "Please Enter your Email Id!", Toast.LENGTH_SHORT).show();
					}
				}else{
					((EditText) forgotPasswordDialog.findViewById(R.id.mobileNumberText)).setError("Email Id is Empty!");
				}
			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(PreferenceManager.getInstance().getUserId() != null || BashApplication.isUserLoggedIn){
			//ActivityManager.startActivity(Login_Bash_Activity.this, HomeActivity.class);
			finish();
		}
	}

	public void sendPasswordToMobileNumber() {

		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("PasswordToMobileNumber",Login_Bash_Activity.this,AppUrlList.ACTION_URL,
				new String[] {"module","action","mobile"}, 
		new String[]{"user","forgotpassword",((EditText) forgotPasswordDialog.findViewById(R.id.mobileNumberText)).getText().toString(),}
		);

		myAsyncTask.execute();
	}

	
	public void LoginOperation() 
	{
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("LoginOperation",Login_Bash_Activity.this,
				AppUrlList.ACTION_URL,
				// Sarvesh Login parameter changed
				new String[] {"module","action","email_id","password"}, 
				new String[]{"user","login",((EditText) findViewById(R.id.emailIdText)).getText().toString(),
				((EditText) findViewById(R.id.passwordText)).getText().toString()}
				/*new String[] {"module","action","email_id","password","device_id","device_type"}, 
				new String[]{"user","registerByFacebookId",((EditText) findViewById(R.id.emailIdText)).getText().toString(),
				((EditText) findViewById(R.id.passwordText)).getText().toString(),
				PreferenceManager.getInstance().getGCMRegistrationId(),"0"}*/
				);
		myAsyncTask.execute();
	}
public void LoginFacebookOperation()
{

	myAsyncTask=new MyAsynTaskManager();
	myAsyncTask.delegate=this;
	myAsyncTask.setupParamsAndUrl("LoginFacebookOperation",Login_Bash_Activity.this,AppUrlList.ACTION_URL,
			new String[] {"module","action","facebookid","facebookname","imagepath","facebookmailid"}, 
			new String[]{"user","facebooklogin",facebookid,facebookname,"facebookimagepath",facebookemail});
	myAsyncTask.execute();

}
	public	class CheckOutProcess extends AsyncTask<Void, Void, Void> 
	{
		//ProgressDialog pd;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			Log.e("Preexecute Called", "");
			//			pd = new ProgressDialog(Login_Bash_Activity.this);
			//			pd.setMessage("Loading CheckOut Process...");
			//			pd.show();
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				ContactsProvider.loadContacts(Login_Bash_Activity.this);
				DataBaseManager.getInstance().checkOutContactTable(responseForLogin.bashusers);	
			} catch (Exception e) {
				// TODO: handle exception
				//pd.dismiss();
				progressDialog.dismiss();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			//pd.dismiss();
			progressDialog.dismiss();
			BashApplication.isUserLoggedIn = true;
			ActivityManager.startActivity(Login_Bash_Activity.this, HomeActivity.class);
		}
	}

	@Override
	public void backgroundProcessFinish(String from, String output) 
	{
		// TODO Auto-generated method stub
		if(from.equalsIgnoreCase("LoginOperation"))
		{
			if(output!=null)
			{
				try {
					if(BuildConfig.DEBUG)
						Log.e("Json Response", output);
					Gson gson = new Gson();

					responseForLogin = gson.fromJson(output, Login_Register_Class.class);

					if(responseForLogin.result) {
						PreferenceManager.getInstance().setUserDetails
						(responseForLogin.iduser, 
								responseForLogin.email_id,
								responseForLogin.fullname,
								responseForLogin.password,
								responseForLogin.userimage, 
								responseForLogin.PartnerCode,
								responseForLogin.PartnerUsername,
								responseForLogin.PartnerPassword,
								responseForLogin.phone_no, 
								responseForLogin.facebookid,
								responseForLogin.pushnotify_on,
								responseForLogin.credit
								);

						cp.execute();

					} else if(responseForLogin.msg.equals("email not verified")){
						DialogManager.showDialog(Login_Bash_Activity.this, "Email Id not Verified! Check Your Mail Inbox!");	
					}
					else if(responseForLogin.msg.equals("not zipuser")) {
						PreferenceManager.getInstance().setTempUserId(responseForLogin.iduser);
						ActivityManager.startActivity(Login_Bash_Activity.this, SignUpStepTwo_A_Activity.class);
						finish();
					}
					else{
						DialogManager.showDialog(Login_Bash_Activity.this, "Email id or Password is Incorrect! Try Again!");
					}
				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(Login_Bash_Activity.this, "Server Error Occured! Try Again!");
				}

			}
			else
			{
				runOnUiThread(new Runnable() {
					public void run() {
						// TODO: handle exception
						DialogManager.showDialog(Login_Bash_Activity.this, "Server Error Occured! Try Again!");
					}
				});

			}
		}
		else if(from.equalsIgnoreCase("PasswordToMobileNumber"))
		{
			if(output!=null)
			{
				try {
					if(BuildConfig.DEBUG)
						Log.e("Json Response", output);
					JSONObject rootObj = new JSONObject(output);
					if(rootObj.getBoolean("result")){
						DialogManager.showDialog(Login_Bash_Activity.this, rootObj.getString("msg"));
					}else{
						DialogManager.showDialog(Login_Bash_Activity.this, rootObj.getString("msg"));
					}

				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(Login_Bash_Activity.this, "Server Error Occured! Try Again!");
				}
				
			}
			else
			{
				// TODO: handle exception
				DialogManager.showDialog(Login_Bash_Activity.this, "Server Error Occured! Try Again!");
			}
		}
	}
	private Session.StatusCallback callback = new Session.StatusCallback() 
	{
	   
		@Override
	    public void call(Session session, SessionState state, Exception exception) {
            if (session.isOpened()) 
            {
                //make request to the /me API
                Log.e("sessionopened", "true");
               Request req= Request.newMeRequest(session, new Request.GraphUserCallback()
                {
                    public void onCompleted(GraphUser user, Response response)
                    {
                        if (user != null) 
                        {
                        	String firstName = user.getFirstName();
                        	String lastName = user.getLastName();
                        	facebookname=firstName+" "+lastName;
                        	facebookid = user.getId();
                        	facebookemail = user.getProperty("email").toString();
                        	facebookimagepath="https://graph.facebook.com/"+facebookid+"/picture?type=large";
                        	Log.e("LOC", facebookid);
                        	Log.e("LOC", firstName);
                        	Log.e("LOC", lastName);
                        	LoginFacebookOperation();

                        }
                    }
                });
               req.executeAsync();
             }
         }
	};
}
