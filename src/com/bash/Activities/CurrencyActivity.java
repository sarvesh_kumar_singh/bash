package com.bash.Activities;

import java.io.InputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.bash.R;
import com.bash.Adapters.ItemArrayAdapter;
import com.bash.ListModels.CurrencyModel;
import com.bash.Managers.CSVFile;
import com.bash.interfaces.ActivityInterface;

public class CurrencyActivity extends Activity {
ImageView currencyBackImage;
ListView lvCurrency;

public static final int RESULT_OK = 101;
public static final int RESULT_CANCELED = 102;
public static ActivityInterface delegate;


private ItemArrayAdapter itemArrayAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_currency_picker);
		
		currencyBackImage = (ImageView) findViewById(R.id.currencybackimage);
		lvCurrency = (ListView) findViewById(R.id.lvCurrencyPicker);
		
		currencyBackImage.setOnClickListener(clickListener);
		
		
	    InputStream inputStream = getResources().openRawResource(R.raw.currencies);
        CSVFile csvFile = new CSVFile(inputStream);
        ArrayList<CurrencyModel> scoreList = csvFile.read();
		
		 itemArrayAdapter = new ItemArrayAdapter(getApplicationContext(), R.layout.currency_item_layout,scoreList);

	        Parcelable state = lvCurrency.onSaveInstanceState();
	        lvCurrency.setAdapter(itemArrayAdapter);
	        lvCurrency.onRestoreInstanceState(state);

	        
	        lvCurrency.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
//					Intent returnIntent = new Intent();
//			        returnIntent.putExtra("result",itemArrayAdapter.getItem(position).getCurrencyCode());
//			        setResult(RESULT_OK,returnIntent);
					
					delegate.onActivityData(itemArrayAdapter.getItem(position).getCurrencyCode());
			        
			        finish();
				}
			});
	        
	}

	private View.OnClickListener clickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {

			switch (v.getId()) {

			case R.id.currencybackimage:
				CurrencyActivity.this.onBackPressed();
				break;


			}
		}
	};
}
