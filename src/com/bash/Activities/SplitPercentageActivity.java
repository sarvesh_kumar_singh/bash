package com.bash.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bash.R;
import com.bash.Activities.AddTransactionActivity;
import com.bash.Activities.HomeActivity;
import com.bash.BaseFragmentClasses.BaseFragment;

public class SplitPercentageActivity extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_percent_amount_page);
		initializeViews();
	}
	
	private void initializeViews() {
		// TODO Auto-generated method stub
		((ImageView) findViewById(R.id.topleftsideImage)).setImageResource(R.drawable.back_btn);
		((ImageView) findViewById(R.id.toprightsideImage)).setBackgroundResource(0);
		
		((ImageView) findViewById(R.id.topleftsideImage)).setOnClickListener(this);
		((RelativeLayout) findViewById(R.id.doneButton)).setOnClickListener(this);
		((RelativeLayout) findViewById(R.id.cancelButton)).setOnClickListener(this);
		
		/*((ImageView) findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		*/
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		finish();
	}
}
