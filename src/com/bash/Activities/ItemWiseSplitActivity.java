package com.bash.Activities;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Adapters.ItemWiseListAdapter;
import com.bash.Adapters.ItemWiseTwoWayViewAdapter;
import com.bash.Application.BashApplication;
import com.bash.ListModels.ItemWiseListModel;
import com.bash.ListModels.Person;
import com.bash.ListModels.PhotosListModel;
import com.bash.ListModels.TwoWayPersonModel;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;

@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
public class ItemWiseSplitActivity extends Activity implements AsyncResponse{

	TextView topleftitemwiseText,toprightitemwisetext, additem_text;
	TwoWayView horizontalvcrollviewphoto;
	public static ListView lvitemwiselist;
	
	
	private String categorySelect = "";
	private String dateString = "";
	private String transationTitle = "";
	private String transationAnount = "";
	private String currencyCodeString = "";
	private ArrayList<Person> addedPersonArrayList;
	private ArrayList<PhotosListModel> photoListArrayList;
	
	ArrayList<TwoWayPersonModel> twowayPersonModelList;
	//ArrayList<String> twowayPersonModelList;
	
	ArrayList<ItemWiseListModel> itemwiseModelList;
	
	ItemWiseTwoWayViewAdapter itemWiseTwoWayViewAdapter;
	ItemWiseListAdapter itemWiseListAdapter;
	
	private AlertDialog taxdialog = null;
	
	private int editTextCount = 0;
	
	boolean taxSkip = false;
	
	ArrayList<HashMap<String, String>> taxdetailsInfo = null;
	ArrayList<HashMap<String, Object>> itemdetailsInfo = null;
	ArrayList<HashMap<String, String>> paidUserInfo = null;
	ArrayList<HashMap<String, String>> receiveUserInfo = null;

	MyAsynTaskManager myAsyncTask = null;
	String userId = "";
	
	Bundle fragbundle = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_item_wise_split);
		
		Intent itemwiseIntent = getIntent();			
		
		fragbundle = itemwiseIntent.getBundleExtra("itemwisebndle");
	
		currencyCodeString = fragbundle.getString("CurrencyCodeString");
		categorySelect = fragbundle.getString("CategorySelect");
		dateString = fragbundle.getString("DateString");
		transationTitle = fragbundle.getString("TransationTitle");
		transationAnount = fragbundle.getString("TransationAnount");
		addedPersonArrayList = fragbundle
				.getParcelableArrayList("PaidUserDetails");
		photoListArrayList = fragbundle
				.getParcelableArrayList("ReceiveUserDetails");
		
		
		topleftitemwiseText = (TextView) findViewById(R.id.topleftitemwiseText);
		toprightitemwisetext = (TextView) findViewById(R.id.toprightitemwisetext);
		additem_text = (TextView) findViewById(R.id.additem_text);
		horizontalvcrollviewphoto = (TwoWayView) findViewById(R.id.itemwisetwowayviewphoto);
		
		lvitemwiselist = (ListView) findViewById(R.id.lvitemwiselist);
		
		userId = PreferenceManager.getInstance().getUserId();
		intializeViews();
	}
private void intializeViews(){
	
	
	
	//twowayPersonModelList = new ArrayList<String>();
	
	twowayPersonModelList = new ArrayList<TwoWayPersonModel>();

	twowayPersonModelList.clear();
	twowayPersonModelList.add(new TwoWayPersonModel("","",""));
	//twowayPersonModelList.add("");
	itemwiseModelList = new ArrayList<ItemWiseListModel>();
	itemwiseModelList.clear();
	itemwiseModelList.add(new ItemWiseListModel("","",twowayPersonModelList));
	
	
	itemWiseListAdapter = new ItemWiseListAdapter(ItemWiseSplitActivity.this,
			R.layout.item_itemwise_layout, itemwiseModelList);
	lvitemwiselist.setAdapter(itemWiseListAdapter);
	
	itemWiseTwoWayViewAdapter = new ItemWiseTwoWayViewAdapter(ItemWiseSplitActivity.this,
			R.layout.item_twowayview_itemwise_layout, photoListArrayList);
	horizontalvcrollviewphoto.setAdapter(itemWiseTwoWayViewAdapter);
	

	//lvitemwiselist.setOnItemClickListener(itemclickListener);
	topleftitemwiseText.setOnClickListener(clickListener);
	toprightitemwisetext.setOnClickListener(clickListener);
	additem_text.setOnClickListener(clickListener);
	
	
	
}
private View.OnClickListener clickListener = new View.OnClickListener() {
	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.topleftitemwiseText:
			
			ItemWiseSplitActivity.this.onBackPressed();
			
			break;
		case R.id.toprightitemwisetext:
			
			if(taxSkip){
				
				getIntemsInfo();
				setPayerReceiverDetails();
				
				sendItemWiseSplitDetails();
				
			}else{
			
			showTaxAlertDialog();
			
			}
			
			break;
		case R.id.additem_text:
			
			if(itemWiseListAdapter.isLastFilled()){
				twowayPersonModelList.clear();
				twowayPersonModelList.add(new TwoWayPersonModel("","",""));
				//twowayPersonModelList.add("");
				itemwiseModelList.add(new ItemWiseListModel("","",twowayPersonModelList));
				itemWiseListAdapter.notifyDataSetChanged();
			}else{
				Toast.makeText(ItemWiseSplitActivity.this, "pls fill last item details", Toast.LENGTH_SHORT).show();
			}
			
			break;
		}
		
	}
};

public ItemWiseListAdapter getItemWiseAdater(){
	
	return itemWiseListAdapter;
}
public ItemWiseTwoWayViewAdapter getItemWiseTwoWayViewAdapter(){
	
	return itemWiseTwoWayViewAdapter;
}

protected void showTaxAlertDialog() {
	// TODO Auto-generated method stub

	Context context = this;
	final LinearLayout taxmainlinear;
	LinearLayout addtaxlinear;
	final EditText tipedittext;
	final EditText discountedittext;
	Button add_button;
	TextView donetext, skiptext;

	final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
			context);
	LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	View view = inflater.inflate(R.layout.activity_item_wise_taxes, null);
	alertDialogBuilder.setView(view);
	alertDialogBuilder.setCancelable(true);
	taxdialog = alertDialogBuilder.create();
	taxdialog.show();
	
	taxmainlinear = (LinearLayout) view.findViewById(R.id.taxmainlinear);
	addtaxlinear = (LinearLayout) view.findViewById(R.id.addtaxlinear);
	tipedittext = (EditText) view.findViewById(R.id.tipedittext);
	discountedittext = (EditText) view.findViewById(R.id.discountedittext);
	add_button = (Button) view.findViewById(R.id.add_button);
	skiptext = (TextView) view.findViewById(R.id.skiptext);
	donetext = (TextView) view.findViewById(R.id.donetext);
	
	add_button.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			editTextCount = editTextCount+1;
			
			createTaxLayout(taxmainlinear);
		}
	});
	
	donetext.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			getTaxLayout(taxmainlinear);
			
			if((tipedittext.getText().toString().trim()).length() > 0){
			HashMap<String, String> taxdetails = new HashMap<String, String>();
			taxdetails.put("type", "tip");
			taxdetails.put("amount", (tipedittext.getText().toString().trim()));
			
			taxdetailsInfo.add(taxdetails);
			}
			
			if((discountedittext.getText().toString().trim()).length() > 0){
			HashMap<String, String> taxdetails1 = new HashMap<String, String>();
			taxdetails1.put("type", "discount");
			taxdetails1.put("amount", (discountedittext.getText().toString().trim()));

			taxdetailsInfo.add(taxdetails1);
			
			getIntemsInfo();
			
			setPayerReceiverDetails();
			
			sendItemWiseSplitDetails();
			
			taxdialog.dismiss();
			}
		}
	});
	
	
	skiptext.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			taxSkip = true;
			taxdialog.dismiss();
		}
	});
}
private void createTaxLayout(LinearLayout taxmainlinear){
	LinearLayout childTaxLinear;
	LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	View dynamicView = inflater.inflate(R.layout.item_taxwise, null);
	final EditText extratax;
	TextView taxName;
	childTaxLinear = (LinearLayout) dynamicView;
	extratax = (EditText) dynamicView.findViewWithTag("taxedit1");
	taxName = (TextView) dynamicView.findViewWithTag("taxtext1");
	
	taxName.setTag("taxtext" +editTextCount);
	taxName.setText("Tax "+(editTextCount+1));
	
	extratax.setTag("taxedit" +editTextCount);
	
	LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
		     LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

		layoutParams.setMargins(0, 0, 0, 15);
		childTaxLinear.setLayoutParams(layoutParams);
	taxmainlinear.addView(childTaxLinear);
}
private void getTaxLayout(LinearLayout taxmainlinear){
	
	taxdetailsInfo = new ArrayList<HashMap<String, String>>();
		
	int childCount = taxmainlinear.getChildCount();
	HashMap<String, String> taxdetails = null;
	
	int i = 0;
	while (i < childCount) {
		
		LinearLayout linearparent = (LinearLayout) taxmainlinear.getChildAt(i);
		
		EditText taxamount = (EditText) linearparent.findViewWithTag("taxedit" + i);
		TextView taxname = (TextView) linearparent.findViewWithTag("taxtext" + i);
		
		taxdetails = new HashMap<String, String>();
		taxdetails.put("type", "tax");
		taxdetails.put("name", (taxname.getText().toString().trim()));
		taxdetails.put("amount",(taxamount.getText().toString().trim()));

		taxdetailsInfo.add(taxdetails);
		
		i++;
	}
	
}
private void getIntemsInfo(){
	ArrayList<HashMap<String, String>> sharedDetailsInfo = null;
	itemdetailsInfo = new ArrayList<HashMap<String, Object>>();
	
	HashMap<String, Object> itemdetails = null;
	HashMap<String, String> shareddetails = null;
	
	for(ItemWiseListModel itemWiseListObj : itemwiseModelList){
		sharedDetailsInfo = new ArrayList<HashMap<String, String>>();

		for(TwoWayPersonModel twoWayPersonObj : itemWiseListObj.getTwowayPersonList()){
			
			shareddetails = new HashMap<String, String>();
			
			shareddetails.put("user_type", BashApplication.userTypeHash.get(twoWayPersonObj.getUserType()));
			shareddetails.put("user_id", twoWayPersonObj.getId());
			
			sharedDetailsInfo.add(shareddetails);
		}
		
		itemdetails = new HashMap<String, Object>();
		
		itemdetails.put("item_name", itemWiseListObj.getPrsnName());
		itemdetails.put("item_cost", itemWiseListObj.getItemAmount());
		itemdetails.put("shared_among", sharedDetailsInfo);
		
		itemdetailsInfo.add(itemdetails);
	}	
}

private void setPayerReceiverDetails() {
	
	if(paidUserInfo == null){
		paidUserInfo = new ArrayList<HashMap<String, String>>();
	}else{
		paidUserInfo.clear();
	}
	if(receiveUserInfo == null){
		receiveUserInfo = new ArrayList<HashMap<String, String>>();
	}else{
		receiveUserInfo.clear();
	}

	HashMap<String, String> paiduser;
	HashMap<String, String> receiveuser;

	for (Person person : addedPersonArrayList) {
		float paidAmt = 0.0f;
		paiduser = new HashMap<String, String>();

		if (person.isPaidpersonbool()) {

			paidAmt = person.getPaidamount();

			paiduser.put("user_type", BashApplication.userTypeHash.get(person.getUserType()));
			paiduser.put("idpayer", person.getId());
			paiduser.put("paid_amount", "" + paidAmt);

			paidUserInfo.add(paiduser);
		}
	}

	for (PhotosListModel photoListObj : photoListArrayList) {
		receiveuser = new HashMap<String, String>();
		receiveuser.put("user_type",
				BashApplication.userTypeHash.get(photoListObj.getUserType()));
		receiveuser.put("idreceiver", photoListObj.getID());

		receiveUserInfo.add(receiveuser);

	}

}

private void sendItemWiseSplitDetails() {

	myAsyncTask = new MyAsynTaskManager();
	myAsyncTask.delegate = this;
	
	if(taxSkip){
		myAsyncTask.setupParamsAndUrl("ItemSplitTransaction",
				this, AppUrlList.ACTION_URL, new String[] { "module",
						"action", "entered_by_iduser", "transaction_currency",
						"transaction_amount", "transaction_category",
						"transaction_date", "transaction_title",
						"payers_information", "receivers_information","items_information"},
				new String[] { "transaction", "insertItemSplitTransaction",
						userId, currencyCodeString, transationAnount,
						"" + categorySelect, dateString, transationTitle,
						paidUserInfo.toString(), receiveUserInfo.toString(),itemdetailsInfo.toString()});
		
	}
	else{
		myAsyncTask.setupParamsAndUrl("ItemSplitTransaction",
				this, AppUrlList.ACTION_URL, new String[] { "module",
						"action", "entered_by_iduser", "transaction_currency",
						"transaction_amount", "transaction_category",
						"transaction_date", "transaction_title",
						"payers_information", "receivers_information","items_information","extra_payment_information"},
				new String[] { "transaction", "insertItemSplitTransaction",
						userId, currencyCodeString, transationAnount,
						"" + categorySelect, dateString, transationTitle,
						paidUserInfo.toString(), receiveUserInfo.toString(),itemdetailsInfo.toString(),taxdetailsInfo.toString()});
		
	}
	myAsyncTask.execute();

}

@Override
public void backgroundProcessFinish(String from, String output) {
	// TODO Auto-generated method stub
	if (from.equalsIgnoreCase("ItemSplitTransaction")) {

		if (output != null && output.length() > 0) {
			JSONObject baseJsonObject;
			try {
				baseJsonObject = new JSONObject(output);
				boolean result = baseJsonObject.getBoolean("result");
				String msgStr = baseJsonObject.getString("msg");
				if (result) {
					
					Toast.makeText(ItemWiseSplitActivity.this, msgStr,
							Toast.LENGTH_SHORT).show();
					
					Intent socialIntent = new Intent(ItemWiseSplitActivity.this, SocialPageActivity.class);
					socialIntent.putExtra("transationdetails", fragbundle);
					socialIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(socialIntent);
					
				} else {
					Toast.makeText(ItemWiseSplitActivity.this, msgStr,
							Toast.LENGTH_SHORT).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	}
}

}
