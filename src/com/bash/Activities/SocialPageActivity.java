package com.bash.Activities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.lucasr.twowayview.TwoWayView;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.bash.R;
import com.bash.Adapters.GalleryAdapter;
import com.bash.ListModels.CustomGallery;
import com.bash.ListModels.PhotosListModel;
import com.bash.Managers.Action;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton.UserInfoChangedCallback;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

public class SocialPageActivity extends Activity {
	
	/* Shared preference keys */
	private static final String PREF_NAME = "sample_twitter_pref";
	private static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
	private static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
	private static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";
	private static final String PREF_USER_NAME = "twitter_user_name";
	
	/* Any number for uniquely distinguish your request */
	public static final int WEBVIEW_REQUEST_CODE = 100;
	
	TextView skipText ;
	EditText CommentEdtText;
	Button shareBtn;
	ImageView ivBash, ivFacebook, ivTwitter,ivAddPhotos;
	
	private ProgressDialog pDialog;

	private String consumerKey = "";
	private String consumerSecret = "";
	private String callbackUrl = "";
	private String oAuthVerifier = "";
	
	private static Twitter twitter;
	private static RequestToken requestToken;
	
	private static SharedPreferences mSharedPreferences;
	
	private boolean bashClickbool = true;
	private boolean fbClickbool = true;
	private boolean twitterClickbool = false;
	
	TwoWayView twViewPhotos;
	ArrayList<PhotosListModel> photosList;
	//TwoWayViewSocialAdapter socialPhotoAdapter;
	
	Handler handler;
	GalleryAdapter adapter;

	ImageView imgSinglePick;
	Button btnGalleryPick;
	Button btnGalleryPickMul;

	String action;
	ViewSwitcher viewSwitcher;
	ImageLoader imageLoader;
	ArrayList<CustomGallery> dataT=null;
	MyAsynTaskManager myAsyncTask;
	String fbPhotoAddress = null;
	
	private UiLifecycleHelper uiHelper;

	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");

	private static String message = "Sample status posted from android app";

	
	private String status = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(this, statusCallback);
		uiHelper.onCreate(savedInstanceState);
		
		setContentView(R.layout.add_social);
		
		/* Enabling strict mode */
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
		skipText = (TextView) findViewById(R.id.toprightsocialtext);
		CommentEdtText = (EditText) findViewById(R.id.commentsedit);
		ivBash = (ImageView) findViewById(R.id.bashsocial);
		ivFacebook = (ImageView) findViewById(R.id.facebooksocial);
		ivTwitter = (ImageView) findViewById(R.id.twittersocial);
		ivAddPhotos = (ImageView) findViewById(R.id.addphotosocial);
		shareBtn = (Button) findViewById(R.id.socialshare);
		twViewPhotos = (TwoWayView) findViewById(R.id.tvphotosocial);
		
		
		initImageLoader();
		initializeViews();
	}
private void initializeViews(){
	initTwitterConfigs();
	
	/* Check if required twitter keys are set */
	if (TextUtils.isEmpty(consumerKey) || TextUtils.isEmpty(consumerSecret)) {
		Toast.makeText(this, "Twitter key and secret not configured",
				Toast.LENGTH_SHORT).show();
		return;
	}
	
	mSharedPreferences = getSharedPreferences(PREF_NAME, 0);
	boolean isLoggedIn = mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
	firstLoginSatus(isLoggedIn);
	
	 dataT = new ArrayList<CustomGallery>();
	
	skipText.setOnClickListener(clickListener);
	ivBash.setOnClickListener(clickListener);
	ivFacebook.setOnClickListener(clickListener);
	ivTwitter.setOnClickListener(clickListener);
	ivAddPhotos.setOnClickListener(clickListener);
	shareBtn.setOnClickListener(clickListener);
	
	handler = new Handler();
	adapter = new GalleryAdapter(getApplicationContext(), imageLoader);
	adapter.setMultiplePick(false);
	twViewPhotos.setAdapter(adapter);
}
private void initImageLoader() {
	DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
			.cacheOnDisc().imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
			.bitmapConfig(Bitmap.Config.RGB_565).build();
	ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(
			this).defaultDisplayImageOptions(defaultOptions).memoryCache(
			new WeakMemoryCache());

	ImageLoaderConfiguration config = builder.build();
	imageLoader = ImageLoader.getInstance();
	imageLoader.init(config);
}
private void initTwitterConfigs() {
	consumerKey = getString(R.string.twitter_consumer_key);
	consumerSecret = getString(R.string.twitter_consumer_secret);
	callbackUrl = getString(R.string.twitter_callback);
	oAuthVerifier = getString(R.string.twitter_oauth_verifier);
}


private View.OnClickListener clickListener = new View.OnClickListener() {
	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.toprightsocialtext:
			
			Intent backHome = new Intent(SocialPageActivity.this,HomeActivity.class);
			backHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(backHome);
			finish();
			
			
			Toast.makeText(SocialPageActivity.this, "Skip", Toast.LENGTH_SHORT).show();
			
			break;
		case R.id.bashsocial:
			Toast.makeText(SocialPageActivity.this, "Bash", Toast.LENGTH_SHORT).show();
			
			break;
		case R.id.facebooksocial:
			if(!fbClickbool){
				ivFacebook.setImageResource(R.drawable.fb_tick_log);	
				fbClickbool = true;
			}else{
				ivFacebook.setImageResource(R.drawable.fb_icon_s);
				fbClickbool = false;
			}
			
				Toast.makeText(SocialPageActivity.this, "Facebook", Toast.LENGTH_SHORT).show();
			
			break;
		case R.id.twittersocial:
			if(!twitterClickbool){
				ivTwitter.setImageResource(R.drawable.twit_tick_log);	
				twitterClickbool = true;
				loginToTwitter();
			}else{
				ivTwitter.setImageResource(R.drawable.twitter_icon_s);
				twitterClickbool = false;
			}
							
			Toast.makeText(SocialPageActivity.this, "twitter", Toast.LENGTH_SHORT).show();
		
		break;
		case R.id.addphotosocial:
			Intent i = new Intent(Action.ACTION_MULTIPLE_PICK);
			startActivityForResult(i, 200);
			
			ivAddPhotos.setVisibility(View.GONE);
			twViewPhotos.setVisibility(View.VISIBLE);
			
			Toast.makeText(SocialPageActivity.this, "add photos", Toast.LENGTH_SHORT).show();
		
		break;
		case R.id.socialshare:
			status = CommentEdtText.getText().toString();
			
			if (status.trim().length() > 0) {
				
				if(fbClickbool){
					if(PreferenceManager.getInstance().getUserFacebookId() == null){
						
						faceBookLogin();
			
						
					}else{
						postImage();
						postStatusMessage();
						Toast.makeText(SocialPageActivity.this, "facebook already login!!", Toast.LENGTH_SHORT).show();
					}
						
					}else{
						Toast.makeText(SocialPageActivity.this, "facebook login failure!!", Toast.LENGTH_SHORT).show();
					}
				
				
				
				
				if(twitterClickbool){
				new updateTwitterStatus().execute(status);
				}else{
					Toast.makeText(SocialPageActivity.this, "Twitter login failure!!", Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(SocialPageActivity.this, "Message is empty!!", Toast.LENGTH_SHORT).show();
			}
			Toast.makeText(SocialPageActivity.this, "socialshare", Toast.LENGTH_SHORT).show();
		
		break;
		}
		
	}
};
private void saveTwitterInfo(AccessToken accessToken) {
	
	long userID = accessToken.getUserId();
	
	User user;
	try {
		user = twitter.showUser(userID);
	
		String username = user.getName();

		/* Storing oAuth tokens to shared preferences */
		Editor e = mSharedPreferences.edit();
		e.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
		e.putString(PREF_KEY_OAUTH_SECRET, accessToken.getTokenSecret());
		e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
		e.putString(PREF_USER_NAME, username);
		e.commit();

	} catch (TwitterException e1) {
		e1.printStackTrace();
		twitterClickbool = false;
	}
}
private void loginToTwitter() {
	boolean isLoggedIn = mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
	
	if (!isLoggedIn) {
		final ConfigurationBuilder builder = new ConfigurationBuilder();
		builder.setOAuthConsumerKey(consumerKey);
		builder.setOAuthConsumerSecret(consumerSecret);

		final Configuration configuration = builder.build();
		final TwitterFactory factory = new TwitterFactory(configuration);
		twitter = factory.getInstance();

		try {
			requestToken = twitter.getOAuthRequestToken(callbackUrl);

			/**
			 *  Loading twitter login page on webview for authorization 
			 *  Once authorized, results are received at onActivityResult
			 *  */
			final Intent intent = new Intent(this, WebViewActivity.class);
			intent.putExtra(WebViewActivity.EXTRA_URL, requestToken.getAuthenticationURL());
			startActivityForResult(intent, WEBVIEW_REQUEST_CODE);
			
		} catch (TwitterException e) {
			e.printStackTrace();
			twitterClickbool = false;
		}
	} else {
		
	}
}
@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {

	if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
		String[] all_path = data.getStringArrayExtra("all_path");

		dataT.clear();

		for (String string : all_path) {
			CustomGallery item = new CustomGallery();
			item.sdcardPath = string;

			dataT.add(item);
		}

		//viewSwitcher.setDisplayedChild(0);
		adapter.addAll(dataT);
	}
	else if (requestCode == WEBVIEW_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
		String verifier = data.getExtras().getString(oAuthVerifier);
		try {
			AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);

			long userID = accessToken.getUserId();
			final User user = twitter.showUser(userID);
			String username = user.getName();
			
			saveTwitterInfo(accessToken);

		} catch (Exception e) {
			Log.e("Twitter Login Failed", e.getMessage());
			twitterClickbool = false;
		}
	}

	super.onActivityResult(requestCode, resultCode, data);
}

private void firstLoginSatus(boolean isLoggedIn){
	
	if (isLoggedIn) {



	} else {

		Uri uri = getIntent().getData();
		
		if (uri != null && uri.toString().startsWith(callbackUrl)) {
		
			String verifier = uri.getQueryParameter(oAuthVerifier);

			try {
				
				/* Getting oAuth authentication token */
				AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);

				/* Getting user id form access token */
				long userID = accessToken.getUserId();
				final User user = twitter.showUser(userID);
				final String username = user.getName();

				/* save updated token */
				saveTwitterInfo(accessToken);
				
			} catch (Exception e) {
				Log.e("Failed to login Twitter!!", e.getMessage());
				twitterClickbool = false;
			}
		}

	}
	
}
class updateTwitterStatus extends AsyncTask<String, String, Void> {
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		pDialog = new ProgressDialog(SocialPageActivity.this);
		pDialog.setMessage("Posting to twitter...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		pDialog.show();
	}

	protected Void doInBackground(String... args) {

		String status = args[0];
		try {
			ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setOAuthConsumerKey(consumerKey);
			builder.setOAuthConsumerSecret(consumerSecret);
			
			// Access Token
			String access_token = mSharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, "");
			// Access Token Secret
			String access_token_secret = mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, "");

			AccessToken accessToken = new AccessToken(access_token, access_token_secret);
			Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

			// Update status
			for(CustomGallery customGallery:dataT){
				StatusUpdate statusUpdate = new StatusUpdate(status);
				File theFile = new File(customGallery.sdcardPath);
				InputStream is = null;
				try {
					 is = new FileInputStream(theFile);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				statusUpdate.setMedia("test1.jpg", is);
				
				twitter4j.Status response = twitter.updateStatus(statusUpdate);
				Log.d("Status", response.getText());
			}
			
		

			
			
		} catch (TwitterException e) {
			Log.d("Failed to post!", e.getMessage());
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		
		/* Dismiss the progress dialog after sharing */
		pDialog.dismiss();
		
		Toast.makeText(SocialPageActivity.this, "Posted to Twitter!", Toast.LENGTH_SHORT).show();

		// Clearing EditText field
		CommentEdtText.setText("");
		dataT.clear();
		adapter.notifyDataSetChanged();
	}

}


@Override
public void onBackPressed() {
	// TODO Auto-generated method stub
	//super.onBackPressed();
	Intent backHome = new Intent(SocialPageActivity.this,HomeActivity.class);
	backHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	startActivity(backHome);
	finish();
	
}
private void faceBookLogin(){
	
	new UserInfoChangedCallback() {
		@Override
		public void onUserInfoFetched(GraphUser user) {
			if (user != null) {
				//userName.setText("Hello, " + user.getName());
				Toast.makeText(SocialPageActivity.this,
						"Hello, " + user.getName(),
						Toast.LENGTH_LONG).show();
				PreferenceManager.getInstance().setUserFacebookId(user.getId());  
			} else {
				Toast.makeText(SocialPageActivity.this,
						"You are not logged",
						Toast.LENGTH_LONG).show();
			//	userName.setText("You are not logged");
			}
		}
	};
}


private Session.StatusCallback statusCallback = new Session.StatusCallback() {
	@Override
	public void call(Session session, SessionState state,
			Exception exception) {
		if (state.isOpened()) {
			Log.d("FacebookSampleActivity", "Facebook session opened");
		} else if (state.isClosed()) {
			Log.d("FacebookSampleActivity", "Facebook session closed");
		}
	}
};
public void postStatusMessage() {
	if (checkPermissions()) {
		Request request = Request.newStatusUpdateRequest(
				Session.getActiveSession(), message,
				new Request.Callback() {
					@Override
					public void onCompleted(Response response) {
						if (response.getError() == null)
							Toast.makeText(SocialPageActivity.this,
									"Status updated successfully",
									Toast.LENGTH_LONG).show();
					}
				});
		request.executeAsync();
	} else {
		requestPermissions();
	}
}



private void postImage() {
//	final BitmapFactory.Options options = new BitmapFactory.Options();
//	options.inSampleSize = 8;
//	Bitmap myBitmap = null;
//		for(CustomGallery customGallery:dataT){
//		
//		File imgFile = new  File(customGallery.sdcardPath);
//		 
//		if(imgFile.exists()){
//			
//		    myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(),options);
//		}
//	
//		uploadImageFb(myBitmap);		
//		
//		 if(myBitmap!=null)
//		   {
//			 myBitmap.recycle();
//			 myBitmap=null;
//		    }
//		}
//		
		
		if (checkPermissions()) {
			Bitmap img = BitmapFactory.decodeResource(getResources(),
					R.drawable.ic_launcher);
			Request uploadRequest = Request.newUploadPhotoRequest(
					Session.getActiveSession(), img, new Request.Callback() {
						@Override
						public void onCompleted(Response response) {
							Toast.makeText(SocialPageActivity.this,
									"Photo uploaded successfully",
									Toast.LENGTH_LONG).show();
						}
					});
			uploadRequest.executeAsync();
		} else {
			requestPermissions();
		}
		
		
		
		
		
		
}
public boolean checkPermissions() {
	Session s = Session.getActiveSession();
	if (s != null) {
		return s.getPermissions().contains("publish_actions");
	} else
		return false;
}

public void requestPermissions() {
	Session s = Session.getActiveSession();
	if (s != null)
		s.requestNewPublishPermissions(new Session.NewPermissionsRequest(
				this, PERMISSIONS));
}





}
