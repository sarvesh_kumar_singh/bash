package com.bash.Activities;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.bash.R;
import com.bash.Managers.ActivityManager;

public class SignUpStepTwo_C_Activity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup_steptwo_c);
		
		((Button)findViewById(R.id.sendSmsButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//sendSMSMessage();
				checkSIMAvailabilityAndSendSMS();
			}
		});
		
		((Button)findViewById(R.id.skipStepButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ActivityManager.startActivity(SignUpStepTwo_C_Activity.this, SignUpStepThreeActivity.class);
				finish();
			}
		});
		
		
	}
	
/*	protected void sendSMSMessage() {
	      String phoneNo = "56767";
	      String message = "ZIP RESET";
	      try {
	         SmsManager smsManager = SmsManager.getDefault();
	         smsManager.sendTextMessage(phoneNo, null, message, null, null);
	         Toast.makeText(getApplicationContext(), "Sending SMS...", Toast.LENGTH_LONG).show();
	         Toast.makeText(getApplicationContext(), "Sending Sent.", Toast.LENGTH_SHORT).show();
	      } catch (Exception e) {
	         Toast.makeText(getApplicationContext(),"SMS faild, please try again.", Toast.LENGTH_LONG).show();
	         e.printStackTrace();
	      }
	   }
	*/
	
	public void checkSIMAvailabilityAndSendSMS() {
		TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
	    int simState = telMgr.getSimState();
	            switch (simState) {
	                case TelephonyManager.SIM_STATE_ABSENT:
	                	Toast.makeText(getApplicationContext(), "Sim State Absent!", Toast.LENGTH_SHORT).show();
	                    break;
	                case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
	                	Toast.makeText(getApplicationContext(), "Sim Network Locked!", Toast.LENGTH_SHORT).show();
	                    break;
	                case TelephonyManager.SIM_STATE_PIN_REQUIRED:
	                	Toast.makeText(getApplicationContext(), "Sim Pin Required!", Toast.LENGTH_SHORT).show();
	                    break;
	                case TelephonyManager.SIM_STATE_PUK_REQUIRED:
	                	Toast.makeText(getApplicationContext(), "Sim Puk Required!", Toast.LENGTH_SHORT).show();
	                    break;
	                case TelephonyManager.SIM_STATE_READY:
	                	sendSMS();
	                	Toast.makeText(getApplicationContext(), "Sim Puk Required!", Toast.LENGTH_SHORT).show();
	                    break;
	                case TelephonyManager.SIM_STATE_UNKNOWN:
	                	Toast.makeText(getApplicationContext(), "Sim State Unknown!", Toast.LENGTH_SHORT).show();
	                    break;
	            }
	}
	
	void sendSMS()
    {      
		Toast.makeText(getApplicationContext(), "Sending SMS...", Toast.LENGTH_SHORT).show();
	            
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
            new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
            new Intent(DELIVERED), 0);

        //---when the SMS has been sent---
        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS sent", 
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure", 
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service", 
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU", 
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off", 
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        //---when the SMS has been delivered---
        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered", 
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered", 
                                Toast.LENGTH_SHORT).show();
                        break;                        
                }
            }
        }, new IntentFilter(DELIVERED));        

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage("56767", null, "ZIP RESET", sentPI, deliveredPI);        
    }
	 
}

