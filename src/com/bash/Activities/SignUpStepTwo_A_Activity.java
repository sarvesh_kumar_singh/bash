package com.bash.Activities;
import java.io.ByteArrayOutputStream;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Bitmap.CompressFormat;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.bash.R;
import com.bash.Managers.ActivityManager;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;
public class SignUpStepTwo_A_Activity extends Activity implements AsyncResponse
{
	String imageBase64String;
	String selectedImage_path;
	String filePath="";
	String FILE_NAME="";
	Uri selectedImage;
	MyAsynTaskManager  myAsyncTask;
	private static final int PICK_FROM_FILE = 3;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// To hide Title Bar and Enable Full Screen
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	    //for Portrait mode only
	    setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    //To Hide Title Bar Only
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup_steptwo_a);
		((Button)findViewById(R.id.continueButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v)
			{
				if(ValidateForm()){
					ProcessUserMobilerNumber();
				}
			}
		});
		((ImageView)findViewById(R.id.imageView1)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
		//Intent.createChooser(intent, "Complete action using")
				startActivityForResult(
						Intent.createChooser(intent, "Complete action using"),PICK_FROM_FILE);
			}
		});
	}
	public boolean ValidateForm()
	{
		
		  if (TextUtils.isEmpty(((EditText) findViewById(R.id.mobileNumberText)).getText().toString())){
			((EditText) findViewById(R.id.mobileNumberText)).setError("Field is Empty");
			return false;
		  }else if(((EditText) findViewById(R.id.mobileNumberText)).length() != 10){
			  Toast.makeText(getApplicationContext(), "Please Enter 10 digit valid mobile number!", Toast.LENGTH_SHORT).show();
			  return false;
		  }
		  
		return true;
	}
	public void ProcessUserMobilerNumber() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl
		("ProcessUserMobilerNumber",SignUpStepTwo_A_Activity.this,AppUrlList.ACTION_URL, new String[]
				{
				"module",
				"action",
				"iduser",
				"mobile"
				 },
			new String[]
				{
				"user",
				"zipregister",
				PreferenceManager.getInstance().getTempUserId(),
				((EditText) findViewById(R.id.mobileNumberText)).getText().toString()
				}
		);
myAsyncTask.execute();
	}

	public void ProcessUserImage() 
	{
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		Log.e("User Id", PreferenceManager.getInstance().getTempUserId());
		myAsyncTask.setupParamsAndUrl("ProcessUserImage",SignUpStepTwo_A_Activity.this,AppUrlList.ACTION_URL, new String[]{
				"module",
				"action",
				"iduser",
				"phone_no",
				"image"
				 },
			new String[]{
				"user",
				"registersteptwo",
				PreferenceManager.getInstance().getTempUserId(),
				((EditText) findViewById(R.id.mobileNumberText)).getText().toString(),
				imageBase64String
				});
myAsyncTask.execute();
	}
	/**
	 * Get a file path from a Uri. This will get the the path for Storage Access
	 * Framework Documents, as well as the _data field for the MediaStore and
	 * other file-based ContentProviders.
	 *
	 * @param context The context.
	 * @param uri The Uri to query.
	 * @author paulburke
	 */
	@SuppressLint("NewApi")
	public static String getPath(final Context context, final Uri uri) {

	    final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

	    // DocumentProvider
	    if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
	        // ExternalStorageProvider
	        if (isExternalStorageDocument(uri)) {
	            final String docId = DocumentsContract.getDocumentId(uri);
	            final String[] split = docId.split(":");
	            final String type = split[0];

	            if ("primary".equalsIgnoreCase(type)) {
	                return Environment.getExternalStorageDirectory() + "/" + split[1];
	            }

	            // TODO handle non-primary volumes
	        }
	        // DownloadsProvider
	        else if (isDownloadsDocument(uri)) {

	            final String id = DocumentsContract.getDocumentId(uri);
	            final Uri contentUri = ContentUris.withAppendedId(
	                    Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

	            return getDataColumn(context, contentUri, null, null);
	        }
	        // MediaProvider
	        else if (isMediaDocument(uri)) {
	            final String docId = DocumentsContract.getDocumentId(uri);
	            final String[] split = docId.split(":");
	            final String type = split[0];

	            Uri contentUri = null;
	            if ("image".equals(type)) {
	                contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
	            } else if ("video".equals(type)) {
	                contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
	            } else if ("audio".equals(type)) {
	                contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
	            }

	            final String selection = "_id=?";
	            final String[] selectionArgs = new String[] {
	                    split[1]
	            };

	            return getDataColumn(context, contentUri, selection, selectionArgs);
	        }
	    }
	    // MediaStore (and general)
	    else if ("content".equalsIgnoreCase(uri.getScheme())) {
	        return getDataColumn(context, uri, null, null);
	    }
	    // File
	    else if ("file".equalsIgnoreCase(uri.getScheme())) {
	        return uri.getPath();
	    }

	    return null;
	}

	/**
	 * Get the value of the data column for this Uri. This is useful for
	 * MediaStore Uris, and other file-based ContentProviders.
	 *
	 * @param context The context.
	 * @param uri The Uri to query.
	 * @param selection (Optional) Filter used in the query.
	 * @param selectionArgs (Optional) Selection arguments used in the query.
	 * @return The value of the _data column, which is typically a file path.
	 */
	public static String getDataColumn(Context context, Uri uri, String selection,
	        String[] selectionArgs) {

	    Cursor cursor = null;
	    final String column = "_data";
	    final String[] projection = {
	            column
	    };

	    try {
	        cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
	                null);
	        if (cursor != null && cursor.moveToFirst()) {
	            final int column_index = cursor.getColumnIndexOrThrow(column);
	            return cursor.getString(column_index);
	        }
	    } finally {
	        if (cursor != null)
	            cursor.close();
	    }
	    return null;
	}


	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is ExternalStorageProvider.
	 */
	public static boolean isExternalStorageDocument(Uri uri) {
	    return "com.android.externalstorage.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is DownloadsProvider.
	 */
	public static boolean isDownloadsDocument(Uri uri) {
	    return "com.android.providers.downloads.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is MediaProvider.
	 */
	public static boolean isMediaDocument(Uri uri) {
	    return "com.android.providers.media.documents".equals(uri.getAuthority());
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK && requestCode == PICK_FROM_FILE) 
		{
			Bitmap myBitmap = null;
			Uri selectedImage = data.getData();
			selectedImage_path = getPath(SignUpStepTwo_A_Activity.this, selectedImage);
			if (selectedImage_path.contains("http")) {
				final AjaxCallback<Bitmap> cb = new AjaxCallback<Bitmap>() 
						{
					@SuppressWarnings("deprecation")
					@Override
					public void callback(String url, Bitmap bm,
							AjaxStatus status) 
					{
						
						ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
						bm.compress(CompressFormat.JPEG, 100, outputStream);
						byte[] byteArray = outputStream.toByteArray();
						/*
						 * Bitmap.createBitmap(306, 306,
						 * Bitmap.Config.ARGB_8888);
						 */
						imageBase64String = Base64.encodeToString(byteArray,
								Base64.DEFAULT);
						// Log.i("Deepr", "Image Returned" + imageBase64String);
					}
				};
				final AQuery aq = new AQuery(SignUpStepTwo_A_Activity.this);
				aq.ajax(selectedImage_path, Bitmap.class, 0, cb);

			} else {
				Bitmap p_image = BitmapFactory.decodeFile(selectedImage_path);
				try {
					ExifInterface exif = new ExifInterface(selectedImage_path);
					int orientation = exif.getAttributeInt(
							ExifInterface.TAG_ORIENTATION, 1);
					Log.d("EXIF", "Exif: " + orientation);
					Matrix matrix = new Matrix();
					if (orientation == 6) {
						matrix.postRotate(90);
					} else if (orientation == 3) {
						matrix.postRotate(180);
					} else if (orientation == 8) {
						matrix.postRotate(270);
					}
					myBitmap = Bitmap.createBitmap(p_image, 0, 0,
							p_image.getWidth(), p_image.getHeight(), matrix,
							true); // rotating bitmap
					/*Bitmap bitmap = Bitmap.createScaledBitmap(myBitmap, 300,
							300, false);*/
					ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
					myBitmap.compress(CompressFormat.JPEG, 100, outputStream);
					byte[] byteArray = outputStream.toByteArray();
					imageBase64String = Base64.encodeToString(byteArray,
							Base64.DEFAULT);
				} catch (Exception e) {

				}

			}

		}

	}
	@Override
	public void backgroundProcessFinish(String from, String output)
	{
		if(from.equalsIgnoreCase("ProcessUserMobilerNumber"))
		{
			if(output!=null)
			{
				
				try {
					JSONObject rootObj = new JSONObject(output);
					if(rootObj.getBoolean("result"))
					{
						PreferenceManager.getInstance().setUserMobileNumber("91"+((EditText) findViewById(R.id.mobileNumberText)).getText().toString());
						if(rootObj.getString("msg").equals("Successfully created account! You will receive your PIN by SMS")) {
							ActivityManager.startActivity(SignUpStepTwo_A_Activity.this, SignUpStepTwo_B_Activity.class);
							finish();
						}else if(rootObj.getString("msg").contains("success")){
							ActivityManager.startActivity(SignUpStepTwo_A_Activity.this, SignUpStepTwo_B_Activity.class);
							finish();
						}
						else if(rootObj.getString("msg").equals("already exist")){
							ActivityManager.startActivity(SignUpStepTwo_A_Activity.this, SignUpStepTwo_C_Activity.class);
							finish();
						}
					}
					else
					{
						DialogManager.showDialog(SignUpStepTwo_A_Activity.this, rootObj.getString("msg"));
					}
				} catch (Exception e) {
					e.printStackTrace();
					DialogManager.showDialog(SignUpStepTwo_A_Activity.this, "Server Error Occured! Try Again!");
				}
			}
			else
			{
				DialogManager.showDialog(SignUpStepTwo_A_Activity.this, "Server Error Occured! Try Again!");
			}

		}

	}
}

