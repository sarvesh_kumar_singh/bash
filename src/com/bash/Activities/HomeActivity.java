package com.bash.Activities;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Adapters.FriendGroupAdapter;
import com.bash.Adapters.SlidingMenuAdapter;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.ListModels.MyTrans_FriendsGroups_Class;
import com.bash.ListModels.SlidingMenu_Class;
import com.bash.Managers.ActivityManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.ContainerProvider;
import com.bash.Utils.AppConstants;
import com.navigation_listview.utills.SlidingMenuLayout;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.sromku.simple.fb.SimpleFacebook;

public class HomeActivity extends FragmentActivity {
	// public static SlidingMenu menu;
	private FragmentTabHost fragmenthost;
	private SimpleFacebook mSimpleFacebook;
	public SlidingMenuAdapter menuAdapter;
	public static SlidingMenuLayout slidingmenu_layout;
	private ListView slidingListView;
	public ArrayList<SlidingMenu_Class> menuList = new ArrayList<SlidingMenu_Class>();

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		// setBehindContentView(R.layout.slidemenu_layout);
		slidingmenu_layout = (SlidingMenuLayout) this.getLayoutInflater()
				.inflate(R.layout.activity_main, null);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(slidingmenu_layout);
		// setContentView(R.layout.activity_home);

		initializeSlidingMenu();
		initializeFragmentTabHost();
		initializeActivityViews();
		// initializeListValues();

		mSimpleFacebook = SimpleFacebook.getInstance(this);

		// Setting Phone Config...
		if (!PreferenceManager.getInstance().isPhoneConfigFixed()) {
			PreferenceManager.getInstance().setPhoneConfiguration(
					getWindowManager().getDefaultDisplay().getWidth(),
					getWindowManager().getDefaultDisplay().getHeight());
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mSimpleFacebook = SimpleFacebook.getInstance(this);
	}

	private void initializeFragmentTabHost() {

		// TODO Auto-generated method stub
		fragmenthost = (FragmentTabHost) findViewById(R.id.tabhost);

		fragmenthost.setup(this, getSupportFragmentManager(),
				R.id.frames_holder);
		fragmenthost.addTab(
				fragmenthost.newTabSpec(AppConstants.HOME_FRAGMENT_TAG)
						.setIndicator(""),
				ContainerProvider.Home_Container.class, null);

		fragmenthost.addTab(fragmenthost.newTabSpec("Newsfeed")
				.setIndicator(""),
				ContainerProvider.Newsfeed_Container.class, null);

		fragmenthost.addTab(fragmenthost.newTabSpec("Notifications")
				.setIndicator(""), ContainerProvider.Notification_Container.class,
				null);

		fragmenthost.addTab(fragmenthost.newTabSpec("Wallet").setIndicator(""),
				ContainerProvider.MyWallet_Container.class, null);

		fragmenthost.addTab(
				fragmenthost.newTabSpec("Friends").setIndicator(""),
				ContainerProvider.MyFriends_Container.class, null);

		fragmenthost.addTab(fragmenthost.newTabSpec("Group").setIndicator(""),
				ContainerProvider.MyGroups_Container.class, null);

		fragmenthost.addTab(fragmenthost.newTabSpec("Settings")
				.setIndicator(""), ContainerProvider.MyCards_Container.class,
				null);

		fragmenthost.addTab(
				fragmenthost.newTabSpec("Support").setIndicator(""),
				ContainerProvider.Privacy_Container.class, null);

		/*
		 * fragmenthost.addTab( fragmenthost.newTabSpec(
		 * AppConstants.MY_FRIENDS_AND_GROUPSFRAGMENT_TAG) .setIndicator(""),
		 * ContainerProvider.MyFriendsAndGroups_Container.class, null);
		 * 
		 * fragmenthost.addTab(
		 * fragmenthost.newTabSpec(AppConstants.INVITEPEOPLE_FRAGMENT_TAG)
		 * .setIndicator(""), ContainerProvider.InvitePeople_Container.class,
		 * null);
		 * 
		 * fragmenthost.addTab(
		 * fragmenthost.newTabSpec(AppConstants.SUPPORT_FRAGMENT_TAG)
		 * .setIndicator(""), ContainerProvider.InvitePeople_Container.class,
		 * null);
		 */

		/*
		 * fragmenthost.addTab(fragmenthost.newTabSpec(AppConstants.
		 * CASH_IN_OUT_FRAGMENT_TAG).setIndicator(""),
		 * ContainerProvider.Cash_In_Out_Container.class, null);
		 */
		/*
		 * fragmenthost.addTab(fragmenthost.newTabSpec(AppConstants.
		 * SLIDING_SESSION_CONTAINER).setIndicator(""), session_container.class,
		 * null); fragmenthost.addTab(fragmenthost.newTabSpec(AppConstants.
		 * SLIDING_MYACCOUNT_CONTAINER).setIndicator(""),
		 * myaccount_container.class, null);
		 * fragmenthost.addTab(fragmenthost.newTabSpec
		 * (AppConstants.SLIDING_SETTINGS_CONTAINER).setIndicator(""),
		 * settings_container.class, null);
		 * fragmenthost.addTab(fragmenthost.newTabSpec
		 * (AppConstants.SLIDING_STUDENTINSESSION_CONTAINER).setIndicator(""),
		 * studentinsession_container.class, null);
		 */
	}

	private void initializeActivityViews() {

		((ImageView) findViewById(R.id.topleftsideImage))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// menu.toggle();
						slidingmenu_layout.toggleMenu();
					}
				});
		
		((ImageView) findViewById(R.id.topleftsidebackImage))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				HomeActivity.this.onBackPressed();
				
			}
		});
	}

	public void setUpTopBarFields(int leftSideImage, String middleText,
			int rightSideImage) {
		
		((ImageView) findViewById(R.id.topleftsideImage)).setVisibility(View.VISIBLE);
		((ImageView) findViewById(R.id.toprightsideImage)).setVisibility(View.VISIBLE);
		((TextView) findViewById(R.id.topmiddleText)).setVisibility(View.VISIBLE);
		
		((ImageView) findViewById(R.id.topleftsidebackImage)).setVisibility(View.GONE);
		((TextView) findViewById(R.id.topleftsidecontinueText)).setVisibility(View.GONE);
		
		
		((ImageView) findViewById(R.id.topleftsideImage))
				.setImageResource(leftSideImage);
		((ImageView) findViewById(R.id.toprightsideImage))
				.setImageResource(rightSideImage);
		((TextView) findViewById(R.id.topmiddleText)).setText(middleText);
		/*
		 * ((TextView) findViewById(R.id.topmiddleText)).setText(Html
		 * .fromHtml("&#8377;"));
		 */
	}

	public void initializeSlidingMenu() {
		// TODO Auto-generated method stub
		/*
		 * menu = getSlidingMenu();
		 * menu.setSecondaryMenu(R.layout.slidemenu_layout);
		 * menu.setMode(SlidingMenu.LEFT);
		 * menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		 * menu.setFadeDegree(0.35f);
		 * menu.setShadowDrawable(R.color.app_slidebgcolor);
		 * menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		 */

		initializedSlidingMenuDatas();
	}

	@SuppressLint("NewApi")
	private void initializedSlidingMenuDatas() {

		// TODO Auto-generated method stub
		/*
		 * ArrayList<String> menuTitle = new ArrayList<String>();
		 * ArrayList<Integer> menuIcon = new ArrayList<Integer>();
		 */

		menuList.clear();

		for (int i = 0; i < MenuTitles.length; i++) {
			menuList.add(new SlidingMenu_Class(MenuTitles[i], MenuIcons[i]));
		}

		/*
		 * menuTitle.add(MenuTitles[i]); menuIcon.add(MenuIcons[i]);
		 */

		// ((ImageView)menu.getMenu().findViewById(R.id.closeSlideButton)).setOnClickListener(new
		// OnClickListener() {
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// menu.toggle();
		// }
		// });

		applySlidingMenuValues();

		/*
		 * ListView slidingListView = (ListView) menu.getMenu().findViewById(
		 * R.id.slidelistView);
		 */
		slidingListView = (ListView) findViewById(R.id.slidelistView);
		// Listview.setDividerHeight(2);
		// Listview.setClickable(true);
		// Listview.setBackgroundColor(Color.GRAY);
		// ListAdapter adapter = new ListAdapter(context, nearby_values);
		// Listview.setAdapter(adapter);

		menuAdapter = new SlidingMenuAdapter(getApplicationContext(), menuList);
		slidingListView.setAdapter(menuAdapter);

		slidingListView
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						/*
						 * if(menuAdapter.getItem(position).getMenuName().equals(
						 * "")
						 */
						slidingmenu_layout.toggleMenu();
						if (menuAdapter.getItem(position).getMenuName()
								.equals("My Tabs")) {
							fragmenthost
									.setCurrentTabByTag(AppConstants.HOME_FRAGMENT_TAG);
						} else if (menuAdapter.getItem(position).getMenuName()
								.equals("Newsfeed")) {
							fragmenthost.setCurrentTabByTag("Newsfeed");
						} else if (menuAdapter.getItem(position).getMenuName()
								.equals("Notifications")) {
							fragmenthost
									.setCurrentTabByTag("Notifications");
						} else if (menuAdapter.getItem(position).getMenuName()
								.equals("Wallet")) {
							fragmenthost.setCurrentTabByTag("Wallet");
						} else if (menuAdapter.getItem(position).getMenuName()
								.equals("Friends")) {
							fragmenthost.setCurrentTabByTag("Friends");
						} else if (menuAdapter.getItem(position).getMenuName()
								.equals("Group")) {
							fragmenthost.setCurrentTabByTag("Group");
						} else if (menuAdapter.getItem(position).getMenuName()
								.equals("Friends & Group Settings")) {
							fragmenthost
									.setCurrentTabByTag(AppConstants.MY_FRIENDS_AND_GROUPSFRAGMENT_TAG);
						} else if (menuAdapter.getItem(position).getMenuName()
								.equals("Invite People")) {
							fragmenthost
									.setCurrentTabByTag(AppConstants.INVITEPEOPLE_FRAGMENT_TAG);
						} else if (menuAdapter.getItem(position).getMenuName()
								.equals("Support")) {
							fragmenthost
									.setCurrentTabByTag(AppConstants.SUPPORT_FRAGMENT_TAG);
						} else if (menuAdapter.getItem(position).getMenuName()
								.equals("Logout")) {
							PreferenceManager.getInstance().resetUserDetails();
							ActivityManager.startActivity(HomeActivity.this,
									BashLandingPage.class);
							BashApplication.isUserLoggedIn = false;
							finish();
						}

						/*
						 * switch (position) { case 0:
						 * fragmenthost.setCurrentTabByTag
						 * (AppConstants.HOME_FRAGMENT_TAG); break; case 1:
						 * fragmenthost
						 * .setCurrentTabByTag(AppConstants.MY_PROFILE_FRAGMENT_TAG
						 * ); break; case 2:
						 * fragmenthost.setCurrentTabByTag(AppConstants
						 * .MY_WALLET_FRAGMENT_TAG); break; case 3:
						 * fragmenthost.setCurrentTabByTag(AppConstants.
						 * MY_FRIENDS_AND_GROUPSFRAGMENT_TAG); break; case 4:
						 * fragmenthost.setCurrentTabByTag(AppConstants.
						 * INVITEPEOPLE_FRAGMENT_TAG);
						 * //fragmenthost.setCurrentTabByTag
						 * (AppConstants.MY_GROUPS_FRAGMENT_TAG); break; case 5:
						 * fragmenthost
						 * .setCurrentTabByTag(AppConstants.SUPPORT_FRAGMENT_TAG
						 * ); //fragmenthost.setCurrentTabByTag(AppConstants.
						 * PRIVACY_FRAGMENT_TAG);
						 * //fragmenthost.setCurrentTabByTag
						 * (AppConstants.BASH_MOMENTS_FRAGMENT_TAG); break; case
						 * 6:
						 * PreferenceManager.getInstance().resetUserDetails();
						 * ActivityManager.startActivity(HomeActivity.this,
						 * SignInActivity.class); BashApplication.isUserLoggedIn
						 * = false; finish();
						 * //fragmenthost.setCurrentTabByTag(AppConstants
						 * .SUPPORT_FRAGMENT_TAG);
						 * //fragmenthost.setCurrentTabByTag
						 * (AppConstants.PRIVACY_FRAGMENT_TAG); break; case 7:
						 * fragmenthost
						 * .setCurrentTabByTag(AppConstants.SUPPORT_FRAGMENT_TAG
						 * ); break; default: break; }
						 */
						// getSlidingMenu().toggle();
					}
				});

		/*
		 * ((EditText) menu.getMenu().findViewById(R.id.filterMenuEditText)).
		 * addTextChangedListener(new TextWatcher() {
		 * 
		 * @Override public void onTextChanged(CharSequence s, int start, int
		 * before, int count) { // TODO Auto-generated method stub if (count <
		 * before) { // We're deleting char so we need to reset the adapter data
		 * menuAdapter.resetData(); }
		 * menuAdapter.getFilter().filter(s.toString()); }
		 * 
		 * @Override public void beforeTextChanged(CharSequence s, int start,
		 * int count, int after) { // TODO Auto-generated method stub }
		 * 
		 * @Override public void afterTextChanged(Editable s) { // TODO
		 * Auto-generated method stub } });
		 */

		((ImageView) findViewById(R.id.profileImage))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						fragmenthost
								.setCurrentTabByTag(AppConstants.MY_PROFILE_FRAGMENT_TAG);
						// getSlidingMenu().toggle();
					}
				});

		((TextView) findViewById(R.id.userNameText))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						fragmenthost
								.setCurrentTabByTag(AppConstants.MY_PROFILE_FRAGMENT_TAG);
						// getSlidingMenu().toggle();
					}
				});

		((TextView) findViewById(R.id.userAmountText))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						fragmenthost
								.setCurrentTabByTag(AppConstants.MY_PROFILE_FRAGMENT_TAG);
						// getSlidingMenu().toggle();
					}
				});

	}

	public void applySlidingMenuValues() {
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				PreferenceManager.getInstance().getPercentageFromWidth(25),
				PreferenceManager.getInstance().getPercentageFromHeight(25));
		params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
		params.setMargins(0, 10, 0, 0);
		((ImageView) findViewById(R.id.profileImage)).setLayoutParams(params);

		if (PreferenceManager.getInstance().getUserCredits() != null
				&& PreferenceManager.getInstance().getUserCredits().length() != 0)
			((TextView) findViewById(R.id.userAmountText))
					.setText(AppConstants.RASYMBOL + " "
							+ PreferenceManager.getInstance().getUserCredits());
		else
			((TextView) findViewById(R.id.userAmountText))
					.setText(AppConstants.RASYMBOL + " 0");

		if (PreferenceManager.getInstance().getUserImagePath() != null
				&& PreferenceManager.getInstance().getUserImagePath().length() != 0) {
			/*
			 * ImageLoader.getInstance().displayImage(PreferenceManager.getInstance
			 * ().getUserImagePath(),
			 * ((ImageView)menu.getMenu().findViewById(R.id.profileImage)),
			 * BashApplication.options, BashApplication.animateFirstListener);
			 */

			/*
			 * Picasso.with(HomeActivity.this).load(PreferenceManager.getInstance
			 * ().getUserImagePath()) .resize(50, 50) .centerCrop()
			 * .transform(new RoundedTransformation(100, 0))
			 * .into(((ImageView)menu
			 * .getMenu().findViewById(R.id.profileImage)));
			 */

			/*
			 * Picasso.with(HomeActivity.this)
			 * .load(PreferenceManager.getInstance().getUserImagePath())
			 * .resize(50, 50) .centerCrop() .transform(new
			 * RoundedTransformation(100, 0))
			 * .into(((ImageView)menu.getMenu().findViewById
			 * (R.id.profileImage)));
			 */

			ImageLoader.getInstance()
					.displayImage(
							PreferenceManager.getInstance().getUserImagePath(),
							((ImageView) findViewById(R.id.profileImage)),
							new DisplayImageOptions.Builder()
									.showImageOnLoading(
											R.drawable.addphoto_img_block)
									.showImageForEmptyUri(
											R.drawable.addphoto_img_block)
									.showImageOnFail(
											R.drawable.addphoto_img_block)
									.cacheInMemory(true).cacheOnDisk(true)
									.considerExifParams(true)
									.bitmapConfig(Bitmap.Config.RGB_565)
									.imageScaleType(ImageScaleType.EXACTLY)
									// .displayer(new
									// RoundedBitmapDisplayer(PreferenceManager.getInstance().getPercentageFromWidth(400)))
									// .displayer(new
									// RoundedBitmapDisplayer(700))
									.build());
		} else
			((ImageView) findViewById(R.id.profileImage))
					.setImageResource(R.drawable.picture_icon_s);
		if (PreferenceManager.getInstance().getUserFullName() != null
				&& PreferenceManager.getInstance().getUserFullName().length() != 0)
			((TextView) findViewById(R.id.userNameText))
					.setText(PreferenceManager.getInstance().getUserFullName());
		// if(PreferenceManager.getInstance().getUserLastName() != null &&
		// PreferenceManager.getInstance().getUserLastName().length() != 0)
		// ((TextView)menu.getMenu().findViewById(R.id.userNameText)).setText(((TextView)menu.getMenu().findViewById(R.id.userNameText)).getText().toString()+" "+PreferenceManager.getInstance().getUserLastName());
	}

	public static Fragment currentFragment;

	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		super.onActivityResult(arg0, arg1, arg2);
		mSimpleFacebook.onActivityResult(this, arg0, arg1, arg2);
		if (currentFragment != null) {
			currentFragment.onActivityResult(arg0, arg1, arg2);
		}
	};

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		boolean isPopFragment = false;
		if (slidingmenu_layout.isMenuShown()) {
			slidingmenu_layout.toggleMenu();
			return;
		}
		String currentTabTag = fragmenthost.getCurrentTabTag();

		if (currentTabTag.equals(AppConstants.HOME_FRAGMENT_TAG)) {
			isPopFragment = ((BaseFragment) getSupportFragmentManager()
					.findFragmentByTag(AppConstants.HOME_FRAGMENT_TAG))
					.popFragment();
		} else if (currentTabTag.equals(AppConstants.MY_WALLET_FRAGMENT_TAG)) {
			isPopFragment = ((BaseFragment) getSupportFragmentManager()
					.findFragmentByTag(AppConstants.MY_WALLET_FRAGMENT_TAG))
					.popFragment();
		}
		/*
		 * else if(currentTabTag.equals(AppConstants.CASH_OUT_FRAGMENT_TAG)) {
		 * isPopFragment = ((BaseFragment)
		 * getSupportFragmentManager().findFragmentByTag
		 * (AppConstants.CASH_OUT_FRAGMENT_TAG)).popFragment(); }
		 */
		else if (currentTabTag
				.equals(AppConstants.MY_FRIENDS_AND_GROUPSFRAGMENT_TAG)) {
			isPopFragment = ((BaseFragment) getSupportFragmentManager()
					.findFragmentByTag(
							AppConstants.MY_FRIENDS_AND_GROUPSFRAGMENT_TAG))
					.popFragment();
		} else if (currentTabTag.equals(AppConstants.MY_GROUPS_FRAGMENT_TAG)) {
			isPopFragment = ((BaseFragment) getSupportFragmentManager()
					.findFragmentByTag(AppConstants.MY_GROUPS_FRAGMENT_TAG))
					.popFragment();
		}
		/*
		 * else if(currentTabTag.equals(AppConstants.BASH_MOMENTS_FRAGMENT_TAG))
		 * { isPopFragment = ((BaseFragment)
		 * getSupportFragmentManager().findFragmentByTag
		 * (AppConstants.BASH_MOMENTS_FRAGMENT_TAG)).popFragment(); } else
		 * if(currentTabTag.equals(AppConstants.PRIVACY_FRAGMENT_TAG)) {
		 * isPopFragment = ((BaseFragment)
		 * getSupportFragmentManager().findFragmentByTag
		 * (AppConstants.PRIVACY_FRAGMENT_TAG)).popFragment(); }
		 */
		else if (currentTabTag.equals(AppConstants.INVITEPEOPLE_FRAGMENT_TAG)) {
			isPopFragment = ((BaseFragment) getSupportFragmentManager()
					.findFragmentByTag(AppConstants.INVITEPEOPLE_FRAGMENT_TAG))
					.popFragment();
		}

		else if (currentTabTag.equals(AppConstants.SUPPORT_FRAGMENT_TAG)) {
			isPopFragment = ((BaseFragment) getSupportFragmentManager()
					.findFragmentByTag(AppConstants.SUPPORT_FRAGMENT_TAG))
					.popFragment();
		} else if (currentTabTag.equals(AppConstants.MY_PROFILE_FRAGMENT_TAG)) {
			isPopFragment = ((BaseFragment) getSupportFragmentManager()
					.findFragmentByTag(AppConstants.MY_PROFILE_FRAGMENT_TAG))
					.popFragment();
		} else if (currentTabTag.equals(AppConstants.MY_CARDS_FRAGMENT_TAG)) {
			isPopFragment = ((BaseFragment) getSupportFragmentManager()
					.findFragmentByTag(AppConstants.MY_CARDS_FRAGMENT_TAG))
					.popFragment();
		}

		if (!isPopFragment) {
			if (doubleBackToExitPressedOnce) {
				finish();
				return;
			}

			this.doubleBackToExitPressedOnce = true;
			Toast.makeText(this, "Please click BACK again to exit",
					Toast.LENGTH_SHORT).show();

			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					doubleBackToExitPressedOnce = false;
				}
			}, 2000);
		}
	}

	boolean doubleBackToExitPressedOnce = false;

	// private final String[] MenuTitles = {"Home", "Cash In/Out", "Cash Out",
	// "My Friends", "My Groups", "Bash Moments", "Privacy & Sharing",
	// "Support"};
	/*
	 * private final Integer[] MenuIcons = {R.drawable.home_icon,
	 * R.drawable.cashin_icon, R.drawable.cashout_icon, R.drawable.friends_icon
	 * ,R.drawable.group_icon, R.drawable.bashmoments_icon,
	 * R.drawable.privacy_icon, R.drawable.support_icon};
	 */
	private final String[] MenuTitles = { "My Tabs", "Newsfeed",
			"Notifications", "Wallet", "Friends", "Group", "Settings",/*
																	 * "My Groups",
																	 * "Bash Moments"
																	 * ,
																	 * "Invite People"
																	 * ,
																	 */
			"Support", "Logout" };
	private final Integer[] MenuIcons = { R.drawable.home_icon,
			R.drawable.bashmoments_icon, R.drawable.privacy_icon,
			R.drawable.cashout_icon, R.drawable.privacy_icon,/*
															 * R.drawable.
															 * friends_icon,
															 */
			R.drawable.group_icon,/* R.drawable.bashmoments_icon, */
			R.drawable.privacy_icon, R.drawable.support_icon,
			R.drawable.privacy_icon };

	// datas
	public static FriendGroupAdapter total_friendadapter, youowe_friendadapter,
			youareowed_friendadapter;
	public ArrayList<String> totallistDataHeader, youowlsitDataHeader,
			youareowedDataHeader;
	public static SparseArray<ArrayList<MyTrans_FriendsGroups_Class>> totalfglist;
	public static SparseArray<ArrayList<MyTrans_FriendsGroups_Class>> youowefglist;
	public static SparseArray<ArrayList<MyTrans_FriendsGroups_Class>> youareowfglist;

	public void initializeListValues() {

		totallistDataHeader = new ArrayList<String>();
		youowlsitDataHeader = new ArrayList<String>();
		youareowedDataHeader = new ArrayList<String>();

		totallistDataHeader.clear();
		totallistDataHeader.add("Groups");
		totallistDataHeader.add("Friends");

		youowlsitDataHeader.clear();
		youowlsitDataHeader.add("Groups");
		youowlsitDataHeader.add("Friends");

		youareowedDataHeader.clear();
		youareowedDataHeader.add("Groups");
		youareowedDataHeader.add("Friends");

		totalfglist = new SparseArray<ArrayList<MyTrans_FriendsGroups_Class>>();
		youowefglist = new SparseArray<ArrayList<MyTrans_FriendsGroups_Class>>();
		youareowfglist = new SparseArray<ArrayList<MyTrans_FriendsGroups_Class>>();

		totalfglist.clear();
		youowefglist.clear();
		youareowfglist.clear();

		total_friendadapter = new FriendGroupAdapter(HomeActivity.this,
				totallistDataHeader, totalfglist);
		youowe_friendadapter = new FriendGroupAdapter(HomeActivity.this,
				youowlsitDataHeader, youowefglist);
		youareowed_friendadapter = new FriendGroupAdapter(HomeActivity.this,
				youareowedDataHeader, youareowfglist);

	}
}
