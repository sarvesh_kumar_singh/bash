package com.bash.Activities;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Adapters.PaidByListAdapter;
import com.bash.ListModels.Person;
import com.bash.interfaces.ActivityInterface;
import com.bash.interfaces.PaidbyInterface;

public class PaidbySplitActivity extends Activity {

	TextView topleftpaidbyText,toprightsidepaidbytext;
	ListView lvpaidbylist;
	public static float totalAmount;
	ArrayList<Person> addedPersonArrayList;
	PaidByListAdapter paidByListAdapter;
	public static PaidbyInterface delegate;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_paidby);
	
		topleftpaidbyText = (TextView) findViewById(R.id.topleftpaidbyText);
		toprightsidepaidbytext = (TextView) findViewById(R.id.toprightsidepaidbytext);
		lvpaidbylist = (ListView) findViewById(R.id.lvpaidbylist);	
	
		addedPersonArrayList = new ArrayList<Person>();
		
		Intent paidbyIntent = getIntent();
		
		if(paidbyIntent != null){
			addedPersonArrayList = paidbyIntent.getParcelableArrayListExtra("AddedPersonList");
			String amtstr = paidbyIntent.getStringExtra("TotalAmount");	
			totalAmount = Float.parseFloat(amtstr);
		}
	
		paidByListAdapter = new PaidByListAdapter(PaidbySplitActivity.this,
				R.layout.adapter_paidby, addedPersonArrayList);
		
		lvpaidbylist.setAdapter(paidByListAdapter);
		
		
		topleftpaidbyText.setOnClickListener(clickListener);
		toprightsidepaidbytext.setOnClickListener(clickListener);
		
	}

	private View.OnClickListener clickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			//String text = "";

			switch (v.getId()) {

			case R.id.topleftpaidbyText:		
				//text = "back";
				PaidbySplitActivity.this.onBackPressed();
				break;
			case R.id.toprightsidepaidbytext:
				
				//Float ttAmount = paidByListAdapter.findTotalAmount();
				if(paidByListAdapter.findTotalAmount() == totalAmount){
							
					delegate.onPaidByData(paidByListAdapter.addedPersonArrayList);
					
					finish();
				}else{
					Toast.makeText(PaidbySplitActivity.this, "Error in Amount", Toast.LENGTH_SHORT).show();
				}
				
				//text = totalAmount+" , "+ttAmount ;
				break;
				
			}
			//Toast.makeText(PaidbySplitActivity.this, text, Toast.LENGTH_SHORT).show();
		}
	};
	
}
