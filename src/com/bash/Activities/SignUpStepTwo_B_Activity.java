package com.bash.Activities;
import org.json.JSONObject;
import android.app.Activity;
import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.bash.R;
import com.bash.Managers.ActivityManager;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;

public class SignUpStepTwo_B_Activity extends Activity implements AsyncResponse{
	MyAsynTaskManager  myAsyncTask;
	View pin_webview;
	Dialog pinWebDialog;
	WebView alertWebView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		// To hide Title Bar and Enable Full Screen
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	    //for Portrait mode only
	    setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    //To Hide Title Bar Only
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup_steptwo_b);
		
		((Button)findViewById(R.id.nextStepButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ActivityManager.startActivity(SignUpStepTwo_B_Activity.this, SignUpStepTwo_C_Activity.class);
				finish();
			}
		});
		
		initializeWebAlertView();
		
		((Button)findViewById(R.id.changePinButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				changeBashPinProcess();
			}
		});
		
//		((Button)findViewById(R.id.skipReportButton)).setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				ActivityManager.startActivity(SignUpStepTwo_B_Activity.this, SignUpStepTwo_C_Activity.class);
//				finish();
//			}
//		});
		
	}
	
	@Override
	public void onBackPressed() {
	
	}
	
	
public void changeBashPinProcess() {
	myAsyncTask=new MyAsynTaskManager();
	myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("changeBashPinProcess",SignUpStepTwo_B_Activity.this,AppUrlList.CHANGE_BASHPIN_URL, new String[] {
				"partnertransactionid",
				"partnercode",
				"username",
				"password",
				"servicetype",
				"usernumber"
				}, 
				new String[]{
				"123456",
				"MOBG",
				"MOBG",
				"9641",
				"CP",
				PreferenceManager.getInstance().getUserMobileNumber()
				}
		);
		myAsyncTask.execute();
		
	}
	
private void initializeWebAlertView(){
	
	pin_webview = View.inflate(SignUpStepTwo_B_Activity.this,
			R.layout.alert_showpinalert_dialog, null);
	pinWebDialog = new Dialog(SignUpStepTwo_B_Activity.this);
	pinWebDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	pinWebDialog.setContentView(pin_webview);

	alertWebView = ((WebView) pin_webview.findViewById(R.id.webView));
	WebSettings settings = alertWebView.getSettings();
    settings.setJavaScriptEnabled(true);
    alertWebView.setHorizontalScrollBarEnabled(true);
}


	public boolean ValidateForm() {
		  if (TextUtils.isEmpty(((EditText) findViewById(R.id.mobileNumberText)).getText().toString())){
			((EditText) findViewById(R.id.mobileNumberText)).setError("Field is Empty");
			return false;
		  }else if(((EditText) findViewById(R.id.mobileNumberText)).length() != 10){
			  Toast.makeText(getApplicationContext(), "Please Enter 10 digit valid mobile number!", Toast.LENGTH_SHORT).show();
			  return false;
		  }
		return true;
	}
	
	 
	public void ProcessUserMobilerNumber(){
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("ProcessUserMobilerNumber",SignUpStepTwo_B_Activity.this,AppUrlList.ACTION_URL, new String[]{
				"module",
				"action",
				"email_id",
				"password",
				"firstname",
				"lastname",
				"dob",
				"mobile_number",
				"username",
				"gender",
				"device_id",
				"device_type"},
				
			new String[]{
				"user",
				"register",
				((EditText) findViewById(R.id.emailIdText)).getText().toString(),
				((EditText) findViewById(R.id.passwordText)).getText().toString(),
				((EditText) findViewById(R.id.fullNameText)).getText().toString(),
				"",
			 	"91"+((EditText) findViewById(R.id.userNameText)).getText().toString(),
				((EditText) findViewById(R.id.userNameText)).getText().toString(),
				PreferenceManager.getInstance().getGCMRegistrationId(),
				"0" });
		myAsyncTask.execute();
		
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		if(from.equalsIgnoreCase("changeBashPinProcess"))
		{
			if(output!=null)
			{

				try {
					Log.e("Response", output);
					alertWebView.loadDataWithBaseURL("", output, "", "", "");
					pinWebDialog.show();
					
				} catch (Exception e) {
					DialogManager.showDialog(SignUpStepTwo_B_Activity.this, "Error Occured in Storing Profile Info!");
					e.printStackTrace();
				}
			
			}
			else
			{
				DialogManager.showDialog(SignUpStepTwo_B_Activity.this, "Error Occured in Storing Profile Info!");
			}
		}
		else if(from.equalsIgnoreCase("ProcessUserMobilerNumber"))
		{
			if(output!=null)
			{

				try {
					JSONObject rootObj = new JSONObject(output);
					if(rootObj.getBoolean("result")){
						/*ActivityManager.startActivity(SignUpStepTwo_B_Activity.this, HomeActivity.class);
						finish();*/
					}else{
						Toast.makeText(getApplicationContext(), "Error Occured!", Toast.LENGTH_SHORT).show();
					}
					 
				} catch (Exception e) {
				 	e.printStackTrace();
					DialogManager.showDialog(SignUpStepTwo_B_Activity.this, "Server Error Occured! Try Again!");
				}
			
			}
			else
			{DialogManager.showDialog(SignUpStepTwo_B_Activity.this, "Server Error Occured! Try Again!");}
		}
		{
			
		}
	}
	
	/*	public class LoadContactsAndSignUpClass extends AsyncTask<Void,	Void, Boolean>{
		
	}*/
	 
	

	 
}

