package com.bash.Activities;


import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;

import com.bash.R;
import com.bash.ListModels.Person;
import com.bash.ListModels.PhotosListModel;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.ContainerProvider;
import com.bash.Utils.AppConstants;

public class AddTabSplitActivity extends FragmentActivity {

	public static FragmentTabHost mTabhost;
	public static View ratioView, unequalView, equalView, percentView, item_wiseView;
	public static TextView tvContinue;
	Bundle bndle;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_tab_split);
		
		Intent intent = getIntent();
		if(intent != null){
			
			bndle =intent.getExtras();
			
		}		
		
		initializeActivityViews();
		initializeFragmentTabHost();
			
		if (!PreferenceManager.getInstance().isPhoneConfigFixed()) {
			PreferenceManager.getInstance().setPhoneConfiguration(
					getWindowManager().getDefaultDisplay().getWidth(),
					getWindowManager().getDefaultDisplay().getHeight());
		}	
	}
	private void initializeActivityViews() {
		
		tvContinue = ((TextView) findViewById(R.id.toprightsidecontinuetext));
		tvContinue.setText("Save");

		((TextView) findViewById(R.id.middleText)).setText("Choose split type");
		
		mTabhost = (FragmentTabHost)findViewById(R.id.addtabsplithost);
		mTabhost.setup(AddTabSplitActivity.this, getSupportFragmentManager(),
				R.id.addtabsplitframes_holder);
		
		
		((ImageView) findViewById(R.id.topleftsidebackimage))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				AddTabSplitActivity.this.onBackPressed();
				
			}
		});
		((TextView) findViewById(R.id.toprightsidecontinuetext))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//
				
			}
		});
	}
	
	private void initializeFragmentTabHost() {
		
		
		ratioView = LayoutInflater.from(AddTabSplitActivity.this).inflate(R.layout.addtrans_tab_layout, null);
		unequalView = LayoutInflater.from(AddTabSplitActivity.this).inflate(R.layout.addtrans_tab_layout, null);
		equalView = LayoutInflater.from(AddTabSplitActivity.this).inflate(R.layout.addtrans_tab_layout,null);
		percentView = LayoutInflater.from(AddTabSplitActivity.this).inflate(R.layout.addtrans_tab_layout, null);
		item_wiseView = LayoutInflater.from(AddTabSplitActivity.this).inflate(R.layout.addtrans_tab_layout, null);

		//mTabhost.setBackgroundColor(Color.parseColor("#FFFFFF"));

		Spannable equalViewtoSpan = new SpannableString("Ratio");
		equalViewtoSpan.setSpan(new ForegroundColorSpan(Color.WHITE), 0, 4,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) ratioView.findViewById(R.id.tabtext))
				.setText(equalViewtoSpan);

		Spannable ratioViewtoSpan = new SpannableString("Unequal");
		ratioViewtoSpan.setSpan(new ForegroundColorSpan(Color.WHITE), 0, 6,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) unequalView.findViewById(R.id.tabtext))
				.setText(ratioViewtoSpan);

		Spannable unequalViewtoSpan = new SpannableString("Equal");
		unequalViewtoSpan.setSpan(new ForegroundColorSpan(Color.WHITE), 0, 4,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) equalView.findViewById(R.id.tabtext))
				.setText(unequalViewtoSpan);
		
		Spannable percentViewtoSpan = new SpannableString("Percent");
		percentViewtoSpan.setSpan(new ForegroundColorSpan(Color.WHITE), 0, 6,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) percentView.findViewById(R.id.tabtext))
				.setText(percentViewtoSpan);
		
		Spannable item_wiseViewtoSpan = new SpannableString("Item-wise");
		item_wiseViewtoSpan.setSpan(new ForegroundColorSpan(Color.WHITE), 0, 8,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) item_wiseView.findViewById(R.id.tabtext))
				.setText(item_wiseViewtoSpan);

		
		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.SPLIT_TAB_RATIO_TAG)
				.setIndicator(ratioView),ContainerProvider.AddTab_Split_Container.class,
				bndle);
		
		mTabhost.addTab(
				mTabhost.newTabSpec(AppConstants.SPLIT_TAB_UNEQUAL_TAG)
						.setIndicator(unequalView),ContainerProvider.AddTab_Split_Container.class,
						bndle);
		
		mTabhost.addTab(
				mTabhost.newTabSpec(AppConstants.SPLIT_TAB_EQUAL_TAG)
						.setIndicator(equalView),
				ContainerProvider.AddTab_Split_Container.class,
				bndle);
		
		mTabhost.addTab(
				mTabhost.newTabSpec(AppConstants.SPLIT_TAB_PERCENT_TAG)
						.setIndicator(percentView),ContainerProvider.AddTab_Split_Container.class,
						bndle);
		
//		mTabhost.addTab(
//				mTabhost.newTabSpec(AppConstants.SPLIT_TAB_ITEMWISE_TAG)
//						.setIndicator(item_wiseView),ContainerProvider.AddTab_Split_Container.class,
//						bndle);
		mTabhost.addTab(
				mTabhost.newTabSpec(AppConstants.SPLIT_TAB_ITEMWISE_TAG)
						.setIndicator(item_wiseView),ContainerProvider.AddTab_Split_Container.class,
						bndle);
		
		mTabhost.setCurrentTab(2);

		mTabhost.setOnTabChangedListener(new OnTabChangeListener() {
			@Override
			public void onTabChanged(String tabId) {
				
				if(mTabhost.getCurrentTabTag().toString().equals(AppConstants.SPLIT_TAB_ITEMWISE_TAG)){
					
					Intent itemwiseintent = new Intent(AddTabSplitActivity.this, ItemWiseSplitActivity.class);
					itemwiseintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					itemwiseintent.putExtra("itemwisebndle", bndle);
					startActivity(itemwiseintent);
					
				}

				//Toast.makeText(AddTabSplitActivity.this, "TabHost Clicks : "+mTabhost.getCurrentTabTag().toString(), Toast.LENGTH_SHORT).show();
				
			}
		});
	}
	
public void setUpTopBarFields(int leftSideImage, String endText) {
		
		
		
		((ImageView) findViewById(R.id.topleftsidebackimage))
				.setImageResource(leftSideImage);
		tvContinue = ((TextView) findViewById(R.id.toprightsidecontinuetext));
		tvContinue.setText(endText);
		/*
		 * ((TextView) findViewById(R.id.topmiddleText)).setText(Html
		 * .fromHtml("&#8377;"));
		 */
	}
	
}
