package com.bash.Activities;
import com.bash.R;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

public class Splash_Activity extends Activity {

	@Override  
	protected void onCreate(Bundle savedInstanceState) {
		// To hide Title Bar and Enable Full Screen
	    // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    
	    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	    
	    //for Portrait mode only
	    setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	    
	    //To Hide Title Bar Only
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);;
		 new Handler().postDelayed(new Runnable() 
			{
				@Override
				public void run() 
				{
					// This method will be executed once the timer is over
					// Start your app main activity				
					//plays the movie
	           Intent i = new Intent(Splash_Activity.this,BashLandingPage.class);
				startActivity(i);
				overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
					finish();
				}
			}
			,1000);
	}
	
}
