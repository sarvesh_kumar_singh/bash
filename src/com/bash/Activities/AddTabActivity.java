package com.bash.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;

import com.bash.R;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.ContainerProvider;
import com.bash.Utils.AppConstants;

public class AddTabActivity extends FragmentActivity {

	
	public static FragmentTabHost mTabhost;
	public static View payView, splitView, receiveView;
	public static TextView tvContinue;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_tab);
		initializeActivityViews();
		initializeFragmentTabHost();
		
		
		if (!PreferenceManager.getInstance().isPhoneConfigFixed()) {
			PreferenceManager.getInstance().setPhoneConfiguration(
					getWindowManager().getDefaultDisplay().getWidth(),
					getWindowManager().getDefaultDisplay().getHeight());
		}
		
	}

	
	private void initializeActivityViews() {
		
		tvContinue = ((TextView) findViewById(R.id.toprightsidecontinuetext));
		tvContinue.setText("Continue");

		mTabhost = (FragmentTabHost)findViewById(R.id.addtabhost);
		mTabhost.setup(AddTabActivity.this, getSupportFragmentManager(),
				R.id.addtabframes_holder);
		
		
		((ImageView) findViewById(R.id.topleftsidebackimage))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				AddTabActivity.this.onBackPressed();
				
			}
		});
		((TextView) findViewById(R.id.toprightsidecontinuetext))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//
				
			}
		});
	}
	private void initializeFragmentTabHost() {
		payView = LayoutInflater.from(AddTabActivity.this).inflate(R.layout.addtrans_tab_layout,null);
		splitView = LayoutInflater.from(AddTabActivity.this).inflate(R.layout.addtrans_tab_layout, null);
		receiveView = LayoutInflater.from(AddTabActivity.this).inflate(R.layout.addtrans_tab_layout, null);

		//mTabhost.setBackgroundColor(Color.parseColor("#FFFFFF"));

		Spannable payViewtoSpan = new SpannableString("Pay");
		payViewtoSpan.setSpan(new ForegroundColorSpan(Color.WHITE), 0, 2,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) payView.findViewById(R.id.tabtext))
				.setText(payViewtoSpan);

		Spannable splittoSpan = new SpannableString("Split");
		splittoSpan.setSpan(new ForegroundColorSpan(Color.WHITE), 0, 4,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) splitView.findViewById(R.id.tabtext))
				.setText(splittoSpan);

		Spannable reeivetoSpan = new SpannableString("Receive");
		reeivetoSpan.setSpan(new ForegroundColorSpan(Color.WHITE), 0, 6,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) receiveView.findViewById(R.id.tabtext))
				.setText(reeivetoSpan);

		mTabhost.addTab(
				mTabhost.newTabSpec(AppConstants.TAB_PAY_TAG)
						.setIndicator(payView),
				ContainerProvider.AddTab_Pay_Container.class,
				null);

		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_SPLIT_TAG)
				.setIndicator(splitView),ContainerProvider.AddTab_Pay_Container.class,
				null);

		mTabhost.addTab(
				mTabhost.newTabSpec(AppConstants.TAB_RECEIVE_TAG)
						.setIndicator(receiveView),ContainerProvider.AddTab_Pay_Container.class,
						null);

		mTabhost.setOnTabChangedListener(new OnTabChangeListener() {
			@Override
			public void onTabChanged(String tabId) {

			}
		});
	}	
	
	public void setUpTopBarFields(int leftSideImage, String endText) {
		
		
		
		((ImageView) findViewById(R.id.topleftsidebackimage))
				.setImageResource(leftSideImage);
		tvContinue = ((TextView) findViewById(R.id.toprightsidecontinuetext));
		tvContinue.setText(endText);
		/*
		 * ((TextView) findViewById(R.id.topmiddleText)).setText(Html
		 * .fromHtml("&#8377;"));
		 */
	}
	
}
