package com.bash.Activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Adapters.MyTabsAdapter;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.Fragments.MyTrans_Pending_Fragment;
import com.bash.ListModels.MyTabs_Model;
import com.bash.Providers.ContainerProvider;
import com.bash.Utils.AppConstants;

public class AddTransactionActivity extends FragmentActivity{
	
	public static FragmentTabHost fragmenthost;
	public ListView lviMyTabs;
	public MyTabs_Model tabModel;
	public MyTabsAdapter myTabsAdapter;
	
	private ImageView ivBackButton;
	private TextView tvContinueButton;
	public static View payView, splitView, receiveView;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addtransaction);
        initializeFragmentTabHost();
    }

    private void initializeFragmentTabHost() {
    	
    	fragmenthost = (FragmentTabHost) findViewById(R.id.tabhost);
 		fragmenthost.setup(this, getSupportFragmentManager(), R.id.frames_holder);
 		
 		View SplitView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.addtrans_tab_layout, null);
		View PayView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.addtrans_tab_layout, null);
		View ReceiveView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.addtrans_tab_layout, null);
		
		((TextView) PayView.findViewById(R.id.tabText)).setText("Pay");
		((TextView) SplitView.findViewById(R.id.tabText)).setText("Split");
		((TextView) ReceiveView.findViewById(R.id.tabText)).setText("Receive");
		
		fragmenthost.addTab(fragmenthost.newTabSpec(AppConstants.TAB_HOME_TOTALBALANCE_TAG).setIndicator(PayView),					
				ContainerProvider.AddDescription_Group_Container.class, null);
		
		fragmenthost.addTab(fragmenthost.newTabSpec(AppConstants.TAB_HOME_YOU_OWE_TAG).setIndicator(SplitView),
				ContainerProvider.AddDescription_Pay_Container.class, null);
		
		fragmenthost.addTab(fragmenthost.newTabSpec(AppConstants.TAB_HOME_YOU_ARE_OWED_TAG).setIndicator(ReceiveView),					
				ContainerProvider.AddDescription_Charge_Container.class, null);
		
		((ImageView)findViewById(R.id.topleftsidebackImage)).setOnClickListener(clickListener);
		((TextView)findViewById(R.id.toprightsidecontinueText)).setOnClickListener(clickListener);
		
 	}
    
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        
            switch (v.getId()) {
            
            case R.id.topleftsidebackImage:     
            	
            	finish();	
			
                break;
                case R.id.toprightsidecontinueText:
                	
                	Toast.makeText(AddTransactionActivity.this, "hi", Toast.LENGTH_SHORT).show(); 
                    break;
            }  	
                      	                  
        }
    };
}
