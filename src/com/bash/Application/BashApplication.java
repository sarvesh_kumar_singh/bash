package com.bash.Application;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import android.Manifest.permission;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.bash.R;
import com.bash.Activities.BashLandingPage;
import com.bash.Activities.Splash_Activity;
import com.bash.GCM.GCMHelper;
import com.bash.Managers.DataBaseManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.facebook.SessionDefaultAudience;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;

public class BashApplication extends Application
{
	private static final String TAG = "BashApplication";
	public static Context mContext;
	private static SharedPreferences mSharedPreferences;
	public static boolean isUserLoggedIn = false;
	public static InputMethodManager softKeypadInstance;
	public static String[] days = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
	public static String[] years = {"2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022"};
	public static ArrayAdapter<String> daysAdapter, yearsAdapter;
	public static DisplayImageOptions options, imageOptions;
	public static ImageLoadingListener animateFirstListener;
	public static HashMap<String, String> userTypeHash = null;
	@Override
	public void onCreate() {
		super.onCreate();
		mContext = getApplicationContext();
		initSharedPreferences();
		initSingletons();
		initSimpleFacebook();
		/*daysAdapter = new ArrayAdapter<String> (this, android.R.layout.simple_spinner_item, days);
        yearsAdapter = new ArrayAdapter<String> (this, android.R.layout.simple_spinner_item, years);
        daysAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/
		daysAdapter = new ArrayAdapter<String> (this, R.layout.spinner_item_textstyle, days);
        yearsAdapter = new ArrayAdapter<String> (this, R.layout.spinner_item_textstyle, years);
        daysAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item_style);
        yearsAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item_style);
        
		Log.e(TAG, "***************");
		softKeypadInstance = (InputMethodManager) mContext.getSystemService(
			      Context.INPUT_METHOD_SERVICE);
		
		
		userTypeHash = new HashMap<String, String>();
		userTypeHash.put("bash", "0");
		userTypeHash.put("group", "1");
		userTypeHash.put("pb", "2");
		userTypeHash.put("email", "3");
		userTypeHash.put("fb", "4");
	}
	

	private void initSimpleFacebook() {
		/*
		try{
	        PackageInfo info = getPackageManager().getPackageInfo(
	                "com.bash", PackageManager.GET_SIGNATURES);
	        for (Signature signature : info.signatures) {
	            MessageDigest md = MessageDigest.getInstance("SHA");
	            md.update(signature.toByteArray());
	            Log.d("KeyHash:",Base64.encodeToString(md.digest(), Base64.DEFAULT));       

	        }
	    } catch (NameNotFoundException e) {

	    } catch (NoSuchAlgorithmException e) {

	    }*/
		
		Permission[] permissions = new Permission[] { 
				Permission.PUBLIC_PROFILE, 
				Permission.USER_FRIENDS,
				Permission.EMAIL,
				Permission.USER_BIRTHDAY,
				Permission.EMAIL,
				Permission.READ_STREAM, 
				Permission.PUBLISH_ACTION
//				Permission.USER_GROUPS, 
//				Permission.USER_LIKES, 
//				Permission.USER_PHOTOS, 
//				Permission.USER_VIDEOS,
//				
//				Permission.USER_EVENTS,
//				Permission.USER_VIDEOS,
//				Permission.USER_RELATIONSHIPS,
//				Permission.READ_STREAM, 
//				Permission.PUBLISH_ACTION
				
				};

		SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
			.setAppId(AppConstants.FB_ID)
			.setNamespace(AppConstants.APP_NAMESPACE)
			.setPermissions(permissions)
			.setDefaultAudience(SessionDefaultAudience.FRIENDS)
			.setAskForAllPermissionsAtOnce(false)
			.build();

		SimpleFacebook.setConfiguration(configuration);
	}

	private void initSingletons() {
		
		DataBaseManager.initInstance(mContext);
		GCMHelper.checkDeviceRegistration(mContext);
		
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mContext)
		.threadPriority(Thread.NORM_PRIORITY - 2)
		.denyCacheImageMultipleSizesInMemory()
		.diskCacheFileNameGenerator(new Md5FileNameGenerator())
		.diskCacheSize(50 * 1024 * 1024) // 50 Mb
		.tasksProcessingOrder(QueueProcessingType.LIFO)
		.writeDebugLogs() // Remove for release app
		.build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
		
		options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.addphoto_img_block)
		.showImageForEmptyUri(R.drawable.addphoto_img_block)
		.showImageOnFail(R.drawable.addphoto_img_block)
		.cacheInMemory(true)
		.cacheOnDisk(true)
		.considerExifParams(true)
		.displayer(new RoundedBitmapDisplayer(70))
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.EXACTLY)
		.build();
		
		imageOptions = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.addphoto_img_block)
		.showImageForEmptyUri(R.drawable.addphoto_img_block)
		.showImageOnFail(R.drawable.addphoto_img_block)
		.cacheInMemory(true)
		.cacheOnDisk(true)
		.considerExifParams(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.imageScaleType(ImageScaleType.EXACTLY)
		.build();
		
		animateFirstListener = new AnimateFirstDisplayListener();
		
/*
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).build();
		ImageLoader.getInstance().init(config);*/
	}
	
	private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}
	
	private void initSharedPreferences(){
		PreferenceManager.initInstance();
		mSharedPreferences = getSharedPreferences("BASH_PREF", MODE_PRIVATE);
	}

	public static SharedPreferences getSharedPreferences() {
		return mSharedPreferences;
	}
	
	public static String getPrivateFilePath() {
		return mContext.getExternalFilesDir(AppConstants.FILE_DIR).getAbsolutePath();
	}
	
}
