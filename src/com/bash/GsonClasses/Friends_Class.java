package com.bash.GsonClasses;

import java.util.ArrayList;

import com.bash.ListModels.BashUsers_Class;
import com.google.gson.annotations.SerializedName;

public class Friends_Class {

	@SerializedName("result")
	public boolean result;
	
	@SerializedName("msg")
	public String msg;
	
 	@SerializedName("friendlist")
	public ArrayList<friendlist> friendlist = new ArrayList<friendlist>();

	public class friendlist {

		@SerializedName("idfriend")
		public String idfriend;

		@SerializedName("phone_no")
		public String phone_no;
		
		@SerializedName("name")
		public String name;
		
		@SerializedName("imagepath")
		public String imagepath;
		
		
		public String getisFriend(){
			return this.idfriend;
		}
		public String getphone_no(){
			return this.phone_no;
		}
		public String getname(){
			return this.name;
		}
		public String getimagepath(){
			return this.imagepath;
		}
		
		public void setFields(String idfriend, String phone_no, String name, String imagepath){
			this.idfriend = idfriend;
			this.phone_no = phone_no;
			this.name = name;
			this.imagepath = imagepath;
		}
	}

	 
}
