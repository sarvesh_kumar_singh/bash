package com.bash.GsonClasses;

import java.util.ArrayList;

import android.util.Log;
import android.util.SparseArray;

import com.bash.ListModels.MyTrans_FriendsGroups_Class;
import com.google.gson.annotations.SerializedName;

public class Pending_New_Class {

	@SerializedName("result")
	public boolean result;
	
	@SerializedName("total_bal")
	public total_bal total_bal;
	
	@SerializedName("you_owe")
	public you_are_owe you_owe;
	
	@SerializedName("you_are_owe")
	public you_are_owe you_are_owe;
	
	public class total_bal {
		
		public 	SparseArray<ArrayList<MyTrans_FriendsGroups_Class>> totalBal = new SparseArray<ArrayList<MyTrans_FriendsGroups_Class>>();
		
		@SerializedName("total")
		public int total;
		
		@SerializedName("friend")
		private ArrayList<MyTrans_FriendsGroups_Class> friend = new ArrayList<MyTrans_FriendsGroups_Class>();
		
		@SerializedName("group")
		private ArrayList<MyTrans_FriendsGroups_Class> group = new ArrayList<MyTrans_FriendsGroups_Class>();
		
		public SparseArray<ArrayList<MyTrans_FriendsGroups_Class>> getTotalBalValues(){
			
		/*	totalBal.setValueAt(0, friend);
			totalBal.setValueAt(1, group);*/
		//	totalBal.clear();
			/*totalBal.append(0, friend);
			totalBal.append(1, group);*/
			
			for(int i = 0; i < group.size(); i++){
				group.get(i).setType("group");
			}
			
			for(int i = 0; i < friend.size(); i++){
				friend.get(i).setType("friend");
			}
			
			if(totalBal == null)
				totalBal = new SparseArray<ArrayList<MyTrans_FriendsGroups_Class>>();
			
			totalBal.put(0, group);
			totalBal.put(1, friend);
			return totalBal;
		}
	}
	
	
	
	public class you_are_owe {
		
		public  SparseArray<ArrayList<MyTrans_FriendsGroups_Class>> youareowedBal = new SparseArray<ArrayList<MyTrans_FriendsGroups_Class>>();
		
		@SerializedName("total")
		public int total;
		
		@SerializedName("friend")
		public ArrayList<MyTrans_FriendsGroups_Class> friend = new ArrayList<MyTrans_FriendsGroups_Class>();
		
		@SerializedName("group")
		public ArrayList<MyTrans_FriendsGroups_Class> group = new ArrayList<MyTrans_FriendsGroups_Class>();
		
		public SparseArray<ArrayList<MyTrans_FriendsGroups_Class>> getYouAreOweValues(){
			//youareowedBal.clear();
			if(youareowedBal == null){
				youareowedBal= new SparseArray<ArrayList<MyTrans_FriendsGroups_Class>>();
			}
			
			for(int i = 0; i < group.size(); i++){
				group.get(i).setType("group");
			}
			
			for(int i = 0; i < friend.size(); i++){
				friend.get(i).setType("friend");
			}
			
			youareowedBal.put(0, group);
			youareowedBal.put(1, friend);
			return youareowedBal;
		}
	}
	
	public class you_owe {
		
		public  SparseArray<ArrayList<MyTrans_FriendsGroups_Class>> youoweBal = new SparseArray<ArrayList<MyTrans_FriendsGroups_Class>>();
		
		@SerializedName("total")
		public int total;
		
		@SerializedName("friend")
		public ArrayList<MyTrans_FriendsGroups_Class> friend = new ArrayList<MyTrans_FriendsGroups_Class>();
		
		@SerializedName("group")
		public ArrayList<MyTrans_FriendsGroups_Class> group = new ArrayList<MyTrans_FriendsGroups_Class>();
		
		public SparseArray<ArrayList<MyTrans_FriendsGroups_Class>> getYouOweValues(){
			if(youoweBal == null){
				youoweBal= new SparseArray<ArrayList<MyTrans_FriendsGroups_Class>>();
			}
			
			for(int i = 0; i < group.size(); i++){
				group.get(i).setType("group");
			}
			
			for(int i = 0; i < friend.size(); i++){
				friend.get(i).setType("friend");
			}
			
			youoweBal.put(0, group);
			youoweBal.put(1, friend);
			return youoweBal;
		}
		
	}

}

