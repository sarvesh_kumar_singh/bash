package com.bash.GsonClasses;

import java.util.ArrayList;

import com.bash.ListModels.BashUsers_Class;
import com.google.gson.annotations.SerializedName;
public class Login_Register_Class {
	@SerializedName("result")
	public boolean result;
	
	@SerializedName("msg")
	public String msg;
	
	@SerializedName("iduser")
	public String iduser;
	
	@SerializedName("email_id")
	public String email_id;
	
	@SerializedName("password")
	public String password;
	
	@SerializedName("fullname")
	public String fullname;
	
	@SerializedName("userimage")
	public String userimage;
	
	@SerializedName("PartnerCode")
	public String PartnerCode;
	
	@SerializedName("PartnerUsername")
	public String PartnerUsername;
	
	@SerializedName("PartnerPassword")
	public String PartnerPassword;
	
	@SerializedName("phone_no")
	public String phone_no;
	
	@SerializedName("facebookid")
	public String facebookid;
	
	@SerializedName("pushnotify_on")
	public String pushnotify_on;
	
	@SerializedName("credit")
	public String credit;
	
	
	@SerializedName("bashusers")
	public ArrayList<BashUsers_Class> bashusers = new ArrayList<BashUsers_Class>();

}
