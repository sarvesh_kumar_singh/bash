package com.bash.GsonClasses;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class MyFriend_Transaction_Class {

	@SerializedName("result")
	public boolean result;
	@SerializedName("msg")
	public String msg;
	@SerializedName("iduser")
	public String iduser;
	@SerializedName("name")
	public String name;
	@SerializedName("imagepath")
	public String imagepath;
	@SerializedName("totalbalance")
	public String totalbalance;
	
	@SerializedName("details")
	public ArrayList<details> details = new ArrayList<details>(); 
	
	public class details {
		@SerializedName("month")
		public String month;
		@SerializedName("year")
		public String year;
		@SerializedName("data")
		public ArrayList<data> data = new ArrayList<data>(); 
	}
	
	public class data {
		@SerializedName("idtrans")
		public String idtrans;
		@SerializedName("amount")
		public String  amount;
		@SerializedName("status")
		public String status;
		@SerializedName("feed_comment")
		public String feed_comment;
		@SerializedName("location")
		public String location;
		@SerializedName("imagepath")
		public String imagepath;
		@SerializedName("date")
		public String date;
	}
	
	/*
	
	{
	    "result": true,
	    "msg": "Success",
	    "iduser": "11",
	    "name": "emulator device",
	    "imagepath": "",
	    "totalbalance": 228,
	    "details": [
	        {
	            "month": "November",
	            "year": "2014",
	            "data": [
	                {
	                    "idtrans": "1",
	                    "feed_comment": "birthday treat",
	                    "amount": "100",
	                    "status": "you lent"
	                },
	                {
	                    "idtrans": "2",
	                    "feed_comment": "Bangalore tour",
	                    "amount": "200",
	                    "status": "you lent"
	                },
	                {
	                    "idtrans": "3",
	                    "feed_comment": "good food",
	                    "amount": "100",
	                    "status": "you barowed"
	                },
	                {
	                    "idtrans": "4",
	                    "feed_comment": "coffee day",
	                    "amount": "50",
	                    "status": "you lent"
	                },
	                {
	                    "idtrans": "20",
	                    "feed_comment": "ggo",
	                    "amount": "22",
	                    "status": "you barowed"
	                }
	            ]
	        }
	    ]
	}*/
	
}

