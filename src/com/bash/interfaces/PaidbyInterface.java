package com.bash.interfaces;

import java.util.ArrayList;

import com.bash.ListModels.Person;

public interface PaidbyInterface {
	void onPaidByData(ArrayList<Person> output);
}
