package com.bash.Fragments;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Providers.ContainerProvider;
import com.bash.Utils.AppConstants;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class DepositFragment extends Fragment{

	View mRootView;
	FragmentTabHost mTabhost;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_mywallet_deposit_page, null);
		initializeView();
		initializeTabHost();
		return mRootView;
	}
	
	private void initializeTabHost() {	
		// TODO Auto-generated method stub
		mTabhost = (FragmentTabHost) mRootView.findViewById(R.id.fragmenttabhost);
		//mTabhost.setup(getActivity(), getActivity().getSupportFragmentManager(), R.id.home);
		mTabhost.setup(getActivity(), getChildFragmentManager(), R.id.fragmentframe_holder);
		
		
		View savedCardsView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_mywallet_savedcards, null);
		View debitCardView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_mywallet_debitcards, null);
		View creditCardView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_mywallet_creditcards, null);
		View newBankingView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_mywallet_netbanking, null);
			
/*		((TextView)savedCardsView.findViewById(R.id.tabText)).setText("Saved Cards");
		((TextView)debitCardView.findViewById(R.id.tabText)).setText("Debit Cards");
		((TextView)creditCardView.findViewById(R.id.tabText)).setText("Credit Cards");
		((TextView)newBankingView.findViewById(R.id.tabText)).setText("Net Banking");*/
		
			
		/*View pendingView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_profile_mytrans_pending, null);
		View historyView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_profile_mytrans_history, null);*/
		
	//	mTabhost.setBackgroundColor(Color.parseColor("#939C9E"));
		
		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_MYWALLET_DEPOSIT_SAVEDCARD).setIndicator(savedCardsView),
				ContainerProvider.Tab_MyWallet_Deposit_SavedCards.class, null);
		
		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_MYWALLET_DEPOSIT_DEBITCARD).setIndicator(debitCardView),
				ContainerProvider.Tab_MyWallet_Deposit_DebitCards.class, null);
		
		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_MYWALLET_DEPOSIT_CREDITCARD).setIndicator(creditCardView),
				ContainerProvider.Tab_MyWallet_Deposit_DebitCards.class, null);
		
		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_MYWALLET_DEPOSIT_NEWBANKING).setIndicator(newBankingView),
				ContainerProvider.Tab_MyWallet_Deposit_SavedCards.class, null);
		
		
	}

	private void initializeView() {
		// TODO Auto-generated method stub
		// ((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setBackgroundResource(0);
		 ((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "Deposit", 0);
		 
		 ((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					WalletFragment.parentFragmentClass.popFragment();
				}
		 });
		 
		  
		 
		 
	}
	
	
}