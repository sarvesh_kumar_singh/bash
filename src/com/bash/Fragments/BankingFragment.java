package com.bash.Fragments;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Providers.ContainerProvider;
import com.bash.Utils.AppConstants;

public class BankingFragment extends Fragment{

	View mRootView;
	FragmentTabHost mTabhost, accountTabhost;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_mywallet_banking_page, null);
		initializeView();
		initializeTabHost();
		return mRootView;
	}
	
	@SuppressLint("ResourceAsColor")
	private void initializeTabHost() {
		
		accountTabhost = (FragmentTabHost) mRootView.findViewById(R.id.fragmentcardtabhost);
		accountTabhost.setup(getActivity(), getChildFragmentManager(), R.id.fragmentcardframe_holder);
		
		accountTabhost.setBackgroundColor(R.color.app_shadecolor);
		
		mTabhost = (FragmentTabHost) mRootView.findViewById(R.id.fragmenttabhost);
		mTabhost.setup(getActivity(), getChildFragmentManager(), R.id.fragmentframe_holder);
		
		mTabhost.setBackgroundColor(R.color.app_shadecolor);
		
		View savedView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_mywallet_savedcards, null);
		View debitView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_mywallet_debitcards, null);
		View creditView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_mywallet_creditcards, null);
		
		View newAccountView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_mywallet_newaccount, null);
		View savedAccountView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_mywallet_savedaccounts, null);
		
		accountTabhost.setBackgroundColor(android.R.color.transparent);
		
		accountTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_MYWALLET_BANKING_SAVEDACCOUNT).setIndicator(savedAccountView),
				ContainerProvider.Tab_MyWallet_Banking_SavedAccount.class, null);
		
		accountTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_MYWALLET_BANKING_NEWACCOUNT).setIndicator(newAccountView),
				ContainerProvider.Tab_MyWallet_Banking_NewAccount.class, null);

		mTabhost.setBackgroundColor(android.R.color.transparent);
		
		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_MYWALLET_BANKING_SAVEDCARD).setIndicator(savedView),
				ContainerProvider.Tab_MyWallet_Banking_SavedCard.class, null);
		
		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_MYWALLET_BANKING_DEBITCARD).setIndicator(debitView),
				ContainerProvider.Tab_MyWallet_Banking_SavedCards.class, null);
		
		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_MYWALLET_BANKING_CREDITCARD).setIndicator(creditView),
				ContainerProvider.Tab_MyWallet_Banking_SavedCards.class, null);
	}

	private void initializeView() {
		// TODO Auto-generated method stub
		// ((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setBackgroundResource(0);
		 ((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "Banking", 0);
		 
		 ((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					WalletFragment.parentFragmentClass.popFragment();
				}
		 });
		 
		 ((RelativeLayout) mRootView.findViewById(R.id.bankOptionWindow)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(((RelativeLayout) mRootView.findViewById(R.id.banksWindow)).getVisibility() == View.VISIBLE){
					((RelativeLayout)mRootView.findViewById(R.id.banksWindow)).setVisibility(View.GONE);
					((ImageView)mRootView.findViewById(R.id.bankIndicator)).setImageResource(R.drawable.downarrow);	
				}else{
					((RelativeLayout)mRootView.findViewById(R.id.banksWindow)).setVisibility(View.VISIBLE);
					((ImageView)mRootView.findViewById(R.id.bankIndicator)).setImageResource(R.drawable.uparrow);
				}
				
			}
		}); 
		 
		 ((RelativeLayout) mRootView.findViewById(R.id.cardOptionWindow)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(((RelativeLayout)mRootView.findViewById(R.id.cardWindow)).getVisibility() == View.VISIBLE){
						((RelativeLayout)mRootView.findViewById(R.id.cardWindow)).setVisibility(View.GONE);
						((ImageView)mRootView.findViewById(R.id.cardIndicator)).setImageResource(R.drawable.downarrow);	
					}else{
						((RelativeLayout)mRootView.findViewById(R.id.cardWindow)).setVisibility(View.VISIBLE);
						((ImageView)mRootView.findViewById(R.id.cardIndicator)).setImageResource(R.drawable.uparrow);
					}
					
				}
			}); 
		 
	}
	
	
}
