package com.bash.Fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.BuildConfig;
import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.GsonClasses.Friends_Class;
import com.bash.ListModels.BashUsers_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DataBaseManager;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MyFriendsFragment extends BaseFragment implements AsyncResponse{
	MyAsynTaskManager  myAsyncTask;
	public static View mRootView;
	public static SwipeListView friendsListView;
	public MyFriendsAdapter adapter;
	public ArrayList<BashUsers_Class> feedList;
	Friends_Class responseForFriends;
	int pos=0;
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_myfriends_page, null);
		initializeView();
		return mRootView;
	}
	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//Sarvesh((HomeActivity)getActivity()).menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// Sarvesh ((HomeActivity)getActivity()).menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
	}
	
	private void initializeView() {
		// TODO Auto-generated method stub
		//Initialize Activity Views
		((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((BaseFragment)getParentFragment()).popFragment();
				((HomeActivity)getActivity()).slidingmenu_layout.toggleMenu();
			}
		});
		final ImageButton fab = (ImageButton) mRootView.findViewById(R.id.fab_image_button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.setSelected(!fab.isSelected());
                fab.setImageResource(fab.isSelected() ? R.drawable.animated_plus : R.drawable.animated_minus);
                Drawable drawable = fab.getDrawable();
                if (drawable instanceof Animatable) {
                    ((Animatable) drawable).start();
                }
            }
        });
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn, "Friends", 0);
		
		//friendsListView = (SwipeSwipeListView) mRootView.findViewById(R.id.friendsListView);
		friendsListView = (SwipeListView) mRootView.findViewById(R.id.friendsListView);
		
		friendsListView.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT); // there are five swiping modes
		friendsListView.setSwipeActionLeft(SwipeListView.SWIPE_MODE_DEFAULT); //there are four swipe actions
		friendsListView.setOffsetLeft(PreferenceManager.getInstance().getPercentageFromWidth(38));
		friendsListView.setAnimationTime(50); // animarion time
		friendsListView.setSwipeOpenOnLongPress(true); // enable or disable SwipeOpenOnLongPress
		
		feedList = new ArrayList<BashUsers_Class>();
		
		adapter = new MyFriendsAdapter(getActivity(), feedList);
		
		friendsListView.setAdapter(adapter);
		
		/*friendsListView.setSwipeListViewListener(new BaseSwipeListViewListener() {
			 @Override
	         public void onClickFrontView(int position) {
	             //Toast.makeText(getActivity(), "Clicked", Toast.LENGTH_SHORT).show();
	            // savelistView.openAnimate(position);
				 Log.d("position", position+"");
				 ((BaseFragment)getParentFragment()).replaceFragment
				 			(new MyFriends_Transaction_Fragment(adapter.getItem(position)), true);
	         }
	 
	         @Override
	         public void onClickBackView(int position) {
	             Log.d("swipe", String.format("onClickBackView %d", position));
	             friendsListView.closeAnimate(position);//when you touch back view it will close
	         }
	 
	         @Override
	         public void onDismiss(int[] reverseSortedPositions) {
	        	 
	         }
	     });*/
		
		 // Capture Text in EditText
        ((EditText) mRootView.findViewById(R.id.searchBox)).addTextChangedListener(new TextWatcher() {
 
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = ((EditText) mRootView.findViewById(R.id.searchBox))
                				.getText().toString().toLowerCase(Locale.getDefault());
                adapter.getFilter().filter(text);
            }
 
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                    int arg2, int arg3) {
                // TODO Auto-generated method stub
            }
 
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                    int arg3) {
                // TODO Auto-generated method stub
            }
        });
        
		
		getFriendsList();
		
	}

	public void updateAdapter(ArrayList<BashUsers_Class> newList){
		/*feedList.clear();
		feedList = DataBaseManager.getInstance().getFriendsList();*/
		//this.feedList = newList;
		//feedList = DataBaseManager.getInstance().getFriendsList();
		if(newList != null && newList.size() != 0){
			adapter.notifyWithDataSet(newList);
		} 
	}
public void getFriendsList() {
	myAsyncTask=new MyAsynTaskManager();
	myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("getFriendsList",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser"
				}, 
				new String[]{
				"group",
				"friendlist",
				PreferenceManager.getInstance().getUserId()
				});
myAsyncTask.execute();
//		new MyAsynTaskManager(getActivity(), new LoadListener() {
//			@Override
//			public void onLoadComplete(final String jsonResponse) {
//				getActivity().runOnUiThread(new Runnable() {
//					@Override
//					public void run() {}
//				});
//			}
//			@Override
//			public void onError(final String errorMessage)  {
//				getActivity().runOnUiThread(new Runnable() {
//					public void run() {
//						DialogManager.showDialog(getActivity(), errorMessage);		
//					}
//				});
//				
//			}
//		}).execute();
	}
public class MyFriendsAdapter extends BaseAdapter implements Filterable
{
	 	public ArrayList<BashUsers_Class> feedList = new ArrayList<BashUsers_Class>();
	 	public ArrayList<BashUsers_Class> originalList = new ArrayList<BashUsers_Class>();
	 	public Activity context;
	 	public LinearLayout.LayoutParams backViewParams;
	    //private BashUsers_Class emptyList = new BashUsers_Class("empty", 0);
	    public FriendFilter filter;
	    
	    public MyFriendsAdapter(Activity context, ArrayList<BashUsers_Class> feedList) {
	        this.context = context;
	    	this.feedList = feedList;
	    	this.originalList = feedList;
	    	backViewParams = new LinearLayout.LayoutParams(PreferenceManager.getInstance().getPercentageFromWidth(20), 
	    			LayoutParams.MATCH_PARENT);
	    }
	    
	    public void notifyWithDataSet(ArrayList<BashUsers_Class> newlist){
	    	this.feedList.clear();
	    	this.feedList = newlist;
	    	this.originalList = newlist;
	    	Log.e("Size of Adapter", String.valueOf(feedList.size()));
	    	this.notifyDataSetChanged();
	    }

	    @Override
	    public View getView(final int position, View convertView, ViewGroup parent) {
	        ViewHolder holder = null;
	        BashUsers_Class listItem = getItem(position);
	        if (convertView == null) {
	        	LayoutInflater inflater = LayoutInflater.from(context);
	        	if(convertView == null)
	        	 {
	        	  	convertView = inflater.inflate(R.layout.custom_myfriendongroup_listview, null);
		            holder = new ViewHolder();
		            holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
		            holder.friendImageSource = (ImageView) convertView.findViewById(R.id.friendImageSource);
		            holder.deleteFriend = (ImageView) convertView.findViewById(R.id.deleteFriend);
		            holder.backView = (RelativeLayout) convertView.findViewById(R.id.backView);
		            holder.tviCharge = (TextView) convertView.findViewById(R.id.tviCharge);
		            holder.tviPay = (TextView) convertView.findViewById(R.id.tviPay);
	        	}
	            convertView.setTag(holder);
	        } else {
	            holder = (ViewHolder) convertView.getTag();
	        }
	        Log.e("String from Adpater", listItem.getphone_no());
	        
	        if(!listItem.getphone_no().equals("0")){
	        	
	        	holder.backView.setLayoutParams(backViewParams);
	        	//holder.friendImageSource.setImageResource(listItem.getfriendImageSource());
	        	holder.friendName.setText(listItem.getname());
	        
	        	holder.deleteFriend.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						pos=position;
						// TODO Auto-generated method stub
						removeFriendFromWebservice(pos);
						/*removeItemFromList(position);
						MyFriendsFragment.friendsListView.closeAnimate(position);*/
					}
				});
	        	holder.friendName.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						((BaseFragment)getParentFragment()).replaceFragment
			 			(new MyFriends_Transaction_Fragment(adapter.getItem(position)), true);
					}
				});
	        	holder.tviCharge.setOnClickListener(new OnClickListener() {
	 				@Override
	 				public void onClick(View v) {
	 					// TODO Auto-generated method stub
	 					pos=position;
	 					Toast.makeText(context, "Charge will Come soon...", Toast.LENGTH_LONG).show();
	 				}
	 			});
	 	      holder.tviPay.setOnClickListener(new OnClickListener() {
	 				@Override
	 				public void onClick(View v) {
	 					// TODO Auto-generated method stub
	 					pos=position;
	 					Toast.makeText(context, "Pay will Come soon...", Toast.LENGTH_LONG).show();
	 				}
	 			});
	        }
	        
	        if(listItem.getimagepath() != null && listItem.getimagepath().length()!= 0 && holder.friendImageSource != null){
	        	ImageLoader.getInstance().displayImage(listItem.getimagepath(), 
	        			holder.friendImageSource, BashApplication.options, BashApplication.animateFirstListener);
	        }else{
	        	holder.friendImageSource.setImageResource(R.drawable.addphoto_img_block);
	        }
	        
	        return convertView;
	    }

	    
	   /* public void checkEmptyList(){
	    	if(feedList.size() == 0){
				this.feedList.add(emptyList);
			}
	    }
	    */
	    @Override
	    public int getCount() {
	    	//	checkEmptyList();
	        return feedList.size();
	    }

	    @Override
	    public BashUsers_Class getItem(int position) {
	        return feedList.get(position);
	    }

	    @Override
	    public long getItemId(int position) {
	        return position;
	    }

	    private class ViewHolder {
	        TextView friendName,tviCharge,tviPay;
	        ImageView friendImageSource, deleteFriend;
	        RelativeLayout backView;
	    }
	    
	    

	    @Override
		public Filter getFilter() {
			// TODO Auto-generated method stub
			  if (filter == null)
				  filter = new FriendFilter();
			    return filter;
		}
		 
		//filter Class... 
			private class FriendFilter extends Filter {
				@Override
				protected FilterResults performFiltering(CharSequence constraint) {
					FilterResults results = new FilterResults();
					// We implement here the filter logic
					if (constraint == null || constraint.length() == 0) {
						// No filter implemented we return all the list
						results.values = originalList;
						results.count = originalList.size();
					} else {
						// We perform filtering operation
						List<BashUsers_Class> tempList = new ArrayList<BashUsers_Class>();
						for (BashUsers_Class p : feedList) {
							if (p.getname().toUpperCase()
									.startsWith(constraint.toString().toUpperCase()))
								tempList.add(p);
						}
						results.values = tempList;
						results.count = tempList.size();
					}
					return results;
				}

				@Override
				protected void publishResults(CharSequence constraint,
						FilterResults results) {
					// Now we have to inform the adapter about the new list filtered
					if (results.count == 0)
						notifyDataSetInvalidated();
					else {
						feedList = (ArrayList<BashUsers_Class>) results.values;
						notifyDataSetChanged();
					}
				}
			}
	    

}
public void removeItemFromList(int position)
{
	DataBaseManager.getInstance().unFriendwithUserId(this.feedList.get(position).idfriend);
	feedList.remove(position);
	adapter.notifyDataSetInvalidated();
	adapter.notifyDataSetChanged();
}
public void removeFriendFromWebservice(final int position) {
	myAsyncTask=new MyAsynTaskManager();
	myAsyncTask.delegate=this;
	myAsyncTask.setupParamsAndUrl("removeFriendFromWebservice",getActivity(),AppUrlList.ACTION_URL, new String[] {
			"module",
			"action",
			"iduser",
			"idfriend"
			}, 
			new String[]{
			"user",
			"removefriend",
			PreferenceManager.getInstance().getUserId(),
			feedList.get(position).getisFriend()
			});
	//getisFriend()
	Log.e("***************", "**************");
	
	Log.e("My User Id", PreferenceManager.getInstance().getUserId());
	Log.e("Removing Friend Id", feedList.get(position).getisFriend());
	myAsyncTask.execute();
	
}
@Override
public void backgroundProcessFinish(String from, String output) {
	// TODO Auto-generated method stub
	ArrayList<BashUsers_Class> newList1 = new ArrayList<BashUsers_Class>();
	
	newList1.add(new BashUsers_Class
			("1", "123456",	"Aphrodit",	"",	"FALSE", "FALSE", "TRUE"));
	newList1.add(new BashUsers_Class
			("2", "123456",	"Athena",	"",	"FALSE", "FALSE", "TRUE"));
	newList1.add(new BashUsers_Class
			("3", "123456",	"Hera",	"",	"FALSE", "FALSE", "TRUE"));
	newList1.add(new BashUsers_Class
			("4", "123456",	"Jeus",	"",	"FALSE", "FALSE", "TRUE"));
	updateAdapter(newList1);
	if(from.equalsIgnoreCase("getFriendsList"))
	{
		if(output!=null)
		{

			
			try {
			if(BuildConfig.DEBUG)
					Log.e("Json Response", output);
			Gson gson = new Gson();
			
			responseForFriends = gson.fromJson(output, Friends_Class.class);
				
			if(responseForFriends.result){
				
				DataBaseManager.getInstance().storeFriendsList(responseForFriends.friendlist);
				//updateAdapter();
				//adapter.notifyDataSetChanged();
				//newList.clear();
				
				ArrayList<BashUsers_Class> newList = new ArrayList<BashUsers_Class>();
				
				for(int i =0; i<responseForFriends.friendlist.size(); i++){
					
					Log.e("Friend Id", responseForFriends.friendlist.get(i).idfriend);
					Log.e("Friend number", responseForFriends.friendlist.get(i).phone_no);
					Log.e("Friend name", responseForFriends.friendlist.get(i).name);
					Log.e("Friend imagepath", responseForFriends.friendlist.get(i).imagepath);
					
					newList.add(new BashUsers_Class
							(responseForFriends.friendlist.get(i).idfriend, 
								responseForFriends.friendlist.get(i).phone_no,
								responseForFriends.friendlist.get(i).name, 
								responseForFriends.friendlist.get(i).imagepath,
								"FALSE", 
								"FALSE", 
								"TRUE"));
				}
				
				//Log.e("Size of file", String.valueOf(feedList.size()));
				updateAdapter(newList);
				 
				
			}else{
				//((RelativeLayout) mRootView.findViewById(R.id.noListLayout)).setVisibility(View.VISIBLE);
				DialogManager.showDialog(getActivity(), "No Friends Found!");
				//updateAdapter(newList);
			}
			} catch (Exception e) {
				// TODO: handle exception
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			}
	
		}
		else
		{

			// TODO: handle exception
			DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
		
			
		}
	}
	else if(from.equalsIgnoreCase("removeFriendFromWebservice"))
	{
		if(output!=null)
		{
			try {
				JSONObject rootObj = new JSONObject(output);
				if(rootObj.getBoolean("result")){
					MyFriendsFragment.friendsListView.closeAnimate(pos);
					removeItemFromList(pos);
					DialogManager.showDialog(getActivity(), "Friend Removed Successfully!");
				}
				else{
					DialogManager.showDialog(getActivity(), "Error Occured to Remove Friend! Try Again!");
				}
			} catch (Exception e) {
				// TODO: handle exception
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			}
		}else
		{
			// TODO: handle exception
			DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
		}
	}
	
}
	 
	
}
