package com.bash.Fragments;

import org.json.JSONObject;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Activities.BashLandingPage;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.Managers.ActivityManager;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynImageTaskManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sromku.simple.fb.Permission.Type;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;

public class EditProfileFragment extends Fragment implements AsyncResponse{
	MyAsynTaskManager  myAsyncTask;
	View mRootView, deleteAlertView, changepasswordView;
	Dialog deleteDialog, changePasswordDialog;
	View camera_gallery, pin_webview;
	Dialog camaeraDialog, pinWebDialog;
	WebView alertWebView;
	Uri mCapturedImageURI;
	public ImageView fileImage;
	String picturePath="";
	private SimpleFacebook mSimpleFacebook;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		
		mRootView = inflater.inflate(R.layout.fragment_editprofile_page, null);
		initializeWebAlertView();
		initializeViews();
		initializeAlertView();
		initializeChangePasswordAlertView();
		initializePictureAlertView();
		((HomeActivity) getActivity()).currentFragment = this;
		return mRootView;
	}
	
	private void initializeViews() {
		 
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "Edit Profile", 0);
		 
		((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((BaseFragment)getParentFragment()).popFragment();
			}
		});
		 
		((ImageView)mRootView.findViewById(R.id.updateButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(((CheckBox)mRootView.findViewById(R.id.iacceptCheckbox)).isChecked())
					saveProfileInfoInServer();
				else
					Toast.makeText(getActivity(), "Please accept Terms & Conditions", Toast.LENGTH_SHORT).show();
			}
		});
		 
		//((EditText)	mRootView.findViewById(R.id.lastName)).setText(PreferenceManager.getInstance().getUserLastName());
		
		((EditText)	mRootView.findViewById(R.id.firstNameEditText)).setText(PreferenceManager.getInstance().getUserFullName());

		fileImage = (ImageView) mRootView.findViewById(R.id.userBigImage);
		
		if(PreferenceManager.getInstance().getUserImagePath() != null
				&& PreferenceManager.getInstance().getUserImagePath().length() != 0){
			/*ImageLoader.getInstance().displayImage(PreferenceManager.getInstance().getUserImagePath(), 
					((ImageView)mRootView.findViewById(R.id.userBigImage)), BashApplication.options, BashApplication.animateFirstListener);*/
			ImageLoader.getInstance().displayImage(PreferenceManager.getInstance().getUserImagePath(), 
					((ImageView)mRootView.findViewById(R.id.userBigImage)), BashApplication.imageOptions);
			/*ImageLoader.getInstance().displayImage(PreferenceManager.getInstance().getUserImagePath(), 
					((ImageView)mRootView.findViewById(R.id.userBigImage)));*/
			/*ImageLoader.getInstance().displayImage(PreferenceManager.getInstance().getUserImagePath(), 
					((ImageView)mRootView.findViewById(R.id.userSmallImage)));*/
		}
			
		((ImageView)mRootView.findViewById(R.id.userBigImage)).setLayoutParams(new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT, 
				PreferenceManager.getInstance().getPercentageFromWidth(35)));
			
	/*	fileImage = (ImageView) mRootView.findViewById(R.id.userSmallImage);
		
		fileImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				camaeraDialog.show();	
			}
		});*/
		
		((ImageView)mRootView.findViewById(R.id.userBigImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				camaeraDialog.show();	
			}
		});
		
		mSimpleFacebook = SimpleFacebook.getInstance(getActivity());
		
		((ImageView)mRootView.findViewById(R.id.facebookConnectButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				makeFacebookLogin();
			}
		});
		
		if(PreferenceManager.getInstance().getUserFacebookId() == null || 
				PreferenceManager.getInstance().getUserFacebookId().length() == 0){
			((ImageView)mRootView.findViewById(R.id.facebookConnectButton)).setVisibility(View.VISIBLE);	
		}else{
			((ImageView)mRootView.findViewById(R.id.facebookConnectButton)).setVisibility(View.GONE);
		}
		
		/*((CheckBox)mRootView.findViewById(R.id.deleteAccount)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked){
					deleteDialog.show();
					((CheckBox)mRootView.findViewById(R.id.deleteAccount)).setChecked(false);
				}
			}
		});
		
		((CheckBox)mRootView.findViewById(R.id.changepasswordImage)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				changePasswordDialog.show();	
			}
		});
		
		((CheckBox)mRootView.findViewById(R.id.changepasswordImage)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				changePasswordDialog.show();	
			}
		});
		*/
		
		if(PreferenceManager.getInstance().getUserMobileNumber() != null)
			((EditText) mRootView.findViewById(R.id.phoneNoText)).setText(PreferenceManager.getInstance().getUserMobileNumber());
		if(PreferenceManager.getInstance().getUserEmailId() != null)
			((EditText) mRootView.findViewById(R.id.emailIdText)).setText(PreferenceManager.getInstance().getUserEmailId());
			}
 
	private void initializePictureAlertView() {
		// TODO Auto-generated method stub
		camera_gallery = View.inflate(getActivity(),
				R.layout.alert_gallerycamera_mode_view, null);
		camaeraDialog = new Dialog(getActivity());
		camaeraDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		camaeraDialog.setContentView(camera_gallery);
 
		((Button) camera_gallery.findViewById(R.id.openGalleryBtn))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						camaeraDialog.dismiss();
						Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						getActivity().startActivityForResult(i,	AppConstants.CODE_GALLERY);
					}
		});

		((Button) camera_gallery.findViewById(R.id.openCameraBtn)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						camaeraDialog.dismiss();
						String fileName = "temp.jpg";
						ContentValues values = new ContentValues();
						values.put(MediaStore.Images.Media.TITLE, fileName);
						mCapturedImageURI = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
										values);
						Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
						intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
						getActivity().startActivityForResult(intent, AppConstants.CODE_CAMERA);
					}
		});

	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);
		
		switch (requestCode) {
		case AppConstants.CODE_CAMERA:
			if (resultCode == getActivity().RESULT_OK) {
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(
						mCapturedImageURI, projection, null, null, null);
				int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				picturePath = cursor.getString(column_index_data);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,	fileImage, BashApplication.imageOptions);
				Log.e("Camera Path", picturePath);
			}
			break;
		case AppConstants.CODE_GALLERY:
			if (resultCode == getActivity().RESULT_OK) {

				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				picturePath = cursor.getString(columnIndex);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,	fileImage, BashApplication.imageOptions);
				//ImageLoader.getInstance().displayImage(picturePath,	fileImage);
			}
			break;
		default:
			break;
		}
		
		mSimpleFacebook.onActivityResult(getActivity(), requestCode, resultCode, data);
	}
	
	
	private void makeFacebookLogin(){
		mSimpleFacebook.getInstance().login(new OnLoginListener() {
			@Override
			public void onFail(String reason) {
				Toast.makeText(getActivity(),"Fb Login Failed!", Toast.LENGTH_SHORT).show();					
			}
			
			@Override
			public void onException(Throwable throwable) {
				Toast.makeText(getActivity(),"Fb Login Failed!", Toast.LENGTH_SHORT).show();
				
			}
			
			@Override
			public void onThinking() {
				
			}
			
			@Override
			public void onNotAcceptingPermissions(Type type) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onLogin() {
				Toast.makeText(getActivity(),"Fb Login Success!", Toast.LENGTH_SHORT).show();
				//SimpleFacebook.getInstance().get
				mSimpleFacebook.getProfile(onProfileListener);
			}
		});
	}
	
	Profile userProfile;
	
	OnProfileListener onProfileListener = new OnProfileListener() {         
	    @Override
	    public void onComplete(Profile profile) {
	    //    Log.i(TAG, "My profile id = " + profile.getId());
	    	android.util.Log.e("profile Id", profile.getId());
	    	android.util.Log.e(" ", "My email id = " + profile.getEmail());
	    	android.util.Log.e(" ", "My birthday id = " + profile.getBirthday());
	    	android.util.Log.e(" ", "My name id = " + profile.getName());
	    	android.util.Log.e(" ", "My gender id = " + profile.getGender());
	    	userProfile = profile;
	    	makeFacebookEntryInServer(userProfile.getId());
	    }
	};
	
	
public void changeBashPinProcess() 
{
	myAsyncTask=new MyAsynTaskManager();
	myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("changeBashPinProcess",getActivity(),AppUrlList.CHANGE_BASHPIN_URL, new String[] {
				"partnertransactionid",
				"partnercode",
				"username",
				"password",
				"servicetype",
				"usernumber"
				}, 
				new String[]{
				"123456",
				"MOBG",
				"MOBG",
				"9641",
				"CP",
				PreferenceManager.getInstance().getUserMobileNumber()
				}
		);
		myAsyncTask.execute();
		
	}
	
private void initializeWebAlertView(){
	
	pin_webview = View.inflate(getActivity(),
			R.layout.alert_showpinalert_dialog, null);
	pinWebDialog = new Dialog(getActivity());
	pinWebDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	pinWebDialog.setContentView(pin_webview);

	alertWebView = ((WebView) pin_webview.findViewById(R.id.webView));
	
	WebSettings settings = alertWebView.getSettings();
    /*settings.setUseWideViewPort(true);
    settings.setLoadWithOverviewMode(true);*/
    settings.setJavaScriptEnabled(true);
    //alertWebView.setVerticalScrollBarEnabled(true);
    alertWebView.setHorizontalScrollBarEnabled(true);
    
 /*   alertWebView.setWebViewClient(new WebViewClient(){
    	@Override
    	public void onPageFinished(WebView view, String url) {
    		// TODO Auto-generated method stub
    		super.onPageFinished(view, url);
    		view.loadUrl(url);
    	}
    });*/
}

	
	
	public void makeFacebookEntryInServer(final String facebookId) {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("makeFacebookEntryInServer",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser",
				"facebookid"
				}, 
				new String[]{
				"user",
				"updatefacebookid",
				PreferenceManager.getInstance().getUserId(),
				facebookId
				}
		);
		myAsyncTask.execute();
		
	
	}
	
	

	
	private void initializeChangePasswordAlertView() {
		changePasswordDialog = new Dialog(getActivity());
		changePasswordDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		changepasswordView = View.inflate(getActivity(), R.layout.alert_change_bashpassword_dialog, null);
		changePasswordDialog.setContentView(changepasswordView);
		
		((Button)changepasswordView.findViewById(R.id.saveButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(ValidatePasswordForm()){
					changePasswordInServer();
					changePasswordDialog.dismiss();	
				}
			}
		});
		
		((Button)changepasswordView.findViewById(R.id.cacelButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				changePasswordDialog.dismiss();
			}
		});
	}
	
	public boolean ValidatePasswordForm(){
		
		if(((EditText)changepasswordView.findViewById(R.id.oldPinText)).getText() == null ||
				((EditText)changepasswordView.findViewById(R.id.oldPinText)).getText().toString().length() == 0){
			((EditText)changepasswordView.findViewById(R.id.oldPinText)).setError("Fill up Old Password!");
			return false;			
		}else if(((EditText)changepasswordView.findViewById(R.id.newPinText)).getText() == null ||
				((EditText)changepasswordView.findViewById(R.id.newPinText)).getText().toString().length() == 0){
			((EditText)changepasswordView.findViewById(R.id.newPinText)).setError("Fill up Old Password!");
			return false;			
		}else if(((EditText)changepasswordView.findViewById(R.id.retypePinText)).getText() == null ||
				((EditText)changepasswordView.findViewById(R.id.retypePinText)).getText().toString().length() == 0){
			((EditText)changepasswordView.findViewById(R.id.retypePinText)).setError("Fill up Old Password!");
			return false;			
		}else if(!((EditText)changepasswordView.findViewById(R.id.newPinText)).getText().toString().equals(
				((EditText)changepasswordView.findViewById(R.id.retypePinText)).getText().toString())){
			Toast.makeText(getActivity(), "New and Retype Passwords Doesn't Match!", Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}
	
	public void saveProfileInfoInServer() {
		
		MyAsynImageTaskManager.setupParamsAndUrl(AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser",
				"firstname",
				"lastname",
				"userimage"
				}, 
				new String[]{
				"user",
				"update",
				PreferenceManager.getInstance().getUserId(),
				(((EditText)mRootView.findViewById(R.id.firstNameEditText)).getText() == null) ? "" :
					((EditText)mRootView.findViewById(R.id.firstNameEditText)).getText().toString(),
				(((EditText)mRootView.findViewById(R.id.lastName)).getText() == null) ? "" :
					((EditText)mRootView.findViewById(R.id.lastName)).getText().toString(),
				picturePath
				},
				new String[]{
				"0", "0", "0", "0", "0", "1"
			}
		);
		
		new MyAsynImageTaskManager(getActivity(), new MyAsynImageTaskManager.LoadListener() {
			@Override
			public void onLoadComplete(String jsonResponse) {
				try {
					Log.e("Response", jsonResponse);
					JSONObject rootObj = new JSONObject(jsonResponse);
					if(rootObj.getBoolean("result")){
						String pathee = rootObj.getString("imagepath");
						String patheev = rootObj.getString("fullname");
						PreferenceManager.getInstance().setUpdatedUserDetails(
								rootObj.getString("fullname"), 
								rootObj.getString("imagepath"));
						
						((HomeActivity)getActivity()).applySlidingMenuValues();
							
						Toast.makeText(getActivity(), "Profile Updated Successfully!", Toast.LENGTH_SHORT).show();
						((BaseFragment)getParentFragment()).popFragment();	
						//Sarvesh
						/*ImageLoader.getInstance().displayImage(PreferenceManager.getInstance().getUserImagePath(),
								((ImageView) ((HomeActivity)getActivity()).menu.getMenu().findViewById(R.id.profileImage)), BashApplication.imageOptions);*/
						
					}else{
						Toast.makeText(getActivity(), "Error in Updating Profile Info!", Toast.LENGTH_SHORT).show();
					}
					
					
				} catch (Exception e) {
					DialogManager.showDialog(getActivity(), "Error Occured in Storing Profile Info!");
					e.printStackTrace();
				}
			}
			@Override
			public void onError(final String errorMessage) {
				getActivity().runOnUiThread(new Runnable() {
					public void run() {
						DialogManager.showDialog(getActivity(), errorMessage);		
					}
				});
				
			}
		}).execute();
	}
	
	
	private void initializeAlertView() {
		// TODO Auto-generated method stub
		deleteDialog = new Dialog(getActivity());
		deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		deleteAlertView = View.inflate(getActivity(), R.layout.alert_deleteaccount_dialog, null);
		deleteDialog.setContentView(deleteAlertView);
		
		((ImageView)deleteAlertView.findViewById(R.id.deleteImageButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				deleteUserAccountDetails();
				deleteDialog.dismiss();
			}
		});
		
		((ImageView)deleteAlertView.findViewById(R.id.cancelImageButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				deleteDialog.dismiss();;
			}
		});
		
		((ImageView)deleteAlertView.findViewById(R.id.cancelDialog)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				deleteDialog.dismiss();
			}
		});
		
	}
	

	public void changePasswordInServer() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("changePasswordInServer",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser",
				"newpassword",
				"oldpassword"
				}, 
				new String[]{
				"user",
				"changepassword",
				PreferenceManager.getInstance().getUserId(),
				((EditText)changepasswordView.findViewById(R.id.retypePinText)).getText().toString(),
				((EditText)changepasswordView.findViewById(R.id.oldPinText)).getText().toString(),
				}
		);
		myAsyncTask.execute();
		
	
	}
	
	
	public void deleteUserAccountDetails() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("deleteUserAccountDetails",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser"
				}, 
				new String[]{
				"user",
				"update",
				PreferenceManager.getInstance().getUserId()
			}
		);
		myAsyncTask.execute();
		
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		if(from.equalsIgnoreCase("changeBashPinProcess"))
		{
			if(output!=null)
			{

				try {
					Log.e("Response", output);
					alertWebView.loadDataWithBaseURL("", output, "", "", "");
					pinWebDialog.show();
					
				} catch (Exception e) {
					DialogManager.showDialog(getActivity(), "Error Occured in Storing Profile Info!");
					e.printStackTrace();
				}
			
			}
			else
			{
				DialogManager.showDialog(getActivity(), "Error Occured in Storing Profile Info!");
			}
			
		}
		else if(from.equalsIgnoreCase("makeFacebookEntryInServer"))
		{
			if(output!=null)
			{
				try {
					Log.e("Response", output);
					JSONObject rootObj = new JSONObject(output);
					if(rootObj.getBoolean("result")){
							Toast.makeText(getActivity(), "Bash Connected With Facebook!", Toast.LENGTH_SHORT).show();
							((ImageView)mRootView.findViewById(R.id.facebookConnectButton)).setVisibility(View.GONE);
							PreferenceManager.getInstance().setUserFacebookId(userProfile.getId());
						}else{
							
							Toast.makeText(getActivity(), "Error In Connecting Bash to Facebook! Try Again!", Toast.LENGTH_SHORT).show();
						}
					
				} catch (Exception e) {
					DialogManager.showDialog(getActivity(), "Error Occured in Storing Profile Info!");
					e.printStackTrace();
				}
			}
			else
			{
				DialogManager.showDialog(getActivity(), "Error Occured in Storing Profile Info!");
			}
			
		}
		else if(from.equalsIgnoreCase("changePasswordInServer"))
		{
		if(output!=null)
		{

			try {
				Log.e("Response", output);
				JSONObject rootObj = new JSONObject(output);
				if(rootObj.getBoolean("result")){
						Toast.makeText(getActivity(), "Password Changed Successfully!", Toast.LENGTH_SHORT).show();
					}else{
						
						Toast.makeText(getActivity(), "Old Password is Incorrect! Try Again!", Toast.LENGTH_SHORT).show();
					}
				
			} catch (Exception e) {
				DialogManager.showDialog(getActivity(), "Error Occured in Storing Profile Info!");
				e.printStackTrace();
			}
		
		}
		else
		{
			DialogManager.showDialog(getActivity(), "Error Occured in Storing Profile Info!");
		}
		}
		else if(from.equalsIgnoreCase("deleteUserAccountDetails"))
		{
			if(output!=null)
			{

				try {
					Log.e("Response", output);
					JSONObject rootObj = new JSONObject(output);
					if(rootObj.getBoolean("result")){
						if(rootObj.getString("msg").equals("you owe")){
							Toast.makeText(getActivity(), "You Owe to Your Friend!", Toast.LENGTH_SHORT).show();
						}else if(rootObj.getString("msg").equals("you are owe")){
							Toast.makeText(getActivity(), "You are Owed Rs. "+rootObj.getString("amount")+" to you Friend!", Toast.LENGTH_SHORT).show();
						}else{
							Toast.makeText(getActivity(), "Account Deleted Successfully!", Toast.LENGTH_SHORT).show();
							PreferenceManager.getInstance().resetUserDetails();
							ActivityManager.startActivity(getActivity(), BashLandingPage.class);
							getActivity().finish();
						}

					}


				} catch (Exception e) {
					DialogManager.showDialog(getActivity(), "Error Occured in Storing Profile Info!");
					e.printStackTrace();
				}

			}
			else
			{

				DialogManager.showDialog(getActivity(), "Error Occured in Storing Profile Info!");
			}
		}
	}

}