package com.bash.Fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.ListModels.MyFriendOnGroup_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;
import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.nostra13.universalimageloader.core.ImageLoader;

@SuppressLint("ValidFragment")
public class MyFriendOnGroupFragment extends Fragment implements AsyncResponse{
	int pos=0;
	MyAsynTaskManager  myAsyncTask;
	View mRootView, addgroupAlertView;
	public static SwipeListView myFriendOnGroupListView;
	Dialog addmemberDialog;
	MyFriendOnGroupAdapter adapter;
	public static String groupName = "Group", groupId;
	ArrayList<MyFriendOnGroup_Class> feedList = new ArrayList<MyFriendOnGroup_Class>();
	
	public MyFriendOnGroupFragment(String groupName, String groupId) {
		this.groupName = groupName;
		this.groupId = groupId;
	}
	
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		mRootView = inflater.inflate(R.layout.fragment_mygroups_page, null);
		return mRootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initializeView();
	}
	
	private void initializeView() {
		// TODO Auto-generated method stub
		 ((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, groupName, R.drawable.add_btn);
		 //Sarvesh ((HomeActivity)getActivity()).menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
		 ((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					((BaseFragment)getParentFragment()).popFragment();
				}
		 });
		 
		 ((ImageView)getActivity().findViewById(R.id.toprightsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					((BaseFragment)getParentFragment()).replaceFragment(new AddFriendsOnGroupFragment(groupId), true);
					//addmemberDialog.show();
				}
		 });
		 
		myFriendOnGroupListView = (SwipeListView)mRootView.findViewById(R.id.groupListView);
		
		adapter = new MyFriendOnGroupAdapter(getActivity(), feedList);
		
		myFriendOnGroupListView.setAdapter(adapter);
		
		myFriendOnGroupListView.setSwipeListViewListener(new BaseSwipeListViewListener() {
			 @Override
	         public void onClickFrontView(int position) {
	             //Toast.makeText(getActivity(), "Clicked", Toast.LENGTH_SHORT).show();
	            // savelistView.openAnimate(position);
	         }
	 
	         @Override
	         public void onClickBackView(int position) {
	             Log.d("swipe", String.format("onClickBackView %d", position));
	             myFriendOnGroupListView.closeAnimate(position);//when you touch back view it will close
	         }
	 
	         @Override
	         public void onDismiss(int[] reverseSortedPositions) {
	         }
	     });
		
		   	myFriendOnGroupListView.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT); // there are five swiping modes
		   	myFriendOnGroupListView.setSwipeActionLeft(SwipeListView.SWIPE_MODE_DEFAULT); //there are four swipe actions
		   	myFriendOnGroupListView.setOffsetLeft(PreferenceManager.getInstance().getPercentageFromWidth(80));
		   	myFriendOnGroupListView.setAnimationTime(50); // animarion time
		   	myFriendOnGroupListView.setSwipeOpenOnLongPress(true); // enable or disable SwipeOpenOnLongPress
		   	
		   	getFriendsinGroupDetails();
	}

	 
 	public void getFriendsinGroupDetails() {
 		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("getFriendsinGroupDetails",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"idgroup",
				"iduser"
				}, 
				new String[]{
				"group",
				"friendsofgroup",
				groupId,
				PreferenceManager.getInstance().getUserId()
				});

		myAsyncTask.execute();
	}
 	public class MyFriendOnGroupAdapter extends BaseAdapter
 	{
 		 	private ArrayList<MyFriendOnGroup_Class>  feedList = new ArrayList<MyFriendOnGroup_Class>();
 			private Activity context;
 		    private LinearLayout.LayoutParams backViewParams;
 		    public MyFriendOnGroupAdapter(Activity context, ArrayList<MyFriendOnGroup_Class> feedList) {
 		        this.context = context;
 		    	this.feedList = feedList;
 		    	backViewParams = new LinearLayout.LayoutParams(PreferenceManager.getInstance().getPercentageFromWidth(20), 
 		    			LayoutParams.MATCH_PARENT);
 		    }

 		    @Override
 		    public View getView(final int position, View convertView, ViewGroup parent) {
 		        ViewHolder holder = null;
 		        MyFriendOnGroup_Class listItem = getItem(position);
 		        if (convertView == null) {
 		        	LayoutInflater inflater = LayoutInflater.from(context);
 		        	convertView = inflater.inflate(R.layout.custom_myfriendongroup_listview, null);
 		            holder = new ViewHolder();
 		            holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
 		            holder.friendImageSource = (ImageView) convertView.findViewById(R.id.friendImageSource);
 		            holder.deleteFriend = (ImageView) convertView.findViewById(R.id.deleteFriend);
 		            holder.backView = (RelativeLayout) convertView.findViewById(R.id.backView);
 		            convertView.setTag(holder);
 		        } else {
 		            holder = (ViewHolder) convertView.getTag();
 		        }
 		        
 		        holder.backView.setLayoutParams(backViewParams);
 		        
 		        if(listItem.getfriendImageSource() != null && listItem.getfriendImageSource().length() != 0){
 		        	ImageLoader.getInstance().displayImage(listItem.getfriendImageSource(), 
 		        			holder.friendImageSource, BashApplication.options, BashApplication.animateFirstListener);
 		        }
 		        
 		        holder.friendName.setText(listItem.getfriendName());
 		        
 		        holder.deleteFriend.setOnClickListener(new OnClickListener() {
 					@Override
 					public void onClick(View v) {
 						// TODO Auto-generated method stub
 						pos=position;
 						removeFriendFromGroupWebservice(pos);
 					}
 				});
 		        
 		        return convertView;
 		    }

 		    
 		    
 		    @Override
 		    public int getCount() {
 		        return feedList.size();
 		    }

 		    @Override
 		    public MyFriendOnGroup_Class getItem(int position) {
 		        return feedList.get(position);
 		    }

 		    @Override
 		    public long getItemId(int position) {
 		        return position;
 		    }

 		    private class ViewHolder {
 		        TextView friendName;
 		        ImageView friendImageSource, deleteFriend;
 		        RelativeLayout backView;
 		    }
 		    
 		    
 		    

 	}
 	public void removeItemFromList(int position)
		{
	    	MyFriendOnGroupFragment.myFriendOnGroupListView.closeAnimate(position);
			feedList.remove(position);
			adapter.notifyDataSetInvalidated();
			adapter.notifyDataSetChanged();
	    }
	@Override
	public void backgroundProcessFinish(String from, String output) {

if(from.equalsIgnoreCase("getFriendsinGroupDetails"))
{
	if(output!=null)
	{

		try {
	//		11-13 11:50:28.172: E/Reponse :(21432): {"result":false,"msg":"No friends found"}
	
			JSONObject rootObj = new JSONObject(output);
			if(rootObj.getBoolean("result")) {
				
					JSONArray groupList = rootObj.getJSONArray("friendlist");
					JSONObject item;
					feedList.clear();
						for(int i = 0;i < groupList.length(); i++){
							item = groupList.getJSONObject(i);
							feedList.add(new MyFriendOnGroup_Class(
									item.getString("idfriend"), 
									"",
									item.getString("phone_no"), 
									item.getString("name"),
									item.getString("imagepath"),
									"0",
									"0"
									));
						}
						adapter.notifyDataSetChanged();
				/*}else if(rootObj.getInt("status_code") == 307){
					DialogManager.showDialog(getActivity(), "No Friends Found in Group!");
				}*/
			}
		else{ 
			DialogManager.showDialog(getActivity(), rootObj.getString("msg"));
		}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
		}

	}
	else
	{

		DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
	
	}
}
else if(from.equalsIgnoreCase("removeFriendFromGroupWebservice"))	
{
	if(output!=null)
	{
			try {
					JSONObject rootObj = new JSONObject(output);
					if(rootObj.getBoolean("result")){
						//MyFriendOnGroupFragment.myFriendOnGroupListView.closeAnimate(position);
						removeItemFromList(pos);
						DialogManager.showDialog(getActivity(), "Friend Removed Successfully!");
					}
				else{
					DialogManager.showDialog(getActivity(), "Error Occured to Remove Friend! Try Again!");
					}
				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			}
		}
	else
	{


		DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
	
	
	}
}
	}
	public void removeFriendFromGroupWebservice(final int position) {
	    	myAsyncTask=new MyAsynTaskManager();
			myAsyncTask.delegate=this;
			myAsyncTask.setupParamsAndUrl("removeFriendFromGroupWebservice",getActivity(),AppUrlList.ACTION_URL, new String[] {
					"module",
					"action",
					"idgroup",
					"idfriend"
					}, 
					new String[]{
					"group",
					"removefriend",
					MyFriendOnGroupFragment.groupId,
					feedList.get(position).getidfriend()
					});
			myAsyncTask.execute();
			
		}
}
