package com.bash.Fragments;

import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.AddTabSplitActivity;
import com.bash.Activities.ItemWiseSplitActivity;
import com.bash.Adapters.SplitTabAdapters;
import com.bash.ListModels.Person;
import com.bash.ListModels.PhotosListModel;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;

public class AddTabSplitFragment extends Fragment implements AsyncResponse {

	LinearLayout ratioamountlinear, equalamountlinear, unequalamountlinear,
			percentamountlinear;
	ListView spittablistview;
	TextView totalpercent, totalunequalamount, equaltotalamount,
			ratiototalamount;
	public static TextView pendingpercent, totalsplitpercent,
			pendingunequalamount, totalsplitunequalamount;

	private String categorySelect = "";
	private String dateString = "";
	private String transationTitle = "";
	private String transationAnount = "";
	private String currencyCodeString = "";
	private ArrayList<Person> addedPersonArrayList;
	private ArrayList<PhotosListModel> photoListArrayList;

	SplitTabAdapters splitTabAdapters;

	HashMap<String, String> userType;
	ArrayList<HashMap<String, String>> paidUserInfo;
	ArrayList<HashMap<String, String>> receiveUserInfo;

	MyAsynTaskManager myAsyncTask = null;

	View mRootView;
	private int fragTag;

	String userId = "";
	
	Bundle fragbundle = null;

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// mRootView = inflater.inflate(R.layout.fragment_home, null);
		mRootView = inflater.inflate(R.layout.addsplit_tab_fragment, null);

		fragbundle = getArguments();
		String tabTag = fragbundle.getString(AppConstants.FRAGMENT_TAG,
				AppConstants.TAB_PAY_TAG);

		currencyCodeString = fragbundle.getString("CurrencyCodeString");
		categorySelect = fragbundle.getString("CategorySelect");
		dateString = fragbundle.getString("DateString");
		transationTitle = fragbundle.getString("TransationTitle");
		transationAnount = fragbundle.getString("TransationAnount");
		addedPersonArrayList = fragbundle
				.getParcelableArrayList("PaidUserDetails");
		photoListArrayList = fragbundle
				.getParcelableArrayList("ReceiveUserDetails");

		if (tabTag != null && (tabTag.equals(AppConstants.SPLIT_TAB_RATIO_TAG))) {
			fragTag = 1;
		} else if (tabTag != null
				&& (tabTag.equals(AppConstants.SPLIT_TAB_UNEQUAL_TAG))) {
			fragTag = 2;
		} else if (tabTag != null
				&& (tabTag.equals(AppConstants.SPLIT_TAB_EQUAL_TAG))) {
			fragTag = 3;
		} else if (tabTag != null
				&& (tabTag.equals(AppConstants.SPLIT_TAB_PERCENT_TAG))) {
			fragTag = 4;
		} else if (tabTag != null
				&& (tabTag.equals(AppConstants.SPLIT_TAB_ITEMWISE_TAG))) {
			fragTag = 5;
		}

		ratioamountlinear = (LinearLayout) mRootView
				.findViewById(R.id.ratioamountlinear);
		equalamountlinear = (LinearLayout) mRootView
				.findViewById(R.id.equalamountlinear);
		unequalamountlinear = (LinearLayout) mRootView
				.findViewById(R.id.unequalamountlinear);
		percentamountlinear = (LinearLayout) mRootView
				.findViewById(R.id.percentamountlinear);

		pendingpercent = (TextView) mRootView.findViewById(R.id.pendingpercent);
		totalpercent = (TextView) mRootView.findViewById(R.id.totalpercent);
		totalsplitpercent = (TextView) mRootView
				.findViewById(R.id.totalsplitpercent);
		pendingunequalamount = (TextView) mRootView
				.findViewById(R.id.pendingunequalamount);
		totalunequalamount = (TextView) mRootView
				.findViewById(R.id.totalunequalamount);
		totalsplitunequalamount = (TextView) mRootView
				.findViewById(R.id.totalsplitunequalamount);
		equaltotalamount = (TextView) mRootView
				.findViewById(R.id.equaltotalamount);
		ratiototalamount = (TextView) mRootView
				.findViewById(R.id.ratiototalamount);

		spittablistview = (ListView) mRootView
				.findViewById(R.id.spittablistview);

		userId = PreferenceManager.getInstance().getUserId();

		intializeUI();

		return mRootView;
	}

	private void intializeUI() {

		userType = new HashMap<String, String>();
		userType.put("bash", "0");
		userType.put("0", "0");

		if (photoListArrayList.size() == 2) {

			AddTabSplitActivity.tvContinue.setText("Continue");

		} else if (photoListArrayList.size() > 2) {

			AddTabSplitActivity.tvContinue.setText("Save");
		}

		if (fragTag == 1) {
			ratioamountlinear.setVisibility(View.VISIBLE);
			equalamountlinear.setVisibility(View.GONE);
			unequalamountlinear.setVisibility(View.GONE);
			percentamountlinear.setVisibility(View.GONE);

			ratiototalamount.setText(transationAnount);

			findRatioEqualAmount();
			SetListAdapter();

		} else if (fragTag == 2) {
			ratioamountlinear.setVisibility(View.GONE);
			equalamountlinear.setVisibility(View.GONE);
			unequalamountlinear.setVisibility(View.VISIBLE);
			percentamountlinear.setVisibility(View.GONE);

			totalunequalamount.setText(transationAnount);
			totalsplitunequalamount.setText(transationAnount);
			pendingunequalamount.setText("0");

			findEqualAmount();
			SetListAdapter();

		} else if (fragTag == 3) {
			ratioamountlinear.setVisibility(View.GONE);
			equalamountlinear.setVisibility(View.VISIBLE);
			unequalamountlinear.setVisibility(View.GONE);
			percentamountlinear.setVisibility(View.GONE);

			equaltotalamount.setText(transationAnount);

			findEqualAmount();
			SetListAdapter();

		} else if (fragTag == 4) {
			ratioamountlinear.setVisibility(View.GONE);
			equalamountlinear.setVisibility(View.GONE);
			unequalamountlinear.setVisibility(View.GONE);
			percentamountlinear.setVisibility(View.VISIBLE);

			totalsplitpercent.setText("100");
			pendingpercent.setText("0");

			findPercentAmount();
			SetListAdapter();

		} else if (fragTag == 5) {
			Toast.makeText(getActivity(), "Item-wise", Toast.LENGTH_SHORT)
					.show();
			
//			Intent itemwiseIntent = new Intent(getActivity(), ItemWiseSplitActivity.class);
//			itemwiseIntent.putExtra("itemwisebndle", fragbundle);		
//			startActivity(itemwiseIntent);
		}

		AddTabSplitActivity.tvContinue.setOnClickListener(clickListener);
	}

	private View.OnClickListener clickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			String text = "";

			switch (v.getId()) {

			case R.id.toprightsidecontinuetext:

				if ((AddTabSplitActivity.tvContinue.getText().toString().trim())
						.equals("Continue")) {

					// pending
					if (fragTag == 1) {

					} else if (fragTag == 2) {

					} else if (fragTag == 3) {

					} else if (fragTag == 4) {

					}

				} else if ((AddTabSplitActivity.tvContinue.getText().toString()
						.trim()).equals("Save")) {

						if (fragTag == 1) {
							setRatioPayerReceiverDetails();
							sendRatioSplitDetails();
						} else if (fragTag == 2) {
							if (splitTabAdapters.findTotalAmount() == Float
									.parseFloat(transationAnount)) {
							setEqualPayerReceiverDetails();
							sendUnEqualSplitDetails();
							} else {
								Toast.makeText(getActivity(),
										"Please check entered amounts",
										Toast.LENGTH_SHORT).show();
							}
						} else if (fragTag == 3) {
								
							if (splitTabAdapters.findTotalAmount() == Float
									.parseFloat(transationAnount)) {
							setEqualPayerReceiverDetails();
							sendEqualSplitDetails();
							
							}else {
									Toast.makeText(getActivity(),
											"Please check entered amounts",
											Toast.LENGTH_SHORT).show();
							}
						} else if (fragTag == 4) {
							if (splitTabAdapters.findTotalPercent() == Float
									.parseFloat(transationAnount)) {
							 setPerentEqualPayerReceiverDetails();
							 sendPercentSplitDetails();
							}else  {
								Toast.makeText(getActivity(),
										"Please check entered amounts",
										Toast.LENGTH_SHORT).show();
						}
						} 
					}

				break;

			}
		}
	};

	private void findEqualAmount() {
		int splitcount = 0;
		for (PhotosListModel photolistObj : photoListArrayList) {

			if (photolistObj.isImageSelect()) {
				splitcount += 1;
			}
		}
		Float equalAmount = ((Float.parseFloat(transationAnount)) / (splitcount));

		for (PhotosListModel photolistObj : photoListArrayList) {

			if (photolistObj.isImageSelect()) {
				photolistObj.setPaidamount(equalAmount);
			}
		}
	}

	private void findRatioEqualAmount() {

		for (PhotosListModel photolistObj : photoListArrayList) {

			if (photolistObj.isImageSelect()) {
				photolistObj.setPaidamount(1.0f);
			}
		}
	}

	private void findPercentAmount() {
		int splitcount = 0;
		for (PhotosListModel photolistObj : photoListArrayList) {

			if (photolistObj.isImageSelect()) {
				splitcount += 1;
			}
		}
		Float equalAmount = (((float) 100) / (splitcount));

		for (PhotosListModel photolistObj : photoListArrayList) {

			if (photolistObj.isImageSelect()) {
				photolistObj.setPaidamount(Float.parseFloat(String.format(
						"%.2f", equalAmount)));
			}
		}
	}

	private void SetListAdapter() {

		splitTabAdapters = new SplitTabAdapters(getActivity(),
				R.layout.item_splittab_layout, photoListArrayList, fragTag,
				Float.parseFloat(transationAnount));

		spittablistview.setAdapter(splitTabAdapters);

	}

	private void setEqualPayerReceiverDetails() {

		paidUserInfo = new ArrayList<HashMap<String, String>>();
		receiveUserInfo = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> paiduser;
		HashMap<String, String> receiveuser;

		for (Person person : addedPersonArrayList) {
			float paidAmt = 0.0f;
			float receiveAmt = 0.0f;
			paiduser = new HashMap<String, String>();

			if (person.isPaidpersonbool()) {

				paidAmt = person.getPaidamount();

				for (PhotosListModel photoObj : photoListArrayList) {

					if (photoObj.getID().equals(person.getId())) {
						receiveAmt = photoObj.getReceiveamount();
						photoListArrayList.remove(photoObj);
						break;
					}
				}

				paiduser.put("user_type", userType.get(person.getUserType()));
				paiduser.put("idpayer", person.getId());
				paiduser.put("paid_amount", "" + paidAmt);
				paiduser.put("had_to_pay", "" + receiveAmt);

				paidUserInfo.add(paiduser);
			}
		}

		for (PhotosListModel photoListObj : photoListArrayList) {
			receiveuser = new HashMap<String, String>();
			receiveuser.put("user_type",
					userType.get(photoListObj.getUserType()));
			receiveuser.put("idreceiver", photoListObj.getID());
			receiveuser.put("had_to_pay", "" + photoListObj.getReceiveamount());

			receiveUserInfo.add(receiveuser);

		}

	}

	private void setRatioPayerReceiverDetails() {

		paidUserInfo = new ArrayList<HashMap<String, String>>();
		receiveUserInfo = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> paiduser;
		HashMap<String, String> receiveuser;

		for (Person person : addedPersonArrayList) {
			float paidAmt = 0.0f;
			float receiveAmt = 0.0f;
			paiduser = new HashMap<String, String>();

			if (person.isPaidpersonbool()) {

				paidAmt = person.getPaidamount();

				for (PhotosListModel photoObj : photoListArrayList) {

					if (photoObj.getID().equals(person.getId())) {
						receiveAmt = photoObj.getReceiveamount();
						photoListArrayList.remove(photoObj);
						break;
					}
				}

				paiduser.put("user_type", userType.get(person.getUserType()));
				paiduser.put("idpayer", person.getId());
				paiduser.put("paid_amount", "" + paidAmt);
				paiduser.put("had_to_pay_ratio", "" + (int) receiveAmt);

				paidUserInfo.add(paiduser);
			}
		}

		for (PhotosListModel photoListObj : photoListArrayList) {
			receiveuser = new HashMap<String, String>();
			receiveuser.put("user_type",
					userType.get(photoListObj.getUserType()));
			receiveuser.put("idreceiver", photoListObj.getID());
			receiveuser.put("had_to_pay_ratio",
					"" + (int) photoListObj.getReceiveamount());

			receiveUserInfo.add(receiveuser);

		}

	}
	private void setPerentEqualPayerReceiverDetails() {

		paidUserInfo = new ArrayList<HashMap<String, String>>();
		receiveUserInfo = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> paiduser;
		HashMap<String, String> receiveuser;

		for (Person person : addedPersonArrayList) {
			float paidAmt = 0.0f;
			float receiveAmt = 0.0f;
			paiduser = new HashMap<String, String>();

			if (person.isPaidpersonbool()) {

				paidAmt = person.getPaidamount();

				for (PhotosListModel photoObj : photoListArrayList) {

					if (photoObj.getID().equals(person.getId())) {
						receiveAmt = photoObj.getReceiveamount();
						photoListArrayList.remove(photoObj);
						break;
					}
				}

				paiduser.put("user_type", userType.get(person.getUserType()));
				paiduser.put("idpayer", person.getId());
				paiduser.put("paid_amount", "" + paidAmt);
				paiduser.put("had_to_pay_percent", "" + (int) receiveAmt);

				paidUserInfo.add(paiduser);
			}
		}

		for (PhotosListModel photoListObj : photoListArrayList) {
			receiveuser = new HashMap<String, String>();
			receiveuser.put("user_type",
					userType.get(photoListObj.getUserType()));
			receiveuser.put("idreceiver", photoListObj.getID());
			receiveuser.put("had_to_pay_percent",
					"" + (int) photoListObj.getReceiveamount());

			receiveUserInfo.add(receiveuser);

		}

	} 

	private void sendEqualSplitDetails() {

		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("EqualSplitTransationDetails",
				getActivity(), AppUrlList.ACTION_URL, new String[] { "module",
						"action", "entered_by_iduser", "transaction_currency",
						"transaction_amount", "transaction_category",
						"transaction_date", "transaction_title",
						"payers_information", "receivers_information" },
				new String[] { "transaction", "insertEqualSplitTransaction",
						userId, currencyCodeString, transationAnount,
						"" + categorySelect, dateString, transationTitle,
						paidUserInfo.toString(), receiveUserInfo.toString() });
		myAsyncTask.execute();

	}

	private void sendUnEqualSplitDetails() {

		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("UnEqualSplitTransationDetails",
				getActivity(), AppUrlList.ACTION_URL, new String[] { "module",
						"action", "entered_by_iduser", "transaction_currency",
						"transaction_amount", "transaction_category",
						"transaction_date", "transaction_title",
						"payers_information", "receivers_information" },
				new String[] { "transaction", "insertUnequalSplitTransaction",
						userId, currencyCodeString, transationAnount,
						"" + categorySelect, dateString, transationTitle,
						paidUserInfo.toString(), receiveUserInfo.toString() });
		myAsyncTask.execute();

	}

	private void sendRatioSplitDetails() {

		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("RatioSplitTransationDetails",
				getActivity(), AppUrlList.ACTION_URL, new String[] { "module",
						"action", "entered_by_iduser", "transaction_currency",
						"transaction_amount", "transaction_category",
						"transaction_date", "transaction_title",
						"payers_information", "receivers_information" },
				new String[] { "transaction", "insertRatioSplitTransaction",
						userId, currencyCodeString, transationAnount,
						"" + categorySelect, dateString, transationTitle,
						paidUserInfo.toString(), receiveUserInfo.toString() });
		myAsyncTask.execute();

	}
	private void sendPercentSplitDetails() {

		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("PercentSplitTransationDetails",
				getActivity(), AppUrlList.ACTION_URL, new String[] { "module",
						"action", "entered_by_iduser", "transaction_currency",
						"transaction_amount", "transaction_category",
						"transaction_date", "transaction_title",
						"payers_information", "receivers_information" },
				new String[] { "transaction", "insertPercentSplitTransaction",
						userId, currencyCodeString, transationAnount,
						"" + categorySelect, dateString, transationTitle,
						paidUserInfo.toString(), receiveUserInfo.toString() });
		myAsyncTask.execute();

	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub

		if (from.equalsIgnoreCase("EqualSplitTransationDetails")) {
			if (output != null && output.length() > 0) {
				Toast.makeText(getActivity(), output.toString(),
						Toast.LENGTH_SHORT).show();
			}
		}
		if (from.equalsIgnoreCase("UnEqualSplitTransationDetails")) {
			if (output != null && output.length() > 0) {
				Toast.makeText(getActivity(), output.toString(),
						Toast.LENGTH_SHORT).show();
			}
		}
		if (from.equalsIgnoreCase("RatioSplitTransationDetails")) {
			if (output != null && output.length() > 0) {
				Toast.makeText(getActivity(), output.toString(),
						Toast.LENGTH_SHORT).show();
			}
		}

	}

}
