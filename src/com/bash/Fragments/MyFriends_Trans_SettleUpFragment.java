package com.bash.Fragments;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.GsonClasses.MyFriend_Transaction_Class;
import com.bash.ListModels.BashUsers_Class;
import com.bash.Managers.PreferenceManager;

@SuppressLint("ValidFragment")
public class MyFriends_Trans_SettleUpFragment extends Fragment{

	View mRootView;
	Typeface tf;  
	BashUsers_Class userInfo;
	String amountToPay= " ";
	
	@SuppressLint("ValidFragment")
	public MyFriends_Trans_SettleUpFragment(BashUsers_Class userInfo, String amount){
		this.userInfo =userInfo;
		this.amountToPay = amount;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.fragment_settlle_up_page, null);
		initializeViews();
		return mRootView;
	}
	
	private void initializeViews() {
		
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "Settle Up", 0);
		
		((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//((HomeActivity) getActivity()).menu.toggle();
					((BaseFragment)getParentFragment()).popFragment();
				}
		 });
		 
		tf = Typeface.createFromAsset(getActivity().getAssets(),"fonts/digital-7.ttf");  
		
		((EditText) mRootView.findViewById(R.id.amoutText)).setTypeface(tf, Typeface.BOLD);
		
		((EditText) mRootView.findViewById(R.id.amoutText)).setText(amountToPay);
		
		((TextView) mRootView.findViewById(R.id.receiverName)).setText(userInfo.getname());
		
		//((EditText) mRootView.findViewById(R.id.amoutText)).setText(userInfo);
		
	}
	
	
}
