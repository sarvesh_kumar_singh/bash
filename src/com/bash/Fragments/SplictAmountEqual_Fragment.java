package com.bash.Fragments;

import java.text.DecimalFormat;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.ListModels.BashUsers_Class;
import com.bash.ListModels.MyFriendOnGroup_Class;
import com.bash.ListModels.ValuseHolder;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;

@SuppressLint("ValidFragment")
public class SplictAmountEqual_Fragment extends Fragment implements AsyncResponse{
	MyAsynTaskManager  myAsyncTask;
	View mRootView;
	ArrayList<BashUsers_Class> feedList = new ArrayList<BashUsers_Class>();
	SelectionFriendsAdapter adapter;
	ListView splitFriendsListView;
	public double amount = 200, splitamount = 0, numberofPeople = 0, percentage = 0;
	//boolean isAdapterLoading = true;
	String groupId;
	
	public SplictAmountEqual_Fragment(int amount, String groupId) {
		this.amount = amount;
		this.groupId = groupId;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		mRootView = inflater.inflate(R.layout.fragment_split_amt_equal_page, null);
		initializeView();
		return mRootView;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		((HomeActivity) getActivity()).setUpTopBarFields(R.drawable.back_btn, "Group Transaction", 0);

		((ImageView) getActivity().findViewById(R.id.topleftsideImage))	.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						((BaseFragment) getParentFragment()).popFragment();
					}
		 	});
	}
	
	private void initializeView() {
		
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "Split By Equal", 0);
		
		((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((BaseFragment)getParentFragment()).popFragment();
			}
		});
		
		((RelativeLayout)mRootView.findViewById(R.id.doneButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				double totalamount = 0;
				boolean haveYouPaidAll = false;
				Log.e("Preference Id", PreferenceManager.getInstance().getUserId());
				
				for(int i = 0; i<AddDescription_Group_Fragment.valuesHolder.size(); i++){
					Log.e("Value in "+i, String.valueOf(AddDescription_Group_Fragment.valuesHolder.get(i).getAmount()));
					totalamount += AddDescription_Group_Fragment.valuesHolder.get(i).getAmount();
					
					Log.e("*****Id", AddDescription_Group_Fragment.valuesHolder.get(i).getId().toString());
					Log.e("*****Amount", AddDescription_Group_Fragment.valuesHolder.get(i).getAmount().toString());
					
					if(PreferenceManager.getInstance().getUserId().equals(
							String.valueOf(Math.round(AddDescription_Group_Fragment.valuesHolder.get(i).getId())))){
						
						Log.e("Percentage", AddDescription_Group_Fragment.valuesHolder.get(i).getPercentage().toString());
						
						if(Math.round(AddDescription_Group_Fragment.valuesHolder.get(i).getPercentage()) == 100){
							haveYouPaidAll = true;
						}
					}
				}
				
				totalamount = Math.round(totalamount);
				Log.e("Total with Floor", String.valueOf(Math.round(totalamount)));
				
				if(haveYouPaidAll){
					DialogManager.showDialog(getActivity(), "Please Select One More User!");
				}
				else if(Math.round(totalamount) > amount) {
					DialogManager.showDialog(getActivity(), "Amount Shouldn't Exceed Total!");
				}else if(Math.round(totalamount) < amount){
					DialogManager.showDialog(getActivity(), "Amount Shouldn't Below Total!");
				}
				else{
					((BaseFragment)getParentFragment()).popFragment();
				}
				
			}
		});
		
		((RelativeLayout) mRootView.findViewById(R.id.cancelButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((BaseFragment)getParentFragment()).popFragment();
			}
		});
		
		
		
		splitFriendsListView = (ListView) mRootView.findViewById(R.id.splitFriendsListView);
		
		/*feedList.add(new BashUsers_Class("0", "123456", "Suresh", "", "", "", ""));
		feedList.add(new BashUsers_Class("0", "88484848", "Ramesh", "", "", "", ""));
		feedList.add(new BashUsers_Class("0", "7898798", "Kamal", "", "", "", ""));
		feedList.add(new BashUsers_Class("0", "7995646", "Vimal", "", "", "", ""));
		 */		
		numberofPeople = AddDescription_Group_Fragment.feedList.size();
		
		if(numberofPeople != 0){
			splitamount = amount / numberofPeople;
			percentage = (splitamount * 100) / amount;
		}
			
		
		adapter = new SelectionFriendsAdapter();
		splitFriendsListView.setAdapter(adapter);
		
		AddDescription_Group_Fragment.valuesHolder.clear();
		
		//getFriendsinGroupDetails();
		initializeDatas();
		loadDatas();
		
	}

	private void loadDatas() {

		if(AddDescription_Group_Fragment.feedList.size() != 0){
			splitamount = (double) (amount / (AddDescription_Group_Fragment.feedList.size()));
			//percentage = (splitamount / amount) * 100;
			percentage = (double)(100 / (AddDescription_Group_Fragment.feedList.size()));
			Log.e("splitamount", splitamount+"");
			Log.e("amount", amount+"");
			Log.e("Print", percentage+"");
		}
	 
		if(AddDescription_Group_Fragment.feedList.size() != 0)
				AddDescription_Group_Fragment.valuesHolder.put(0, new ValuseHolder(Double.parseDouble(PreferenceManager.getInstance().getUserId()),
			    		Double.parseDouble(f.format(percentage)),
						Double.parseDouble(f.format(splitamount))));
			
			int i = 0;
			for(i = 0;i < AddDescription_Group_Fragment.feedList.size()-1; i++) {
				
				AddDescription_Group_Fragment.valuesHolder.put(i+1, new ValuseHolder(Double.parseDouble(AddDescription_Group_Fragment.feedList.get(i).getidfriend()),
						Double.parseDouble(f.format(percentage)),
						Double.parseDouble(f.format(splitamount))));
				}
			
		    
			
			numberofPeople = AddDescription_Group_Fragment.feedList.size();
			
			if(AddDescription_Group_Fragment.feedList.size() != 0){
				splitamount =(double) (amount / AddDescription_Group_Fragment.feedList.size());
				percentage = (double)((AddDescription_Group_Fragment.feedList.size() * 100)/ amount);
			}
		
	}

	public void initializeDatas(){
	 
		
		splitamount = (double) (amount / (AddDescription_Group_Fragment.feedList.size()));
		//percentage = (splitamount / amount) * 100;
		percentage = (double)(100 / (AddDescription_Group_Fragment.feedList.size()));
		
		 	if(AddDescription_Group_Fragment.feedList.size() != 0)
				AddDescription_Group_Fragment.valuesHolder.put(0, new ValuseHolder(Double.parseDouble(PreferenceManager.getInstance().getUserId()),
			    		Double.parseDouble(f.format(percentage)),
						Double.parseDouble(f.format(splitamount))));
			
			int i = 0;
			for(i = 0;i < AddDescription_Group_Fragment.feedList.size()-1; i++) {
				
				AddDescription_Group_Fragment.valuesHolder.put(i+1, new ValuseHolder(Double.parseDouble(AddDescription_Group_Fragment.feedList.get(i).getidfriend()),
						Double.parseDouble(f.format(percentage)),
						Double.parseDouble(f.format(splitamount))));
			}
			
		    
			numberofPeople = AddDescription_Group_Fragment.feedList.size();
			
			if(AddDescription_Group_Fragment.feedList.size() != 0){
				splitamount =(double) (amount / AddDescription_Group_Fragment.feedList.size());
				percentage = (double)((AddDescription_Group_Fragment.feedList.size() * 100)/ amount);
			}
			adapter.notifyDataSetChanged();
	}

	public void getFriendsinGroupDetails() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("getFriendsinGroupDetails",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"idgroup",
				"iduser"
				},
				
				new String[]{
				"group",
				"friendsofgroup",
				groupId,
				PreferenceManager.getInstance().getUserId()
				});
myAsyncTask.execute();
		
	}
	
	DecimalFormat f = new DecimalFormat("##.00");
	
	class SelectionFriendsAdapter extends BaseAdapter{
		
		boolean isItemChecked = false;
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return AddDescription_Group_Fragment.feedList.size();
		}

		@Override
		public MyFriendOnGroup_Class getItem(int position) {
			// TODO Auto-generated method stub
			return AddDescription_Group_Fragment.feedList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
				ViewHolder holder = null;
				MyFriendOnGroup_Class listItem = getItem(position);
		        if (convertView == null) {
		        	LayoutInflater inflater = LayoutInflater.from(getActivity());
		        	convertView = inflater.inflate(R.layout.custom_splitamt_equal_listview, null);
		            holder = new ViewHolder();
		            holder.isSelected = (CheckBox) convertView.findViewById(R.id.isSelected);
		            holder.mobileNumberText = (TextView) convertView.findViewById(R.id.mobileNumberText);
		            holder.amountEditText = (EditText) convertView.findViewById(R.id.amountEditText);
		            holder.percentageText = (EditText) convertView.findViewById(R.id.percentageText);
		            holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
		            holder.userImage = (ImageView) convertView.findViewById(R.id.userImage);
		            convertView.setTag(holder);
		            
		        } else {
		            holder = (ViewHolder) convertView.getTag();
		        }
		        //holder.userImage.setImageResource(listItem.getimagepath());
		        holder.mobileNumberText.setText(listItem.getphone_no());
	            holder.friendName.setText(listItem.getfriendName());
	            
	            final EditText amtText = holder.amountEditText;
	            final EditText percentageText = holder.percentageText;
	            final CheckBox is = holder.isSelected;
	            
	            holder.isSelected.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						if(isChecked){
							numberofPeople++;
							
							percentageText.setText(String.valueOf(Math.round(100/numberofPeople)));
				            amtText.setText(String.valueOf(Math.round(amount/numberofPeople)));
				            
				            for(int i = 0; i<AddDescription_Group_Fragment.valuesHolder.size(); i++){
				            	
				            	if(position == i || AddDescription_Group_Fragment.valuesHolder.get(i).getAmount() != 0){
				            		AddDescription_Group_Fragment.valuesHolder.get(i).setAmount((double)Math.round(amount/numberofPeople));
						            AddDescription_Group_Fragment.valuesHolder.get(i).setPercentage((double)Math.round(100/numberofPeople));	
				            	}
				            }
				            
						}else{
							
							numberofPeople--;
							
							percentageText.setText(String.valueOf(Math.round(100/numberofPeople)));
				            amtText.setText(String.valueOf(Math.round(amount/numberofPeople)));
				            
				            for(int i = 0; i<AddDescription_Group_Fragment.valuesHolder.size(); i++){
				            	
				            	if(position == i || AddDescription_Group_Fragment.valuesHolder.get(i).getAmount() == 0){
				            		AddDescription_Group_Fragment.valuesHolder.get(i).setAmount(0d);
									AddDescription_Group_Fragment.valuesHolder.get(i).setPercentage(0d);		
				            	}else {
				            		AddDescription_Group_Fragment.valuesHolder.get(i).setAmount((double)Math.round(amount/numberofPeople));
						            AddDescription_Group_Fragment.valuesHolder.get(i).setPercentage((double)Math.round(100/numberofPeople));	
				            	}
				            }
							amtText.setText("0");
							percentageText.setText("0");
						}
						notifyDataSetChanged();
					}
				});
	            
	            holder.percentageText.setOnEditorActionListener(new OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
						// TODO Auto-generated method stub
						if(actionId == EditorInfo.IME_ACTION_DONE){
							if(!percentageText.getText().toString().equals("0")){
								
								AddDescription_Group_Fragment.valuesHolder.get(position).setAmount((double)Math.round(amount * Double.parseDouble(percentageText.getText().toString())/100));
								AddDescription_Group_Fragment.valuesHolder.get(position).setPercentage((double)Double.parseDouble(percentageText.getText().toString()));
					            
								/*int modifiedamount = (100 - Integer.parseInt(percentageText.getText().toString())) / numberofPeople;
								
								for(int i = 0; i<AddDescription_Group_Fragment.feedList.size(); i++){
					            	if(position == i){
					            		valuesHolder.get(position).setAmount(Math.round(amount * Integer.parseInt(percentageText.getText().toString())/100));
							            valuesHolder.get(position).setPercentage(Integer.parseInt(percentageText.getText().toString()));		
					            	}else{
					            		valuesHolder.get(position).setAmount(modifiedamount);
							            valuesHolder.get(position).setPercentage(100 - Integer.parseInt(percentageText.getText().toString()));
					            	}
					            }*/
					            //valuesHolder.get(position).setPercentage(Math.round(100/numberofPeople));
								//amtText.setText(String.valueOf(Math.round((amount * Integer.parseInt(percentageText.getText().toString()))/100)));	
							}else{
								AddDescription_Group_Fragment.valuesHolder.get(position).setAmount(0d);
								AddDescription_Group_Fragment.valuesHolder.get(position).setPercentage(0d);
								/*amtText.setText("0");
								percentageText.setText("0");*/
							}
							notifyDataSetChanged();
						}
						return false;
					}
				});
	            
	            //Log.e("Position", String.valueOf(position));
	            
	            
	            //Log.e("Split amount", AddDescription_Group_Fragment.feedList.get(position).getfriendAmount());
	            
	            if(AddDescription_Group_Fragment.valuesHolder.size() != 0) {
	            	holder.percentageText.setText(String.valueOf(AddDescription_Group_Fragment.valuesHolder.get(position).getPercentage()));
		            holder.amountEditText.setText(String.valueOf(AddDescription_Group_Fragment.valuesHolder.get(position).getAmount()));
	            }
	            	            
	            return convertView;
		}
		
		  private class ViewHolder {
		    	EditText amountEditText, percentageText;
		    	TextView mobileNumberText;
		    	TextView friendName;
		    	ImageView userImage;
		    	CheckBox isSelected;
		     }
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		if(from.equalsIgnoreCase("getFriendsinGroupDetails"))
		{
			if(output!=null)
			{

				try {
					JSONObject rootObj = new JSONObject(output);
					if(rootObj.getBoolean("result")) {

						JSONArray groupList = rootObj.getJSONArray("friendlist");
						JSONObject item;

						if(groupList.length() != 0){
							splitamount = (double) (amount / (groupList.length()+1));
							//percentage = (splitamount / amount) * 100;
							percentage = (double)(100 / (groupList.length()+1));
							Log.e("splitamount", splitamount+"");
							Log.e("amount", amount+"");
							Log.e("Print", percentage+"");
						}

						AddDescription_Group_Fragment.feedList.clear();

						AddDescription_Group_Fragment.feedList.add(new MyFriendOnGroup_Class(
								PreferenceManager.getInstance().getUserId(), 
								"",
								PreferenceManager.getInstance().getUserMobileNumber(), 
								PreferenceManager.getInstance().getUserFullName(),
								" ",
								"0",
								"0"
								));


						if(groupList.length() != 0)
							AddDescription_Group_Fragment.valuesHolder.put(0, new ValuseHolder(Double.parseDouble(PreferenceManager.getInstance().getUserId()),
									Double.parseDouble(f.format(percentage)),
									Double.parseDouble(f.format(splitamount))));

						int i = 0;
						for(i = 0;i < groupList.length(); i++) {

							item = groupList.getJSONObject(i);

							AddDescription_Group_Fragment.feedList.add(new MyFriendOnGroup_Class(
									item.getString("idfriend"), 
									"",
									item.getString("phone_no"), 
									item.getString("name"),
									item.getString("imagepath"),
									String.valueOf(f.format(splitamount)),
									String.valueOf(f.format(percentage))
									));

							AddDescription_Group_Fragment.valuesHolder.put(i+1, new ValuseHolder(Double.parseDouble(item.getString("idfriend")),
									Double.parseDouble(f.format(percentage)),
									Double.parseDouble(f.format(splitamount))));
						}



						numberofPeople = AddDescription_Group_Fragment.feedList.size();

						if(AddDescription_Group_Fragment.feedList.size() != 0){
							splitamount =(double) (amount / AddDescription_Group_Fragment.feedList.size());
							percentage = (double)((AddDescription_Group_Fragment.feedList.size() * 100)/ amount);
						}
						adapter.notifyDataSetChanged();

					}
					else { 
						DialogManager.showDialog(getActivity(), "No Friends Found in Group!");
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
				}

			}
			else
			{
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			}
		}

	}
	
	
	/*class SelectionFriendsAdapter extends BaseAdapter{
		 boolean isItemChecked = false;
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return AddDescription_Group_Fragment.feedList.size();
		}

		@Override
		public MyFriendOnGroup_Class getItem(int position) {
			// TODO Auto-generated method stub
			return AddDescription_Group_Fragment.feedList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
				ViewHolder holder = null;
				MyFriendOnGroup_Class listItem = getItem(position);
		        if (convertView == null) {
		        	LayoutInflater inflater = LayoutInflater.from(getActivity());
		        	convertView = inflater.inflate(R.layout.custom_splitamt_equal_listview, null);
		            holder = new ViewHolder();
		            holder.isSelected = (CheckBox) convertView.findViewById(R.id.isSelected);
		            holder.mobileNumberText = (TextView) convertView.findViewById(R.id.mobileNumberText);
		            holder.amountEditText = (EditText) convertView.findViewById(R.id.amountEditText);
		            holder.percentageText = (EditText) convertView.findViewById(R.id.percentageText);
		            holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
		            holder.userImage = (ImageView) convertView.findViewById(R.id.userImage);
		            convertView.setTag(holder);
		            
		        } else {
		            holder = (ViewHolder) convertView.getTag();
		        }
		        //holder.userImage.setImageResource(listItem.getimagepath());
		        holder.mobileNumberText.setText(listItem.getphone_no());
	            holder.friendName.setText(listItem.getfriendName());
	            
	            final EditText amtText = holder.amountEditText;
	            final EditText percentageText = holder.percentageText;
	            final CheckBox is = holder.isSelected;
	            
	            holder.isSelected.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						if(isChecked){
							numberofPeople++;
							
							percentageText.setText(String.valueOf(Math.round(100/numberofPeople)));
				            amtText.setText(String.valueOf(Math.round(amount/numberofPeople)));
				            
				            for(int i = 0; i<AddDescription_Group_Fragment.valuesHolder.size(); i++){
				            	
				            	if(position == i || AddDescription_Group_Fragment.valuesHolder.get(i).getAmount() != 0){
				            		AddDescription_Group_Fragment.valuesHolder.get(i).setAmount((double)Math.round(amount/numberofPeople));
						            AddDescription_Group_Fragment.valuesHolder.get(i).setPercentage((double)Math.round(100/numberofPeople));	
				            	}
				            }
				            
						}else{
							
							numberofPeople--;
							
							percentageText.setText(String.valueOf(Math.round(100/numberofPeople)));
				            amtText.setText(String.valueOf(Math.round(amount/numberofPeople)));
				            
				            for(int i = 0; i<AddDescription_Group_Fragment.valuesHolder.size(); i++){
				            	
				            	if(position == i || AddDescription_Group_Fragment.valuesHolder.get(i).getAmount() == 0){
				            		AddDescription_Group_Fragment.valuesHolder.get(i).setAmount(0d);
									AddDescription_Group_Fragment.valuesHolder.get(i).setPercentage(0d);		
				            	}else {
				            		AddDescription_Group_Fragment.valuesHolder.get(i).setAmount((double)Math.round(amount/numberofPeople));
						            AddDescription_Group_Fragment.valuesHolder.get(i).setPercentage((double)Math.round(100/numberofPeople));	
				            	}
				            }
							amtText.setText("0");
							percentageText.setText("0");
						}
						notifyDataSetChanged();
					}
				});
	            
	            holder.percentageText.setOnEditorActionListener(new OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
						// TODO Auto-generated method stub
						if(actionId == EditorInfo.IME_ACTION_DONE){
							if(!percentageText.getText().toString().equals("0")){
								
								AddDescription_Group_Fragment.valuesHolder.get(position).setAmount((double)Math.round(amount * Double.parseDouble(percentageText.getText().toString())/100));
								AddDescription_Group_Fragment.valuesHolder.get(position).setPercentage((double)Double.parseDouble(percentageText.getText().toString()));
					            
								int modifiedamount = (100 - Integer.parseInt(percentageText.getText().toString())) / numberofPeople;
								
								for(int i = 0; i<AddDescription_Group_Fragment.feedList.size(); i++){
					            	if(position == i){
					            		valuesHolder.get(position).setAmount(Math.round(amount * Integer.parseInt(percentageText.getText().toString())/100));
							            valuesHolder.get(position).setPercentage(Integer.parseInt(percentageText.getText().toString()));		
					            	}else{
					            		valuesHolder.get(position).setAmount(modifiedamount);
							            valuesHolder.get(position).setPercentage(100 - Integer.parseInt(percentageText.getText().toString()));
					            	}
					            }
					            //valuesHolder.get(position).setPercentage(Math.round(100/numberofPeople));
								//amtText.setText(String.valueOf(Math.round((amount * Integer.parseInt(percentageText.getText().toString()))/100)));	
							}else{
								AddDescription_Group_Fragment.valuesHolder.get(position).setAmount(0d);
								AddDescription_Group_Fragment.valuesHolder.get(position).setPercentage(0d);
								amtText.setText("0");
								percentageText.setText("0");
							}
							notifyDataSetChanged();
						}
						return false;
					}
				});
	            
	            //Log.e("Position", String.valueOf(position));
	            
	            
	            //Log.e("Split amount", AddDescription_Group_Fragment.feedList.get(position).getfriendAmount());
	            
	            if(AddDescription_Group_Fragment.valuesHolder.size() != 0) {
	            	holder.percentageText.setText(String.valueOf(AddDescription_Group_Fragment.valuesHolder.get(position).getPercentage()));
		            holder.amountEditText.setText(String.valueOf(AddDescription_Group_Fragment.valuesHolder.get(position).getAmount()));
	            }
	            	            
	            return convertView;
		}
		
		  private class ViewHolder {
		    	EditText amountEditText, percentageText;
		    	TextView mobileNumberText;
		    	TextView friendName;
		    	ImageView userImage;
		    	CheckBox isSelected;
		     }
	}*/
}

