package com.bash.Fragments;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Adapters.NothingSelectedAdapter;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.ListModels.MyFriendOnGroup_Class;
import com.bash.ListModels.ValuseHolder;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynImageTaskManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.TwitterConnection.TwitterFragment;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.bash.Utils.MyLocation;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Feed;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnPublishListener;

public class AddDescription_Group_Fragment extends Fragment implements AsyncResponse{
	MyAsynTaskManager  myAsyncTask;
	View mRootView;
	public static boolean isChecked = false;
	TwitterFragment twitteraccess;
	
	View camera_gallery;
	Dialog camaeraDialog;
	Uri mCapturedImageURI;
	public ImageView fileImage;
	String picturePath="", locationPath="";

	public Spinner groupSpinner;
	public NothingSelectedAdapter nothingAdapter;
	//public ArrayAdapter<String> dataAdapter;
	public MyCustomAdapter dataAdaper;
	
	public ArrayList<String> spinnerGroupList = new ArrayList<String>();
	public ArrayList<String> groupIdCollection = new ArrayList<String>();
	
	public static SparseArray<selectionModel> selectionList = new SparseArray<selectionModel>();
	public ProgressDialog pd;
	
	public static ArrayList<MyFriendOnGroup_Class> feedList = new ArrayList<MyFriendOnGroup_Class>();
	
	public static ArrayList<MyFriendOnGroup_Class> tempfeedList = new ArrayList<MyFriendOnGroup_Class>();
	public static ArrayList<MyFriendOnGroup_Class> filteredList = new ArrayList<MyFriendOnGroup_Class>();
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,	Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		mRootView = inflater.inflate(R.layout.fragment_addtransaction_group, null);
		return mRootView;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
	 
	public static SparseArray<ValuseHolder> valuesHolder = new SparseArray<ValuseHolder>();
	

	static class selectionModel{

		String selectionId;
		String selectionAmount;
		public selectionModel(String selectionId, String selectionAmount){
			this.selectionId = selectionId;
			this.selectionAmount = selectionAmount;
		}
		
		public String getSelectionId(){
			return selectionId;
		}
		public String getselectionAmount(){
			return selectionAmount;
		}
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initializeView();
		initializeAlertView();
		initializeAttendeesView();
		((HomeActivity) getActivity()).currentFragment = this;
	}

	View attendees_view;
	Dialog attendeesDialog;
	ListView attendeesListview;
	SelectionFriendsAdapter attendeeAdapter;
	
	private void initializeAttendeesView() {
		
		attendees_view = View.inflate(getActivity(), R.layout.alert_attendees_window, null);
		attendeesDialog = new Dialog(getActivity());
		attendeesDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		attendeesDialog.setContentView(attendees_view);
		
		((Button) attendees_view.findViewById(R.id.okButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.e("Selection List", tempfeedList.size()+"");
				filteredList.clear();
				for(int i = 0; i < tempfeedList.size(); i++){
					if(tempfeedList.get(i).getIsSelected()){
						filteredList.add(tempfeedList.get(i));
						Log.e("I value", tempfeedList.get(i).getfriendName());
					}
					else
						Log.e("Not Selected!", "hoi");
				}
				if(filteredList.size() != 0 && filteredList.size() == tempfeedList.size())
					((EditText) mRootView.findViewById(R.id.attendessSpinner)).setText("All Attendees");
				else
					((EditText) mRootView.findViewById(R.id.attendessSpinner)).setText(filteredList.size()+" Contacts Added");
				attendeesDialog.dismiss();
			}
		});
		
		((Button) attendees_view.findViewById(R.id.cancelButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				for(int i = 0; i < filteredList.size(); i++){
					for(int j = 0; j < tempfeedList.size(); j++){
						if(filteredList.get(i).getidfriend().equals(tempfeedList.get(j).getidfriend())){
							tempfeedList.get(j).setIsSelected(true);
						}
					}
				}
				
				if(filteredList.size() != 0 && filteredList.size() == tempfeedList.size())
					((EditText) mRootView.findViewById(R.id.attendessSpinner)).setText("All Attendees");
				else
					((EditText) mRootView.findViewById(R.id.attendessSpinner)).setText(filteredList.size()+" Contacts Added");
				attendeesDialog.dismiss();
			}
		});
		
		attendeesListview = (ListView) attendees_view.findViewById(R.id.splitedListView);
		
		attendeeAdapter = new SelectionFriendsAdapter();
		
		attendeesListview.setAdapter(attendeeAdapter);
		
   }
	
	class SelectionFriendsAdapter extends BaseAdapter {
			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				return tempfeedList.size();
			}

			@Override
			public MyFriendOnGroup_Class getItem(int position) {
				// TODO Auto-generated method stub
				return tempfeedList.get(position);
			}

			@Override
			public long getItemId(int position) {
				return 0;
			}

			@Override
			public View getView(final int position, View convertView, ViewGroup parent) {

					ViewHolder holder = null;
				
					final MyFriendOnGroup_Class listItem = getItem(position);
					
			        if (convertView == null) {
			        	LayoutInflater inflater = LayoutInflater.from(getActivity());
			        	convertView = inflater.inflate(R.layout.custom_viewlist_listview, null);
			            holder = new ViewHolder();
			            holder.selectionBox = (CheckBox) convertView.findViewById(R.id.selectionBox);
			            holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
			            holder.userImage = (ImageView) convertView.findViewById(R.id.userImage);
			            convertView.setTag(holder);
			        } else {
			            holder = (ViewHolder) convertView.getTag();
			        }
			        
			        holder.friendName.setText(listItem.getfriendName());
			        holder.selectionBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
						@Override
						public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
							if(isChecked){
								tempfeedList.get(position).setIsSelected(true);
							}else{
								tempfeedList.get(position).setIsSelected(false);
							}
						}
					});
			        
			        if(tempfeedList.get(position).getIsSelected())
			        	holder.selectionBox.setChecked(true);
			        else
			        	holder.selectionBox.setChecked(false);
			        
		            if(listItem.getfriendImageSource() != null && listItem.getfriendImageSource().length() != 0)
		            	ImageLoader.getInstance().displayImage(listItem.getfriendImageSource(), 
			        			holder.userImage, BashApplication.options, BashApplication.animateFirstListener);
		        //    		ImageLoader.getInstance().displayImage(listItem.getfriendImageSource(), holder.userImage);
		        
		            return convertView;
		         }
		    		
		  		  private class ViewHolder {
		  		    	CheckBox selectionBox;
		  		    	TextView friendName;
		  		    	ImageView userImage;
		  		     }
		  	}
	
	private void initializeAlertView() {

		// TODO Auto-generated method stub
		camera_gallery = View.inflate(getActivity(),
				R.layout.alert_gallerycamera_mode_view, null);
		camaeraDialog = new Dialog(getActivity());
		camaeraDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		camaeraDialog.setContentView(camera_gallery);
 
		((Button) camera_gallery.findViewById(R.id.openGalleryBtn)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						camaeraDialog.dismiss();
						Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						getActivity().startActivityForResult(i,	AppConstants.CODE_GALLERY);
					}
		});

		((Button) camera_gallery.findViewById(R.id.openCameraBtn)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						camaeraDialog.dismiss();
						String fileName = "temp.jpg";
						ContentValues values = new ContentValues();
						values.put(MediaStore.Images.Media.TITLE, fileName);
						mCapturedImageURI = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
										values);
						Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
						intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
						getActivity().startActivityForResult(intent, AppConstants.CODE_CAMERA);
					}
				});

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case AppConstants.CODE_CAMERA:
			if (resultCode == getActivity().RESULT_OK) {
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(
						mCapturedImageURI, projection, null, null, null);
				int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				picturePath = cursor.getString(column_index_data);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,	fileImage, BashApplication.imageOptions);
				Log.e("Camera Path", picturePath);
			}
			break;
		case AppConstants.CODE_GALLERY:
			if (resultCode == getActivity().RESULT_OK) {

				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				picturePath = cursor.getString(columnIndex);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,	fileImage, BashApplication.imageOptions);
			}
			break;
		default:
			break;
		}
	}

	private void initializeView() {


		((HomeActivity) getActivity()).setUpTopBarFields(R.drawable.back_btn, "Group Transaction", 0);

		((ImageView) getActivity().findViewById(R.id.topleftsideImage))	.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						((BaseFragment) getParentFragment()).popFragment();
					}
		});

		fileImage = (ImageView) mRootView.findViewById(R.id.addPhotoButton);

		groupSpinner = (Spinner) mRootView.findViewById(R.id.groupSpinner);
		 
	 
	 	((EditText) mRootView.findViewById(R.id.attendessSpinner)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				attendeesDialog.show();	
				if(tempfeedList.size() == 0){
					Toast.makeText(getActivity(), "Please Select Group!", Toast.LENGTH_SHORT).show();
				}else{
					attendeesDialog.show();	
				}
			}
		});
		
		
		((ImageView) mRootView.findViewById(R.id.facebookButton)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(ValidateForm()){
							if (SimpleFacebook.getInstance().isLogin()) {
								callFbPost();
							} else {
								SimpleFacebook.getInstance().login(onLoginListener);
							}	
						}
					}
				});

		((ImageView) mRootView.findViewById(R.id.twitterButton)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						twitteraccess.updateToTwitt(((EditText) mRootView
								.findViewById(R.id.commentText)).getText()
								.toString());
					}
				});

		((ImageView) mRootView.findViewById(R.id.addPhotoButton)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						camaeraDialog.show();
					}
		});

		((RelativeLayout) mRootView.findViewById(R.id.createTransactionButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(ValidateForm()){
					if(((RadioButton) mRootView.findViewById(R.id.manualOption)).isChecked()
						&& selectionList.size() != 0) {
						
						if(AddDescription_Group_Fragment.selectionList.size() == 0){
							Log.e("Selection List", "No Item Found");
							AddFriendsOnGroupFragment.parentFragment.popFragment();
						}else{
							String selectionString = "", amountString = "";
							for(int i = 0; i < AddDescription_Group_Fragment.selectionList.size(); i++) {
									if(AddDescription_Group_Fragment.selectionList.size() == i+1){
										//selectionString += selectionList.keyAt(i);
										if(!AddDescription_Group_Fragment.selectionList.get(i).getselectionAmount().equals("0") && amountString.length() != 0
												&& !AddDescription_Group_Fragment.selectionList.get(i).getSelectionId().equals(PreferenceManager.getInstance().getUserId())){
										amountString += ","+AddDescription_Group_Fragment.selectionList.get(i).getselectionAmount();
										selectionString += ","+AddDescription_Group_Fragment.feedList.get(AddDescription_Group_Fragment.selectionList.keyAt(i)).getidfriend();}
										else if(!AddDescription_Group_Fragment.selectionList.get(i).getselectionAmount().equals("0")
												&& !AddDescription_Group_Fragment.selectionList.get(i).getSelectionId().equals(PreferenceManager.getInstance().getUserId())){
											amountString += AddDescription_Group_Fragment.selectionList.get(i).getselectionAmount();
											selectionString += AddDescription_Group_Fragment.feedList.get(AddDescription_Group_Fragment.selectionList.keyAt(i)).getidfriend();
											}
										}
									else{
										if(!AddDescription_Group_Fragment.selectionList.get(i).getselectionAmount().equals("0") && amountString.length() != 0
												&& !AddDescription_Group_Fragment.selectionList.get(i).getSelectionId().equals(PreferenceManager.getInstance().getUserId())){
										amountString += ","+AddDescription_Group_Fragment.selectionList.get(i).getselectionAmount();
										selectionString += ","+AddDescription_Group_Fragment.feedList.get(AddDescription_Group_Fragment.selectionList.keyAt(i)).getidfriend();
										}else if(!AddDescription_Group_Fragment.selectionList.get(i).getselectionAmount().equals("0")
												&& !AddDescription_Group_Fragment.selectionList.get(i).getSelectionId().equals(PreferenceManager.getInstance().getUserId())){
											amountString += AddDescription_Group_Fragment.selectionList.get(i).getselectionAmount();
											selectionString += AddDescription_Group_Fragment.feedList.get(AddDescription_Group_Fragment.selectionList.keyAt(i)).getidfriend();
										}
									}
										//selectionString += selectionList.keyAt(i)+",";
								}
							//addFriendsInGroup();
							ProcessGroupTask(selectionString, amountString);
							
						}
 						
					}
				else if(((RadioButton) mRootView.findViewById(R.id.equalOption)).isChecked()
						&& groupIdCollection.size() != 0){
						//ProcessGroupTask();
					
					
						String paid_for = "", amount = "";
						//groupIdCollection.get(groupSpinner.getSelectedItemPosition()-1);
						groupIdCollection.get(groupSpinner.getSelectedItemPosition());
						
						for(int i = 0; i<feedList.size(); i++){
							if(i == feedList.size()-1){
								if(!feedList.get(i).getfriendAmount().equals("0") && amount.length() != 0
										&& !feedList.get(i).getidfriend().equals(PreferenceManager.getInstance().getUserId())){
									paid_for += ","+feedList.get(i).getidfriend() ;
									amount += ","+feedList.get(i).getfriendAmount();	
								}else if(!feedList.get(i).getfriendAmount().equals("0") &&
										!feedList.get(i).getidfriend().equals(PreferenceManager.getInstance().getUserId())){
									paid_for += feedList.get(i).getidfriend();
									amount += feedList.get(i).getfriendAmount();
								}
							}else{
								if(!feedList.get(i).getfriendAmount().equals("0") && amount.length() != 0 &&
										!feedList.get(i).getidfriend().equals(PreferenceManager.getInstance().getUserId())){
									paid_for +=  ","+ feedList.get(i).getidfriend();
									amount += ","+ feedList.get(i).getfriendAmount();
								}else if(!feedList.get(i).getfriendAmount().equals("0") &&
										!feedList.get(i).getidfriend().equals(PreferenceManager.getInstance().getUserId())){
									paid_for += feedList.get(i).getidfriend() ;
									amount += feedList.get(i).getfriendAmount();
								}
									
							}
						} 
						
						
						String selectionString = "", amountString = "";
						
						
						for(int i = 0; i<valuesHolder.size(); i++){
							
							if(i == valuesHolder.size()-1){
								if(!(valuesHolder.get(i).getAmount() == 0 && amount.length() != 0) &&
										!String.valueOf(Math.round(valuesHolder.get(i).getId())).equals(PreferenceManager.getInstance().getUserId())){
									selectionString += ","+valuesHolder.get(i).getId() ;
									amountString += ","+valuesHolder.get(i).getAmount();	
								}else if(! (valuesHolder.get(i).getAmount() == 0) &&
										!String.valueOf(Math.round(valuesHolder.get(i).getId())).equals(PreferenceManager.getInstance().getUserId())){
									selectionString += valuesHolder.get(i).getId();
									amountString += valuesHolder.get(i).getAmount();
								}
							}else{
								if(!(valuesHolder.get(i).getAmount() == 0 && amount.length() != 0) &&
										!String.valueOf(Math.round(valuesHolder.get(i).getId())).equals(PreferenceManager.getInstance().getUserId())){
									selectionString +=  ","+ valuesHolder.get(i).getId();
									amountString += ","+ valuesHolder.get(i).getAmount();
								}else if(!(valuesHolder.get(i).getAmount() == 0) &&
										!String.valueOf(Math.round(valuesHolder.get(i).getId())).equals(PreferenceManager.getInstance().getUserId())){
									selectionString += valuesHolder.get(i).getId() ;
									amountString += valuesHolder.get(i).getAmount();
								}
									
							}
							
							
							
							/*if(i == valuesHolder.size()-1){
								selectionString = valuesHolder.get(i).get
										
							}else{
									
							}*/
						}
						
						selectionString = selectionString.substring(1, selectionString.length());
						
						amountString = amountString.substring(1, amountString.length());
						
						Log.e("selectionString", selectionString.substring(1, selectionString.length()));
						Log.e("amountString", amountString.substring(1, amountString.length()));
						
						ProcessGroupTask(selectionString, amountString);
					}
				}
			/*else{
					Toast.makeText(getActivity(), "Please Select Any Split Type!", Toast.LENGTH_SHORT).show();
				}*/
				
			/*	if(ValidateForm() && groupIdCollection.size() != 0)
					ProcessGroupTask();*/
			}
		});
		
		((EditText) mRootView.findViewById(R.id.dateText)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getDatePickerDialog().show();
			}
		});
		
		((EditText) mRootView.findViewById(R.id.dateText)).setText(AppConstants.simpleDateFormat.format(new Date()));
		
		twitteraccess = new TwitterFragment(getActivity());

		((RadioGroup) mRootView.findViewById(R.id.splitTypeGroup)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						
						if(filteredList.size() == 0){
							Toast.makeText(getActivity(), "Please Select Any One Friend From List!", Toast.LENGTH_SHORT).show(); 
						}else{
							feedList.clear();
							
							for(int i = 0; i< filteredList.size(); i++){
								feedList.add(filteredList.get(i));
							}
							
							feedList.add(new MyFriendOnGroup_Class(
									PreferenceManager.getInstance().getUserId(), 
									"",
									PreferenceManager.getInstance().getUserMobileNumber(), 
									PreferenceManager.getInstance().getUserFullName(),
									PreferenceManager.getInstance().getUserImagePath(),
									"0",
									"0",
									true
								));
							
							switch (checkedId) {
							case R.id.manualOption:
								
								if (!TextUtils.isEmpty(((EditText) mRootView.findViewById(R.id.amountText)).getText().toString())) {
									((BaseFragment) getParentFragment()).addFragment(new SplictAmountManual_Fragment(
											Double.parseDouble(((EditText) mRootView.findViewById(R.id.amountText)).getText().toString()),
											groupIdCollection.get(groupSpinner.getSelectedItemPosition())), true);
									isChecked = true;
								}else{
									((RadioButton) mRootView.findViewById(R.id.manualOption)).setChecked(false);
									Toast.makeText(getActivity(),"Please Enter Amount!",Toast.LENGTH_SHORT).show();
								}
								/*
								if (!TextUtils.isEmpty(((EditText) mRootView.findViewById(R.id.amountText)).getText().toString())) {
									if(groupSpinner.getSelectedItemPosition() > 0){
										if(groupIdCollection.get(groupSpinner.getSelectedItemPosition()-1)!= null){
											Log.e("Selected Group Position", String.valueOf(groupSpinner.getSelectedItemPosition()));
											((BaseFragment) getParentFragment()).addFragment(new SplictAmountManual_Fragment(
													Double.parseDouble(((EditText) mRootView.findViewById(R.id.amountText)).getText().toString()),
													groupIdCollection.get(groupSpinner.getSelectedItemPosition()-1)), true);
											isChecked = true;
										}
									}else{
										Toast.makeText(getActivity(),"Please Select Any Group!",Toast.LENGTH_SHORT).show();
									}
								} else {
									((RadioButton) mRootView.findViewById(R.id.manualOption)).setChecked(false);
									Toast.makeText(getActivity(),"Please Enter Amount!",Toast.LENGTH_SHORT).show();
								}*/
							 
								break;
							case R.id.equalOption:
								if (!TextUtils.isEmpty(((EditText) mRootView.findViewById(R.id.amountText)).getText().toString())) {
									((BaseFragment) getParentFragment()).addFragment(new SplictAmountEqual_Fragment(
											Integer.parseInt(((EditText) mRootView.findViewById(R.id.amountText)).getText().toString()),
											groupIdCollection.get(groupSpinner.getSelectedItemPosition())), true);
									isChecked = true;
								}else{
									((RadioButton) mRootView.findViewById(R.id.equalOption)).setChecked(false);
									Toast.makeText(getActivity(),"Please Enter Amount!",Toast.LENGTH_SHORT).show();
								}
								
								/*if (!TextUtils.isEmpty(((EditText) mRootView.findViewById(R.id.amountText)).getText().toString())) {
									if(groupSpinner.getSelectedItemPosition() > 0){
										Log.e("Selected Group Position", String.valueOf(groupSpinner.getSelectedItemPosition()));
										if(groupIdCollection.get(groupSpinner.getSelectedItemPosition()-1)!= null){
									((BaseFragment) getParentFragment()).addFragment(new SplictAmountEqual_Fragment(
											Integer.parseInt(((EditText) mRootView.findViewById(R.id.amountText)).getText().toString()),
											groupIdCollection.get(groupSpinner.getSelectedItemPosition()-1)), true);
									isChecked = true;
										}
									}else{
										Toast.makeText(getActivity(),"Please Select Any Group!",Toast.LENGTH_SHORT).show();
									}
									
								} else {
									((RadioButton) mRootView.findViewById(R.id.equalOption)).setChecked(false);
									Toast.makeText(getActivity(),"Please Enter Amount!",Toast.LENGTH_SHORT).show();
								}*/
								break;
							default:
								break;
							}
						}
						
					}
				});
		
		pd = new ProgressDialog(getActivity());
		
		((ImageView) mRootView.findViewById(R.id.locationButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				pd.setMessage("Tracking Your Location...");
				pd.setCancelable(false);
				MyLocation locationPicker = new MyLocation();
				pd.show();
				locationPicker.getLocation(getActivity(), new MyLocation.LocationResult() {
					@Override
					public void gotLocation(final Location location) {
						getActivity().runOnUiThread(new Runnable() {
							@Override
						public void run() {
						pd.dismiss();
						if(location == null){
							Toast.makeText(getActivity(), "Unable to Get Your Location! Try Again!", Toast.LENGTH_SHORT).show();
						}else{
							try {
								Geocoder geocoder;
								List<Address> addresses;
								geocoder = new Geocoder(getActivity(), Locale.getDefault());
								addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
								locationPath = addresses.get(0).getSubLocality()+", "+addresses.get(0).getLocality();
								((TextView)mRootView.findViewById(R.id.locationInfo)).setText(locationPath);
						 		
							} catch (Exception e) {
								e.printStackTrace();
							}
							
						}
							}
							});

					}
						
				});
			}
		});
	
		 getGroupList();
	}
	
	
	Calendar cal = Calendar.getInstance();
	 
	public void getFriendsinGroupDetails(String groupId) {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("getFriendsinGroupDetails",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"idgroup",
				"iduser"
				}, 
				new String[]{
				"group",
				"friendsofgroup",
				groupId,
				PreferenceManager.getInstance().getUserId()
				});
		myAsyncTask.execute();
	}
	
	
	public DatePickerDialog getDatePickerDialog() {
			DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
				 
	            @Override
	            public void onDateSet(DatePicker view, int year,
	                    int monthOfYear, int dayOfMonth) {
	            	((EditText) mRootView.findViewById(R.id.dateText)).setText(dayOfMonth + "/"
	                        + (monthOfYear + 1) + "/" + year);
	            }
	        }, cal.get(Calendar.YEAR),  cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			
		 return datePickerDialog;
	 }

	
	public boolean ValidateForm(){
		try {
			if(groupSpinner.getSelectedItemPosition() < 0){
				Toast.makeText(getActivity(), "Please Select Group Name!", Toast.LENGTH_SHORT).show();
			}
			else if(feedList.size() == 0){
				Toast.makeText(getActivity(), "Please Select Any One Friend From Manual/Equal Split type!", Toast.LENGTH_SHORT).show();
				return false;
			}else if(((EditText) mRootView.findViewById(R.id.amountText)).getText() == null || 
					((EditText) mRootView.findViewById(R.id.amountText)).getText().toString().length() == 0){
				Toast.makeText(getActivity(), "Please Enter Amout!", Toast.LENGTH_SHORT).show();
				return false;
			}/*else if(((EditText) mRootView.findViewById(R.id.dateText)).getText() == null || 
					((EditText) mRootView.findViewById(R.id.dateText)).getText().toString().length() == 0){
				Toast.makeText(getActivity(), "Please Select Date!", Toast.LENGTH_SHORT).show();
				return false;
			}*/
			else if(((EditText) mRootView.findViewById(R.id.commentText)).getText() == null || 
					((EditText) mRootView.findViewById(R.id.commentText)).getText().toString().length() == 0){
				Toast.makeText(getActivity(), "What's your payment for? Please enter details!", Toast.LENGTH_SHORT).show();
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Toast.makeText(getActivity(), "Please Select Group Name!", Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}
	
	
	public void ProcessGroupTask(String paid_for_text, String amount_text) {
		
		String paid_for = paid_for_text, amount = amount_text;
	
		Log.e("****************", "**");
		//Log.e("Group Id", groupIdCollection.get(groupSpinner.getSelectedItemPosition()-1));
		Log.e("Group Id", groupIdCollection.get(groupSpinner.getSelectedItemPosition()));
		Log.e("Paid for", paid_for);
		Log.e("Amount", amount);
		
		Log.e("Values Holder", valuesHolder.toString());
		//			Log.e("picturePath", picturePath);
		Log.e("****************", "**");
		
		MyAsynImageTaskManager.setupParamsAndUrl(AppUrlList.ACTION_URL, new String[] {
					"module",
					"action",
					"paid_by",
					"idgroup",
					"paid_for",
					"amount",
					"feed_content",
					"location",
					"feedimage",
					"date",
					"trans_type",
					"total_amount"
					}, 
					new String[]{
					"feed",
					"addgroup",
					PreferenceManager.getInstance().getUserId(),
					 groupIdCollection.get(groupSpinner.getSelectedItemPosition()),
					//groupIdCollection.get(groupSpinner.getSelectedItemPosition()-1)
					paid_for,
					amount,
					((EditText) mRootView.findViewById(R.id.commentText)).getText().toString(),
					"Chennai",
					picturePath,
					((EditText) mRootView.findViewById(R.id.dateText)).getText().toString(),
					"individual",
					((EditText) mRootView.findViewById(R.id.amountText)).getText().toString()
					},
					new String[]{
					"0", "0", "0", "0", "0", "0", "0", "0", "1", "0", "0", "0"
				});

			new MyAsynImageTaskManager(getActivity(), new MyAsynImageTaskManager.LoadListener() {
				@Override
				public void onLoadComplete(final String jsonResponse) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							try {
								JSONObject rootObj = new JSONObject(jsonResponse);
								 if(rootObj.getBoolean("result")) {
										((BaseFragment)getParentFragment()).popFragment();
									 //DialogManager.showDialog(getActivity(), "Group Transaction Created Successfully!");
									 Toast.makeText(getActivity(), "Group Transaction Created Successfully!", Toast.LENGTH_SHORT).show();
									 //AddTransactionFragment.shouldSkipThisBill = true;
									 ((BaseFragment)getParentFragment()).popFragment();
								}else{
									DialogManager.showDialog(getActivity(), "Error In Creating Group Transaction! Try Again!");
								}
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
								DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
							}
					}
					});
				}
				@Override
				public void onError(final String errorMessage) {
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							DialogManager.showDialog(getActivity(), errorMessage);		
						}
					});
					
				}
			}).execute();
		 
		}
	
	
	public void getGroupList() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		 myAsyncTask.setupParamsAndUrl("getGroupList",getActivity(),AppUrlList.ACTION_URL, new String[] {
					"module",
					"action",
					"iduser"
					}, 
					new String[]{
					"group",
					"grouplist",
					PreferenceManager.getInstance().getUserId()
					});

			myAsyncTask.execute();
		 
		}
	

	private void callFbPost() {

		// TODO Auto-generated method stub
		Log.e("CAlling Fb Post", "....");
		final Feed feed = new Feed.Builder()
				.setName(((EditText) mRootView.findViewById(R.id.amountText)).getText().toString())
				.setCaption("Amount Spent "+ ((EditText) mRootView.findViewById(R.id.amountText)).getText().toString())
				.setDescription(((EditText) mRootView.findViewById(R.id.commentText)).getText().toString())
				
				 .setPicture("https://www.facebook.com/images/fb_icon_325x325.png")
				 
				 .setLink("http://www.bash.co/")
				 
				 .build();
		
	/*	final Feed feed = new Feed.Builder()
		.setName("test").setCaption("Amount Spent ").setDescription("test")
		.build();*/
		
	 	SimpleFacebook.getInstance().publish(feed, true,
				new OnPublishListener() {
					@Override
					public void onException(Throwable throwable) {
						DialogManager.showDialog(getActivity(),
								"Error Occured to Share Message! Try Again!");
					}

					@Override
					public void onFail(String reason) {
						DialogManager.showDialog(getActivity(),
								"Error Occured to Share Message! Try Again! Reason : "+reason);
					}

					@Override
					public void onComplete(String response) {
						DialogManager.showDialog(getActivity(),
								"Message Wrote of FB Wall Sucessfully!");
					}
				});

	}

	final OnLoginListener onLoginListener = new OnLoginListener() {

		@Override
		public void onFail(String reason) {
			Log.e("Login Failed", "....");
		}

		@Override
		public void onException(Throwable throwable) {
			Log.e("Login Failed", "....");
		}

		@Override
		public void onThinking() {
			// show progress bar or something to the user while login is
			Log.e("Login Failed", "....");
		}

		@Override
		public void onLogin() {
			// change the state of the button or do whatever you want
			Log.e("Login Success", "....");
			callFbPost();
		}

		@Override
		public void onNotAcceptingPermissions(Permission.Type type) {
			// toast(String.format("You didn't accept %s permissions",
			// type.name()));
			Log.e("Login Failed", "....");
		}
	};

		 	
	public class MyCustomAdapter extends ArrayAdapter<String>{
 
 		public MyCustomAdapter(Context context, int textViewResourceId, ArrayList<String> objects) {
			
			super(context, textViewResourceId, objects);
			// TODO Auto-generated constructor stub
			}
		
			public void setNotificationwithData(ArrayList<String> objects){
				 
			}

			@Override
			public View getDropDownView(int position, View convertView,
			ViewGroup parent) {
			// TODO Auto-generated method stub
			return getCustomView(position, convertView, parent);
			}

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			return getCustomView(position, convertView, parent);
			}
			
			public View getCustomView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
				LayoutInflater inflater=LayoutInflater.from(getContext());
				convertView = inflater.inflate(R.layout.a_custom_spinner, parent, false);
				TextView label=(TextView)convertView.findViewById(android.R.id.text1);
				label.setText(getItem(position));
			return convertView;
			}
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		if(from.equalsIgnoreCase("getFriendsinGroupDetails"))
		{
			if(output!=null)
			{
				try {
					JSONObject rootObj = new JSONObject(output);
					if(rootObj.getBoolean("result")) {
						JSONArray groupList = rootObj.getJSONArray("friendlist");
						JSONObject item;
						tempfeedList.clear();
						filteredList.clear();

						((EditText) mRootView.findViewById(R.id.attendessSpinner)).setText("All Attendees");

						for(int i = 0; i < groupList.length(); i++) {
							item = groupList.getJSONObject(i);
							Log.e("Friend Id", item.getString("idfriend"));
							Log.e("Friend Number", item.getString("name"));
							Log.e("Friend imagepath", item.getString("imagepath"));
							filteredList.add(new MyFriendOnGroup_Class(
									item.getString("idfriend"), 
									"",
									item.getString("phone_no"), 
									item.getString("name"),
									item.getString("imagepath"),
									"0",
									"0",
									true
									));

							tempfeedList.add(new MyFriendOnGroup_Class(
									item.getString("idfriend"), 
									"",
									item.getString("phone_no"), 
									item.getString("name"),
									item.getString("imagepath"),
									"0",
									"0",
									true
									));
						}

						attendeeAdapter.notifyDataSetChanged();
					}
					else{ 
						DialogManager.showDialog(getActivity(), "No Friends Found in Group!");
						tempfeedList.clear();
						filteredList.clear();
						attendeeAdapter.notifyDataSetChanged();
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
				}

			}
			else
			{
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			}

		}
		else if(from.equalsIgnoreCase("getGroupList"))
				{
					if(output!=null)
					{
						try {
							JSONObject rootObj = new JSONObject(output);
							if(rootObj.getBoolean("result")) {
								groupSpinner.setClickable(true);
								JSONArray groupList = rootObj.getJSONArray("grouplist");
								JSONObject item;
								groupIdCollection.clear();
								spinnerGroupList.clear();
								//spinnerGroupList.add("Select Group Name");
								for(int i = 0;i < groupList.length(); i++){
									item = groupList.getJSONObject(i);
									groupIdCollection.add(item.getString("idgroup"));
									spinnerGroupList.add(item.getString("groupname"));
								}

								dataAdaper = new MyCustomAdapter(getActivity(), R.layout.a_custom_spinner, spinnerGroupList);
								groupSpinner.setAdapter(dataAdaper);

								groupSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
									@Override
									public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
										if(groupIdCollection.size() > 0){
											getFriendsinGroupDetails(groupIdCollection.get(position));	
										}
									}
									@Override
									public void onNothingSelected(AdapterView<?> parentView) {
										// your code here
									}
								});

								groupSpinner.setSelection(0);

							}else{

								groupIdCollection.clear();
								spinnerGroupList.clear();

								groupSpinner.setClickable(false);
								spinnerGroupList.add("No Group Found!");
								DialogManager.showDialog(getActivity(), "No Group Found! First You have to create Group!");
								//AddTransactionFragment.shouldSkipThisBill = true;
								((BaseFragment)getParentFragment()).popFragment();
							}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
							DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
						}
					}
					else 
					{
						DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
					}


				}
		

	}
	 
}
