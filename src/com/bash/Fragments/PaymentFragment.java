package com.bash.Fragments;

import java.util.Random;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;

public class PaymentFragment extends Fragment implements AsyncResponse{
	MyAsynTaskManager  myAsyncTask;
	WebView webview;
	public String RSAkey = "";
	public String randomGeneratedString = "";
	ProgressBar progressBar;
	View mRootView;
	String CHARSET_AZ_09 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	EditText amountText;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		mRootView = inflater.inflate(R.layout.fragment_paymentgateway_page,
				null);
		initializeView();
		return mRootView;
	}

	private void initializeView() {

		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "Make Payment", 0);
		 
		((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				WalletFragment.parentFragmentClass.popFragment();
			}
		});

		webview = (WebView) mRootView.findViewById(R.id.paymentGatewayWebview);
		progressBar = (ProgressBar) mRootView.findViewById(R.id.progressBar);
		amountText= (EditText) mRootView.findViewById(R.id.amountText);
		
		randomGeneratedString = generateString(new Random(), CHARSET_AZ_09, 30);

		Log.e("Random String", randomGeneratedString);

		getRSAkey();

		((Button) mRootView.findViewById(R.id.processPaymentButton)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if(amountText.getText() != null && amountText.getText().toString().length() != 0){
							callPayment();
						}else{
							Toast.makeText(getActivity(), "Please Enter Amount!", Toast.LENGTH_SHORT).show();
						}
				}
		});
	}

	private void getRSAkey() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask
				.setupParamsAndUrl("getRSAkey",getActivity(),
						"http://mobileaps.askpundit.com/bash/paymentgateway/GetRSA.php",
						new String[] { "order_id" },
						new String[] { randomGeneratedString, });
		myAsyncTask.execute();

//		new MyAsynTaskManager(getActivity(), new LoadListener() {
//			@Override
//			public void onLoadComplete(final String jsonResponse) {
//
//				getActivity().runOnUiThread(new Runnable() {
//					public void run() {}
//				});
//			}
//			@Override
//			public void onError(String errorMessage) {
//
//			}
//		}).execute();
	}

	public void callPayment() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask
				.setupParamsAndUrl("callPayment",getActivity(),
						"http://mobileaps.askpundit.com/bash/paymentgateway/ccavRequestHandler.php",
						new String[] { "merchant_id", "order_id", "amount",
								"currency", "redirect_url", "cancel_url",
								"language", "RSAkey", "customer_identifier" },
						new String[] {
								"50301",
								randomGeneratedString,
								amountText.getText().toString(),
								"INR",
								"http://mobileaps.askpundit.com/bash/paymentgateway/ccavResponseHandler.php",
								"http://mobileaps.askpundit.com/bash/paymentgateway/ccavResponseHandler.php",
								"EN", RSAkey, PreferenceManager.getInstance().getUserEmailId() });

		myAsyncTask.execute();
	}

	

	public static String generateString(Random rng, String characters,
			int length) {
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = characters.charAt(rng.nextInt(characters.length()));
		}
		return new String(text);
	}

	@SuppressWarnings("unused")
	class MyJavaScriptInterface {
		@JavascriptInterface
		public void processHTML(String html) {
			// process the html as needed by the app
			// System.out.println(html);
			String status = null;
			if (html.indexOf("Failure") != -1) {
				status = "Transaction Declined!";
			} else if (html.indexOf("Success") != -1) {
				status = "Transaction Successful!";
			} else if (html.indexOf("Aborted") != -1) {
				status = "Transaction Cancelled!";
			} else {
				status = "Status Not Known!";
			}
			Toast.makeText(getActivity(), status, Toast.LENGTH_SHORT)
					.show();
			/*
			 * Intent intent = new Intent(getActivity(), StatusActivity.class);
			 * intent.putExtra("transStatus", status); startActivity(intent);
			 */
		}
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		if(from.equalsIgnoreCase("getRSAkey"))
		{
			if(output!=null)
			{

				RSAkey = output;
			
			}
			else
			{

				Toast.makeText(getActivity(),
						"Oh no! " + "Server Error",
						Toast.LENGTH_SHORT).show();
			
			}
		}
		else if(from.equalsIgnoreCase("callPayment"))
		{
			if(output!=null)
			{

				progressBar.setVisibility(View.VISIBLE);
				webview.getSettings().setJavaScriptEnabled(true);
				webview.loadData(output, null, null);
				webview.addJavascriptInterface(
						new MyJavaScriptInterface(), "HTMLOUT");
				webview.setWebViewClient(new WebViewClient() {
					@Override
					public void onPageFinished(WebView view, String url) {
						super.onPageFinished(webview, url);
					    new Handler().postDelayed(new Runnable() {
					        @Override
					        public void run() {
					        	progressBar.setVisibility(View.GONE);                       
					        }
					    }, 5000);
						if (url.indexOf("/ccavResponseHandler.jsp") != -1) {
							webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
						}
					}
					
					@Override
					public void onReceivedError(WebView view,
							int errorCode, String description,
							String failingUrl) {
						Toast.makeText(getActivity(),
								"Oh no! " + description,
								Toast.LENGTH_SHORT).show();
					}
				});

			
			}
			else
			{

				Toast.makeText(getActivity(),
						"Oh no! " + "Server Error",
						Toast.LENGTH_SHORT).show();
			
			}
		}
	}

}
