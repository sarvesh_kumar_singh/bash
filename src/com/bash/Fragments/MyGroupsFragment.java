package com.bash.Fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.HomeActivity;

import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.ListModels.BashUsers_Class;
import com.bash.ListModels.MyGroups_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynImageTaskManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MyGroupsFragment extends Fragment implements AsyncResponse{
	int pos=0;
	MyAsynTaskManager  myAsyncTask;
	View mRootView, addgroupAlertView;
	public static SwipeListView myGroupListView;
	Dialog addmemberDialog;
	MyGroupsAdapter adapter;
	ArrayList<MyGroups_Class> feedList = new ArrayList<MyGroups_Class>();
	View camera_gallery;
	Dialog camaeraDialog;
	Uri mCapturedImageURI;
	public ImageView fileImage;
	String picturePath="";
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_mygroups_page, null);
		((HomeActivity)getActivity()).currentFragment = this;
		return mRootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initializeView();
		initializePictureAlertView();
	}
	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//Sarvesh((HomeActivity)getActivity()).menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//Sarvesh((HomeActivity)getActivity()).menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
	}
	 
	
	private void initializeView() {
		// TODO Auto-generated method stub
		 //((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "My Groups", R.drawable.add_btn);
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn, "Groups", 0);
		 ((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					((BaseFragment)getParentFragment()).popFragment();
					((HomeActivity) getActivity()).slidingmenu_layout.toggleMenu();
				}
		 });
		 
		 //initializeAlertView();
		 
		 ((ImageView)getActivity().findViewById(R.id.toprightsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					addmemberDialog.show();
				}
		 });
		 
		feedList.clear();
		myGroupListView = (SwipeListView)mRootView.findViewById(R.id.groupListView);
	 	adapter = new MyGroupsAdapter(getActivity(), feedList);
		myGroupListView.setAdapter(adapter);
	
		getGroupList();
		
		myGroupListView.setSwipeListViewListener(new BaseSwipeListViewListener() {
			 @Override
	         public void onClickFrontView(int position) {
				 /*((BaseFragment)getParentFragment()).replaceFragment(new MyFriendOnGroupFragment
						 (feedList.get(position).getgroupName()), true);*/
				 ((BaseFragment)getParentFragment()).replaceFragment(new MyGroups_Transaction_Fragment(
						 (feedList.get(position))), true);
	         }
	 
	         @Override
	         public void onClickBackView(int position) {
	             Log.d("swipe", String.format("onClickBackView %d", position));
	             myGroupListView.closeAnimate(position);//when you touch back view it will close
	         }
	 
	         @Override
	         public void onDismiss(int[] reverseSortedPositions) {
	         }
	     });
		
		   	myGroupListView.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT); // there are five swiping modes
		   	myGroupListView.setSwipeActionLeft(SwipeListView.SWIPE_MODE_DEFAULT); //there are four swipe actions
		   	myGroupListView.setOffsetLeft(PreferenceManager.getInstance().getPercentageFromWidth(38));
		   	myGroupListView.setAnimationTime(50); // animarion time
		   	myGroupListView.setSwipeOpenOnLongPress(true); // enable or disable SwipeOpenOnLongPress
		   	
		   	// Capture Text in EditText
	        ((EditText) mRootView.findViewById(R.id.searchBox)).addTextChangedListener(new TextWatcher() {
	            @Override
	            public void afterTextChanged(Editable arg0) {
	                String text = ((EditText) mRootView.findViewById(R.id.searchBox))
	                				.getText().toString();
	                adapter.getFilter().filter(text);
	            }
	 
	            @Override
	            public void beforeTextChanged(CharSequence arg0, int arg1,
	                    int arg2, int arg3) {
	            }
	 
	            @Override
	            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
	                    int arg3) {
	            }
	        });
	        
	}

	
	private void initializeAlertView() {

		addmemberDialog = new Dialog(getActivity());
		addmemberDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		addgroupAlertView = View.inflate(getActivity(), R.layout.alert_addgroup_dialog, null);
		addmemberDialog.setContentView(addgroupAlertView);
		
		fileImage = ((ImageView) addgroupAlertView.findViewById(R.id.groupImage));
	
		 ((ImageView) addgroupAlertView.findViewById(R.id.cancelDialog)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					addmemberDialog.dismiss();
				}
		 });
//	block	 
//		 ((ImageView) addgroupAlertView.findViewById(R.id.addtoGroupButton)).setOnClickListener(new OnClickListener() {
//				@Override
//				public void onClick(View v)
//				{
//					createGroupInServer(((EditText)addgroupAlertView.findViewById(R.id.groupText)).getText().toString());
//				}
//		 });
		
		 ((ImageView) addgroupAlertView.findViewById(R.id.groupImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					camaeraDialog.show();
				}
		 });
		 
		 
	}
	
	private void initializePictureAlertView() {
		// TODO Auto-generated method stub
		camera_gallery = View.inflate(getActivity(),
				R.layout.alert_gallerycamera_mode_view, null);
		camaeraDialog = new Dialog(getActivity());
		camaeraDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		camaeraDialog.setContentView(camera_gallery);
		
		
		((Button) camera_gallery.findViewById(R.id.openGalleryBtn))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						camaeraDialog.dismiss();
						Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						getActivity().startActivityForResult(i,	AppConstants.CODE_GALLERY);
					}
		});

		((Button) camera_gallery.findViewById(R.id.openCameraBtn)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						camaeraDialog.dismiss();
						String fileName = "temp.jpg";
						ContentValues values = new ContentValues();
						values.put(MediaStore.Images.Media.TITLE, fileName);
						mCapturedImageURI = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
										values);
						Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
						intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
						getActivity().startActivityForResult(intent, AppConstants.CODE_CAMERA);
					}
		});

	}
	
	

	 public void getGroupList() {
		 myAsyncTask=new MyAsynTaskManager();
			myAsyncTask.delegate=this;
			myAsyncTask.setupParamsAndUrl("getGroupList",getActivity(),AppUrlList.ACTION_URL, new String[] {
					"module",
					"action",
					"iduser"
					}, 
					new String[]{
					"group",
					"grouplist",
					PreferenceManager.getInstance().getUserId()
					});
			myAsyncTask.execute();

//			new MyAsynTaskManager(getActivity(), new LoadListener() {
//				@Override
//				public void onLoadComplete(final String jsonResponse) {
//					getActivity().runOnUiThread(new Runnable() {
//						@Override
//						public void run() {}
//					});
//				}
//				@Override
//				public void onError(final String errorMessage) {
//					getActivity().runOnUiThread(new Runnable() {
//						public void run() {
//							DialogManager.showDialog(getActivity(), errorMessage);		
//						}
//					});
//					
//				}
//			}).execute();
		}
	 
	
	 public void createGroupInServer(final String groupName) {
		 
		 MyAsynImageTaskManager.setupParamsAndUrl(AppUrlList.ACTION_URL, new String[] {
					"module",
					"action",
					"iduser",
					"groupname",
					"groupimage"
					}, 
					new String[]{
					"group",
					"add",
					PreferenceManager.getInstance().getUserId(),
					groupName,
					picturePath
					}, new String[]{
				 	"0", "0", "0", "0", "1"  
		 			});

		 new MyAsynImageTaskManager(getActivity(), new MyAsynImageTaskManager.LoadListener() {
			
			@Override
			public void onLoadComplete(final String jsonResponse) {

				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						try {
							if(jsonResponse == null){
								Log.e("Null", "retured");
							}
							//01-07 15:52:46.816: E/Reponse :(15432): {"result":true,"msg":"success","idgroup":1}
							//01-07 15:53:32.783: E/Reponse :(15432): {"result":true,"msg":"success","grouplist":[{"idgroup":"1","groupname":"test group","imagepath":""}]}

							JSONObject rootObj = new JSONObject(jsonResponse);
							if(rootObj.getBoolean("result")){
								/*feedList.add(new MyGroups_Class(
										groupName,
										rootObj.getString("idgroup"), 
										rootObj.getString("imagepath")));*/
								feedList.add(new MyGroups_Class(
										groupName,
										rootObj.getString("idgroup"), 
										rootObj.getString("imagepath")));
								
								adapter.notifyWithDataSet(feedList);
								//adapter.notifyDataSetChanged();
								addmemberDialog.dismiss();
								DialogManager.showDialog(getActivity(), "Group Created Successfully!");
								
								((BaseFragment)getParentFragment()).replaceFragment(new AddFriendsOnGroupFragment(
										rootObj.getString("idgroup")), true);
								
								/*((BaseFragment)getParentFragment()).replaceFragment(new MyGroups_Transaction_Fragment(
										 (feedList.get(feedList.size()-1))), true);*/
							}
						else{
							if(rootObj.getString("msg").equals("Group Name exist"))
								DialogManager.showDialog(getActivity(), "Group Name is Already Exist!");
							else
								DialogManager.showDialog(getActivity(), "Error In Creating New Group! Try Again!");
						}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
							DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
						}
				}
				});
				
			}
			
			@Override
			public void onError(final String errorMessage) {
				getActivity().runOnUiThread(new Runnable() {
					public void run() {
						DialogManager.showDialog(getActivity(), errorMessage);		
					}
				});
				
			}
		}).execute();
			
		}
	 
	 
	 public void onActivityResult(int requestCode, int resultCode, Intent data) {

			super.onActivityResult(requestCode, resultCode, data);
			
			switch (requestCode) {
			case AppConstants.CODE_CAMERA:
				if (resultCode == getActivity().RESULT_OK) {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = getActivity().getContentResolver().query(
							mCapturedImageURI, projection, null, null, null);
					int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					picturePath = cursor.getString(column_index_data);
					cursor.close();
					ImageLoader.getInstance().displayImage("file://" + picturePath,	fileImage);
					Log.e("Camera Path", picturePath);
				}
				break;
			case AppConstants.CODE_GALLERY:
				if (resultCode == getActivity().RESULT_OK) {

					Uri selectedImage = data.getData();
					String[] filePathColumn = { MediaStore.Images.Media.DATA };
					Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
					cursor.moveToFirst();
					int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
					picturePath = cursor.getString(columnIndex);
					cursor.close();
					ImageLoader.getInstance().displayImage("file://" + picturePath,	fileImage);
				}
				break;
			default:
				break;
			}
			
	 }
	 public class MyGroupsAdapter extends BaseAdapter implements Filterable
	 {
	 		public ArrayList<MyGroups_Class> originalList = new ArrayList<MyGroups_Class>();
	 	 	private ArrayList<MyGroups_Class>  feedList = new ArrayList<MyGroups_Class>();
	 		private Activity context;
	 	    private LinearLayout.LayoutParams backViewParams;
	 	    //private MyGroups_Class emptyList = new MyGroups_Class("empty", 0);
	 	    public GroupFilter myFilter;
	 	    public MyGroupsAdapter(Activity context, ArrayList<MyGroups_Class> feedList) {
	 	        this.context = context;
	 	    	this.feedList = feedList;
	 	    	this.originalList = feedList;
	 	    	backViewParams = new LinearLayout.LayoutParams(PreferenceManager.getInstance().getPercentageFromWidth(35), 
	 	    			LayoutParams.MATCH_PARENT);
	 	    }

	 	    public void notifyWithDataSet(ArrayList<MyGroups_Class> feedList){
	 	    	this.feedList = feedList;
	 	    	this.originalList = feedList;
	 	    	this.notifyDataSetChanged();
	 	    }
	 	    
	 	    @Override
	 	    public View getView(final int position, View convertView, ViewGroup parent) {
	 	        ViewHolder holder = null;
	 	        final MyGroups_Class listItem = getItem(position);
	 	        if (convertView == null) {
	 	        	LayoutInflater inflater = LayoutInflater.from(context);
	 	        	convertView = inflater.inflate(R.layout.custom_mygroups_listview, null);
	 	            holder = new ViewHolder();
	 	            holder.groupName = (TextView) convertView.findViewById(R.id.groupName);
	 	            holder.groupImageSource = (ImageView) convertView.findViewById(R.id.groupImageSource);
	 	            holder.editGroup = (ImageView) convertView.findViewById(R.id.editGroup);
	 	            holder.deleteGroup = (ImageView) convertView.findViewById(R.id.deleteGroup);
	 	            holder.backView = (RelativeLayout) convertView.findViewById(R.id.backView);
	 	            holder.tviAddabs = (TextView) convertView.findViewById(R.id.tviAddTabs);
	 	            holder.tviSettings = (TextView) convertView.findViewById(R.id.tviSettings);
	 	            convertView.setTag(holder);
	 	        } else {
	 	            holder = (ViewHolder) convertView.getTag();
	 	        }
	 	        
	 	        holder.backView.setLayoutParams(backViewParams);
	 	        
	 	        if(listItem.getgroupName().equals("empty")){
	 	        	((RelativeLayout) convertView.findViewById(R.id.frontView)).setVisibility(View.GONE);
	 		        ((LinearLayout) convertView.findViewById(R.id.backViewLayout)).setVisibility(View.GONE);	
	 		        ((TextView) convertView.findViewById(R.id.emptyMessage)).setVisibility(View.VISIBLE);
	 	        }
	 	        if(listItem.getimagePath() != null && listItem.getimagePath().length() != 0){
	 	        	ImageLoader.getInstance().displayImage(listItem.getimagePath(), 
	 						holder.groupImageSource, BashApplication.options, BashApplication.animateFirstListener);
	 	        	}
	 	        else
	 	        	holder.groupImageSource.setImageResource(R.drawable.addphoto_img_block);
	 	        
	 	        holder.groupName.setText(listItem.getgroupName());
	 	        
	 	        Log.e("Image path "+position, listItem.getimagePath());
	 	        
	 	        holder.deleteGroup.setOnClickListener(new OnClickListener() {
	 				@Override
	 				public void onClick(View v) {
	 					// TODO Auto-generated method stub
	 					pos=position;
	 					removeGroupFromServer(listItem.getgroupId(), pos);
	 				}
	 			});
	 	       holder.tviAddabs.setOnClickListener(new OnClickListener() {
	 				@Override
	 				public void onClick(View v) {
	 					// TODO Auto-generated method stub
	 					pos=position;
	 					Toast.makeText(context, "Add Tabs will Come soon...", Toast.LENGTH_LONG).show();
	 				}
	 			});
	 	      holder.tviSettings.setOnClickListener(new OnClickListener() {
	 				@Override
	 				public void onClick(View v) {
	 					// TODO Auto-generated method stub
	 					pos=position;
	 					Toast.makeText(context, "Settings will Come soon...", Toast.LENGTH_LONG).show();
	 				}
	 			});
	 	        
	 	        /*if(listItem.getimagepath() != null &&
	 	        		listItem.getimagepath().length()!=0)
	 	        	ImageLoader.getInstance().displayImage(listItem.getimagepath(), holder.friendImageSource);*/
	 	        
	 	        return convertView;
	 	    }

	 	    
	 	    
	 	   /* public void checkEmptyList(){
	 	    	if(feedList.size() == 0){
	 				this.feedList.add(emptyList);
	 			}
	 	    }
	 	    */
	 	    @Override
	 	    public int getCount() {
	 	    	//	checkEmptyList();
	 	        return feedList.size();
	 	    }

	 	    @Override
	 	    public MyGroups_Class getItem(int position) {
	 	        return feedList.get(position);
	 	    }

	 	    @Override
	 	    public long getItemId(int position) {
	 	        return position;
	 	    }

	 	    private class ViewHolder {
	 	        TextView groupName,tviAddabs,tviSettings;
	 	        ImageView groupImageSource, editGroup, deleteGroup;
	 	        RelativeLayout backView;
	 	    }

	 	   
	 	    
	 	 // Filter Class
	 	    public void filter(String charText) {
	 	        charText = charText.toLowerCase(Locale.getDefault());
	 	        Log.e("charText", charText+"");
	 	        Log.e("Originallengh", String.valueOf(originalList.size()));
	 	        feedList.clear();
	 	        if (charText.length() == 0) {
	 	        	feedList.addAll(originalList);
	 	        }else
	 	        {
	 	            for (MyGroups_Class wp : originalList)
	 	            {
	 	                if (wp.getgroupName().startsWith(charText))
	 	                {
	 	                	Log.e("Groupname", wp.getgroupName());
	 	                	feedList.add(wp);
	 	                }
	 	            }
	 	        }
	 	        this.notifyDataSetChanged();
	 	        this.notifyDataSetInvalidated();
	 	    }

	 	@Override
	 	public Filter getFilter() {
	 		// TODO Auto-generated method stub
	 		  if (myFilter == null)
	 			  myFilter = new GroupFilter();
	 		    return myFilter;
	 	}
	 	 
	 	//filter Class... 
	 		private class GroupFilter extends Filter {
	 			@Override
	 			protected FilterResults performFiltering(CharSequence constraint) {
	 				FilterResults results = new FilterResults();
	 				// We implement here the filter logic
	 				if (constraint == null || constraint.length() == 0) {
	 					// No filter implemented we return all the list
	 					results.values = originalList;
	 					results.count = originalList.size();
	 				} else {
	 					// We perform filtering operation
	 					List<MyGroups_Class> tempList = new ArrayList<MyGroups_Class>();
	 					for (MyGroups_Class p : feedList) {
	 						if (p.getgroupName().toUpperCase()
	 								.startsWith(constraint.toString().toUpperCase()))
	 							tempList.add(p);
	 					}
	 					results.values = tempList;
	 					results.count = tempList.size();
	 				}
	 				return results;
	 			}

	 			@Override
	 			protected void publishResults(CharSequence constraint,
	 					FilterResults results) {
	 				// Now we have to inform the adapter about the new list filtered
	 				if (results.count == 0)
	 					notifyDataSetInvalidated();
	 				else {
	 					feedList = (ArrayList<MyGroups_Class>) results.values;
	 					notifyDataSetChanged();
	 				}
	 			}
	 		}
	     	
	     		
	 		 
	 		  
	 		 
	 	    
	 	    
	 }
	 public void removeGroupFromServer(final String groupId,final int poistion) {
	    	myAsyncTask=new MyAsynTaskManager();
			myAsyncTask.delegate=this;
		 	Log.e("Group Name", groupId);	
			myAsyncTask.setupParamsAndUrl("removeGroupFromServer",getActivity(),AppUrlList.ACTION_URL, new String[] {
					"module",
					"action",
					"idgroup",
					"iduser"
					}, 
					new String[]{
					"group",
					"remove",
					groupId,
					PreferenceManager.getInstance().getUserId()
					});
myAsyncTask.execute();

		}
	@Override
	public void backgroundProcessFinish(String from, String output) {
		
		feedList.add(new MyGroups_Class("Bash","1",""));
		feedList.add(new MyGroups_Class("Appartment","2",""));
		feedList.add(new MyGroups_Class("College","3",""));
		feedList.add(new MyGroups_Class("The Gang","4",""));
		
		adapter.notifyDataSetChanged();
		if(from.equalsIgnoreCase("getGroupList"))
		{
			if(output!=null)
			{

				try {
					JSONObject rootObj = new JSONObject(output);
					if(rootObj.getBoolean("result")) {
							JSONArray groupList = rootObj.getJSONArray("grouplist");
							JSONObject item;
							feedList.clear();
								for(int i = 0;i < groupList.length(); i++){
									item = groupList.getJSONObject(i);
									feedList.add(new MyGroups_Class(
											item.getString("groupname"), 
											item.getString("idgroup"), 
											item.getString("imagepath")));
									}
								adapter.notifyDataSetChanged();
					}
				else{ 
					//((RelativeLayout) mRootView.findViewById(R.id.noListLayout)).setVisibility(View.VISIBLE);
					DialogManager.showDialog(getActivity(), "No Groups Found!");
					//DialogManager.showDialog(getActivity(), "Error In Getting Groups List! Try Again!");
				}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
				}
		
			}
			else
			{
				
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			}
		}
		else if(from.equalsIgnoreCase("removeGroupFromServer"))
		{
			if(output!=null)
			{

					try {
						if(output == null){
							Log.e("Null", "retured");
						}
						JSONObject rootObj = new JSONObject(output);
						if(rootObj.getBoolean("result")){
							removeItemFromList(pos); 
							DialogManager.showDialog(getActivity(), "Group Deleted Successfully!");
							
						}
					else{
						DialogManager.showDialog(getActivity(), "Error in Delete Group! Try Again!");
					}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
						DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
					}
			
			}else
			{

				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			
			}
		}
	}
	public void removeItemFromList(int position)
		{
	    	MyGroupsFragment.myGroupListView.closeAnimate(position);
			feedList.remove(position);
	 		adapter.notifyDataSetInvalidated();
			adapter.notifyDataSetChanged();
	    }
}
