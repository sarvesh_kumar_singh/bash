package com.bash.Fragments;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Adapters.TagFriendsAdapter;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.ListModels.MyFeedComment_Class;
import com.bash.ListModels.MyFriendOnGroup_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;
import com.nostra13.universalimageloader.core.ImageLoader;
@SuppressLint("ValidFragment")
public class TagPhotoFragment extends Fragment implements AsyncResponse{
	MyAsynTaskManager  myAsyncTask;
	String commentactionview="";
	View view;
	int pos=0;
	View mRootView, tagDialogView, commentsDialogView;
	Dialog tagDialog, commentsDialog;
	int photoId;
	TagFriendsAdapter adapter;
	ArrayList<MyFriendOnGroup_Class> feedlist = new ArrayList<MyFriendOnGroup_Class>();
	ListView friendsListView, commentsListView;
	
	ListView myfeedListView;
	MyFeedCommentAdapter commentsadapter;
	ArrayList<MyFeedComment_Class> commentsfeedList = new ArrayList<MyFeedComment_Class>();
	
	public TagPhotoFragment(int id){
		this.photoId = id;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		mRootView = inflater.inflate(R.layout.fragment_tagphoto_page, null);
		initializeViews();
		return mRootView;
	}

	private void initializeViews() {
		// TODO Auto-generated method stub
		
		initializeAlertViews();
		initializeCommentBoxAlertViews();
		
		((ImageView)mRootView.findViewById(R.id.friendImage)).setImageResource(photoId);
		
		((ImageView)mRootView.findViewById(R.id.friendImage))
						.setOnTouchListener(new View.OnTouchListener() {
	        public boolean onTouch(View v, MotionEvent event) {
	            float x = event.getX();
	            float y = event.getY();
	            
	            tagDialog.show();
	            
				return false;
	        } 
		});
		
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "Tag Photo", 0);
		 
		 ((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					((BaseFragment)getParentFragment()).popFragment();
				}
		 });
		 
		 
		 ((CheckBox) mRootView.findViewById(R.id.commentButton)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				commentsDialog.show();	
			}
		});
		 
	 
	}
	
	
	/*private void setCommentsDatas() {
		// TODO Auto-generated method stub
		for(int i = 0; i<5; i++){
			switch (i) {
			case 0:
				commentsfeedList.add(new MyFeedComment_Class(R.drawable.profileone, 
						"Sam Anderson", "1", "Hope you guys enjoyed your meals at Smoke House Deli.", String.valueOf((1+i)+"/10/2014")));
				break;
			case 1:
				commentsfeedList.add(new MyFeedComment_Class(R.drawable.profilefour, 
						"Ragavan", "2", "Hope you guys enjoyed your meals at Smoke House Deli.", String.valueOf((1+i)+"/10/2014")));
				break;
			case 2:
				commentsfeedList.add(new MyFeedComment_Class(R.drawable.profilethree, 
						"Anbu Selvan", "3", "Hope you guys enjoyed your meals at Smoke House Deli.", String.valueOf((1+i)+"/10/2014")));
				break;
			case 3:
				commentsfeedList.add(new MyFeedComment_Class(R.drawable.profiletwo, 
						"Deena", "4", "Hope you guys enjoyed your meals at Smoke House Deli.", String.valueOf((1+i)+"/10/2014")));
				break;
			case 4:
				commentsfeedList.add(new MyFeedComment_Class(R.drawable.profilefour, 
						"Jagthesh", "5", "Hope you guys enjoyed your meals at Smoke House Deli.", String.valueOf((1+i)+"/10/2014")));
				break;
			case 5:
				commentsfeedList.add(new MyFeedComment_Class(R.drawable.profileone, 
						"Karuppaiyah", "6", "Hope you guys enjoyed your meals at Smoke House Deli.", String.valueOf((1+i)+"/10/2014")));
			default:
				break;
			}
		}
			}
			*/


	
	private void initializeCommentBoxAlertViews() {
		// TODO Auto-generated method stub
		
		commentsDialogView = View.inflate(getActivity(), R.layout.alert_comment_photo, null);
		
		commentsDialog = new Dialog(getActivity());
		
		commentsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		commentsDialog.setContentView(commentsDialogView);
		
		commentsListView = (ListView) commentsDialogView.findViewById(R.id.commentListview);
		
		if(commentsfeedList.size() == 0)
			//setCommentsDatas();
		
		commentsadapter = new MyFeedCommentAdapter(getActivity(), commentsfeedList);
		
		commentsListView.setAdapter(commentsadapter);
		
		((ImageView) commentsDialogView.findViewById(R.id.commentButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				commentsDialog.dismiss();
			}
		});
		
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(commentsDialog.getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		commentsDialog.getWindow().setAttributes(lp);
	}
	
	private void initializeAlertViews() {
		// TODO Auto-generated method stub
		
		tagDialogView = View.inflate(getActivity(), R.layout.friendscreen, null);
		
		tagDialog = new Dialog(getActivity());
		
		tagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		tagDialog.setContentView(tagDialogView);
		
		friendsListView = (ListView) tagDialogView.findViewById(R.id.friendsListView);
		
		if(feedlist.size() == 0)
			//setDatas();
		
		adapter = new TagFriendsAdapter(getActivity(), feedlist);
		
		friendsListView.setAdapter(adapter);
		
		friendsListView.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				tagDialog.dismiss();
				Toast.makeText(getActivity(), "Photo Tagged!", Toast.LENGTH_SHORT).show();
			}
		});
		
		((EditText) tagDialogView.findViewById(R.id.searchFriendsEditText)).addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				 if (count < before) {
					 // We're deleting char so we need to reset the adapter data
					 adapter.resetData();
				 }
				 adapter.getFilter().filter(s.toString());
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});
		
 		((ImageView) tagDialogView.findViewById(R.id.cancelButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tagDialog.dismiss();
			}
		});
 		
	}
	

	/*private void setDatas() {
		// TODO Auto-generated method stub
		for(int i = 0; i < 10; i++){
			switch (i) {
			case 0:
				feedlist.add(new MyFriendOnGroup_Class("Thomas Stephen", R.drawable.profilefour));
				break;
			case 1:
				feedlist.add(new MyFriendOnGroup_Class("Sam Anderson", R.drawable.profileone));
				break;
			case 2:
				feedlist.add(new MyFriendOnGroup_Class("John Paul", R.drawable.profiletwo));
				break;
			case 3:
				feedlist.add(new MyFriendOnGroup_Class("Christina Jenifer", R.drawable.profilethree));
				break;
			case 4:
				feedlist.add(new MyFriendOnGroup_Class("Charles Xavier", R.drawable.profileone));
				break;
			case 5:
				feedlist.add(new MyFriendOnGroup_Class("Paul Jackson", R.drawable.profilefour));
				break;
			case 6:
				feedlist.add(new MyFriendOnGroup_Class("Simmons John Paul", R.drawable.profiletwo));
				break;
			case 7:
				feedlist.add(new MyFriendOnGroup_Class("Richard Rov", R.drawable.profilethree));
				break;
			case 8:
				feedlist.add(new MyFriendOnGroup_Class("Thomson", R.drawable.profileone));
				break;
			case 9:
				feedlist.add(new MyFriendOnGroup_Class("Alen Peter", R.drawable.profileone));
				break;
			case 10:
				feedlist.add(new MyFriendOnGroup_Class("Babie Darling", R.drawable.profilethree));
				break;
			default:
				break;
			}
		}
	}*/
	public class MyFeedCommentAdapter extends BaseAdapter
	{
		 	private ArrayList<MyFeedComment_Class> feedList = new ArrayList<MyFeedComment_Class>();
			private Activity context;
		    
		    public MyFeedCommentAdapter(Activity context, ArrayList<MyFeedComment_Class> feedList) {
		        this.context = context;
		    	this.feedList = feedList;
		    }
		    
		    public void notifyDataSet(){
		    	this.notifyDataSetChanged();
		    }
		    
		    public void notifyWithDataSet(ArrayList<MyFeedComment_Class> newList){
		    	this.feedList = newList;
		    	this.notifyDataSetChanged();
		    }

		    @Override
		    public View getView(final int position, View convertView, ViewGroup parent) {
		        ViewHolder holder = null;
		        final MyFeedComment_Class listItem = getItem(position);
		        if (convertView == null) {
		        	LayoutInflater inflater = LayoutInflater.from(context);
		        	convertView = inflater.inflate(R.layout.custom_feed_myfeed_comments_new, null);
		        	holder = new ViewHolder();
		        	
		           	holder.commentantName = (TextView) convertView.findViewById(R.id.commentantName);
		        	holder.commentText = (TextView) convertView.findViewById(R.id.commentText);
		        	holder.commentLikeCount = (TextView) convertView.findViewById(R.id.commentLikeCount);
		        	holder.timeofComment = (TextView) convertView.findViewById(R.id.timeofComment);
		        	holder.commentLikeButton  = (ImageView) convertView.findViewById(R.id.commentLikeButton);
		        	holder.commentantImage  = (ImageView) convertView.findViewById(R.id.commentantImage);
		        	holder.commentLikeBox  = (LinearLayout) convertView.findViewById(R.id.commentLikeBox);
		            convertView.setTag(holder);
		            
		        } else {
		            holder = (ViewHolder) convertView.getTag();
		        }

		        holder.commentantName.setText(listItem.getcommentantName());
		        holder.commentText.setText(listItem.getcommentText());
		        holder.commentLikeCount.setText(listItem.getcommentLikeCount()+" Likes");
		        //holder.commentantImage.setImageResource(listItem.getcommentantImage());
		        
		        final ImageView commentantImageView = holder.commentantImage; 
		        
		        ImageLoader.getInstance().displayImage(listItem.getcommentantImage(), 
	        			holder.commentantImage, BashApplication.options, BashApplication.animateFirstListener);
		        
		        if(listItem.getcommentantImage() != null && listItem.getcommentantImage().length() != 0){
		        	ImageLoader.getInstance().displayImage(listItem.getcommentantImage(), 
		        			holder.commentantImage, BashApplication.options, BashApplication.animateFirstListener);
		        	}
		        else
		        	holder.commentantImage.setImageResource(R.drawable.addphoto_img_block);
		        
		        holder.timeofComment.setText(listItem.gettimeofComment());
		        
		        
		        if(listItem.getisLiked().equals("1"))
		        	holder.commentLikeButton.setImageResource(R.drawable.like_icon_selected);
		        else
		        	holder.commentLikeButton.setImageResource(R.drawable.like_icon_unselected);
		     
		       // final View view = convertView;
		        view=convertView;
		        
		        holder.commentLikeBox.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if(listItem.getisLiked().equals("1")){
							pos=position;
							commentactionview="unlikecomment";
							makeAnLikeView(listItem.getcommentId(), view, commentactionview, pos);
						}else{
							pos=position;
							commentactionview="commentlike";
							makeAnLikeView(listItem.getcommentId(), view, commentactionview, pos);
						}
						
					}
				});
		        
		         
		        
		        
		     /*   holder.commentantImage.setImageBitmap(CurvedImageBitmapProvider.getRoundedCornerBitmap(
		        		(BitmapFactory.decodeResource(context.getResources(), R.drawable.addphoto_img_block)), 10));*/
		        
		       /* if(listItem.isLike)
		        	holder.isLikeButton.setChecked(true);
		        else
		        	holder.isLikeButton.setChecked(false);
		        
		        if(listItem.isComment)
		        	holder.isCommentButton.setChecked(true);
		        else
		        	holder.isCommentButton.setChecked(false);*/
		        return convertView;
		    }
	 
		    @Override
		    public int getCount() {
		        return feedList.size();
		    }

		    @Override
		    public MyFeedComment_Class getItem(int position) {
		        return feedList.get(position);
		    }

		    @Override
		    public long getItemId(int position) {
		        return position;
		    }
		    
		    public class ViewHolder {
		        TextView commentantName;
		        TextView commentText;
		        TextView commentLikeCount;
		        TextView timeofComment;
		        ImageView commentantImage;
		        ImageView commentLikeButton;
		        LinearLayout commentLikeBox;
		    }
		    
		    
		    
		    
		   
		    
	 
		    
		    
	}

	 public void makeAnLikeView(String commetnId, final View view, final String commentAction, final int position){
	    	myAsyncTask=new MyAsynTaskManager();
			myAsyncTask.delegate=this;
	    	myAsyncTask.setupParamsAndUrl("makeAnLikeView",getActivity(),AppUrlList.ACTION_URL, new String[] {
					"module",
					"action",
					"idcomment",
					"iduser"
				 	}, 
					new String[]{
					"feed",
					commentAction,
					commetnId,
					PreferenceManager.getInstance().getUserId()
					});
	    	myAsyncTask.execute();
		}
	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		
	}
}
