package com.bash.Fragments;

import java.util.ArrayList;

import com.bash.R;
import com.bash.ListModels.MyFeed_Class;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class TabUserFragment extends Fragment{
	
	View mRootView;
	ListView myfeedListView;
	ArrayList<MyFeed_Class> feedList = new ArrayList<MyFeed_Class>();
	
	//FragmentTabHost mTabhost;
	
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		//mTabhost = new FragmentTabHost(getActivity());
		/*mRootView = inflater.inflate(R.layout.fragment_home, null);
		mTabhost = (FragmentTabHost)mRootView.findViewById(R.id.fragmenttabhost);
		//mTabhost.setup(getActivity(), getActivity().getSupportFragmentManager(), R.id.home);
		mTabhost.setup(getActivity(), getChildFragmentManager(), R.id.fragmentframe_holder);
		initializeTabHost();*/
		//mRootView.setLayerType(View.LAYER_TYPE_NONE, null);
		//initializeTabHost();
		//return mTabhost;
		mRootView = inflater.inflate(R.layout.fragment_feed_page, null);
		
		return mRootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initializeView();
	}
	
	private void initializeView() {
		// TODO Auto-generated method stub
		myfeedListView = (ListView)mRootView.findViewById(R.id.generalListView);
		if(feedList.size() == 0)
			setDatas();
	/*	adapter = new MyFeedAdapter(getActivity(), feedList);
		myfeedListView.setAdapter(adapter);*/
	}

	private void setDatas() {
		// TODO Auto-generated method stub
		for(int i = 0; i<5; i++){
			switch (i) {
			case 0:
				break;
			case 1:
				feedList.add(new MyFeed_Class(R.drawable.addphoto_img_block, 
						"Ramesh", "Suresh", "Payment for dinner and Lunch", "Chennai", String.valueOf((1+i)+"/10/2014")));
				
				break;
			case 2:
				feedList.add(new MyFeed_Class(R.drawable.addphoto_img_block, 
						"Ramesh", "Suresh", "Payment for dinner and Lunch", "Chennai", String.valueOf((1+i)+"/10/2014")));
				
				break;
			case 3:
				feedList.add(new MyFeed_Class(R.drawable.addphoto_img_block, 
						"Ramesh", "Suresh", "Payment for dinner and Lunch", "Chennai", String.valueOf((1+i)+"/10/2014")));
				
				break;
			case 4:
				feedList.add(new MyFeed_Class(R.drawable.addphoto_img_block, 
						"Ramesh", "Suresh", "Payment for dinner and Lunch", "Chennai", String.valueOf((1+i)+"/10/2014")));
				break;
			case 5:
				feedList.add(new MyFeed_Class(R.drawable.addphoto_img_block, 
						"Ramesh", "Suresh", "Payment for dinner and Lunch", "Chennai", String.valueOf((1+i)+"/10/2014")));
			default:
				break;
			}
		}
		/*
		for(int i = 0; i<5; i++)
			feedList.add(new MyFeed_Class(R.drawable.addphoto_img_block, 
				"Ramesh", "Suresh", "Payment for dinner and Lunch", "Chennai"));*/
	}
	
	
	/*private void initializeTabHost() {
		// TODO Auto-generated method stub
		View MyFeedView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_feed_myfeed_bg, null);
		View TrendingView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_feed_myfeed_bg, null);
		
		((TextView)TrendingView.findViewById(R.id.tabText)).setText("Trending");
		((TextView)MyFeedView.findViewById(R.id.tabText)).setText("My Feed");
		
		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_FEED_MYFEED_TAG).setIndicator(MyFeedView),
				ContainerProvider.Tab_Feed_MyFeed_Container.class, null);
		
		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_FEED_TRENDING_TAG).setIndicator(TrendingView),
				ContainerProvider.Tab_Feed_Trending_Container.class, null);
		
		mTabhost.setBackgroundColor(Color.parseColor("#939C9E"));
		
		mTabhost.addTab(setIndicator(getActivity(), mTabhost.newTabSpec(AppConstants.TAB_FEED_MYFEED_TAG),
				MyFeedView), Tab_recentsearch_container.class, null);

		mTabhost.addTab(setIndicator(getActivity(), mTabhost.newTabSpec(AppConstants.TAB_FEED_TRENDING_TAG),
				TrendingView), Tab_watchlist_container.class, null);
	        
	}*/
	
	 
}
