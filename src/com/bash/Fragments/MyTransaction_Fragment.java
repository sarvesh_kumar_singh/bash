package com.bash.Fragments;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.bash.R;
import com.bash.Providers.ContainerProvider;
import com.bash.Utils.AppConstants;

@SuppressLint("ValidFragment")
public class MyTransaction_Fragment extends Fragment{
	
	View mRootView;
	FragmentTabHost mTabhost;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.fragment_home, null);
		mTabhost = (FragmentTabHost) mRootView.findViewById(R.id.fragmenttabhost);
		mTabhost.setup(getActivity(), getChildFragmentManager(), R.id.fragmentframe_holder);
		initializeTabHost();
		return mRootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}
	
	private void initializeTabHost() {
		// TODO Auto-generated method stub
		
		View pendingView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_graybackground_selector_bg, null);
		View historyView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_graybackground_selector_bg, null);
			
		((TextView)pendingView.findViewById(R.id.tabText)).setText("Pending");
		((TextView)historyView.findViewById(R.id.tabText)).setText("History");
			
		mTabhost.setBackgroundColor(Color.parseColor("#939C9E"));
		
		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_MYPROFILE_MYTRANSACTION_PENDING_TAG).setIndicator(pendingView),
				ContainerProvider.Tab_MyProfile_MyTransaction_Pending_Container.class, null);
		
		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_MYPROFILE_HISTORY_PENDING_TAG).setIndicator(historyView),
				ContainerProvider.Tab_MyProfile_MyTransaction_History_Container.class, null);
		
	}
	
	 
	
}
