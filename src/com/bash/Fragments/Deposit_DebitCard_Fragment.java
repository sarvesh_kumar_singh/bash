package com.bash.Fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Spinner;

import com.bash.R;
import com.bash.Application.BashApplication;

public class Deposit_DebitCard_Fragment extends Fragment {

	Spinner daysSpinner;
	Spinner yearsSpinner;
	View mRootView, pin_webview;
	
	Dialog pinWebDialog;
	public WebView alertWebView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater
				.inflate(R.layout.fragment_wallbank_debitpage, null);
		initializeWebAlertView();
		initializeView();
		return mRootView;
	}

	private void initializeView() {
		
		((Button) mRootView.findViewById(R.id.mainButton))
				.setText("Proceed to Transaction");
		((Button) mRootView.findViewById(R.id.mainButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pinWebDialog.show();
			}
		});
		
		//https://test.ccavenue.com/transaction/transaction.do?command=getJsonData&access_code='+access_code+'&currency='+currency+'&amount='+amount
		daysSpinner = (Spinner) mRootView.findViewById(R.id.daysSpinner);
		yearsSpinner = (Spinner) mRootView.findViewById(R.id.yearsSpinner);

		daysSpinner.setAdapter(BashApplication.daysAdapter);
		yearsSpinner.setAdapter(BashApplication.yearsAdapter);
	}

	private void initializeWebAlertView() {
		pin_webview = View.inflate(getActivity(),
				R.layout.alert_showpinalert_dialog, null);
		pinWebDialog = new Dialog(getActivity());
		pinWebDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		pinWebDialog.setContentView(pin_webview);
 
		alertWebView = ((WebView) pin_webview.findViewById(R.id.webView));
		
		WebSettings settings = alertWebView.getSettings();
        /*settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);*/
        settings.setJavaScriptEnabled(true);
        //alertWebView.setVerticalScrollBarEnabled(true);
        alertWebView.setHorizontalScrollBarEnabled(true);
        alertWebView.loadDataWithBaseURL("https://test.ccavenue.com/transaction/transaction.do?command=getJsonData&access_code='AVNV03BK06AN44VNNA'&currency='INR'&amount='50", null, null, null, null);
        alertWebView.setWebViewClient(new WebViewClient(){
        	@Override
        	public void onPageFinished(WebView view, String url) {
        		// TODO Auto-generated method stub
        		super.onPageFinished(view, url);
        		view.loadUrl("https://test.ccavenue.com/transaction/transaction.do?command=getJsonData&access_code='AVNV03BK06AN44VNNA'&currency='INR'&amount='50");
        	}
        });
	}
	
	
}
