package com.bash.Fragments;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Adapters.FriendsTransactionsAdapter;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.GsonClasses.MyFriend_Transaction_Class;
import com.bash.ListModels.BashUsers_Class;
import com.bash.ListModels.CashInOutFriend_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;

@SuppressLint("ValidFragment")
public class MyFriends_Transaction_Fragment extends Fragment implements AsyncResponse{
	MyAsynTaskManager  myAsyncTask;
	View mRootView, remainderView;
	Dialog remainderDialog;
	
	ExpandableListView transactionExpandableListView;
	FriendsTransactionsAdapter adapter;
	
	BashUsers_Class userInfo;
	
	SparseArray<ArrayList<CashInOutFriend_Class>> listChildDatas = new SparseArray<ArrayList<CashInOutFriend_Class>>();
	List<String> listDataHeader= new ArrayList<String>();
	
	MyFriend_Transaction_Class responseForTransaction; 
	
	@SuppressLint("ValidFragment")
	public MyFriends_Transaction_Fragment(BashUsers_Class userInfo) {
		this.userInfo = userInfo;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,	Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		mRootView = inflater.inflate(R.layout.fragment_myfriends_transactions_page, null);
		//mRootView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		return mRootView;
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "My Friends", 0);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initializeRemainderAlertView();
		initializeView();
	}
	
	private void initializeRemainderAlertView() {
		// TODO Auto-generated method stub
		remainderDialog = new Dialog(getActivity());
		remainderDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		remainderView = View.inflate(getActivity(), R.layout.alert_sendremainder_page, null);
		
		remainderDialog.setContentView(remainderView);
	
		 ((TextView) remainderView.findViewById(R.id.userName)).setText(userInfo.getname());
		
		 ((Button) remainderView.findViewById(R.id.cancelButton)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					remainderDialog.dismiss();
				}
		 });
		 
		 
		 ((Button) remainderView.findViewById(R.id.sendButton)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
				// TODO Auto-generated method stub
					if(((EditText) remainderView.findViewById(R.id.notificationText)).getText() != null &&
							((EditText) remainderView.findViewById(R.id.notificationText)).getText().toString().length() != 0){
						remainderDialog.dismiss();
						sendNotification();	
					}else{
						Toast.makeText(getActivity(), "Please Enter Reminder Text!", Toast.LENGTH_SHORT).show();
					}
					//createGroupInServer(((EditText)remainderView.findViewById(R.id.groupText)).getText().toString());
				}
		 });
		
	}

	
	public void showDialog()
    {
    	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    	builder.setMessage("How you would like to send remainder?")
    		.setNegativeButton("Mail", dialogClickListener)
    		.setNeutralButton("Cancel", dialogClickListener)
    		.setPositiveButton("Notification", dialogClickListener)
    	    .show();
    }
	
	public DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	    @Override
	    public void onClick(DialogInterface dialog, int which) {
	        switch (which){
	        case DialogInterface.BUTTON_NEUTRAL:
	        	dialog.dismiss();
	            break;
	        case DialogInterface.BUTTON_NEGATIVE:
	        	if(userInfo.getEmailId() == null || userInfo.getEmailId().length() == 0){
	        		Toast.makeText(getActivity(), "Email Id is Not Available to Your Friend!", Toast.LENGTH_SHORT).show();
	        	}else{
	        		Intent intent = new Intent(Intent.ACTION_SEND);
					intent.setType("text/plain");
					intent.putExtra(Intent.EXTRA_EMAIL, userInfo.getEmailId());
					intent.putExtra(Intent.EXTRA_SUBJECT, "Remainder for Balance");
					intent.putExtra(Intent.EXTRA_TEXT, "Hi, My Decent Remainder to send my amount!");
					startActivity(Intent.createChooser(intent, "Send Email"));	
	        	}
	        	break;
	        case DialogInterface.BUTTON_POSITIVE:
	        	remainderDialog.show();
	        	dialog.dismiss();
	            break;
	        }
	    }
	};

	private void initializeView() {
		
		if(userInfo.getname().length() > 13){
			
		}
		
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, 
				((userInfo.getname().length() > 13) ? userInfo.getname().substring(0, 12)+".." : userInfo.getname()), 0);
		((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//((HomeActivity) getActivity()).menu.toggle();
					((BaseFragment)getParentFragment()).popFragment();
				}
		 });
		 
		((ImageView)mRootView.findViewById(R.id.remaindMeButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialog();
			}
		});
		
		if(userInfo.getimagepath() != null &&  userInfo.getimagepath().length()!=0)
			ImageLoader.getInstance().displayImage(userInfo.getimagepath(), 
					((ImageView)mRootView.findViewById(R.id.userImage)), BashApplication.options, BashApplication.animateFirstListener);
	        	//ImageLoader.getInstance().displayImage(userInfo.getimagepath(),	((ImageView)mRootView.findViewById(R.id.userImage)));
		  
		 ((ImageView) mRootView.findViewById(R.id.settleupButton)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					/*((BaseFragment)getParentFragment()).addFragment
		 						(new MyFriends_Trans_SettleUpFragment(userInfo, responseForTransaction.totalbalance), true);*/
					
					/*((BaseFragment)getParentFragment()).addFragment
						(new MyFriends_Trans_SettleUpFragment(userInfo, responseForTransaction.totalbalance), true);*/
					if(Integer.parseInt(responseForTransaction.totalbalance) < 0)
						((BaseFragment)getParentFragment()).replaceFragment
							(new MyFriends_Transaction_Pay_Fragment(userInfo, responseForTransaction), true);
					else
						Toast.makeText(getActivity(), "You don't owe "+userInfo.getname()+" any money!", Toast.LENGTH_SHORT).show();
					/*((BaseFragment)getParentFragment()).replaceFragment(new MyFriends_Trans_SettleUpFragment(), 
							true);*/
				}
		 });
		 
		transactionExpandableListView = (ExpandableListView)mRootView.findViewById(R.id.transactionListview);
			
		((TextView) mRootView.findViewById(R.id.userName)).setText(userInfo.getname());
		 
		/*if(listDataHeader.size() == 0)
				setDatas();*/
		
		adapter = new FriendsTransactionsAdapter(getActivity(), listDataHeader, listChildDatas);
		transactionExpandableListView.setAdapter(adapter);
			
		transactionExpandableListView.setOnChildClickListener(new OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				
				if(listChildDatas.get(groupPosition).get(childPosition).getHaveYouPaid())
						((BaseFragment)getParentFragment()).replaceFragment(
								new MyFriends_Transactions_Charge_Fragment(listChildDatas.get(groupPosition).get(childPosition),
										responseForTransaction),
								true);
				else
					Toast.makeText(getActivity(), "You Borrowed From your Friend!", Toast.LENGTH_SHORT).show();
				return false;
			}
		});
		
		getHistoryDetails();
	}
 
	
	public void sendNotification() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("sendNotification",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser",
				"idfriend",
				"text"
				}, 
			new String[]{
				"feed",
				"friendreminder",
				PreferenceManager.getInstance().getUserId(),
				userInfo.getisFriend(),
				((EditText) remainderView.findViewById(R.id.notificationText)).getText().toString()
				});
		myAsyncTask.execute();
	
	}
	

	public void getHistoryDetails() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;

		myAsyncTask.setupParamsAndUrl("getHistoryDetails",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser",
				"idfriend"
				}, 
				new String[]{
				"feed",
				"getfeedbyfriend",
				PreferenceManager.getInstance().getUserId(),
				userInfo.getisFriend()
				});
		myAsyncTask.execute();
		
	}
	
	private void setDatas() {
		
		ArrayList<CashInOutFriend_Class> auguestDatas = new ArrayList<CashInOutFriend_Class>();
		
		listChildDatas.clear();
		listDataHeader.clear();
		
		for(int i =0; i<responseForTransaction.details.size(); i++){
			
			listDataHeader.add(responseForTransaction.details.get(i).month+" - "+responseForTransaction.details.get(i).year);
			
			auguestDatas.clear();
			for(int j = 0; j < responseForTransaction.details.get(i).data.size(); j++){
				
				auguestDatas.add(new CashInOutFriend_Class(
						responseForTransaction.details.get(i).data.get(j).feed_comment, 
						responseForTransaction.details.get(i).data.get(j).amount,
						responseForTransaction.details.get(i).data.get(j).amount,
						(responseForTransaction.details.get(i).data.get(j).status.equals("you lent") 
								? true : false),
						responseForTransaction.details.get(i).data.get(j).date,
						responseForTransaction.details.get(i).data.get(j).location,
						responseForTransaction.details.get(i).data.get(j).imagepath,
						responseForTransaction.details.get(i).data.get(j).feed_comment,
						responseForTransaction.details.get(i).data.get(j).idtrans
						));
			}
			
			listChildDatas.put(i, auguestDatas);
		}
		
		adapter.notifyDataSetChanged();
		
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		if(from.equalsIgnoreCase("sendNotification"))
		{
			if(output!=null)
			{

				try {
				Log.e("Json Response", output);
				JSONObject rootObj = new JSONObject(output);
				if(rootObj.getBoolean("result")){
					DialogManager.showDialog(getActivity(), "Reminder Sent to "+userInfo.getname());	
					//Toast.makeText(getActivity(), "Remainder Sent to "+userInfo.getname(), Toast.LENGTH_SHORT).show();
				}
				 
				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
				}
		
			}
			else
			{
				// TODO: handle exception
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			}
		}
		else if(from.equalsIgnoreCase("getHistoryDetails"))	
		{
			if(output!=null)
			{

				try {
				Log.e("Json Response", output);
				Gson gson = new Gson();
				responseForTransaction = gson.fromJson(output, MyFriend_Transaction_Class.class);
				
				if(Integer.parseInt(responseForTransaction.totalbalance) > 0){
					((TextView) mRootView.findViewById(R.id.userName)).setText("You are Owed to "+userInfo.getname());
					((TextView) mRootView.findViewById(R.id.userName)).setTextColor(Color.parseColor("#378E34"));
					((TextView) mRootView.findViewById(R.id.amountToPayText)).setTextColor(Color.parseColor("#378E34"));
					((ImageView)mRootView.findViewById(R.id.remaindMeButton)).setEnabled(true);
				}else if(Integer.parseInt(responseForTransaction.totalbalance) < 0){
					((TextView) mRootView.findViewById(R.id.userName)).setText("You Owe "+userInfo.getname());
					((TextView) mRootView.findViewById(R.id.userName)).setTextColor(Color.RED);
					((TextView) mRootView.findViewById(R.id.amountToPayText)).setTextColor(Color.RED);
					((ImageView)mRootView.findViewById(R.id.remaindMeButton)).setEnabled(false);
				}else if((Integer.parseInt(responseForTransaction.totalbalance) == 0)){
					((TextView) mRootView.findViewById(R.id.userName)).setText("You aren't Owe or Owed to "+userInfo.getname());
					((TextView) mRootView.findViewById(R.id.userName)).setTextColor(Color.BLACK);
					((TextView) mRootView.findViewById(R.id.amountToPayText)).setTextColor(Color.BLACK);
					((ImageView)mRootView.findViewById(R.id.remaindMeButton)).setEnabled(false);
				}
				
				((TextView) mRootView.findViewById(R.id.amountToPayText)).setText(AppConstants.RASYMBOL+" "+
						responseForTransaction.totalbalance+" ");
				setDatas();
				 
				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
				}
		
			}
			else
			{

				// TODO: handle exception
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			
			}
		}
	}

	 
	
}
