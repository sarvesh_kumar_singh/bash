package com.bash.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.BaseFragmentClasses.BaseFragment;

public class MyFriendsAndGroupsFragment extends Fragment{

	View mRootView;
	public static BaseFragment parentFragmentClass;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_myfriends_groups_page, null);
		initializeView();
		return mRootView;
	}
	
	private void initializeView() {
		// TODO Auto-generated method stub
		// ((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setBackgroundResource(0);
		 
		 parentFragmentClass = ((BaseFragment)getParentFragment());
		 
		 ((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn, "Friends & Groups", 0);
		 
		 ((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//Sarvesh
					//((HomeActivity)getActivity()).getSlidingMenu().toggle();
				}
		 });
	
		 ((LinearLayout)mRootView.findViewById(R.id.myfriendsIcon)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					parentFragmentClass.replaceFragment(new MyFriendsFragment(), true);
				
				}
		 });
		 
		 ((LinearLayout)mRootView.findViewById(R.id.mygroupsIcon)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					parentFragmentClass.replaceFragment(new MyGroupsFragment(), true);
				}
		 });
		 
		 
	}
	
	
}
