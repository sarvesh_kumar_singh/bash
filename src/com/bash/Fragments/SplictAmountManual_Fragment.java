package com.bash.Fragments;

import java.text.DecimalFormat;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.ListModels.MyFriendOnGroup_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
@SuppressLint("ValidFragment")
public class SplictAmountManual_Fragment extends Fragment implements AsyncResponse{
	MyAsynTaskManager  myAsyncTask;
	View mRootView;
//	ArrayList<BashUsers_Class> feedList = new ArrayList<BashUsers_Class>();
	SelectionFriendsAdapter adapter;
	ListView splitFriendsListView;
	//boolean isAdapterLoading = true;
	double amount = 200, splitamount= 0, numberofPeople = 0;
	String groupId;
	
	public Dialog alertDialog;
	public View alertView;
	
	@SuppressLint("ValidFragment")
	public SplictAmountManual_Fragment(double amount, String groupId){
		this.amount =  amount;
		this.groupId = groupId;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_split_amt_equal_page, null);
		initializeView();
		initializeAlertView();
		return mRootView;
	}
	
	private void initializeAlertView() {
		// TODO Auto-generated method stub
		alertDialog = new Dialog(getActivity());
		alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		alertView = View.inflate(getActivity(), R.layout.alert_addcard_delete, null);
		alertDialog.setContentView(alertView);
		
		((Button) alertView.findViewById(R.id.okButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				alertDialog.dismiss();
				((BaseFragment)getParentFragment()).popFragment();
			}
		});
		
		((Button) alertView.findViewById(R.id.cancelButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				alertDialog.dismiss();
			}
		});
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		((HomeActivity) getActivity()).setUpTopBarFields(R.drawable.back_btn, "Group Transaction", 0);

		((ImageView) getActivity().findViewById(R.id.topleftsideImage))	.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						((BaseFragment) getParentFragment()).popFragment();
					}
				});
	}
	
	
	private void initializeView() {
		
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "Manual Split", 0);
		
		((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((BaseFragment)getParentFragment()).popFragment();
			}
		});
		
		AddDescription_Group_Fragment.selectionList.clear();
		
		((RelativeLayout) mRootView.findViewById(R.id.doneButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				double totalamount = 0;
				boolean haveYouPaidAll = true;
				
				for(int i = 0; i < AddDescription_Group_Fragment.selectionList.size(); i++) {
					totalamount += 
							Double.parseDouble(AddDescription_Group_Fragment.selectionList.get(i).selectionAmount);
					
					
					if(!PreferenceManager.getInstance().getUserId().equals(
							String.valueOf(AddDescription_Group_Fragment.selectionList.get(i).getSelectionId()))){
						
						if(!AddDescription_Group_Fragment.selectionList.get(i).getselectionAmount().equals("0")){
							haveYouPaidAll = false;
						}
					}

				}
				
				Log.e("Total Amt", totalamount+"");
				Log.e("Total Received Amount", amount+"");
				//totalamount = Math.round(totalamount);
				Log.e("Total with Floor", String.valueOf(Math.round(totalamount)));
				
				if(haveYouPaidAll){
					DialogManager.showDialog(getActivity(), "Please Select Atleast One User!");
				}
				else if(totalamount > amount){
					((TextView)alertView.findViewById(R.id.oldPinText)).setText(
							"Total Amount is "+AppConstants.RASYMBOL+" "+amount+"\n"+
							"Excess Amount is "+AppConstants.RASYMBOL+" "+f.format((totalamount-amount))
							);
					alertDialog.show();
					//DialogManager.showDialog(getActivity(), "Amount Shouldn't Exceed Total!");
				}else if(totalamount < amount){
					((TextView)alertView.findViewById(R.id.oldPinText)).setText(
							"Total Amount is "+AppConstants.RASYMBOL+" "+amount+"\n"+
							"Less Amount is "+AppConstants.RASYMBOL+" "+ f.format((amount-totalamount))
							);
					alertDialog.show();
					//DialogManager.showDialog(getActivity(), "Amount Shouldn't Below Total!");
				}
				else{
					((BaseFragment)getParentFragment()).popFragment();
				}
				
				//AddDescription_Group_Fragment.selectionList.
				
				/*if(AddDescription_Group_Fragment.selectionList.size() == 0){
					Log.e("Selection List", "No Item Found");
					AddFriendsOnGroupFragment.parentFragment.popFragment();
				}else{
					String selectionString = " ", amountString = " ";
					for(int i = 0; i < AddDescription_Group_Fragment.selectionList.size(); i++) {
							if(AddDescription_Group_Fragment.selectionList.size() == i+1){
								//selectionString += selectionList.keyAt(i);
								if(!AddDescription_Group_Fragment.selectionList.get(i).getselectionAmount().equals("0")){
								amountString += AddDescription_Group_Fragment.selectionList.get(i).getselectionAmount();
								selectionString += AddDescription_Group_Fragment.feedList.get(AddDescription_Group_Fragment.selectionList.keyAt(i)).getidfriend();}
								}
							else{
								if(!AddDescription_Group_Fragment.selectionList.get(i).getselectionAmount().equals("0")){
								amountString += AddDescription_Group_Fragment.selectionList.get(i).getselectionAmount()+",";
								selectionString +=AddDescription_Group_Fragment.feedList.get(AddDescription_Group_Fragment.selectionList.keyAt(i)).getidfriend()+",";
								}
							}
								//selectionString += selectionList.keyAt(i)+",";
						}
					//addFriendsInGroup();
					Log.e("Selection List", selectionString);
					Log.e("Amount List", amountString);
				}*/
				
				
			}
		});
		
		((RelativeLayout) mRootView.findViewById(R.id.cancelButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((BaseFragment)getParentFragment()).popFragment();
			}
		});
		
		splitFriendsListView = (ListView) mRootView.findViewById(R.id.splitFriendsListView);
		
		numberofPeople = AddDescription_Group_Fragment.feedList.size();
		
		if(AddDescription_Group_Fragment.feedList.size() != 0)
			splitamount = (amount / AddDescription_Group_Fragment.feedList.size());
		
		Log.e("Split Amount", String.valueOf(splitamount));
		
		adapter = new SelectionFriendsAdapter();
		splitFriendsListView.setAdapter(adapter);
		splitFriendsListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
	/*	splitFriendsListView.setOnTouchListener(new OnTouchListener() {
		    // Setting on Touch Listener for handling the touch inside ScrollView
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
		    // Disallow the touch request for parent scroll on touch of child view
		    v.getParent().requestDisallowInterceptTouchEvent(true);
		    return false;
		    }
		});
		*/
		//getFriendsinGroupDetails();
		AddDescription_Group_Fragment.valuesHolder.clear();
		
		initializeDatas();
	}

	public void initializeDatas() {
		
		splitamount = (amount / AddDescription_Group_Fragment.feedList.size());
		
		numberofPeople = AddDescription_Group_Fragment.feedList.size();
			
			if(AddDescription_Group_Fragment.feedList.size() != 0){
				splitamount = (amount / AddDescription_Group_Fragment.feedList.size());
				 
			     System.out.println(f.format(splitamount));
			     
				for(int i = 0; i<AddDescription_Group_Fragment.feedList.size(); i++){
					AddDescription_Group_Fragment.selectionList.put(i, new AddDescription_Group_Fragment.selectionModel(
							AddDescription_Group_Fragment.feedList.get(i).getidfriend(), String.valueOf(f.format(splitamount))));
				 }
			}
			
			adapter.notifyDataSetChanged();
	}
	/* public void getMembersistFromGroup() {
		 
		 MyAsynTaskManager.setupParamsAndUrl(AppUrlList.ACTION_URL, new String[] {
					"module",
					"action",
					"iduser"
					}, 
					new String[]{
					"group",
					"grouplist",
					PreferenceManager.getInstance().getUserId()
					});

			new MyAsynTaskManager(getActivity(), new LoadListener() {
				@Override
				public void onLoadComplete(final String jsonResponse) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							try {
								JSONObject rootObj = new JSONObject(jsonResponse);
								if(rootObj.getBoolean("result")) {
									if(rootObj.getInt("status_code") == 200){
										JSONArray friendslist = rootObj.getJSONArray("friendslist");
										JSONObject item;
										feedList.clear();
											for(int i = 0;i < friendslist.length(); i++){
												item = friendslist.getJSONObject(i);
												feedList.add(new BashUsers_Class
															(item.getString("idfriend"), 
															item.getString("phone_no"),
															item.getString("imagepath"),
															item.getString("idfriend"),
															"FALSE", 
															"FALSE", 
															"TRUE"));
												}
											adapter.notifyDataSetChanged();
									}else{
										((RelativeLayout) mRootView.findViewById(R.id.noListLayout)).setVisibility(View.VISIBLE);
										DialogManager.showDialog(getActivity(), "No Friends Found in Group!");
									}
								}
							else{ 
								DialogManager.showDialog(getActivity(), "Error In Getting Friends List! Try Again!");
							}
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
								DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
							}
					}
					});
				}
				@Override
				public void onError(final String errorMessage) {
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							DialogManager.showDialog(getActivity(), errorMessage);		
						}
					});
					
				}
			}).execute();
		 
		}*/
	 
	 public void getFriendsinGroupDetails() {
		 myAsyncTask=new MyAsynTaskManager();
			myAsyncTask.delegate=this;
			myAsyncTask.setupParamsAndUrl("getFriendsinGroupDetails",getActivity(),AppUrlList.ACTION_URL, new String[] {
					"module",
					"action",
					"idgroup",
					"iduser"
					}, 
					new String[]{
					"group",
					"friendsofgroup",
					groupId,
					PreferenceManager.getInstance().getUserId()
					});

			myAsyncTask.execute();
		}
	 DecimalFormat f = new DecimalFormat("##.00");
	class SelectionFriendsAdapter extends BaseAdapter {
		 
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return AddDescription_Group_Fragment.feedList.size();
		}

		@Override
		public MyFriendOnGroup_Class getItem(int position) {
			// TODO Auto-generated method stub
			return AddDescription_Group_Fragment.feedList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

				ViewHolder holder = null;
			
				final MyFriendOnGroup_Class listItem = getItem(position);
				
		        if (convertView == null) {
		        	LayoutInflater inflater = LayoutInflater.from(getActivity());
		        	convertView = inflater.inflate(R.layout.custom_splitamt_manual_listview, null);
		            holder = new ViewHolder();
		            holder.isSelected = (CheckBox) convertView.findViewById(R.id.isSelected);
		            holder.mobileNumberText = (TextView) convertView.findViewById(R.id.mobileNumberText);
		            holder.amountEditText = (EditText) convertView.findViewById(R.id.amountEditText);
		            holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
		            holder.userImage = (ImageView) convertView.findViewById(R.id.userImage);
		            convertView.setTag(holder);
		        } else {
		            holder = (ViewHolder) convertView.getTag();
		        }
		        //holder.userImage.setImageResource(listItem.getimagepath());
		        holder.mobileNumberText.setText(listItem.getphone_no());
	            holder.friendName.setText(listItem.getfriendName());
	            
	            final EditText amtText = holder.amountEditText;
	            
	            Log.e("Position", String.valueOf(position));
	            
	            if(AddDescription_Group_Fragment.selectionList.size() != 0 && AddDescription_Group_Fragment.selectionList.get(position) != null && AddDescription_Group_Fragment.selectionList.get(position).getselectionAmount() != null 
	            		&& AddDescription_Group_Fragment.selectionList.get(position).getselectionAmount().length() != 0){
	            	amtText.setText(AddDescription_Group_Fragment.selectionList.get(position).getselectionAmount());}
	            else
	            	amtText.setText(String.valueOf(splitamount));
	            
	       		
	            holder.amountEditText.addTextChangedListener(new TextWatcher() {
					@Override
					public void onTextChanged(CharSequence s, int start, int before, int count) {
						
					}
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count,
							int after) {
						// TODO Auto-generated method stub
					}
					@Override
					public void afterTextChanged(Editable s) {
						if(amtText.getText() == null || amtText.getText().toString().length() == 0)
							AddDescription_Group_Fragment.selectionList.put(position, new AddDescription_Group_Fragment.selectionModel(listItem.getidfriend(), "0"));
						else
							AddDescription_Group_Fragment.selectionList.put(position, new AddDescription_Group_Fragment.selectionModel(listItem.getidfriend(), amtText.getText().toString()));						// TODO Auto-generated method stub
					}
				});
	            	  
	            holder.amountEditText.setOnEditorActionListener(new OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
						if(actionId == EditorInfo.IME_ACTION_DONE){
							if(amtText.getText() == null || amtText.getText().toString().length() == 0)
								AddDescription_Group_Fragment.selectionList.put(position, new AddDescription_Group_Fragment.selectionModel(listItem.getidfriend(), "0"));
							else
								AddDescription_Group_Fragment.selectionList.put(position, new AddDescription_Group_Fragment.selectionModel(listItem.getidfriend(), amtText.getText().toString()));
						}
						return false;
					}
				});
	            
	            holder.isSelected.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						if(isChecked){
							if(amtText.getText() == null || amtText.getText().toString().length() == 0)
								AddDescription_Group_Fragment.selectionList.put(position, new AddDescription_Group_Fragment.selectionModel(listItem.getidfriend(), "0"));
							else
								AddDescription_Group_Fragment.selectionList.put(position, new AddDescription_Group_Fragment.selectionModel(listItem.getidfriend(), f.format(splitamount)));
							amtText.setText(String.valueOf(f.format(splitamount)));
						}else{
							AddDescription_Group_Fragment.selectionList.put(position, new AddDescription_Group_Fragment.selectionModel(listItem.getidfriend(), "0"));
							//AddDescription_Group_Fragment.selectionList.remove(position);
							amtText.setText("0");
						}
					}
				});
	            
	            return convertView;
	         }
	    		
	  		  private class ViewHolder {
	  		    	EditText amountEditText;
	  		    	TextView mobileNumberText;
	  		    	TextView friendName;
	  		    	ImageView userImage;
	  		    	CheckBox isSelected;
	  		     }
	  	}
	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		if(from.equalsIgnoreCase("getFriendsinGroupDetails"))
		{
			if(output!=null)
			{

				try {
					JSONObject rootObj = new JSONObject(output);
					if(rootObj.getBoolean("result")) {
							JSONArray groupList = rootObj.getJSONArray("friendlist");
							JSONObject item;
							
							if(groupList.length() != 0)
								splitamount = (amount / groupList.length());
							
							
							AddDescription_Group_Fragment.feedList.clear();
							
							AddDescription_Group_Fragment.feedList.add(new MyFriendOnGroup_Class(
									PreferenceManager.getInstance().getUserId(), 
									"",
									PreferenceManager.getInstance().getUserMobileNumber(), 
									PreferenceManager.getInstance().getUserFullName(),
									" ",
									"0",
									"0"
									));
							
								for(int i = 0;i < groupList.length(); i++) {
									item = groupList.getJSONObject(i);
									 
									AddDescription_Group_Fragment.feedList.add(new MyFriendOnGroup_Class(
											item.getString("idfriend"), 
											"",
											item.getString("phone_no"), 
											item.getString("name"),
											item.getString("imagepath"),
											"0",
											"0"
											));
									}
								
								numberofPeople = AddDescription_Group_Fragment.feedList.size();
								
								if(AddDescription_Group_Fragment.feedList.size() != 0){
									splitamount = (amount / AddDescription_Group_Fragment.feedList.size());
									 
								     System.out.println(f.format(splitamount));
								     
									for(int i = 0; i<AddDescription_Group_Fragment.feedList.size(); i++){
										AddDescription_Group_Fragment.selectionList.put(i, new AddDescription_Group_Fragment.selectionModel(
												AddDescription_Group_Fragment.feedList.get(i).getidfriend(), String.valueOf(f.format(splitamount))));
									 }
								}
								
								adapter.notifyDataSetChanged();
					}
				else{ 
					DialogManager.showDialog(getActivity(), "No Friends Found in Group!");
				}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
				}
		
			}
			else
			{
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			}
		}
	}
	  }
