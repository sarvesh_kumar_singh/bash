package com.bash.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import com.bash.R;
import com.bash.Activities.HomeActivity;

public class PrivacyFragment extends Fragment{
	
	
	public static View mRootView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_privacy_page, null);
		initializeView();
		return mRootView;
	}
	
	private void initializeView() {
		// TODO Auto-generated method stub
		// ((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setBackgroundResource(0);
		 ((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn, "Privacy", 0);
		 
		 ((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//Sarvesh
					//((HomeActivity) getActivity()).menu.toggle();
					((HomeActivity)getActivity()).slidingmenu_layout.toggleMenu();
				}
		 });
	}
 	
}
