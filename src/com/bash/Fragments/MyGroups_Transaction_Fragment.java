package com.bash.Fragments;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Adapters.GroupsTransactionsAdapter;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.GsonClasses.MyGroup_Transaction_Class;
import com.bash.ListModels.CashInOutGroup_Class;
import com.bash.ListModels.MyGroups_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynImageTaskManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;

@SuppressLint("ValidFragment")
public class MyGroups_Transaction_Fragment extends Fragment implements AsyncResponse{
	MyAsynTaskManager  myAsyncTask;
	Dialog remainderDialog, editGroupDialog;
	View mRootView, remainderView, editGroupView;
	
	View camera_gallery;
	Dialog camaeraDialog;
	
	ExpandableListView transactionExpandableListView;
	GroupsTransactionsAdapter adapter;
	//BashUsers_Class userInfo;
	MyGroups_Class groupDetails;
	
	SparseArray<ArrayList<CashInOutGroup_Class>> listChildDatas = new SparseArray<ArrayList<CashInOutGroup_Class>>();
	List<String> listDataHeader= new ArrayList<String>();
	
	MyGroup_Transaction_Class responseForTransaction;
	
	Uri mCapturedImageURI;
	public ImageView fileImage;
	String picturePath="";
	
	@SuppressLint("ValidFragment")
	public MyGroups_Transaction_Fragment(MyGroups_Class groupDetails){
		this.groupDetails = groupDetails;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,	Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		mRootView = inflater.inflate(R.layout.fragment_mygroups_transactions_page, null);
		((HomeActivity) getActivity()).currentFragment = this;
		return mRootView;
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "My Groups", 0);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initializeRemainderAlertView();
		initializePictureAlertView();
		initializeGroupAlertView();
		initializeView();
	}
	
	private void initializeView() {
		
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn,
				((groupDetails.getgroupName().length() > 13) ? groupDetails.getgroupName().substring(0, 13) :
						groupDetails.getgroupName())
				, 0);
		
		((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//((HomeActivity) getActivity()).menu.toggle();
					((BaseFragment)getParentFragment()).popFragment();
				}
		 });
		 
		((ImageView)mRootView.findViewById(R.id.myeditGroupButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((BaseFragment)getParentFragment()).replaceFragment(new MyFriendOnGroupFragment(
						groupDetails.getgroupName(), groupDetails.getgroupId()), true);
			}
		});
		
		((ImageView)getActivity().findViewById(R.id.remaindButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				remainderDialog.show();	
			}
		});
		
		((ImageView) mRootView.findViewById(R.id.editGroupButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				editGroupDialog.show();	
			}
		});
		
		transactionExpandableListView = (ExpandableListView)mRootView.findViewById(R.id.transactionListview);
			
		((TextView) mRootView.findViewById(R.id.userName)).setText(groupDetails.getgroupName());
		
		if(groupDetails.getimagePath() != null && groupDetails.getimagePath().length() != 0){
			ImageLoader.getInstance().displayImage(groupDetails.getimagePath(), 
					((ImageView)mRootView.findViewById(R.id.userImage)), BashApplication.options, BashApplication.animateFirstListener);
		}
		
		listDataHeader.clear();
		listChildDatas.clear();
		
		adapter = new GroupsTransactionsAdapter(getActivity(), listDataHeader, listChildDatas);
		transactionExpandableListView.setAdapter(adapter);
		
		getHistoryDetails();
	}
 
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);
		
		switch (requestCode) {
		case AppConstants.CODE_CAMERA:
			if (resultCode == getActivity().RESULT_OK) {
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(
						mCapturedImageURI, projection, null, null, null);
				int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				picturePath = cursor.getString(column_index_data);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,	fileImage);
				Log.e("Camera Path", picturePath);
			}
			break;
		case AppConstants.CODE_GALLERY:
			if (resultCode == getActivity().RESULT_OK) {

				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				picturePath = cursor.getString(columnIndex);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,	fileImage);
			}
			break;
		default:
			break;
		}
		
	}
	
	
	
	private void initializeGroupAlertView() {
		
		editGroupDialog = new Dialog(getActivity());
		
		editGroupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		editGroupView = View.inflate(getActivity(), R.layout.alert_editgroup_dialog, null);
		
		editGroupDialog.setContentView(editGroupView);
	
		 ((EditText) editGroupView.findViewById(R.id.groupName)).setText(groupDetails.getgroupName());
		
		 fileImage = ((ImageView) editGroupView.findViewById(R.id.groupImage));
		 
		 if(groupDetails.getimagePath() != null && groupDetails.getimagePath().length() != 0){
			 ImageLoader.getInstance().displayImage(groupDetails.getimagePath(), 
					 fileImage, BashApplication.options, BashApplication.animateFirstListener);
		 }
			 	//ImageLoader.getInstance().displayImage(groupDetails.getimagePath(), fileImage);
			 
		 fileImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				camaeraDialog.show();
			}
		 });
		 
		 ((Button) editGroupView.findViewById(R.id.cancelButton)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					editGroupDialog.dismiss();
				}
		 });
		 
		 ((Button) editGroupView.findViewById(R.id.okButton)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
				// TODO Auto-generated method stub
					if(((EditText) editGroupView.findViewById(R.id.groupName)).getText() != null &&
							((EditText) editGroupView.findViewById(R.id.groupName)).getText().toString().length() != 0){
						saveNewGroupInformation();	
					}else{
						Toast.makeText(getActivity(), "Please Enter Group Name!", Toast.LENGTH_SHORT).show();
					}
					//createGroupInServer(((EditText)remainderView.findViewById(R.id.groupText)).getText().toString());
				}
		 });
		
	}

	private void initializePictureAlertView() {
		// TODO Auto-generated method stub
		camera_gallery = View.inflate(getActivity(),
				R.layout.alert_gallerycamera_mode_view, null);
		camaeraDialog = new Dialog(getActivity());
		camaeraDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		camaeraDialog.setContentView(camera_gallery);
 
		((Button) camera_gallery.findViewById(R.id.openGalleryBtn))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						camaeraDialog.dismiss();
						Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						getActivity().startActivityForResult(i,	AppConstants.CODE_GALLERY);
					}
		});

		((Button) camera_gallery.findViewById(R.id.openCameraBtn)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						camaeraDialog.dismiss();
						String fileName = "temp.jpg";
						ContentValues values = new ContentValues();
						values.put(MediaStore.Images.Media.TITLE, fileName);
						mCapturedImageURI = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
										values);
						Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
						intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
						getActivity().startActivityForResult(intent, AppConstants.CODE_CAMERA);
					}
		});

	}
	
	
	private void initializeRemainderAlertView() {

		
		remainderDialog = new Dialog(getActivity());
		
		remainderDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		remainderView = View.inflate(getActivity(), R.layout.alert_sendremainder_page, null);
		
		remainderDialog.setContentView(remainderView);
	
		 ((TextView) remainderView.findViewById(R.id.userName)).setText(groupDetails.getgroupName());
		
		 ((Button) remainderView.findViewById(R.id.cancelButton)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					remainderDialog.dismiss();
				}
		 });
		 
		 
		 ((Button) remainderView.findViewById(R.id.sendButton)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
				// TODO Auto-generated method stub
					if(((EditText) remainderView.findViewById(R.id.notificationText)).getText() != null &&
							((EditText) remainderView.findViewById(R.id.notificationText)).getText().toString().length() != 0){
						remainderDialog.dismiss();
						sendNotification();	
					}else{
						Toast.makeText(getActivity(), "Please Enter Reminder Text!", Toast.LENGTH_SHORT).show();
					}
					//createGroupInServer(((EditText)remainderView.findViewById(R.id.groupText)).getText().toString());
				}
		 });
		
	}
	
	public void saveNewGroupInformation() {
		
		MyAsynImageTaskManager.setupParamsAndUrl(AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"idgroup",
				"groupname",
				"groupimage"
				},
			new String[]{
				"group",
				"update",
				groupDetails.getgroupId(),
				((EditText) editGroupView.findViewById(R.id.groupName)).getText().toString(),
				picturePath
				},
			new String[]{
				"0","0","0","0","1"
				}
			);
	
	new MyAsynImageTaskManager(getActivity(), new MyAsynImageTaskManager.LoadListener() {
			@Override
			public void onLoadComplete(final String jsonResponse) {
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						try {
						JSONObject rootObj = new JSONObject(jsonResponse);
						if(rootObj.getBoolean("result")) {
							editGroupDialog.dismiss();
							ImageLoader.getInstance().displayImage(rootObj.getString("imagepath"), 
									((ImageView)mRootView.findViewById(R.id.userImage)), BashApplication.options, BashApplication.animateFirstListener);
							//ImageLoader.getInstance().displayImage(rootObj.getString("imagepath"), ((ImageView)mRootView.findViewById(R.id.userImage)));
							((TextView) mRootView.findViewById(R.id.userName)).setText(rootObj.getString("groupname"));
							((EditText) editGroupView.findViewById(R.id.groupName)).setText(rootObj.getString("groupname"));
							
							groupDetails.setFieldValues(rootObj.getString("groupname"), groupDetails.getgroupId(), 
									rootObj.getString("imagepath"));
							
							Toast.makeText(getActivity(), "Group Details Updated Successfully!", Toast.LENGTH_SHORT).show();
						}else{
							Toast.makeText(getActivity(), "Error Occured To Update Group Details! Try Again!", Toast.LENGTH_SHORT).show();
						}
						 
						} catch (Exception e) {
							// TODO: handle exception
							DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
						}
					}
				});
			}
			
			@Override
			public void onError(final String errorMessage)  {
				getActivity().runOnUiThread(new Runnable() {
					public void run() {
						DialogManager.showDialog(getActivity(), errorMessage);		
					}
				});
				
			}
		}).execute();
	}
	 
	 
	public void sendNotification() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("sendNotification",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser",
				"idgroup",
				"text"
				}, 
			new String[]{
				"feed",
				"groupreminder",
				PreferenceManager.getInstance().getUserId(),
				groupDetails.getgroupId(),
				((EditText) remainderView.findViewById(R.id.notificationText)).getText().toString()
				});
		
		//((EditText) remainderView.findViewById(R.id.notificationText)).getText().toString();
		
		
	}
	 
	public void getHistoryDetails() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("getHistoryDetails",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser",
				"idgroup"
				}, 
				new String[]{
				"feed",
				"getfeedbygroup",
				PreferenceManager.getInstance().getUserId(),
				groupDetails.getgroupId()
				});
		
		Log.e("Group Id", groupDetails.getgroupId());
		
		myAsyncTask.execute();
	}
	
	

	private void setDatas() {
		// TODO Auto-generated method stub
		 
		
		ArrayList<CashInOutGroup_Class> auguestDatas = new ArrayList<CashInOutGroup_Class>();
		
		for(int i =0; i<responseForTransaction.details.size(); i++) {
			
			listDataHeader.add(responseForTransaction.details.get(i).month+" - "+responseForTransaction.details.get(i).year);
			
			auguestDatas.clear();
			for(int j = 0; j < responseForTransaction.details.get(i).data.size(); j++){
				
				auguestDatas.add(new CashInOutGroup_Class(
						responseForTransaction.details.get(i).data.get(j).feed_comment, 
						responseForTransaction.details.get(i).data.get(j).amount,
						responseForTransaction.details.get(i).data.get(j).amount,
						(responseForTransaction.details.get(i).data.get(j).status.equals("you lent") ?	0 : 
							(responseForTransaction.details.get(i).data.get(j).status.equals("you borrowed") ? 1 : 2))
							));
				
				/*(responseForTransaction.details.get(i).data.get(j).status.equals("you lent") ?	0 : 
					(responseForTransaction.details.get(i).data.get(j).status.equals("you borrowed") ? 1 : 2))*/
			}
			
			listChildDatas.put(i, auguestDatas);
		}
		
		adapter.notifyDataSetChanged();
		
 
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		if(from.equalsIgnoreCase("sendNotification"))
		{
			if(output!=null)
		{

			try {
			Log.e("Json Response", output);
			JSONObject rootObj = new JSONObject(output);
			if(rootObj.getBoolean("result")){
				DialogManager.showDialog(getActivity(), "Reminder Sent to "+groupDetails.getgroupName());	
				//Toast.makeText(getActivity(), "Remainder Sent to "+userInfo.getname(), Toast.LENGTH_SHORT).show();
			}
			 
			} catch (Exception e) {
				// TODO: handle exception
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			}
	
		}
		else
		{
			// TODO: handle exception
			DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
		
			
		}
		}
		else if(from.equalsIgnoreCase("getHistoryDetails"))
		{
			if(output!=null)
			{

				try {
				Log.e("Json Response", output);
				Gson gson = new Gson();
				responseForTransaction = gson.fromJson(output, MyGroup_Transaction_Class.class);
				
				((TextView) mRootView.findViewById(R.id.amountToPayText)).setText(AppConstants.RASYMBOL+" "+
						responseForTransaction.totalbalance+" ");
				
				if(Integer.parseInt(responseForTransaction.totalbalance) > 0){
					((TextView) mRootView.findViewById(R.id.userName)).setText("You are Owed to "+responseForTransaction.groupname);
					((TextView) mRootView.findViewById(R.id.userName)).setTextColor(Color.parseColor("#378E34"));
					((TextView) mRootView.findViewById(R.id.amountToPayText)).setTextColor(Color.parseColor("#378E34"));
				}else if((Integer.parseInt(responseForTransaction.totalbalance) < 0)){
					((TextView) mRootView.findViewById(R.id.userName)).setText("You Owe to "+responseForTransaction.groupname);
					((TextView) mRootView.findViewById(R.id.userName)).setTextColor(Color.RED);
					((TextView) mRootView.findViewById(R.id.amountToPayText)).setTextColor(Color.RED);
				} else if((Integer.parseInt(responseForTransaction.totalbalance) == 0)){
					((TextView) mRootView.findViewById(R.id.userName)).setText("You aren't Owe or Owed to "+responseForTransaction.groupname);
					((TextView) mRootView.findViewById(R.id.userName)).setTextColor(Color.BLACK);
					((TextView) mRootView.findViewById(R.id.amountToPayText)).setTextColor(Color.BLACK);
					((ImageView)getActivity().findViewById(R.id.remaindButton)).setEnabled(false);
				}
				
				//((TextView) mRootView.findViewById(R.id.userName)).setText(responseForTransaction.groupname);
				Log.e("Image Path", responseForTransaction.imagepath);
				if(responseForTransaction.imagepath != null && responseForTransaction.imagepath.length() != 0){
					ImageLoader.getInstance().displayImage(responseForTransaction.imagepath, 
							((ImageView)mRootView.findViewById(R.id.userImage)), BashApplication.options, BashApplication.animateFirstListener);
				}
				//	ImageLoader.getInstance().displayImage(responseForTransaction.imagepath, ((ImageView)mRootView.findViewById(R.id.userImage)));
				
				//((TextView) mRootView.findViewById(R.id.userName)).setText(responseForTransaction.groupname);
				((EditText) editGroupView.findViewById(R.id.groupName)).setText(responseForTransaction.groupname);
				groupDetails.setFieldValues(responseForTransaction.groupname, responseForTransaction.idgroup, 
						responseForTransaction.imagepath);
				
				setDatas();
				 
				} catch (Exception e) {
					e.printStackTrace();
					DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
				}
		
			}
			else
			{
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			}
			
			
		}
		
	}

	 
	
}
