package com.bash.Fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TabHost.OnTabChangeListener;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.Providers.ContainerProvider;
import com.bash.Utils.AppConstants;
import com.bash.Utils.LogUtils;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

public class HomeFragment extends BaseFragment {
	
	public FragmentTabHost mChildTabhost;
	public static View mRootView;
	public static BaseFragment parentFragment;
 
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_home, null);
		LogUtils.errorLog("Inside Home Fragment", "True");
		initializeView();
		return mRootView;
	}
	
	private void initializeView() {
		
		//Initialize Activity Views
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn, "Bash", R.drawable.add_btn);
		
		((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//Saevesh
				((HomeActivity)getActivity()).slidingmenu_layout.toggleMenu();
			}
		});
		
		parentFragment = ((BaseFragment)getParentFragment());
		
		((ImageView) getActivity().findViewById(R.id.toprightsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//ActivityManager.startActivity(getActivity(), AddTransactionActivity.class);
				parentFragment.replaceFragment(new AddTransactionFragment(), true);
			}
		});
		
		View myfeedView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_widget_bg_for_myfeed, null);
		
		View mytransactionView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_widget_bg_for_mytransactions, null);
		
		mChildTabhost = (FragmentTabHost) mRootView.findViewById(R.id.fragmenttabhost);
		
		mChildTabhost.setup(getActivity(), getChildFragmentManager(), R.id.fragmentframe_holder);
		
		// Fragment Tab Host Initialization... 
		
		/*mChildTabhost.addTab(mChildTabhost.newTabSpec(AppConstants.TAB_FEED_TAG).setIndicator(myfeedView),
				ContainerProvider.Tab_Feed_Container.class, null);*/
		
		mChildTabhost.addTab(mChildTabhost.newTabSpec(AppConstants.TAB_FEED_TAG).setIndicator(myfeedView),
				ContainerProvider.Tab_Feed_Container.class, null);
		
		mChildTabhost.addTab(mChildTabhost.newTabSpec(AppConstants.TAB_FEED_LATEST_TAG).setIndicator(mytransactionView),
				ContainerProvider.Tab_MyTransaction_Container.class, null);
		
		/*mChildTabhost.addTab(mChildTabhost.newTabSpec(AppConstants.TAB_USER_TAG).setIndicator(mytransactionView),
		ContainerProvider.Tab_User_Container.class, null);*/
		//Sarvesh
		/*mChildTabhost.setOnTabChangedListener(new OnTabChangeListener() {
			@Override
			public void onTabChanged(String tabId) {
				// TODO Auto-generated method stub
				if(tabId == AppConstants.TAB_FEED_LATEST_TAG){
					((HomeActivity)getActivity()).menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
				}else{
					((HomeActivity)getActivity()).menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
				}
					
			}
		});*/
	}
	
	

	
}
