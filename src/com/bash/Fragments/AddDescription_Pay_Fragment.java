package com.bash.Fragments;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.BuildConfig;
import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Adapters.NothingSelectedAdapter;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.GsonClasses.Friends_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.TwitterConnection.TwitterFragment;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.bash.Utils.MyLocation;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Feed;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnPublishListener;

public class AddDescription_Pay_Fragment extends Fragment implements AsyncResponse{
	MyAsynTaskManager  myAsyncTask;
	View mRootView;
	
	View camera_gallery, pin_webview;
	Dialog camaeraDialog, pinWebDialog;
	Uri mCapturedImageURI;
	public ImageView fileImage;
	
	public Friends_Class responseForFriends;
	
	public ArrayList<String> friendNameList = new ArrayList<String>();
	public ArrayList<String> friendIdList = new ArrayList<String>();
	public ArrayList<String> friendPhoneNumberList = new ArrayList<String>();
	
	public ArrayList<Transactions_Class> transactionList = new ArrayList<Transactions_Class>();
	public ArrayList<String> transactionsNameList = new ArrayList<String>();
	
	public Spinner friendsSpinner;
	public TwitterFragment twitteraccess;
	public NothingSelectedAdapter nothingAdapter, nothingTransAdapter;
	public MyCustomAdapter dataAdaper, transactionAdapter;

	public String selectedFriendId, locationPath="";
	public WebView alertWebView;
	
	public ProgressDialog pd;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		mRootView = inflater.inflate(R.layout.fragment_addtransaction_pay_page, null);
		//mRootView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		return mRootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initializeView();
		initializeAlertView();
		initializeWebAlertView();
		((HomeActivity) getActivity()).currentFragment = this;
	}
	
	private void initializeAlertView() {

		// TODO Auto-generated method stub
		camera_gallery = View.inflate(getActivity(),
				R.layout.alert_gallerycamera_mode_view, null);
		camaeraDialog = new Dialog(getActivity());
		camaeraDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		camaeraDialog.setContentView(camera_gallery);
 
		((Button) camera_gallery.findViewById(R.id.openGalleryBtn))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						camaeraDialog.dismiss();
						Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						getActivity().startActivityForResult(i,	AppConstants.CODE_GALLERY);
				}
		});

		((Button) camera_gallery.findViewById(R.id.openCameraBtn)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						camaeraDialog.dismiss();
						String fileName = "temp.jpg";
						ContentValues values = new ContentValues();
						values.put(MediaStore.Images.Media.TITLE, fileName);
						mCapturedImageURI = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
										values);
						Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
						intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
						getActivity().startActivityForResult(intent, AppConstants.CODE_CAMERA);
					}
				});
		
		((ImageView) mRootView.findViewById(R.id.addPhotoButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				camaeraDialog.show();
			}
		});

	}
	
	
	private void initializeWebAlertView(){
		
		pin_webview = View.inflate(getActivity(),
				R.layout.alert_showpinalert_dialog, null);
		pinWebDialog = new Dialog(getActivity());
		pinWebDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		pinWebDialog.setContentView(pin_webview);
 
		alertWebView = ((WebView) pin_webview.findViewById(R.id.webView));
		
		WebSettings settings = alertWebView.getSettings();
        /*settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);*/
        settings.setJavaScriptEnabled(true);
        //alertWebView.setVerticalScrollBarEnabled(true);
        alertWebView.setHorizontalScrollBarEnabled(true);
        
     /*   alertWebView.setWebViewClient(new WebViewClient(){
        	@Override
        	public void onPageFinished(WebView view, String url) {
        		// TODO Auto-generated method stub
        		super.onPageFinished(view, url);
        		view.loadUrl(url);
        	}
        });*/
	}
	
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case AppConstants.CODE_CAMERA:
			if (resultCode == getActivity().RESULT_OK) {
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(
						mCapturedImageURI, projection, null, null, null);
				int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				String picturePath = cursor.getString(column_index_data);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,	fileImage, BashApplication.imageOptions);
				Log.e("Camera Path", picturePath);
			}
			break;
		case AppConstants.CODE_GALLERY:
			if (resultCode == getActivity().RESULT_OK) {

				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String picturePath = cursor.getString(columnIndex);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,	fileImage, BashApplication.imageOptions);
			}
			break;
		default:
			break;
		}
	}
	
	private void initializeView() {
		
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "Pay Friend", 0);
    	
		((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((BaseFragment)getParentFragment()).popFragment();
			}
		});
		 
		friendsSpinner = ((Spinner) mRootView.findViewById(R.id.friendsSpinner));
		
		dataAdaper = new MyCustomAdapter(getActivity(), R.layout.a_custom_spinner_for_friend_old, friendNameList);
		nothingAdapter = new NothingSelectedAdapter(dataAdaper, R.layout.a_custom_spinner_for_friend_old, getActivity());
		friendsSpinner.setAdapter(nothingAdapter);
		
		((EditText) mRootView.findViewById(R.id.dateText)).setText(AppConstants.simpleDateFormat.format(new Date()));
		pd = new ProgressDialog(getActivity());
		pd.setMessage("Tracking Your Location...");
		pd.setCancelable(false);
		
		friendsSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				if(position >= 1){
					Log.e("Friend Id", friendIdList.get(position-1));
					selectedFriendId = friendIdList.get(position-1);	
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});
		
		((ImageView) mRootView.findViewById(R.id.facebookButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(ValidateForm()){
					if (SimpleFacebook.getInstance().isLogin()) {
						callFbPost();
					} else {
						SimpleFacebook.getInstance().login(onLoginListener);
					}	
				}
			}
		});
		
		twitteraccess = new TwitterFragment(getActivity());
		
		((ImageView) mRootView.findViewById(R.id.twitterButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				twitteraccess.updateToTwitt(((EditText) mRootView
						.findViewById(R.id.feedCommentText)).getText()
						.toString());
			}
		});
		
		((RelativeLayout) mRootView.findViewById(R.id.paynowButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(ValidateForm()){
					ProcessForPayment(friendPhoneNumberList.get(friendsSpinner.getSelectedItemPosition()-1));
				}
			}
		});
		

		((RelativeLayout) mRootView.findViewById(R.id.payByCashButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(ValidateForm()){
					ProcessForByCash();
				}
			}
		});
		
		((ImageView) mRootView.findViewById(R.id.locationButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				pd.show();
				MyLocation locationPicker = new MyLocation();
				locationPicker.getLocation(getActivity(), new MyLocation.LocationResult() {
					@Override
					public void gotLocation(final Location location) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
						pd.dismiss();
						if(location == null){
							Toast.makeText(getActivity(), "Unable to Get Your Location! Try Again!", Toast.LENGTH_SHORT).show();
						}else{
							try {
								Geocoder geocoder;
								List<Address> addresses;
								geocoder = new Geocoder(getActivity(), Locale.getDefault());
								addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
								locationPath = addresses.get(0).getSubLocality()+", "+addresses.get(0).getLocality();
								((TextView)mRootView.findViewById(R.id.locationInfo)).setText(locationPath);
								
							} catch (Exception e) {
								e.printStackTrace();
							}
							
						}
							}
						});
					}
				});
			}
		});
		
		
		getOweFriendsDetails();
	}
	
	
	private void callFbPost() {

		// TODO Auto-generated method stub
		Log.e("CAlling Fb Post", "....");
		final Feed feed = new Feed.Builder()
				.setName(((EditText) mRootView.findViewById(R.id.amountText)).getText().toString())
				.setCaption("Amount Spent "+ ((EditText) mRootView.findViewById(R.id.amountText)).getText().toString())
				.setDescription(((EditText) mRootView.findViewById(R.id.feedCommentText)).getText().toString())
				
				 .setPicture("https://www.facebook.com/images/fb_icon_325x325.png")
				 
				 .setLink("http://www.bash.co/")
				 
				 .build();
		
	/*	final Feed feed = new Feed.Builder()
		.setName("test").setCaption("Amount Spent ").setDescription("test")
		.build();*/
		
	 	SimpleFacebook.getInstance().publish(feed, true,
				new OnPublishListener() {
					@Override
					public void onException(Throwable throwable) {
						DialogManager.showDialog(getActivity(),
								"Error Occured to Share Message! Try Again!");
					}

					@Override
					public void onFail(String reason) {
						DialogManager.showDialog(getActivity(),
								"Error Occured to Share Message! Try Again! Reason : "+reason);
					}

					@Override
					public void onComplete(String response) {
						DialogManager.showDialog(getActivity(),
								"Message Wrote of FB Wall Sucessfully!");
					}
				});

	}

	final OnLoginListener onLoginListener = new OnLoginListener() {

		@Override
		public void onFail(String reason) {
			Log.e("Login Failed", "....");
		}

		@Override
		public void onException(Throwable throwable) {
			Log.e("Login Failed", "....");
		}

		@Override
		public void onThinking() {
			// show progress bar or something to the user while login is
			Log.e("Login Failed", "....");
		}

		@Override
		public void onLogin() {
			// change the state of the button or do whatever you want
			Log.e("Login Success", "....");
			callFbPost();
		}

		@Override
		public void onNotAcceptingPermissions(Permission.Type type) {
			// toast(String.format("You didn't accept %s permissions",
			// type.name()));
			Log.e("Login Failed", "....");
		}
	};

	
	public void ProcessForPayment(String payeeNumber){

		String transactionId = String.valueOf("MGGP"+AppConstants.dateformatforZipCash.format(new Date())+PreferenceManager.getInstance().getUserId());
		
	 	Log.e("Transaction Id", transactionId);
	 	myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("ProcessForPayment",getActivity(),AppConstants.URL_ZIP_CASH, new String[] {
				"partnercode",
				"username",
				"password",
				"partnertransactionid",
				"servicetype",
				"merchantCode",
				"amount",
				"usernumber",
				"returl"
				}, 
				new String[]{
				PreferenceManager.getInstance().getPartnerCode(),
				PreferenceManager.getInstance().getPartnerUserName(),
				PreferenceManager.getInstance().getPartnerPassword(),
				transactionId,
				"PP",
				payeeNumber,
				((EditText) mRootView.findViewById(R.id.amountText)).getText().toString(),
				PreferenceManager.getInstance().getUserMobileNumber(),
				""
				});

		myAsyncTask.execute();
	}
	
	public void ProcessForByCash() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
	 	myAsyncTask.setupParamsAndUrl("ProcessForByCash",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser",
				"idfriend",
				"date",
				"location",
				"pay_mode",
				"comment",
				"amount"
				}, 
				new String[]{
				"feed",
				"peartopear",
				PreferenceManager.getInstance().getUserId(),
				selectedFriendId,
				((EditText) mRootView.findViewById(R.id.dateText)).getText().toString(),
				"Chennai",
				"cash",
				((EditText) mRootView.findViewById(R.id.feedCommentText)).getText().toString(),
				((EditText) mRootView.findViewById(R.id.amountText)).getText().toString()
				});
	 	myAsyncTask.execute();
		
	 
	}
	
	public void getOweFriendsDetails() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("getOweFriendsDetails",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser"
				}, 
				new String[]{
				"feed",
				"needtopay",
				PreferenceManager.getInstance().getUserId()
				});

myAsyncTask.execute();
	}

	public boolean ValidateForm(){
		try {
			if(friendsSpinner.getSelectedItemPosition() <= 0){
				Toast.makeText(getActivity(), "Please Select Friend Name!", Toast.LENGTH_SHORT).show();
				return false;}
			 else if(((EditText) mRootView.findViewById(R.id.amountText)).getText() == null || 
					((EditText) mRootView.findViewById(R.id.amountText)).getText().toString().length() == 0){
				 	Toast.makeText(getActivity(), "Please Your Amout to Pay!", Toast.LENGTH_SHORT).show();
				return false;
			}else if(((EditText) mRootView.findViewById(R.id.dateText)).getText() == null || 
					((EditText) mRootView.findViewById(R.id.dateText)).getText().toString().length() == 0){
			 	Toast.makeText(getActivity(), "Please Select Date Details!", Toast.LENGTH_SHORT).show();
			 	return false;
			}else if(((EditText) mRootView.findViewById(R.id.feedCommentText)).getText() == null || 
					((EditText) mRootView.findViewById(R.id.feedCommentText)).getText().toString().length() == 0){
			 	Toast.makeText(getActivity(), "What's your payment for? Please enter details!", Toast.LENGTH_SHORT).show();
			 	return false;
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Toast.makeText(getActivity(), "Please Select Friend Name!", Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}
	
	Calendar cal = Calendar.getInstance();
	 
	public DatePickerDialog getDatePickerDialog() {
			DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
				 
	            @Override
	            public void onDateSet(DatePicker view, int year,
	                    int monthOfYear, int dayOfMonth) {
	            	((EditText) mRootView.findViewById(R.id.dateText)).setText(dayOfMonth + "/"
	                        + (monthOfYear + 1) + "/" + year);
	            }
	        }, cal.get(Calendar.YEAR),  cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			
		 return datePickerDialog;
	 }

		
	public class MyCustomAdapter extends ArrayAdapter<String> {

 		public MyCustomAdapter(Context context, int textViewResourceId, ArrayList<String> objects) {
			
			super(context, textViewResourceId, objects);
			// TODO Auto-generated constructor stub
			}
		
			public void setNotificationwithData(ArrayList<String> objects){
				 
			}

			@Override
			public View getDropDownView(int position, View convertView,
			ViewGroup parent) {
			// TODO Auto-generated method stub
			return getCustomView(position, convertView, parent);
			}

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			return getCustomView(position, convertView, parent);
			}
			
			public View getCustomView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			TextView label = null;
			if(convertView == null){
				LayoutInflater inflater=LayoutInflater.from(getContext());
				convertView = inflater.inflate(R.layout.a_custom_spinner_for_friend_old, parent, false);
				label=(TextView)convertView.findViewById(android.R.id.text1);
			}
			label.setText(getItem(position));
			return convertView;
			}
	}
	 
	class Transactions_Class {

		String transactionid, amount, feedcomment;
		
		public Transactions_Class(String transactionid, String feedcomment, String amount){
			this.transactionid = transactionid;
			this.feedcomment = feedcomment;
			this.amount = amount;
		}
		
		public String getTransactionId(){
			return transactionid;
		}
		public String getFeedComment(){
			return feedcomment;
		}
		public String getAmount(){
			return amount;
		}
		
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		if(from.equalsIgnoreCase("ProcessForPayment"))
		{
			
			if(output!=null) 
			{
				alertWebView.loadDataWithBaseURL("", output, "", "", "");
				pinWebDialog.show();	
			}
			else
			{

				DialogManager.showDialog(getActivity(), "Failed To Proceed Transaction! Try Again!");
			
			}
			
	
		}
		else if(from.equalsIgnoreCase("ProcessForByCash"))
		{

			if(output!=null) 
			{
				try {
				JSONObject rootObj = new JSONObject(output);
				if(rootObj.getBoolean("result")){
					Toast.makeText(getActivity(), "Transaction Processed Successfully!", Toast.LENGTH_SHORT).show();
					((BaseFragment)getParentFragment()).popFragment();
				}else{
					DialogManager.showDialog(getActivity(), "Failed To Proceed Transaction! Try Again!");
				}
			} catch (Exception e) {
					e.printStackTrace();
			}
				}
			else
			{

				DialogManager.showDialog(getActivity(), "Failed To Proceed Transaction! Try Again!");
			
			}
			
		}
		else if(from.equalsIgnoreCase("getOweFriendsDetails"))
		{
			if(output!=null)
			{
				try {
					if(BuildConfig.DEBUG)
							Log.e("Json Response", output);
				 
					JSONObject rootObj = new JSONObject(output);
					
					if(rootObj.getBoolean("result")){
						
						friendNameList.clear();
						friendIdList.clear();
						friendPhoneNumberList.clear();
						
						JSONArray jsonArray = rootObj.getJSONArray("friendlist");
						JSONObject jObj;
						for(int i = 0; i<jsonArray.length(); i++){
							jObj = jsonArray.getJSONObject(i);
							friendNameList.add(jObj.getString("firstname"));
							friendIdList.add(jObj.getString("idfriend"));
							friendPhoneNumberList.add(jObj.getString("phone_no"));
						}
						
						dataAdaper.notifyDataSetChanged();
						//friendsSpinner.setSelection(1);
						
					}else{
						//DialogManager.showDialog(getActivity(), "You Haven't owe to any One!");
						Toast.makeText(getActivity(), "You don't owe anybody!", Toast.LENGTH_SHORT).show();
					//	AddTransactionFragment.shouldSkipThisBill = true;
						((BaseFragment)getParentFragment()).popFragment();
					}
					
					} catch (Exception e) {
						// TODO: handle exception
						DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
					}
			}
			else
			{
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			}
			
	}
		
	}
}
