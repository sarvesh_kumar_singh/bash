package com.bash.Fragments;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import me.grantland.widget.AutofitTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.AddTabActivity;
import com.bash.Activities.AddTabSplitActivity;
import com.bash.Activities.CurrencyActivity;
import com.bash.Activities.PaidbySplitActivity;
import com.bash.Activities.SocialPageActivity;
import com.bash.Adapters.TwoWayViewPhotosAdapter;
import com.bash.CustomViews.CircularImageView;
import com.bash.CustomViews.ContactsCompletionView;
import com.bash.ListModels.Person;
import com.bash.ListModels.PhotosListModel;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.bash.interfaces.ActivityInterface;
import com.bash.interfaces.PaidbyInterface;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.tokenautocomplete.FilteredArrayAdapter;
import com.tokenautocomplete.TokenCompleteTextView;

@SuppressWarnings("rawtypes")
public class AddTabPayFragment extends Fragment implements AsyncResponse,
		TokenCompleteTextView.TokenListener, ActivityInterface, PaidbyInterface {

	TwoWayView horizontalViewPhotos;
	ArrayList<PhotosListModel> photosList;
	TwoWayViewPhotosAdapter horizontalPhotoAdapter;
	MyAsynTaskManager myAsyncTask;
	ContactsCompletionView searchView;
	Person[] people;
	ArrayList<Person> personList;
	ArrayList<Person> addedPersonList;
	FilteredArrayAdapter<Person> adapter;
	LinearLayout moretwouserlinear, twouserlinear;
	ImageView arrowpayid, arrowreceiveid, datepickerimageview,
			categoryimageview;
	CircularImageView receiveuserimageView, paiduserimageView;
	Button btnSave, btnSavePay;
	TextView paidusernametext, receiveusernametext, paidby, currencyText,
			paidbytext;
	AutofitTextView amountedittext;
	EditText titleedittext;
	View mRootView;
	private int fragTag;
	private int categorySelect = 9;
	private String dateString = "";
	private String transationTitle = "";
	private String transationAnount = "";
	private String currencyCodeString = "INR";
	private AlertDialog catedialog = null;
	private AlertDialog continuedialog = null;
	private boolean dateSet;
	private String userame = "";
	private String userimagepath = "";
	private String userid = "";

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// mRootView = inflater.inflate(R.layout.fragment_home, null);
		mRootView = inflater.inflate(R.layout.addtab_pay_fragment_, null);
		String fragTagstr;
		Bundle fragbundle = getArguments();
		String tabTag = fragbundle.getString(AppConstants.FRAGMENT_TAG,
				AppConstants.TAB_PAY_TAG);
		if (tabTag != null && (tabTag.equals(AppConstants.TAB_PAY_TAG))) {
			fragTag = 1;
		} else if (tabTag != null
				&& (tabTag.equals(AppConstants.TAB_SPLIT_TAG))) {
			fragTag = 2;
		} else if (tabTag != null
				&& (tabTag.equals(AppConstants.TAB_RECEIVE_TAG))) {
			fragTag = 3;
		}

		if (AddTabActivity.mTabhost.getCurrentTabTag() != null) {

			fragTagstr = AddTabActivity.mTabhost.getCurrentTabTag().toString();

			if (fragTagstr != null
					&& (fragTagstr.equals(AppConstants.TAB_PAY_TAG))) {
				fragTag = 1;
			} else if (fragTagstr != null
					&& (fragTagstr.equals(AppConstants.TAB_SPLIT_TAG))) {
				fragTag = 2;
			} else if (fragTagstr != null
					&& (fragTagstr.equals(AppConstants.TAB_RECEIVE_TAG))) {
				fragTag = 3;
			}
			Toast.makeText(getActivity(), "fragment = " + fragTag,
					Toast.LENGTH_SHORT).show();

		}

		searchView = (ContactsCompletionView) mRootView
				.findViewById(R.id.searchView);
		moretwouserlinear = (LinearLayout) mRootView
				.findViewById(R.id.moretwouserlinear);
		twouserlinear = (LinearLayout) mRootView
				.findViewById(R.id.twouserlinear);
		arrowpayid = (ImageView) mRootView.findViewById(R.id.arrowpayid);
		arrowreceiveid = (ImageView) mRootView
				.findViewById(R.id.arrowreceiveid);
		datepickerimageview = (ImageView) mRootView
				.findViewById(R.id.datepickerimageview);
		categoryimageview = (ImageView) mRootView
				.findViewById(R.id.categoryimageview);
		receiveuserimageView = (CircularImageView) mRootView
				.findViewById(R.id.receiveuserimageView);
		paiduserimageView = (CircularImageView) mRootView
				.findViewById(R.id.paiduserimageView);
		paidusernametext = (TextView) mRootView
				.findViewById(R.id.paidusernametext);
		receiveusernametext = (TextView) mRootView
				.findViewById(R.id.receiveusernametext);
		paidby = (TextView) mRootView.findViewById(R.id.paidby);
		paidbytext = (TextView) mRootView.findViewById(R.id.paidbytext);
		currencyText = (TextView) mRootView.findViewById(R.id.currencyText);
		amountedittext = (AutofitTextView) mRootView
				.findViewById(R.id.amountedittext);
		titleedittext = (EditText) mRootView.findViewById(R.id.titleedittext);

		searchView
				.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Delete);
		intializeUI();

		return mRootView;
	}

	private void intializeUI() {
		if (personList == null) {
			personList = new ArrayList<Person>();
		}
		if (addedPersonList == null) {
			addedPersonList = new ArrayList<Person>();
		}
		if (photosList == null) {
			photosList = new ArrayList<PhotosListModel>();
		}

		userame = PreferenceManager.getInstance().getUser_FullName();
		userimagepath = PreferenceManager.getInstance().getUserImagePath();
		userid = PreferenceManager.getInstance().getUserId();

		if ((personList.size() == 0)) {
			personList.add(new Person("0", userid, "robin", ""));
		}

		// people = new Person[]{
		// new Person("0", userid, "robin", "")
		// };

		setMultiAutoTextViewAdapter();

		if (!addedPersonList.contains(personList.get(0))) {
			searchView.addObject(personList.get(0));
		}

		// searchView.setPrefix(" and ");
		searchView.setTokenListener(this);

		if (fragTag == 1) {
			paidby.setText(personList.get(0).getName());

			twouserlinear.setVisibility(View.VISIBLE);
			moretwouserlinear.setVisibility(View.GONE);
			arrowpayid.setVisibility(View.VISIBLE);
			arrowreceiveid.setVisibility(View.GONE);
		} else if (fragTag == 2) {
			twouserlinear.setVisibility(View.GONE);
			moretwouserlinear.setVisibility(View.VISIBLE);
			// arrowpayid.setVisibility(View.VISIBLE);
			// arrowreceiveid.setVisibility(View.GONE);
		} else if (fragTag == 3) {

			twouserlinear.setVisibility(View.VISIBLE);
			moretwouserlinear.setVisibility(View.GONE);
			arrowpayid.setVisibility(View.GONE);
			arrowreceiveid.setVisibility(View.VISIBLE);
		}

		datepickerimageview.setOnClickListener(clickListener);
		categoryimageview.setOnClickListener(clickListener);
		currencyText.setOnClickListener(clickListener);
		paidby.setOnClickListener(clickListener);

		AddTabActivity.tvContinue.setOnClickListener(clickListener);

		searchView.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				String chrstr = s.toString();
				int commaindex = chrstr.lastIndexOf(",");
				chrstr = chrstr.substring(commaindex + 1);
				if (chrstr.length() > 3) {

					// Toast.makeText(
					// getActivity(),
					// chrstr.trim() + " ,dd, " + start + " , " + before
					// + " , " + count, Toast.LENGTH_SHORT).show();

					getSearchUserDetails(chrstr.trim());
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		if (fragTag == 2) {

			horizontalViewPhotos = (TwoWayView) mRootView
					.findViewById(R.id.horizontalvcrollviewphoto);

			horizontalPhotoAdapter = new TwoWayViewPhotosAdapter(getActivity(),
					R.layout.item_twowayhorizontal_layout, photosList);
			horizontalViewPhotos.setAdapter(horizontalPhotoAdapter);

		}
	}

	private View.OnClickListener clickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			String text = "";

			switch (v.getId()) {

			case R.id.datepickerimageview:
				text = dateString;
				dateDialog();
				break;
			case R.id.categoryimageview:
				showCategoryAlertDialog();
				break;
			case R.id.currencyText:
				text = "currencyText";
				CurrencyActivity.delegate = AddTabPayFragment.this;
				Intent startIntent = new Intent(getActivity(),
						CurrencyActivity.class);
				startActivity(startIntent);
				break;
			case R.id.linear1:
				categorySelect = 1;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.linear2:
				categorySelect = 2;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.linear3:
				categorySelect = 3;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.linear4:
				categorySelect = 4;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.linear5:
				categorySelect = 5;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.linear6:
				categorySelect = 6;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.linear7:
				categorySelect = 7;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.linear8:
				categorySelect = 8;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.linear9:
				categorySelect = 9;
				text = "" + categorySelect;
				if (catedialog != null)
					catedialog.dismiss();
				break;
			case R.id.toprightsidecontinuetext:
				if (fragTag == 1 || fragTag == 3) {
					if (validateData()) {

						showContinueAlertDialog();

					}
				} else if (fragTag == 2) {

					if (validateSplit()) {

						setPaidUserList();
						setReceiverList();

						Bundle bndle = new Bundle();
						bndle.putParcelableArrayList("PaidUserDetails",
								addedPersonList);
						bndle.putParcelableArrayList("ReceiveUserDetails",
								photosList);
						bndle.putString("CategorySelect", "" + categorySelect);
						bndle.putString("DateString", "" + dateString);
						bndle.putString("TransationTitle", "" + transationTitle);
						bndle.putString("TransationAnount", ""
								+ transationAnount);
						bndle.putString("CurrencyCodeString", ""
								+ currencyCodeString);

						Intent splittabIntent = new Intent(getActivity(),
								AddTabSplitActivity.class);
						splittabIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						splittabIntent.putExtras(bndle);
						startActivity(splittabIntent);

					}

				}
				text = "" + "Continue Click";

				break;
			case R.id.continuesavebtn:
				int type = 0;

				// if(addedPersonList.get(1).getUserType().){
				//
				// }
				sendPayTransationDetails(type, fragTag);

				if (continuedialog != null && continuedialog.isShowing())
					continuedialog.dismiss();

				break;
			case R.id.continuesavepaybtn:
				text = "save and pay clicked";

				if (continuedialog != null && continuedialog.isShowing())
					continuedialog.dismiss();
				
				
				
				break;
			case R.id.paidby:
				text = "paidby clicked";

				if (validatePaidBy()) {
					PaidbySplitActivity.delegate = AddTabPayFragment.this;
					Intent paidByIntent = new Intent(getActivity(),
							PaidbySplitActivity.class);
					paidByIntent.putExtra("TotalAmount", amountedittext
							.getText().toString());
					paidByIntent.putParcelableArrayListExtra("AddedPersonList",
							addedPersonList);
					startActivity(paidByIntent);

				}

				break;

			}
			Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
		}
	};

	@Override
	public void onTokenAdded(Object token) {
		// TODO Auto-generated method stub
		Person personobj = (Person) token;
		if (!addedPersonList.contains(personobj)) {
			addedPersonList.add(personobj);
		}

		if (fragTag == 1 || fragTag == 3) {
			if (addedPersonList.size() == 1) {
				showSinglePayLayout(addedPersonList.size());
			} else if (addedPersonList.size() == 2) {

				showSinglePayLayout(addedPersonList.size());
			}
		}
		if (fragTag == 2) {

			if (addedPersonList.size() > 0) {

				for (Person person : addedPersonList) {
					boolean contains = false;
					for (PhotosListModel photosListModel : photosList) {
						if (photosListModel.getID().equals(person.getId())) {

							contains = true;

						}
					}

					if (!contains) {

						photosList
								.add(new PhotosListModel(person.getUserType(),
										person.getId(), person
												.getImage_location(), person
												.getName()));
					}
				}
				horizontalPhotoAdapter.notifyDataSetChanged();
			}

			boolean paidbybool = false;
			StringBuilder sb = new StringBuilder();

			if (addedPersonList.size() > 0) {
				for (Person paidPersonobj : addedPersonList) {

					if (paidPersonobj.getPaidamount() != 0.0) {
						paidbybool = true;

						sb.append(paidPersonobj.getName());
						sb.append(" & ");
					}
				}
				if (!paidbybool) {
					paidby.setText(addedPersonList.get(0).getName());
				} else {
					int sbindex = sb.lastIndexOf("&");
					sb.deleteCharAt(sbindex);
					String sbstr = (sb.toString().trim());
					paidby.setText(sbstr);
				}
			}

		}

	}

	@Override
	public void onTokenRemoved(Object token) {
		// TODO Auto-generated method stub
		Person personobj = (Person) token;
		addedPersonList.remove(personobj);

		if (fragTag == 1 || fragTag == 3) {
			if (addedPersonList.size() == 1) {
				showSinglePayLayout(addedPersonList.size());
			} else if (addedPersonList.size() == 2) {

				showSinglePayLayout(addedPersonList.size());
			}
		}
		if (fragTag == 2) {
			if (addedPersonList.size() > 0) {

				for (Person person : addedPersonList) {
					if (!photosList.contains(person)) {
						photosList
								.add(new PhotosListModel(person.getUserType(),
										person.getId(), person
												.getImage_location(), person
												.getName()));

					}
				}
				horizontalPhotoAdapter.notifyDataSetChanged();
			}
		}
		Toast.makeText(getActivity(),
				"searchView :" + searchView.getText().toString(),
				Toast.LENGTH_LONG).show();
	}

	private void getSearchUserDetails(String searchText) {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask
				.setupParamsAndUrl("getSearchUserDetails", getActivity(),
						AppUrlList.ACTION_URL, new String[] { "module",
								"action", "iduser", "search_string" },
						new String[] { "transaction", "getList",
								PreferenceManager.getInstance().getUserId(),
								searchText });
		myAsyncTask.execute();
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		if (from.equalsIgnoreCase("getSearchUserDetails")) {
			if (output != null && output.length() > 0) {
				try {
					Gson gson = new Gson();

					JSONObject baseJsonObject = new JSONObject(output);
					boolean result = baseJsonObject.getBoolean("result");
					if (result) {

						JSONArray resultsArray = baseJsonObject
								.getJSONArray("results");

						Type collectionType = new TypeToken<ArrayList<Person>>() {
						}.getType();
						personList.clear();
						ArrayList<Person> detailList = gson.fromJson(
								resultsArray.toString(), collectionType);

				
						personList.addAll(detailList);

					

						setMultiAutoTextViewAdapter();

						try {
							searchView.showDropDown();

						} catch (Exception e) {
							e.printStackTrace();
						}

					} else {
						adapter.notifyDataSetInvalidated();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}
		if (from.equalsIgnoreCase("PayTransationDetails")) {
			if (output != null && output.length() > 0) {
				JSONObject baseJsonObject;
				try {
					baseJsonObject = new JSONObject(output);
					boolean result = baseJsonObject.getBoolean("result");
					String msgStr = baseJsonObject.getString("msg");
					if (result) {
						Toast.makeText(getActivity(), msgStr,
								Toast.LENGTH_SHORT).show();
						startActivity(new Intent(getActivity(), SocialPageActivity.class));
					} else {
						Toast.makeText(getActivity(), msgStr,
								Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
		if (from.equalsIgnoreCase("ReceiveTransationDetails")) {
			if (output != null && output.length() > 0) {
				JSONObject baseJsonObject;
				try {
					baseJsonObject = new JSONObject(output);
					boolean result = baseJsonObject.getBoolean("result");
					String msgStr = baseJsonObject.getString("msg");
					if (result) {
						
						Toast.makeText(getActivity(), msgStr,
								Toast.LENGTH_SHORT).show();
						
						startActivity(new Intent(getActivity(), SocialPageActivity.class));
						
					} else {
						Toast.makeText(getActivity(), msgStr,
								Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	protected void showCategoryAlertDialog() {
		// TODO Auto-generated method stub

		Context context = getActivity();
		LinearLayout linear1, linear2, linear3, linear4, linear5, linear6, linear7, linear8, linear9;

		final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.dialog_categories_picker, null);
		alertDialogBuilder.setView(view);
		alertDialogBuilder.setCancelable(true);
		catedialog = alertDialogBuilder.create();
		catedialog.show();
		linear1 = (LinearLayout) view.findViewById(R.id.linear1);
		linear2 = (LinearLayout) view.findViewById(R.id.linear2);
		linear3 = (LinearLayout) view.findViewById(R.id.linear3);
		linear4 = (LinearLayout) view.findViewById(R.id.linear4);
		linear5 = (LinearLayout) view.findViewById(R.id.linear5);
		linear6 = (LinearLayout) view.findViewById(R.id.linear6);
		linear7 = (LinearLayout) view.findViewById(R.id.linear7);
		linear8 = (LinearLayout) view.findViewById(R.id.linear8);
		linear9 = (LinearLayout) view.findViewById(R.id.linear9);

		if (categorySelect == 1) {
			linear1.setPressed(true);
			linear9.setFocusableInTouchMode(false);
			linear2.setFocusableInTouchMode(false);
			linear3.setFocusableInTouchMode(false);
			linear4.setFocusableInTouchMode(false);
			linear5.setFocusableInTouchMode(false);
			linear6.setFocusableInTouchMode(false);
			linear7.setFocusableInTouchMode(false);
			linear8.setFocusableInTouchMode(false);
		} else if (categorySelect == 2) {
			linear2.setPressed(true);
			linear1.setFocusableInTouchMode(false);
			linear9.setFocusableInTouchMode(false);
			linear3.setFocusableInTouchMode(false);
			linear4.setFocusableInTouchMode(false);
			linear5.setFocusableInTouchMode(false);
			linear6.setFocusableInTouchMode(false);
			linear7.setFocusableInTouchMode(false);
			linear8.setFocusableInTouchMode(false);
		} else if (categorySelect == 3) {
			linear3.setPressed(true);
			linear1.setFocusableInTouchMode(false);
			linear2.setFocusableInTouchMode(false);
			linear9.setFocusableInTouchMode(false);
			linear4.setFocusableInTouchMode(false);
			linear5.setFocusableInTouchMode(false);
			linear6.setFocusableInTouchMode(false);
			linear7.setFocusableInTouchMode(false);
			linear8.setFocusableInTouchMode(false);
		} else if (categorySelect == 4) {
			linear4.setPressed(true);
			linear1.setFocusableInTouchMode(false);
			linear2.setFocusableInTouchMode(false);
			linear3.setFocusableInTouchMode(false);
			linear9.setFocusableInTouchMode(false);
			linear5.setFocusableInTouchMode(false);
			linear6.setFocusableInTouchMode(false);
			linear7.setFocusableInTouchMode(false);
			linear8.setFocusableInTouchMode(false);
		} else if (categorySelect == 5) {
			linear5.setPressed(true);
			linear1.setFocusableInTouchMode(false);
			linear2.setFocusableInTouchMode(false);
			linear3.setFocusableInTouchMode(false);
			linear4.setFocusableInTouchMode(false);
			linear9.setFocusableInTouchMode(false);
			linear6.setFocusableInTouchMode(false);
			linear7.setFocusableInTouchMode(false);
			linear8.setFocusableInTouchMode(false);
		} else if (categorySelect == 6) {
			linear6.setPressed(true);
			linear1.setFocusableInTouchMode(false);
			linear2.setFocusableInTouchMode(false);
			linear3.setFocusableInTouchMode(false);
			linear4.setFocusableInTouchMode(false);
			linear5.setFocusableInTouchMode(false);
			linear9.setFocusableInTouchMode(false);
			linear7.setFocusableInTouchMode(false);
			linear8.setFocusableInTouchMode(false);
		} else if (categorySelect == 7) {
			linear7.setPressed(true);
			linear1.setFocusableInTouchMode(false);
			linear2.setFocusableInTouchMode(false);
			linear3.setFocusableInTouchMode(false);
			linear4.setFocusableInTouchMode(false);
			linear5.setFocusableInTouchMode(false);
			linear6.setFocusableInTouchMode(false);
			linear9.setFocusableInTouchMode(false);
			linear8.setFocusableInTouchMode(false);
		} else if (categorySelect == 8) {
			linear8.setPressed(true);
			linear1.setFocusableInTouchMode(false);
			linear2.setFocusableInTouchMode(false);
			linear3.setFocusableInTouchMode(false);
			linear4.setFocusableInTouchMode(false);
			linear5.setFocusableInTouchMode(false);
			linear6.setFocusableInTouchMode(false);
			linear7.setFocusableInTouchMode(false);
			linear9.setFocusableInTouchMode(false);
		} else if (categorySelect == 9) {
			linear9.setPressed(true);
			linear1.setFocusableInTouchMode(false);
			linear2.setFocusableInTouchMode(false);
			linear3.setFocusableInTouchMode(false);
			linear4.setFocusableInTouchMode(false);
			linear5.setFocusableInTouchMode(false);
			linear6.setFocusableInTouchMode(false);
			linear7.setFocusableInTouchMode(false);
			linear8.setFocusableInTouchMode(false);
		}

		linear1.setOnClickListener(clickListener);
		linear2.setOnClickListener(clickListener);
		linear3.setOnClickListener(clickListener);
		linear4.setOnClickListener(clickListener);
		linear5.setOnClickListener(clickListener);
		linear6.setOnClickListener(clickListener);
		linear7.setOnClickListener(clickListener);
		linear8.setOnClickListener(clickListener);
		linear9.setOnClickListener(clickListener);

	}

	public void dateDialog() {
		// TODO Auto-generated method stub
		int day, month, year;
		Calendar cal = Calendar.getInstance();
		day = cal.get(Calendar.DAY_OF_MONTH);
		month = cal.get(Calendar.MONTH);
		year = cal.get(Calendar.YEAR);
		final DateSetListener _datePickerDialogCallback = new DateSetListener();
		final DatePickerDialog dpd = new DatePickerDialog(getActivity(),
				_datePickerDialogCallback, year, month, day);

		dpd.setButton(DialogInterface.BUTTON_POSITIVE, "SET",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (which == DialogInterface.BUTTON_POSITIVE) {
							dateSet = true;
							DatePicker datePicker = dpd.getDatePicker();
							_datePickerDialogCallback.onDateSet(datePicker,
									datePicker.getYear(),
									datePicker.getMonth(),
									datePicker.getDayOfMonth());
						}
					}
				});

		dpd.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (which == DialogInterface.BUTTON_NEGATIVE) {
							dateSet = false;
							dpd.hide();
						}
					}
				});
		dpd.show();
	}

	private class DateSetListener implements DatePickerDialog.OnDateSetListener {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			if (dateSet) {

				dateString = year + "-" + (1 + monthOfYear) + "-" + dayOfMonth;

			}
		}
	}

	@Override
	public void onActivityData(String output) {
		// TODO Auto-generated method stub
		currencyCodeString = output.trim();
		Toast.makeText(getActivity(), output, Toast.LENGTH_SHORT).show();
	}

	private void showSinglePayLayout(int size) {
		if ((addedPersonList.get(0).getImage_location() != null)
				&& (addedPersonList.get(0).getImage_location().length() > 0)) {
			UrlImageViewHelper.setUrlDrawable(paiduserimageView,
					addedPersonList.get(0).getImage_location());
		}
		if ((addedPersonList.get(0).getName() != null)
				&& (addedPersonList.get(0).getName().length() > 0)) {
			paidusernametext.setText(addedPersonList.get(0).getName());
		} else {
			paidusernametext.setText("robin");
		}

		if (size == 2) {
			if (fragTag == 3)
				paidby.setText(addedPersonList.get(1).getName());

			if ((addedPersonList.get(1).getImage_location() != null)
					&& (addedPersonList.get(1).getImage_location().length() > 0)) {
				UrlImageViewHelper.setUrlDrawable(receiveuserimageView,
						addedPersonList.get(1).getImage_location());
			}
			if ((addedPersonList.get(1).getName() != null)
					&& (addedPersonList.get(1).getName().length() > 0)) {
				receiveusernametext.setText(addedPersonList.get(1).getName());

				// arrowpayid.setVisibility(View.GONE);
				// arrowpayid.setVisibility(View.GONE);
			} else {
				// arrowpayid.setVisibility(View.GONE);
				// arrowpayid.setVisibility(View.GONE);
			}
		}

	}

	private boolean validateData() {
		boolean validate = true;
		transationAnount = amountedittext.getText().toString().trim();
		transationTitle = titleedittext.getText().toString().trim();

		if (TextUtils.isEmpty(transationAnount)) {
			amountedittext.setError("Field is Empty");
			return false;
		}
		if (TextUtils.isEmpty(transationTitle)) {
			titleedittext.setError("Field is Empty");
			return false;
		}
		if (addedPersonList.size() < 2) {
			Toast.makeText(getActivity(), "Please add transation client",
					Toast.LENGTH_SHORT).show();
			return false;
		}

		return validate;
	}

	private void sendPayTransationDetails(int type, int fragmentint) {

		if (fragmentint == 1) {
			myAsyncTask = new MyAsynTaskManager();
			myAsyncTask.delegate = this;
			myAsyncTask.setupParamsAndUrl("PayTransationDetails",
					getActivity(), AppUrlList.ACTION_URL, new String[] {
							"module", "action", "payer_iduser",
							"receiver_user_type", "receiver_user_id",
							"transaction_currency", "transaction_amount",
							"transaction_category", "transaction_date",
							"transaction_title" }, new String[] {
							"transaction", "insertPayTransaction", userid,
							"" + type, addedPersonList.get(1).getId(),
							currencyCodeString, transationAnount,
							"" + categorySelect, dateString, transationTitle });
			myAsyncTask.execute();
		} else if (fragmentint == 3) {
			myAsyncTask = new MyAsynTaskManager();
			myAsyncTask.delegate = this;
			myAsyncTask.setupParamsAndUrl("ReceiveTransationDetails",
					getActivity(), AppUrlList.ACTION_URL, new String[] {
							"module", "action", "receiver_iduser",
							"payer_user_type", "payer_user_id",
							"transaction_currency", "transaction_amount",
							"transaction_category", "transaction_date",
							"transaction_title" }, new String[] {
							"transaction", "insertReceiveTransaction", userid,
							"" + type, addedPersonList.get(1).getId(),
							currencyCodeString, transationAnount,
							"" + categorySelect, dateString, transationTitle });
			myAsyncTask.execute();
		}
	}

	protected void showContinueAlertDialog() {
		// TODO Auto-generated method stub

		Context context = getActivity();

		final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.continue_save_dialog, null);
		alertDialogBuilder.setView(view);
		alertDialogBuilder.setCancelable(true);
		continuedialog = alertDialogBuilder.create();
		continuedialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		WindowManager.LayoutParams wmlp = continuedialog.getWindow()
				.getAttributes();

		wmlp.gravity = Gravity.TOP | Gravity.RIGHT;
		wmlp.x = 10; // x position
		wmlp.y = 10; // y position
		continuedialog.show();
		btnSave = (Button) view.findViewById(R.id.continuesavebtn);
		btnSavePay = (Button) view.findViewById(R.id.continuesavepaybtn);
		if(fragTag == 2){
			btnSavePay.setVisibility(View.GONE);
		}

		btnSave.setOnClickListener(clickListener);
		btnSavePay.setOnClickListener(clickListener);

	}

	private boolean validatePaidBy() {
		boolean validate = true;
		transationAnount = amountedittext.getText().toString().trim();

		if (TextUtils.isEmpty(transationAnount)) {
			amountedittext.setError("Field is Empty");
			return false;
		}
		if (addedPersonList.size() < 2) {
			Toast.makeText(getActivity(), "Please add transation client",
					Toast.LENGTH_SHORT).show();
			return false;
		}

		return validate;
	}

	private boolean validateSplit() {
		boolean validate = true;
		transationAnount = amountedittext.getText().toString().trim();
		transationTitle = titleedittext.getText().toString().trim();

		boolean imageSelectBool = false;
		transationAnount = amountedittext.getText().toString().trim();
		transationTitle = titleedittext.getText().toString().trim();

		for (PhotosListModel photolistobj : horizontalPhotoAdapter
				.getAllPhotosList()) {
			if (photolistobj.isImageSelect()) {
				imageSelectBool = true;
			}
		}
		if (!imageSelectBool) {
			Toast.makeText(getActivity(), "Please select your friends",
					Toast.LENGTH_SHORT).show();
			return false;
		}

		if (TextUtils.isEmpty(transationAnount)) {
			amountedittext.setError("Field is Empty");
			return false;
		}
		if (TextUtils.isEmpty(transationTitle)) {
			titleedittext.setError("Field is Empty");
			return false;
		}
		if (addedPersonList.size() < 2) {
			Toast.makeText(getActivity(), "Please add transation client",
					Toast.LENGTH_SHORT).show();
			return false;
		}

		return validate;
	}

	private void setMultiAutoTextViewAdapter() {

		adapter = new FilteredArrayAdapter<Person>(getActivity(),
				R.layout.person_layout, personList) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				if (convertView == null) {

					LayoutInflater l = (LayoutInflater) getContext()
							.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
					convertView = l.inflate(R.layout.person_layout, parent,
							false);
				}

				Person p = getItem(position);
				((TextView) convertView.findViewById(R.id.personnametext))
						.setText(p.getName());

				if (p.getImage_location().length() > 0) {

					UrlImageViewHelper.setUrlDrawable(
							(CircularImageView) convertView
									.findViewById(R.id.personimageview), p
									.getImage_location());
				}

				return convertView;
			}

			@Override
			protected boolean keepObject(Person person, String mask) {
				mask = mask.toLowerCase();
				return person.getName().toLowerCase().startsWith(mask);
			}
		};
		searchView.setAdapter(adapter);
	}

	@Override
	public void onPaidByData(ArrayList<Person> output) {
		// TODO Auto-generated method stub
		addedPersonList = output;

		Toast.makeText(getActivity(), "PaidbySuccess", Toast.LENGTH_SHORT)
				.show();

		setPaidUserList();

	}

	private void setPaidUserList() {
		boolean paidbybool = false;
		StringBuilder sb = new StringBuilder();

		if (addedPersonList.size() > 0) {
			for (Person paidPersonobj : addedPersonList) {

				if (paidPersonobj.isPaidpersonbool()) {
					paidbybool = true;

					sb.append(paidPersonobj.getName());
					sb.append(" & ");
				}
			}
			if (!paidbybool) {
				paidby.setText(addedPersonList.get(0).getName());
				addedPersonList.get(0).setPaidpersonbool(true);
			} else {
				int sbindex = sb.lastIndexOf("&");
				sb.deleteCharAt(sbindex);
				String sbstr = (sb.toString().trim());
				paidby.setText(sbstr);
			}
		}
	}

	private void setReceiverList() {
		PhotosListModel photosListObj = null;

		Iterator<PhotosListModel> photoIterator = photosList.iterator();
		while (photoIterator.hasNext()) {

			photosListObj = photoIterator.next();
			if (!photosListObj.isImageSelect()) {
				photoIterator.remove();
			}
		}

	}
}
