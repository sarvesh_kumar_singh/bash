package com.bash.Fragments;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import org.json.JSONObject;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.bash.BuildConfig;
import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Adapters.FriendAdapter;
import com.bash.Adapters.NothingSelectedAdapter;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.GsonClasses.Friends_Class;
import com.bash.ListModels.BashUsers_Class;
import com.bash.ListModels.Change_Friend_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DataBaseManager;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynImageTaskManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.TwitterConnection.TwitterFragment;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.bash.Utils.MyLocation;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Feed;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnPublishListener;

public class AddDescription_Charge_Fragment extends Fragment implements AsyncResponse{
	View mRootView;
	TwitterFragment twitteraccess;
	MyAsynTaskManager  myAsyncTask;
	View camera_gallery, split_equal_view, split_manual_view;
	Dialog camaeraDialog, split_equal_dialog, split_manual_dialog;
	Uri mCapturedImageURI;
	public ImageView fileImage;
	public Friends_Class responseForFriends;
	public NothingSelectedAdapter nothingAdapter;
	public int splitamount=0, amount=0;
	public FriendAdapter adapter;
	public SelectionFriendsAdapter selectionAdapter;
	public AutoCompleteTextView friendsEditText;
	public ProgressDialog pd;
	public ArrayList<Change_Friend_Class> friendNameList = new ArrayList<Change_Friend_Class>();
	public ArrayList<Change_Friend_Class> selectedFriendNameList = new ArrayList<Change_Friend_Class>();
	
	public ArrayList<SelectedFriendsClass> selectedEqualList = new ArrayList<SelectedFriendsClass>();
	public ArrayList<SelectedFriendsClass> selectedManualist = new ArrayList<SelectedFriendsClass>();
	
	//public ArrayList<String> friendNameList = new ArrayList<String>();
	
	public ArrayList<BashUsers_Class> friendsList = new ArrayList<BashUsers_Class>();
	public String picturePath, locationPath="";
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_addtransaction_charge, null);
		//mRootView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		return mRootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initializeView();
		initializeAlertView();
		initializeChargeEqualAlertView();
		initializeChargeMaualAlertView();
		initializeExceedAlertView();
		((HomeActivity) getActivity()).currentFragment = this;
	}
	
	ListView splitEqualView, splitManualView; 
	
	public Dialog exceedAlertDialog;
	public View exceedView;
	
	private void initializeExceedAlertView() {
		
		exceedAlertDialog = new Dialog(getActivity());
		
		exceedAlertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		exceedView = View.inflate(getActivity(), R.layout.alert_addcard_delete, null);
		exceedAlertDialog.setContentView(exceedView);
		
		((Button) exceedView.findViewById(R.id.okButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				exceedAlertDialog.dismiss();
				ProcessPayment();
			}
		});
		
		((Button) exceedView.findViewById(R.id.cancelButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				exceedAlertDialog.dismiss();
				Toast.makeText(getActivity(), "Please Select Manual Type to Split Amount!", Toast.LENGTH_SHORT).show();
			}
		});
		
	}
	
	
	private void initializeChargeEqualAlertView() {

		split_equal_view = View.inflate(getActivity(),	R.layout.alert_chargefriend_split_window, null);
		split_equal_dialog = new Dialog(getActivity());
		split_equal_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		split_equal_dialog.setContentView(split_equal_view);
		
		((Button) split_equal_view.findViewById(R.id.okButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				split_equal_dialog.dismiss();
			}
		});
		
		((Button) split_equal_view.findViewById(R.id.cancelButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				split_equal_dialog.dismiss();
			}
		});
		
		((TextView) split_equal_view.findViewById(R.id.titleText)).setText("Split By Equal");
		
		splitEqualView = (ListView) split_equal_view.findViewById(R.id.splitedListView);
		
		split_equal_dialog.setCancelable(false);
		
   }
	
	private void initializeChargeMaualAlertView() {


		split_manual_view = View.inflate(getActivity(),	R.layout.alert_chargefriend_split_window, null);
		split_manual_dialog = new Dialog(getActivity());
		split_manual_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		split_manual_dialog.setContentView(split_manual_view);
		
		selectionAdapter = new SelectionFriendsAdapter();
		
		splitManualView = (ListView) split_manual_view.findViewById(R.id.splitedListView);
		
		splitManualView.setAdapter(selectionAdapter);
		
		((Button) split_manual_view.findViewById(R.id.okButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				int amt = 0;
				for (int i = 0; i < selectedManualist.size(); i++) {
					if(selectedManualist.get(i).getUserAmount() != null)
						amt += Integer.parseInt(selectedManualist.get(i).getUserAmount());
				}
				if(amt == amount)
					split_manual_dialog.dismiss();
				else
					Toast.makeText(getActivity(), "Total Amount Doesn't match to entered Amount!", Toast.LENGTH_SHORT).show();
			}
		});
		
		((Button) split_manual_view.findViewById(R.id.cancelButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				split_manual_dialog.dismiss();
				//((RadioButton) mRootView.findViewById(R.id.manualOption)).setChecked(false);
			}
		});
		
		((TextView) split_manual_view.findViewById(R.id.titleText)).setText("Split By Manual");
   }
	
	private void initializeAlertView() {

		camera_gallery = View.inflate(getActivity(),
				R.layout.alert_gallerycamera_mode_view, null);
		camaeraDialog = new Dialog(getActivity());
		camaeraDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		camaeraDialog.setContentView(camera_gallery);
 
		((Button) camera_gallery.findViewById(R.id.openGalleryBtn))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						camaeraDialog.dismiss();
						Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						getActivity().startActivityForResult(i,	AppConstants.CODE_GALLERY);
					}
		});

		((Button) camera_gallery.findViewById(R.id.openCameraBtn)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						camaeraDialog.dismiss();
						String fileName = "temp.jpg";
						ContentValues values = new ContentValues();
						values.put(MediaStore.Images.Media.TITLE, fileName);
						mCapturedImageURI = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
										values);
						Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
						intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
						getActivity().startActivityForResult(intent, AppConstants.CODE_CAMERA);
					}
		});
		
		pd = new ProgressDialog(getActivity());
		
		((ImageView) mRootView.findViewById(R.id.locationButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				pd.setMessage("Tracking Your Location...");
				pd.setCancelable(false);
				MyLocation locationPicker = new MyLocation();
				pd.show();
				locationPicker.getLocation(getActivity(), new MyLocation.LocationResult() {
					@Override
					public void gotLocation(final Location location) {
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								pd.dismiss();
								if(location == null){
									Toast.makeText(getActivity(), "Unable to Get Your Location! Try Again!", Toast.LENGTH_SHORT).show();
								}else{
									try {
										Geocoder geocoder;
										List<Address> addresses;
										geocoder = new Geocoder(getActivity(), Locale.getDefault());
										addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
										locationPath = addresses.get(0).getSubLocality()+", "+addresses.get(0).getLocality();
										((TextView)mRootView.findViewById(R.id.locationInfo)).setText(locationPath);
						 
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							
							
							}
						});
					}
				});
			}
		});
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case AppConstants.CODE_CAMERA:
			if (resultCode == getActivity().RESULT_OK) {
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(
						mCapturedImageURI, projection, null, null, null);
				int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				picturePath = cursor.getString(column_index_data);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,	fileImage, BashApplication.imageOptions);
				Log.e("Camera Path", picturePath);
			}
			break;
		case AppConstants.CODE_GALLERY:
			if (resultCode == getActivity().RESULT_OK) {

				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				picturePath = cursor.getString(columnIndex);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,	fileImage, BashApplication.imageOptions);
			}
			break;
		default:
			break;
		}
	}
	
	private void initializeView() {
		
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "Charge Friend", 0);
    	
		((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((BaseFragment)getParentFragment()).popFragment();
			}
		});
		
		fileImage = (ImageView) mRootView.findViewById(R.id.addPhotoButton);
		
		((ImageView) mRootView.findViewById(R.id.addPhotoButton)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						camaeraDialog.show();
					}
				});
		 
		((EditText) mRootView.findViewById(R.id.dateText)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getDatePickerDialog().show();
			}
		});
		
		friendsEditText = (AutoCompleteTextView) mRootView.findViewById(R.id.friendsEditText);
		
		friendsEditText.setThreshold(1);
		
	 	friendsEditText.setOnTouchListener(new View.OnTouchListener(){
			   @Override
			   public boolean onTouch(View v, MotionEvent event){
				   friendsEditText.showDropDown();
			      return false;
			   }
		});
		
		((RelativeLayout) mRootView.findViewById(R.id.paynowButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				if(ValidateForm()){
					
					if(((RadioButton) mRootView.findViewById(R.id.equalOption)).isChecked()){
						int amt = 0;
						for(int i = 0; i< selectedEqualList.size(); i++){
							Log.e("Id"+i, selectedEqualList.get(i).getUserId());
							Log.e("Amount"+i, selectedEqualList.get(i).getUserAmount());
							amt += Integer.parseInt(selectedEqualList.get(i).getUserAmount());
						}	
						if(amt > amount){
							((TextView)exceedView.findViewById(R.id.oldPinText)).setText(
									"Total Amount is "+AppConstants.RASYMBOL+" "+amount+"\n"+
									"Excess Amount is "+AppConstants.RASYMBOL+" "+(amt - amount));
							exceedAlertDialog.show();
						}else if(amt  < amount){
							((TextView)exceedView.findViewById(R.id.oldPinText)).setText(
									"Total Amount is "+AppConstants.RASYMBOL+" "+amount+"\n"+
									"Excess Amount is "+AppConstants.RASYMBOL+" "+(amount - amt));
							exceedAlertDialog.show();
						}
						else{
							ProcessPayment();
						}
						
					}
					if(((RadioButton) mRootView.findViewById(R.id.manualOption)).isChecked()){
						for(int i = 0; i< selectedManualist.size(); i++){
							Log.e("Id"+i, selectedManualist.get(i).getUserId());
							Log.e("Amount"+i, selectedManualist.get(i).getUserAmount());
						}
						
						ProcessPayment();
					}
					//ProcessPayment();
				}
			}
		});
		
		((RadioGroup) mRootView.findViewById(R.id.splitTypeGroup)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if(isFriendNotSelected()) {
					Toast.makeText(getActivity(), "Please Select Any One Friend!", Toast.LENGTH_SHORT).show();
				}
				else{
					if(((EditText) mRootView.findViewById(R.id.amountText)).getText() == null ||
							((EditText) mRootView.findViewById(R.id.amountText)).getText().toString().length() == 0){
						Toast.makeText(getActivity(), "Please Enter the Amount!", Toast.LENGTH_SHORT).show();
					}else{
						
						amount = Integer.parseInt(((EditText) mRootView.findViewById(R.id.amountText)).getText().toString());
						splitamount = Math.round(amount/(adapter.getSelectionList().size()+1));
						Log.e("Split Amount Text", splitamount+"");
						
						switch (checkedId) {
						case R.id.manualOption:
							setSelectionList();
							((TextView) split_manual_view.findViewById(R.id.totalAmtText)).setText("Total : Rs. "+amount);						
							split_manual_dialog.show();
							break;
						case R.id.equalOption:
							selectedFriendNameList.clear();
							selectedFriendNameList = adapter.getSelectionList();
							splitamount = Math.round(amount/(selectedFriendNameList.size()+1));
							selectedEqualList.clear();
							for(int i =0; i < selectedFriendNameList.size(); i++){
								selectedEqualList.add(new SelectedFriendsClass(selectedFriendNameList.get(i).getFriendName(),
										selectedFriendNameList.get(i).getFriendImagePath(), 
										splitamount+"", 
										selectedFriendNameList.get(i).getFriendId()));
							}
							
							selectedEqualList.add(new SelectedFriendsClass(PreferenceManager.getInstance().getUserFullName(), 
									PreferenceManager.getInstance().getUserImagePath(), splitamount+"", PreferenceManager.getInstance().getUserId()));
							break;
						default:
							break;
						}
					}
					
				}
			}
		});
		
/*
		if(selectedEqualList.size() == 0){
			selectedFriendNameList = adapter.getSelectionList();
			
			splitamount = Math.round(amount/(selectedFriendNameList.size()+1));
			
			for(int i =0; i < selectedFriendNameList.size(); i++){
				selectedEqualList.add(new SelectedFriendsClass(selectedFriendNameList.get(i).getFriendName(),
						selectedFriendNameList.get(i).getFriendImagePath(), 
						splitamount+"", 
						selectedFriendNameList.get(i).getFriendId()));
			}
			
			selectedEqualList.add(new SelectedFriendsClass(PreferenceManager.getInstance().getUserFirstName(), 
					PreferenceManager.getInstance().getUserImagePath(), splitamount+"", PreferenceManager.getInstance().getUserId()));
		}*/
		
		((ImageView) mRootView.findViewById(R.id.facebookButton)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(ValidateForm()){
							if (SimpleFacebook.getInstance().isLogin()) {
								callFbPost();
							} else {
								SimpleFacebook.getInstance().login(onLoginListener);
							}	
						}
					}
				});

		twitteraccess = new TwitterFragment(getActivity());
		
		((ImageView) mRootView.findViewById(R.id.twitterButton)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						twitteraccess.updateToTwitt(((EditText) mRootView
								.findViewById(R.id.feedCommentText)).getText()
								.toString());
					}
				});

		

		getFriendsList();
	}
	
	
	private void callFbPost() {

		// TODO Auto-generated method stub
		Log.e("CAlling Fb Post", "....");
		final Feed feed = new Feed.Builder()
				.setName(((EditText) mRootView.findViewById(R.id.amountText)).getText().toString())
				.setCaption("Amount Spent "+ ((EditText) mRootView.findViewById(R.id.amountText)).getText().toString())
				.setDescription(((EditText) mRootView.findViewById(R.id.feedCommentText)).getText().toString())
				
				 .setPicture("https://www.facebook.com/images/fb_icon_325x325.png")
				 
				 .setLink("http://www.bash.co/")
				 
				 .build();
		
	/*	final Feed feed = new Feed.Builder()
		.setName("test").setCaption("Amount Spent ").setDescription("test")
		.build();*/
		
	 	SimpleFacebook.getInstance().publish(feed, true,
				new OnPublishListener() {
					@Override
					public void onException(Throwable throwable) {
						DialogManager.showDialog(getActivity(),
								"Error Occured to Share Message! Try Again!");
					}

					@Override
					public void onFail(String reason) {
						DialogManager.showDialog(getActivity(),
								"Error Occured to Share Message! Try Again! Reason : "+reason);
					}

					@Override
					public void onComplete(String response) {
						DialogManager.showDialog(getActivity(),
								"Message Wrote of FB Wall Sucessfully!");
					}
				});

	}

	final OnLoginListener onLoginListener = new OnLoginListener() {

		@Override
		public void onFail(String reason) {
			Log.e("Login Failed", "....");
		}

		@Override
		public void onException(Throwable throwable) {
			Log.e("Login Failed", "....");
		}

		@Override
		public void onThinking() {
			// show progress bar or something to the user while login is
			Log.e("Login Failed", "....");
		}

		@Override
		public void onLogin() {
			// change the state of the button or do whatever you want
			Log.e("Login Success", "....");
			callFbPost();
		}

		@Override
		public void onNotAcceptingPermissions(Permission.Type type) {
			// toast(String.format("You didn't accept %s permissions",
			// type.name()));
			Log.e("Login Failed", "....");
		}
	};

	public boolean ValidateForm() {

		try {
			if(isFriendNotSelected()){
				Toast.makeText(getActivity(), "Please Select Any One Friend!", Toast.LENGTH_SHORT).show();
				return false;
			}else if(((EditText) mRootView.findViewById(R.id.amountText)).getText() == null || 
					((EditText) mRootView.findViewById(R.id.amountText)).getText().toString().length() == 0){
				Toast.makeText(getActivity(), "Please Enter Amout!", Toast.LENGTH_SHORT).show();
				return false;
			}else if(((EditText) mRootView.findViewById(R.id.dateText)).getText() == null || 
					((EditText) mRootView.findViewById(R.id.dateText)).getText().toString().length() == 0){
				Toast.makeText(getActivity(), "Please Select Date!", Toast.LENGTH_SHORT).show();
				return false;
			}else if(((EditText) mRootView.findViewById(R.id.feedCommentText)).getText() == null || 
					((EditText) mRootView.findViewById(R.id.feedCommentText)).getText().toString().length() == 0){
				Toast.makeText(getActivity(), "What's your payment for? Please enter details!", Toast.LENGTH_SHORT).show();
				return false;
			}else{
				if(((RadioButton) mRootView.findViewById(R.id.manualOption)).isChecked()){
					int amt = 0;
					for (int i = 0; i < selectedManualist.size(); i++) {
						if(selectedManualist.get(i).getUserAmount() != null)
							amt += Integer.parseInt(selectedManualist.get(i).getUserAmount());
					}
					if(amt == amount){
						split_manual_dialog.dismiss();
					}else{
						Toast.makeText(getActivity(), "Total Amount Doesn't match to entered Amount in Equal Split!", Toast.LENGTH_SHORT).show();
						return false;
					}
				}else if(!((RadioButton) mRootView.findViewById(R.id.equalOption)).isChecked()){
					Toast.makeText(getActivity(), "Please Select Manual or Equal Type!", Toast.LENGTH_SHORT).show();
				}
					
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Toast.makeText(getActivity(), "Please Select Friend Name!", Toast.LENGTH_SHORT).show();
			return false;
		}
	
		return true;
	}
	
	public boolean isFriendNotSelected() {
		
		if(adapter != null && adapter.getSelectionList().size() == 0)
			return true;
		else {
			return false;
		}
	}
	
	
	public void setSelectionList() {
		selectedFriendNameList.clear();
		selectedFriendNameList = adapter.getSelectionList();
		selectedManualist.clear();
		splitamount = Math.round(amount/(selectedFriendNameList.size()+1));
			Log.e("split amt", splitamount+"");
			for(int i = 0; i < selectedFriendNameList.size(); i++){
				selectedManualist.add(new SelectedFriendsClass(selectedFriendNameList.get(i).getFriendName(),
						selectedFriendNameList.get(i).getFriendImagePath(), splitamount+"", selectedFriendNameList.get(i).getFriendId()));
			}
			selectedManualist.add(new SelectedFriendsClass(PreferenceManager.getInstance().getUserFullName(), 
					PreferenceManager.getInstance().getUserImagePath(), splitamount+"", PreferenceManager.getInstance().getUserId()));
			selectionAdapter.notifyDataSetChanged();
	}
	
	Calendar cal = Calendar.getInstance();
	 
	public DatePickerDialog getDatePickerDialog() {
			DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
				 
	            @Override
	            public void onDateSet(DatePicker view, int year,
	                    int monthOfYear, int dayOfMonth) {
	            	((EditText) mRootView.findViewById(R.id.dateText)).setText(dayOfMonth + "/"
	                        + (monthOfYear + 1) + "/" + year);
	            }
	        }, cal.get(Calendar.YEAR),  cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			
		 return datePickerDialog;
	 }

	public void ProcessPayment() {
		 
		String paid_for = "", amount = "";
		
		if(((RadioButton) mRootView.findViewById(R.id.equalOption)).isChecked()){
			for(int i = 0; i< selectedEqualList.size()-1; i++){
				if(i == selectedEqualList.size()-2){
					paid_for += selectedEqualList.get(i).getUserId();
					amount += selectedEqualList.get(i).getUserAmount();
				}else{
					paid_for += selectedEqualList.get(i).getUserId()+",";
					amount += selectedEqualList.get(i).getUserAmount()+",";
				}
			}	
		}
		
		else if(((RadioButton) mRootView.findViewById(R.id.manualOption)).isChecked()){
			for(int i = 0; i< selectedManualist.size()-1; i++){
				if(i == selectedManualist.size()-2){
					paid_for += selectedManualist.get(i).getUserId();
					amount += selectedManualist.get(i).getUserAmount();
				}else{
					paid_for += selectedManualist.get(i).getUserId()+",";
					amount += selectedManualist.get(i).getUserAmount()+",";
				}
			}	
		}
		
		Log.e("my user id", PreferenceManager.getInstance().getUserId());
		Log.e("paid for Text", paid_for);
		Log.e("amount for Text", amount);
		 
			MyAsynImageTaskManager.setupParamsAndUrl(AppUrlList.ACTION_URL, new String[] {
					"module",
					"action",
					"paid_by",
					"paid_for",
					"amount",
					"feed_content",
					"location",
					"feedimage",
					"date"
					}, 
					new String[]{
					"feed",
					"addcharge",
					PreferenceManager.getInstance().getUserId(),
					paid_for,
					amount,
					((EditText)mRootView.findViewById(R.id.feedCommentText)).getText().toString(),
					locationPath,
					picturePath,
					((EditText)mRootView.findViewById(R.id.dateText)).getText().toString()
					}, 
					new String[]{
					"0", "0", "0", "0", "0", "0", "0", "1", "0" 
					});

			new MyAsynImageTaskManager(getActivity(), new MyAsynImageTaskManager.LoadListener() {
				@Override
				public void onLoadComplete(final String jsonResponse) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							try {
							if(BuildConfig.DEBUG)
									Log.e("Json Response", jsonResponse);
							JSONObject rootObj = new JSONObject(jsonResponse);
							if(rootObj.getBoolean("result")){
									((BaseFragment)getParentFragment()).popFragment();
									//DialogManager.showDialog(getActivity(), "New Transaction Created Successfully!");
									Toast.makeText(getActivity(), "New Transaction Created Successfully!", Toast.LENGTH_SHORT).show();
									//AddTransactionFragment.shouldSkipThisBill = true;
									((BaseFragment)getParentFragment()).popFragment();
									
							}else{
								DialogManager.showDialog(getActivity(), "Error To Charge Friend! Try Again!");
							}
							} catch (Exception e) {
								// TODO: handle exception
								DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
							}
					}
					});
				}
				@Override
				public void onError(final String errorMessage)  {
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							DialogManager.showDialog(getActivity(), errorMessage);		
						}
					});
					
				}
			}).execute();
		}
	 
	
	public void getFriendsList() {

		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("getFriendsList",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser"
				}, 
				new String[]{
				"group",
				"friendlist",
				PreferenceManager.getInstance().getUserId()
				});

		myAsyncTask.execute();
	}

	
	class SelectedFriendsClass {
		String userName, userImagepath, userAmount, userId;

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getUserImagepath() {
			return userImagepath;
		}

		public void setUserImagepath(String userImagepath) {
			this.userImagepath = userImagepath;
		}

		public String getUserAmount() {
			return userAmount;
		}

		public void setUserAmount(String userAmount) {
			this.userAmount = userAmount;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public SelectedFriendsClass(String userName, String userImagepath,
				String userAmount, String userId) {
			super();
			this.userName = userName;
			this.userImagepath = userImagepath;
			this.userAmount = userAmount;
			this.userId = userId;
		}

		 
		
	}
	
	
	class SelectionFriendsAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return selectedManualist.size();
		}

		@Override
		public SelectedFriendsClass getItem(int position) {
			// TODO Auto-generated method stub
			return selectedManualist.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

				ViewHolder holder = null;
			
				final SelectedFriendsClass listItem = getItem(position);
				
		        if (convertView == null) {
		        	LayoutInflater inflater = LayoutInflater.from(getActivity());
		        	convertView = inflater.inflate(R.layout.custom_chargefriend_split_listview, null);
		            holder = new ViewHolder();
		            holder.amountEditText = (EditText) convertView.findViewById(R.id.amountEditText);
		            holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
		            holder.userImage = (ImageView) convertView.findViewById(R.id.userImage);
		            convertView.setTag(holder);
		        } else {
		            holder = (ViewHolder) convertView.getTag();
		        }
		        
		        holder.friendName.setText(listItem.getUserName());
	            
	            final EditText amtText = holder.amountEditText;
	            
	            holder.amountEditText.addTextChangedListener(new TextWatcher() {
					@Override
					public void onTextChanged(CharSequence s, int start, int before, int count) {
						
					}
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count,
							int after) {
						// TODO Auto-generated method stub
					}
					@Override
					public void afterTextChanged(Editable s) {
						if(amtText.getText() == null || amtText.getText().toString().length() == 0)
							selectedManualist.get(position).setUserAmount("0");
						else
							selectedManualist.get(position).setUserAmount(amtText.getText().toString());
					}
				});
	            	  
	            holder.amountEditText.setOnEditorActionListener(new OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
						if(amtText.getText() == null || amtText.getText().toString().length() == 0)
							selectedManualist.get(position).setUserAmount("0");
						else
							selectedManualist.get(position).setUserAmount(amtText.getText().toString());
						Log.e("Purrrr....", amtText.getText().toString());
						return false;
					}
				});
	            
	            if(listItem.getUserImagepath() != null && listItem.getUserImagepath().length() != 0)
	            		ImageLoader.getInstance().displayImage(listItem.getUserImagepath(), holder.userImage);
	            
	            if(selectedManualist.get(position).getUserAmount() != null 
	            		&& selectedManualist.get(position).getUserAmount().length() != 0){
	            	amtText.setText(selectedManualist.get(position).getUserAmount());
	            	Log.e("Amount", selectedManualist.get(position).getUserAmount());
	            }else{
	            	amtText.setText(String.valueOf(splitamount));
	            }
	            	
	            
	        	//amtText.setText(String.valueOf(Math.round(splitamount/2)));
	        	
	       		
	     
	            return convertView;
	         }
	    		
	  		  private class ViewHolder {
	  		    	EditText amountEditText;
	  		    	TextView friendName;
	  		    	ImageView userImage;
	  		     }
	  	}


	@Override
	public void backgroundProcessFinish(String from, String output) {
		if(from.equalsIgnoreCase("getFriendsList"))
				{
					if(output!=null)
					{

						//ArrayList<BashUsers_Class> newList = new ArrayList<BashUsers_Class>();
						
						try {
						if(BuildConfig.DEBUG)
								Log.e("Json Response", output);
						Gson gson = new Gson();
						
						responseForFriends = gson.fromJson(output, Friends_Class.class);
							
						if(responseForFriends.result){
							
							DataBaseManager.getInstance().storeFriendsList(responseForFriends.friendlist);
							//updateAdapter();
							//adapter.notifyDataSetChanged();
							
							friendNameList.clear();
							friendsList.clear();
							
							for(int i =0; i<responseForFriends.friendlist.size(); i++){
								
								/*Log.e("Friend Id", responseForFriends.friendlist.get(i).idfriend);
								Log.e("Friend Id", responseForFriends.friendlist.get(i).phone_no);
								Log.e("Friend Id", responseForFriends.friendlist.get(i).name);
								Log.e("Friend Id", responseForFriends.friendlist.get(i).imagepath);*/
								
								/*String idfriend, String phone_no, String name, String imagepath, String is_send_request, 
								String is_request_received, String is_friend)*/
								
								friendsList.add(new BashUsers_Class
										(responseForFriends.friendlist.get(i).idfriend, 
											responseForFriends.friendlist.get(i).phone_no,
											responseForFriends.friendlist.get(i).name, 
											responseForFriends.friendlist.get(i).imagepath,
											"FALSE", 
											"FALSE", 
											"TRUE"));
								
								friendNameList.add(new Change_Friend_Class(responseForFriends.friendlist.get(i).name,
										responseForFriends.friendlist.get(i).idfriend, responseForFriends.friendlist.get(i).imagepath, false));
								
								Log.e("******************", responseForFriends.friendlist.get(i).name);
								Log.e("Friend Name", "");
								Log.e("******************", "");
							}
							
							Log.e("Size of Name List", friendNameList.size()+"");
							
		 					adapter = new FriendAdapter(getActivity(), R.layout.a_custom_spinner_for_friend, friendNameList,
		 									((ImageView)mRootView.findViewById(R.id.viewlistButton)), 
		 									((TextView)mRootView.findViewById(R.id.infoText))
		 									);
		 					
		 					friendsEditText.setAdapter(adapter);
		 					
						}else{
							//DialogManager.showDialog(getActivity(), "No Friends Found!");
							Toast.makeText(getActivity(), "No Friends Found!", Toast.LENGTH_SHORT).show();
							//AddTransactionFragment.shouldSkipThisBill = true;
							((BaseFragment)getParentFragment()).popFragment();
						}
						} catch (Exception e) {
							// TODO: handle exception
							DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
						}
					
					}
					else
					{
						// TODO: handle exception
						DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
					}
				}
		
	}
	
	
}

