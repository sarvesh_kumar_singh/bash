package com.bash.Fragments;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.ListModels.BashUsers_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;
import com.nostra13.universalimageloader.core.ImageLoader;

@SuppressLint("ValidFragment")
public class AddFriendsOnGroupFragment extends Fragment implements AsyncResponse{
	MyAsynTaskManager  myAsyncTask;
	View mRootView, addgroupAlertView;
	public static ListView myFriendsListview;
	Dialog addmemberDialog;
	AddFriendsOnGroupAdapter adapter;
	String groupName = "Group";
	ArrayList<BashUsers_Class> feedList = new ArrayList<BashUsers_Class>();
	public static String groupId;
	public static BaseFragment parentFragment; 
	
	public AddFriendsOnGroupFragment(String groupId){
		this.groupId = groupId;
		Log.e("Group Id", groupId);
	}
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_addfriendstogroup_page, null);
		initializeView();
		return mRootView;
	}
	
	private void initializeView() {
		// TODO Auto-generated method stub
		 ((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "Add Friends", 0);
		 
		 ((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					((BaseFragment)getParentFragment()).popFragment();
				}
		 });
		 
		// initializeAlertView();
		 parentFragment = ((BaseFragment)getParentFragment());
		 
		 ((ImageView)getActivity().findViewById(R.id.toprightsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//((BaseFragment)getParentFragment()).replaceFragment(new AddFriendsOnGroupFragment(), true);
				}
		 });
		 
		 /*((RelativeLayout)mRootView.findViewById(R.id.doneButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((BaseFragment)getParentFragment()).popFragment();
			}
		 });*/
		 ((RelativeLayout)mRootView.findViewById(R.id.cancelButton)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					((BaseFragment)getParentFragment()).popFragment();
				}
		});	
		 
		myFriendsListview = (ListView) mRootView.findViewById(R.id.friendsList);
		
		adapter = new AddFriendsOnGroupAdapter(getActivity(), feedList, 
				((RelativeLayout) mRootView.findViewById(R.id.doneButton)));
		
		myFriendsListview.setAdapter(adapter);
		
		/*feedList = DataBaseManager.getInstance().getFriendsList();
		
		if(feedList != null){
			adapter.notifyWithDataSet(feedList);
			//adapter.notifyDataSetChanged();
			Log.e("Adapter Notified", "");
		}*/
		
		 // Capture Text in EditText
	       ((EditText) mRootView.findViewById(R.id.searchBox)).addTextChangedListener(new TextWatcher() {

	           @Override
	           public void afterTextChanged(Editable arg0) {
	               // TODO Auto-generated method stub
	               
	           }

	           @Override
	           public void beforeTextChanged(CharSequence arg0, int arg1,
	                   int arg2, int arg3) {
	               // TODO Auto-generated method stub
	           }

	           @Override
	           public void onTextChanged(CharSequence arg0, int arg1, int arg2,
	                   int arg3) {
	        	   if(arg3 < arg2) {
	        		   adapter.resetDatas();
					 }
	        	   adapter.getFilter().filter(arg0.toString());
	           }
	       });
	       
		getFriendsinGroupDetails();
			
	}

	private void initializeAlertView() {
		
		addmemberDialog = new Dialog(getActivity());
		addmemberDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		addgroupAlertView = View.inflate(getActivity(), R.layout.alert_addgroup_dialog, null);
		addmemberDialog.setContentView(addgroupAlertView);
	
		 ((ImageView) addgroupAlertView.findViewById(R.id.cancelDialog)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					addmemberDialog.dismiss();
				}
		 });
		 

	}

	 
	 public void getFriendsinGroupDetails() {

		 Log.e("Group Id", groupId);
		 myAsyncTask=new MyAsynTaskManager();
			myAsyncTask.delegate=this;
			myAsyncTask.setupParamsAndUrl("getFriendsinGroupDetails",getActivity(),AppUrlList.ACTION_URL, new String[] {
					"module",
					"action",
					"idgroup",
					"iduser"
					}, 
					new String[]{
					"group",
					"friendsoutofgroup",
					groupId,
					PreferenceManager.getInstance().getUserId()
					});
			myAsyncTask.execute();
			
		}
	 public class AddFriendsOnGroupAdapter extends BaseAdapter
	 {
	 	 	private ArrayList<BashUsers_Class>  feedList = new ArrayList<BashUsers_Class>();
	 	 	public ArrayList<BashUsers_Class> originalList = new ArrayList<BashUsers_Class>();
	 		private Activity context;
	 	    private LinearLayout.LayoutParams backViewParams;
	 	    SparseArray<String> selectionList = new SparseArray<String>();
	 	    String selectionString = "";
	 	    public FriendFilter filter;
	 	    
	 	    public AddFriendsOnGroupAdapter(Activity context, ArrayList<BashUsers_Class> feedList, RelativeLayout actionDone) {
	 	        this.context = context;
	 	    	this.feedList = feedList;
	 	    	this.originalList = feedList;
	 	    	
	 	    	actionDone.setOnClickListener(new OnClickListener() {
	 				@Override
	 				public void onClick(View v) {
	 					selectionList.clear();
	 					int index = 0;
	 				 
	 					for(BashUsers_Class item : originalList){
	 						if(item.getIsSelected()){
	 							selectionList.put(index, item.getisFriend());
	 							index++;
	 						}
	 					}
	 					
	 					if(selectionList.size() == 0){
	 						AddFriendsOnGroupFragment.parentFragment.popFragment();
	 					}else{
	 						for(int i = 0; i < selectionList.size(); i++) {
	 								if(selectionList.size() == i+1)
	 									//selectionString += selectionList.keyAt(i);
	 									selectionString += getItem(selectionList.keyAt(i)).getisFriend();
	 								else
	 									selectionString += getItem(selectionList.keyAt(i)).getisFriend()+",";
	 									//selectionString += selectionList.keyAt(i)+",";
	 							}
	 						
	 						//addFriendsInGroup();
	 						Log.e("Selection List", selectionString);
	 						addFriendsInGroup();
	 					}
	 				}
	 			});
	 	     }

	 	    public class selectionClass {
	 	    	int position;
	 	    	String selectionName;
	 	    }
	 	    
	 	    @Override
	 	    public View getView(final int position, View convertView, ViewGroup parent) {
	 	        ViewHolder holder = null;
	 	        final BashUsers_Class listItem = getItem(position);
	 	        if (convertView == null) {
	 	        	LayoutInflater inflater = LayoutInflater.from(context);
	 	        	convertView = inflater.inflate(R.layout.custom_addfriendsongroup_listview, null);
	 	            holder = new ViewHolder();
	 	            holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
	 	            holder.mobileNumberText = (TextView) convertView.findViewById(R.id.mobileNumberText);
	 	            holder.userImage = (ImageView) convertView.findViewById(R.id.userImage);
	 	            holder.isSelected = (CheckBox) convertView.findViewById(R.id.isSelected);
	 	            convertView.setTag(holder);
	 	        } else {
	 	            holder = (ViewHolder) convertView.getTag();
	 	        }
	 	        
	 	        if(listItem.getimagepath() != null && listItem.getimagepath().length() != 0){
	 	        	ImageLoader.getInstance().displayImage(listItem.getimagepath(), 
	 	        			holder.userImage, BashApplication.options, BashApplication.animateFirstListener);
	 	        }
	 	        	//ImageLoader.getInstance().displayImage(listItem.getimagepath(), holder.userImage);
	 	        
	 	        //ImageLoader.getInstance().displayImage(listItem.getimagepath(), holder.friendImageSource);
	 	        
	 	        holder.friendName.setText(listItem.getname());
	 	        holder.mobileNumberText.setText(listItem.getphone_no());
	 	        
	 	        holder.isSelected.setOnCheckedChangeListener(new OnCheckedChangeListener() {
	 				@Override
	 				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	 					// TODO Auto-generated method stub
	 					if(isChecked)
	 						getItem(position).setIsSelected(true);
	 						//selectionList.put(position, listItem.getisFriend());
	 					else
	 						getItem(position).setIsSelected(false);
	 						/*originalList.get(position).setIsSelected(true);
	 						selectionList.remove(position);*/
	 				}
	 			});
	 	        
	 	        
	 	        if(listItem.getIsSelected())
	 	        	holder.isSelected.setChecked(true);
	 	        else
	 	        	holder.isSelected.setChecked(false);
	 	        
	 	        return convertView;
	 	    }

	 	    public void notifyWithDataSet(ArrayList<BashUsers_Class> newlist){

	 	    	this.feedList.clear();
	 	    	this.originalList = newlist;
	 	    	this.feedList = this.originalList;
	 	    	this.notifyDataSetChanged();
	 	    }
	 	    
	 	    public void removeItemFromList(int position)
	 		{
	 			this.feedList.remove(position);
	 			this.notifyDataSetInvalidated();
	 			this.notifyDataSetChanged();
	 	    }
	 	    
	 	    @Override
	 	    public int getCount() {
	 	        return feedList.size();
	 	    }

	 	    @Override
	 	    public BashUsers_Class getItem(int position) {
	 	        return feedList.get(position);
	 	    }

	 	    @Override
	 	    public long getItemId(int position) {
	 	        return position;
	 	    }

	 	    private class ViewHolder {
	 	        TextView friendName, mobileNumberText;
	 	        ImageView userImage;
	 	        CheckBox isSelected;
	 	        
	 	    }
	 	    
	 	    
	 	    public void resetDatas(){
	 	    	feedList = originalList;
	 	    	notifyDataSetChanged();
	 	    }
	 	    
	 	    
	 		public Filter getFilter() {
	 	  			// TODO Auto-generated method stub
	 	  			  if (filter == null)
	 	  				  filter = new FriendFilter();
	 	  			    return filter;
	 	    }
	 	    
	 		//filter Class... 
	 		private class FriendFilter extends Filter {
	 			@Override
	 			protected FilterResults performFiltering(CharSequence constraint) {
	 				FilterResults results = new FilterResults();
	 				// We implement here the filter logic
	 				if (constraint == null || constraint.length() == 0) {
	 					// No filter implemented we return all the list
	 					results.values = originalList;
	 					results.count = originalList.size();
	 				} else {
	 					// We perform filtering operation
	 					List<BashUsers_Class> tempList = new ArrayList<BashUsers_Class>();
	 					for (BashUsers_Class p : originalList) {
	 						if (p.getname().toUpperCase()
	 								.startsWith(constraint.toString().toUpperCase()))
	 							tempList.add(p);
	 					}
	 					results.values = tempList;
	 					results.count = tempList.size();
	 				}
	 				return results;
	 			}

	 			@Override
	 			protected void publishResults(CharSequence constraint,
	 					FilterResults results) {
	 				// Now we have to inform the adapter about the new list filtered
	 				if (results.count == 0){
	 					feedList = (ArrayList<BashUsers_Class>) results.values;
	 					notifyDataSetInvalidated();
	 					}
	 				else {
	 					feedList = (ArrayList<BashUsers_Class>) results.values;
	 					notifyDataSetChanged();
	 				}
	 				 
	 			}
	 		}
	     
	 		public void notifyDataSet(){
	 			this.notifyDataSetChanged();
	 		}
	 		
	 	    public void addFriendsInGroup() {
	 	    	myAsyncTask=new MyAsynTaskManager();
	 			myAsyncTask.delegate=(AsyncResponse) this;
	 	    	 	myAsyncTask.setupParamsAndUrl("addFriendsInGroup",getActivity(),AppUrlList.ACTION_URL, new String[] {
	 						"module",
	 						"action",
	 						"idgroup",
	 						"friends"
	 						}, 
	 						new String[]{
	 						"group",
	 						"addfriends",
	 						AddFriendsOnGroupFragment.groupId,
	 						selectionString
	 						});
myAsyncTask.execute();

	 			}
	 }

	@Override
	public void backgroundProcessFinish(String from, String output) {
		 if(from.equalsIgnoreCase("getFriendsinGroupDetails"))
		 {
			 if(output!=null)
			 {

				 try {
					 JSONObject rootObj = new JSONObject(output);
					 if(rootObj.getBoolean("result")) {
						 JSONArray friendsList = rootObj.getJSONArray("friendlist");
						 JSONObject item;
						 ArrayList<BashUsers_Class> newList = new ArrayList<BashUsers_Class>();
						 for(int i = 0;i < friendsList.length(); i++) {
							 item = friendsList.getJSONObject(i);
							 newList.add(new BashUsers_Class(item.getString("idfriend"),
									 item.getString("phone_no"),
									 item.getString("name"),
									 item.getString("imagepath"),
									 "0", 
									 "0",
									 "0"));
						 }
						 feedList = newList;
						 adapter.notifyWithDataSet(newList);
						 //adapter.notifyDataSetChanged();
					 }
					 else{ 
						 DialogManager.showDialog(getActivity(), "No Friends Found in Group!");
					 }
				 } catch (Exception e) {
					 // TODO: handle exception
					 e.printStackTrace();
					 DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
				 }


			 }
			 else
			 {
				 DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!"); 
			 }
		 }
		 else if(from.equalsIgnoreCase("addFriendsInGroup"))
		 {
			 if(output!=null)
			 {

					try {
						JSONObject rootObj = new JSONObject(output);
						if(rootObj.getBoolean("result")) { 
							AddFriendsOnGroupFragment.parentFragment.popFragment();
							Toast.makeText(getActivity(), "Friends Added Successfully!", Toast.LENGTH_SHORT).show();
						}
					else{ 
						DialogManager.showDialog(getActivity(), "Failed to Add Friends in Group! Try Again!");
					}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
						DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
					}
			
			 }
			 else
			 {
				 DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!"); 
				 }
		 }
		
	}
	
}
