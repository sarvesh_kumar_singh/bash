package com.bash.Fragments;
import org.json.JSONObject;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;

public class MyBash_Fragment extends Fragment implements AsyncResponse{
	boolean ischeck=false;
	MyAsynTaskManager  myAsyncTask;
	View mRootView, setupPinView, changePinView;
	Dialog setupPinDialog, changePinDialog;
	public static BaseFragment parentFragmentClass;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) 
	{
		mRootView = inflater.inflate(R.layout.fragment_my_settings, null);
		//mRootView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		return mRootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initializeView();
		initializeWebAlertView();
		initializeSMSAlertView();
	}
	
	private void initializeView() {

		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn, "My Bash", 0);
		((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Sarvesh
				//((HomeActivity)getActivity()).menu.toggle();
				((HomeActivity)getActivity()).slidingmenu_layout.toggleMenu();
			}
		});
		
		setupPinDialog = new Dialog(getActivity());
		changePinDialog = new Dialog(getActivity());
		
		initializeAlertViews();
		
		parentFragmentClass = ((BaseFragment)getParentFragment());
		
		((RelativeLayout)getView().findViewById(R.id.profileLayout)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((BaseFragment)getParentFragment()).replaceFragment(new EditProfileFragment(), true);
			}
		});
		
		((RelativeLayout)getView().findViewById(R.id.setupPinLayout)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(PreferenceManager.getInstance().getPinText() != null)
					changePinDialog.show();
				else
					setupPinDialog.show();
			}
		});
		
		((CheckBox)getView().findViewById(R.id.notificationCheckButton)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(mayICheckNotification){
					if(isChecked){
						ischeck=true;
						setNotificationStatus(true);
					}else{
						ischeck=false;
						setNotificationStatus(false);
					}
				}else{
					mayICheckNotification = true;
				}
			}
		});
		mayICheckNotification = false;
		if(PreferenceManager.getInstance().getUserNotificationSettings() != null && PreferenceManager.getInstance().getUserNotificationSettings().equals("1"))
			((CheckBox)getView().findViewById(R.id.notificationCheckButton)).setChecked(true);
		else
			((CheckBox)getView().findViewById(R.id.notificationCheckButton)).setChecked(false);
			
//		((RelativeLayout)getView().findViewById(R.id.changeZipPin)).setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				changeBashPinProcess();
//			}
//		});
		
	}

boolean mayICheckNotification = false;

View pin_webview;
Dialog pinWebDialog;

WebView alertWebView;


private void initializeWebAlertView(){
	
	pin_webview = View.inflate(getActivity(),
			R.layout.alert_showpinalert_dialog, null);
	pinWebDialog = new Dialog(getActivity());
	pinWebDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	pinWebDialog.setContentView(pin_webview);

	alertWebView = ((WebView) pin_webview.findViewById(R.id.webView));
	
	WebSettings settings = alertWebView.getSettings();
	
    settings.setJavaScriptEnabled(true);
    
    alertWebView.setHorizontalScrollBarEnabled(true);
 
}


View sendsmswindow;
Dialog sendSmsDialog;

private void initializeSMSAlertView(){
	
	sendsmswindow = View.inflate(getActivity(),
			R.layout.alert_changewalltepin_dialog, null);
	sendSmsDialog = new Dialog(getActivity());
	sendSmsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	sendSmsDialog.setContentView(sendsmswindow);
	
	
	((Button) sendsmswindow.findViewById(R.id.sendSMSbutton)).setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			checkSIMAvailabilityAndSendSMS();
		}
	});
	
	((Button) sendsmswindow.findViewById(R.id.cancelButton)).setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			sendSmsDialog.dismiss();
		}
	});
 
//	((TextView)getView().findViewById(R.id.sendSMStextView)).setOnClickListener(new OnClickListener() {
//		@Override
//		public void onClick(View v) {
//			sendSmsDialog.show();
//		}
//	});
}


public void changeBashPinProcess() {
	myAsyncTask=new MyAsynTaskManager();
	myAsyncTask.delegate=this;
	myAsyncTask.setupParamsAndUrl("changeBashPinProcess",getActivity(),AppUrlList.CHANGE_BASHPIN_URL, new String[] {
			"partnertransactionid",
			"partnercode",
			"username",
			"password",
			"servicetype",
			"usernumber"
			}, 
			new String[]{
			"123456",
			"MOBG",
			"MOBG",
			"9641",
			"CP",
			PreferenceManager.getInstance().getUserMobileNumber()
			}
	);
	
	myAsyncTask.execute();
}


public void checkSIMAvailabilityAndSendSMS() {
	TelephonyManager telMgr = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
    int simState = telMgr.getSimState();
            switch (simState) {
                case TelephonyManager.SIM_STATE_ABSENT:
                	Toast.makeText(getActivity(), "Sim State Absent!", Toast.LENGTH_SHORT).show();
                    break;
                case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                	Toast.makeText(getActivity(), "Sim Network Locked!", Toast.LENGTH_SHORT).show();
                    break;
                case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                	Toast.makeText(getActivity(), "Sim Pin Required!", Toast.LENGTH_SHORT).show();
                    break;
                case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                	Toast.makeText(getActivity(), "Sim Puk Required!", Toast.LENGTH_SHORT).show();
                    break;
                case TelephonyManager.SIM_STATE_READY:
                	sendSMS();
                	Toast.makeText(getActivity(), "Sim Puk Required!", Toast.LENGTH_SHORT).show();
                    break;
                case TelephonyManager.SIM_STATE_UNKNOWN:
                	Toast.makeText(getActivity(), "Sim State Unknown!", Toast.LENGTH_SHORT).show();
                    break;
            }
}

void sendSMS()
{      
	Toast.makeText(getActivity(), "Sending SMS...", Toast.LENGTH_SHORT).show();
            
    String SENT = "SMS_SENT";
    String DELIVERED = "SMS_DELIVERED";

    PendingIntent sentPI = PendingIntent.getBroadcast(getActivity(), 0,
        new Intent(SENT), 0);

    PendingIntent deliveredPI = PendingIntent.getBroadcast(getActivity(), 0,
        new Intent(DELIVERED), 0);

    //---when the SMS has been sent---
    getActivity().registerReceiver(new BroadcastReceiver(){
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            switch (getResultCode())
            {
                case Activity.RESULT_OK:
                    Toast.makeText(getActivity(), "SMS sent", 
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(getActivity(), "Generic failure", 
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(getActivity(), "No service", 
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(getActivity(), "Null PDU", 
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(getActivity(), "Radio off", 
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }, new IntentFilter(SENT));

    //---when the SMS has been delivered---
    getActivity().registerReceiver(new BroadcastReceiver(){
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            switch (getResultCode())
            {
                case Activity.RESULT_OK:
                    Toast.makeText(getActivity(), "SMS delivered", 
                            Toast.LENGTH_SHORT).show();
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(getActivity(), "SMS not delivered", 
                            Toast.LENGTH_SHORT).show();
                    break;                        
            }
        }
    }, new IntentFilter(DELIVERED));        

    SmsManager sms = SmsManager.getDefault();
    sms.sendTextMessage("56767", null, "ZIP RESET", sentPI, deliveredPI);        
}


public void setNotificationStatus(final boolean isChecked) {
	myAsyncTask=new MyAsynTaskManager();
	myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("setNotificationStatus",getActivity(), AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser",
				"status"
				}, 
				new String[]{
				"user",
				"notification",
				PreferenceManager.getInstance().getUserId(),
				((isChecked)) ? "on" : "off"
				}
		);
		myAsyncTask.execute();
		
	
	}

	private void initializeAlertViews() {

		setupPinView = View.inflate(getActivity(), R.layout.alert_setuppindialog, null);
		changePinView = View.inflate(getActivity(), R.layout.alert_changepindialog, null);

		setupPinDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		changePinDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setupPinDialog.setContentView(setupPinView);
		changePinDialog.setContentView(changePinView);
		
		((EditText) setupPinView.findViewById(R.id.pinText)).
			setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
		
		((EditText) changePinView.findViewById(R.id.oldPinText)).
			setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
		
		((EditText) changePinView.findViewById(R.id.newPinText)).
			setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
		
		((EditText) changePinView.findViewById(R.id.retypePinText)).
			setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
		
		((ImageView) setupPinView.findViewById(R.id.setupImageButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(((EditText) setupPinView.findViewById(R.id.pinText)).getText() != null && 
						((EditText) setupPinView.findViewById(R.id.pinText)).getText().toString().length() != 0){
					PreferenceManager.getInstance().setPinText(((EditText) setupPinView.findViewById(R.id.pinText)).getText().toString());
					setupPinDialog.dismiss();
				}else{
					Toast.makeText(getActivity(), "Please Enter the Pin Number Text!", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		((Button) setupPinView.findViewById(R.id.cancelDialog)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setupPinDialog.dismiss();
			}
		});
		
		((Button) changePinView.findViewById(R.id.updateImageButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(Validate()){
					changePinDialog.dismiss();
					PreferenceManager.getInstance().setPinText(
					((EditText) changePinView.findViewById(R.id.newPinText)).getText().toString());
					((EditText) changePinView.findViewById(R.id.oldPinText)).setText("");
					((EditText) changePinView.findViewById(R.id.newPinText)).setText("");
					((EditText) changePinView.findViewById(R.id.retypePinText)).setText("");
				}
			}
		});
		
		((ImageView) changePinView.findViewById(R.id.cancelDialog)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				changePinDialog.dismiss();
			}
		});
		
	}
	
	private boolean Validate(){
		if(((EditText) changePinView.findViewById(R.id.oldPinText)).getText() == null || 
				((EditText) changePinView.findViewById(R.id.oldPinText)).getText().toString().length() == 0){
			((EditText) changePinView.findViewById(R.id.oldPinText)).setError("Should Not be Empty!");
			return false;
		}else if(((EditText) changePinView.findViewById(R.id.newPinText)).getText() == null || 
				((EditText) changePinView.findViewById(R.id.newPinText)).getText().toString().length() == 0){
			((EditText) changePinView.findViewById(R.id.newPinText)).setError("Should Not be Empty!");
			return false;
		}else if(((EditText) changePinView.findViewById(R.id.retypePinText)).getText() == null || 
				((EditText) changePinView.findViewById(R.id.retypePinText)).getText().toString().length() == 0){
			((EditText) changePinView.findViewById(R.id.retypePinText)).setError("Should Not be Empty!");
			return false;
		}else if(!((EditText) changePinView.findViewById(R.id.newPinText)).getText().toString().equals( 
				((EditText) changePinView.findViewById(R.id.retypePinText)).getText().toString())){
			Toast.makeText(getActivity(), "New Pin Number & Re-Type Pin Number Doesn't Match!", Toast.LENGTH_SHORT).show();
			return false;
		}else if(!((EditText) changePinView.findViewById(R.id.oldPinText)).getText().toString().equals( 
				PreferenceManager.getInstance().getPinText())){
			Toast.makeText(getActivity(), "New Pin Number Doesn't Match to Old Pin Number!", Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		if(from.equalsIgnoreCase("changeBashPinProcess"))
		{
			if(output!=null)
			{
				try {
					Log.e("Response", output);
					alertWebView.loadDataWithBaseURL("", output, "", "", "");
					pinWebDialog.show();

				} catch (Exception e) {
					DialogManager.showDialog(getActivity(), "Error Occured in Storing Profile Info!");
					e.printStackTrace();
				}

			}
			else
			{
				DialogManager.showDialog(getActivity(), "Error Occured in Storing Profile Info!");
			}
		}
		else if(from.equalsIgnoreCase("setNotificationStatus"))
		{
			if(output!=null)
			{

				try {
					Log.e("Response", output);
					JSONObject rootObj = new JSONObject(output);
					if(rootObj.getBoolean("result")){
						if(ischeck){
							PreferenceManager.getInstance().setUserNotificationSettings("1");
							Toast.makeText(getActivity(), "Notifications are on!", Toast.LENGTH_SHORT).show();
						}else{
							PreferenceManager.getInstance().setUserNotificationSettings("0");
							Toast.makeText(getActivity(), "Notifications are off!", Toast.LENGTH_SHORT).show();
						}
						}else{
							mayICheckNotification = false;
							if(ischeck)
								((CheckBox)getView().findViewById(R.id.notificationCheckButton)).setChecked(false);
							else
								((CheckBox)getView().findViewById(R.id.notificationCheckButton)).setChecked(true);
							Toast.makeText(getActivity(), "Error In Setting Notification Settings! Try Again!", Toast.LENGTH_SHORT).show();
						}
					
				} catch (Exception e) {
					DialogManager.showDialog(getActivity(), "Error Occured in Storing Profile Info!");
					e.printStackTrace();
				}
			
			}
			else
			{
				DialogManager.showDialog(getActivity(), "Error Occured in Storing Profile Info!");
				//e.printStackTrace();
			}
		}
	}
	
}
