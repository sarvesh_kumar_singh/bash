package com.bash.Fragments;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.BuildConfig;
import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.GsonClasses.Login_Register_Class;
import com.bash.ListModels.BashUsers_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DataBaseManager;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.ContactsProvider;
import com.bash.Utils.AppUrlList;
import com.google.gson.Gson;

public class InvitationsFragment extends BaseFragment implements AsyncResponse{
	public Activity context;
	public ArrayList<BashUsers_Class> feedList = new ArrayList<BashUsers_Class>();
 	public ArrayList<BashUsers_Class> originalList = new ArrayList<BashUsers_Class>();
	MyAsynTaskManager  myAsyncTask;
	public int current_position = 0;
	Dialog tagDialog, inviteDialog;
	public View mRootView, inviteDialogView;
	ListView normalUsersListView, bashUsersListView, inviteListView;
	public static BashUsersAdapter bashuserAdapter;
	public static NormalUsersAdapter normalAdapter;
	public static InviteUsersAdapter inviteAlertAdapter;
	Login_Register_Class responseForLogin;
	public static ArrayList<BashUsers_Class> normalAlertOriginalUsersList = new ArrayList<BashUsers_Class>();
	public static ArrayList<BashUsers_Class> normalAlertFilterUsersList = new ArrayList<BashUsers_Class>();
	public static ArrayList<BashUsers_Class> normalUsersList = new ArrayList<BashUsers_Class>();
	public static ArrayList<BashUsers_Class> originalnormalUsersList = new ArrayList<BashUsers_Class>();
	public static ArrayList<BashUsers_Class> bashUsersList = new ArrayList<BashUsers_Class>();
	
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,	Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		mRootView = inflater.inflate(R.layout.fragment_inivitation_page, null);
		initializeView();
		initializeCommentBoxAlertViews();
		return mRootView;
	}
	
	private void initializeCommentBoxAlertViews() {

	 	inviteDialogView = View.inflate(getActivity(), R.layout.alert_inviteall_dialog, null);
		
		inviteDialog = new Dialog(getActivity(), android.R.style.Theme_Translucent);
		
		inviteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		inviteDialog.setContentView(inviteDialogView);
		
		inviteListView = (ListView) inviteDialogView.findViewById(R.id.inviteListView);
		
		inviteAlertAdapter = new InviteUsersAdapter();
		
		inviteListView.setAdapter(inviteAlertAdapter);	
		
		((ImageView) inviteDialogView.findViewById(R.id.sendMailAllButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ArrayList<String> selectionList = new ArrayList<String>();
				if(normalAlertFilterUsersList.size() == 0){
					Toast.makeText(getActivity(), "Please Select Any One Item From List!", Toast.LENGTH_SHORT).show();
				}else{
					
				for (int j = 0; j < normalAlertFilterUsersList.size(); j++) {
					if(normalAlertFilterUsersList.get(j).isselected && normalAlertFilterUsersList.get(j).getEmailId() != null
							&& normalAlertFilterUsersList.get(j).getEmailId().length() != 0)
						selectionList.add(normalAlertFilterUsersList.get(j).getEmailId());
				}
				
				if(selectionList.size() == 0){
					Toast.makeText(getActivity(), "No Email Ids Found In Selected Items!", Toast.LENGTH_SHORT).show();
				}else{
					String[] stockArr = new String[selectionList.size()];
					stockArr = selectionList.toArray(stockArr);
					Intent intent = new Intent(Intent.ACTION_SEND);
					intent.setType("text/plain");
					intent.putExtra(Intent.EXTRA_EMAIL, stockArr);
					intent.putExtra(Intent.EXTRA_SUBJECT, "Add Bash App Man!");
					intent.putExtra(Intent.EXTRA_TEXT, "Hi, Download Bash App from Google Play store.. http://google.com");
					startActivity(Intent.createChooser(intent, "Send Email"));
				}
			  }
			}
		});
		
 		((ImageView) inviteDialogView.findViewById(R.id.messageAllButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ArrayList<String> selectionList = new ArrayList<String>();
				if(normalAlertFilterUsersList.size() == 0){
					Toast.makeText(getActivity(), "Please Select Any One Item From List!", Toast.LENGTH_SHORT).show();
				}else{
				for (int j = 0; j < normalAlertFilterUsersList.size(); j++) {
					if(normalAlertFilterUsersList.get(j).isselected && normalAlertFilterUsersList.get(j).getphone_no() != null
							&& normalAlertFilterUsersList.get(j).getphone_no().length() != 0)
						selectionList.add(normalAlertFilterUsersList.get(j).getphone_no());
				}
				
				if(selectionList.size() == 0){
					Toast.makeText(getActivity(), "No Items Selected From List!", Toast.LENGTH_SHORT).show();
				}else{
					String messageReceipentString = selectionList.toString().replaceAll(",", ";");
					messageReceipentString = messageReceipentString.replaceAll("\\[", "");
					messageReceipentString = messageReceipentString.replaceAll("\\]", "");
					Intent smsIntent = new Intent(Intent.ACTION_VIEW); 
					smsIntent.setType("vnd.android-dir/mms-sms");
					smsIntent.putExtra("address", messageReceipentString);
					smsIntent.putExtra("sms_body","Hi, Download Bash App from Google Play store.. http://google.com");
					startActivity(smsIntent);
				}
			  }
			
			}
		});
		
		
		
		/*((ImageView) inviteDialogView.findViewById(R.id.commentButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				inviteDialog.dismiss();
			}
		});
		*/
		 // Capture Text in EditText
	       ((EditText) inviteDialogView.findViewById(R.id.searchText)).addTextChangedListener(new TextWatcher() {

	           @Override
	           public void afterTextChanged(Editable arg0) {
	               // TODO Auto-generated method stub
	               
	           }

	           @Override
	           public void beforeTextChanged(CharSequence arg0, int arg1,
	                   int arg2, int arg3) {
	               // TODO Auto-generated method stub
	           }

	           @Override
	           public void onTextChanged(CharSequence arg0, int arg1, int arg2,
	                   int arg3) {
	        	   if(arg3 < arg2) {
	        		   inviteAlertAdapter.resetDatas();
					 }
	        	   inviteAlertAdapter.getFilter().filter(arg0.toString());
	           }
	       });
	       
	    ((CheckBox) inviteDialogView.findViewById(R.id.selectAllButton)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				
			 		if(isChecked){
						for(int i = 0; i < normalAlertFilterUsersList.size(); i++){
							normalAlertFilterUsersList.get(i).setIsSelected(true);
						}
					}else{
						for(int i = 0; i < normalAlertFilterUsersList.size(); i++){
							normalAlertFilterUsersList.get(i).setIsSelected(false);
						}
					}
					
					inviteAlertAdapter.notifyDataSetChanged();
			}
		});
			
	    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(inviteDialog.getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		inviteDialog.getWindow().setAttributes(lp);
		
		((ImageView) mRootView.findViewById(R.id.showInviteAlertButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				inviteDialog.show();
			}
		});
		
	}
	
	private void initializeView() {
		
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn, "Invitations", R.drawable.syn_icon);
		
		((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//Sarvesh
				((HomeActivity)getActivity()).slidingmenu_layout.toggleMenu();
			}
		});
		
		bashUsersListView = (ListView)mRootView.findViewById(R.id.bashUsersListView);
		normalUsersListView = (ListView)mRootView.findViewById(R.id.normalUsersListView);
		
		bashUsersListView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 
				PreferenceManager.getInstance().getPercentageFromHeight(32)));
		
		bashUsersList.clear();
		normalUsersList.clear();
		normalAlertOriginalUsersList.clear();
		normalAlertFilterUsersList.clear();
		originalnormalUsersList.clear();
		
		if(bashUsersList == null || bashUsersList.size() == 0){
			bashUsersList = new ArrayList<BashUsers_Class>();
		}
			
		if(normalUsersList == null || normalUsersList.size() == 0){
			normalUsersList = new ArrayList<BashUsers_Class>();
		}
		
		if(normalAlertOriginalUsersList == null || normalAlertOriginalUsersList.size() == 0){
			normalAlertOriginalUsersList = new ArrayList<BashUsers_Class>();
		}
		
		if(normalAlertFilterUsersList == null || normalAlertFilterUsersList.size() == 0){
			normalAlertFilterUsersList = new ArrayList<BashUsers_Class>();
		}
		
		bashUsersList = DataBaseManager.getInstance().getBashUsersList();
		normalUsersList = DataBaseManager.getInstance().getNormalUsersList();
		normalAlertOriginalUsersList = DataBaseManager.getInstance().getNormalUsersList();
		normalAlertFilterUsersList = DataBaseManager.getInstance().getNormalUsersList();
		originalnormalUsersList = DataBaseManager.getInstance().getNormalUsersList();
		
		bashuserAdapter = new BashUsersAdapter(getActivity(), bashUsersList);
		bashUsersListView.setAdapter(bashuserAdapter);	
		normalAdapter = new NormalUsersAdapter();
		normalUsersListView.setAdapter(normalAdapter);
		
		 // Capture Text in EditText
        ((EditText) mRootView.findViewById(R.id.searchFriendBox)).addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
         /*       String text = ((EditText) mRootView.findViewById(R.id.searchFriendBox))
                				.getText().toString().toLowerCase(Locale.getDefault());
                
                bashuserAdapter.getFilter().filter(text);*/
            }
 
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                    int arg2, int arg3) {
                // TODO Auto-generated method stub
            }
 
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                    int arg3) {
            	 if (arg3 < arg2) {
					 // We're deleting char so we need to reset the adapter data
            		 bashuserAdapter.resetData();
				 }
            	 
            	bashuserAdapter.getFilter().filter(arg0.toString());
                // TODO Auto-generated method stub
            }
        });
        

		 // Capture Text in EditText
       ((EditText) mRootView.findViewById(R.id.searchNotFriendsBox)).addTextChangedListener(new TextWatcher() {

           @Override
           public void afterTextChanged(Editable arg0) {
               // TODO Auto-generated method stub
               
           }

           @Override
           public void beforeTextChanged(CharSequence arg0, int arg1,
                   int arg2, int arg3) {
               // TODO Auto-generated method stub
           }

           @Override
           public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                   int arg3) {
        	   if(arg3 < arg2) {
        		   normalAdapter.resetDatas();
				 }
        	   normalAdapter.getFilter().filter(arg0.toString());
           }
       });
       
       
   		((ImageView) getActivity().findViewById(R.id.toprightsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						reloadBashUserIds();
					}
				});
			}
		});
		
       getBashUserIds();
	}
	
	public static void notifyAdapters() {
		
		bashUsersList.clear();
		normalUsersList.clear();
		normalAlertOriginalUsersList.clear();
		normalAlertFilterUsersList.clear();
		originalnormalUsersList.clear();
		
		if(bashUsersList == null || bashUsersList.size() == 0){
			bashUsersList = new ArrayList<BashUsers_Class>();
		} 
			
		if(normalUsersList == null || normalUsersList.size() == 0){
			normalUsersList = new ArrayList<BashUsers_Class>();
		}
		
	 	if(normalAlertOriginalUsersList == null || normalAlertOriginalUsersList.size() == 0){
			normalAlertOriginalUsersList = new ArrayList<BashUsers_Class>();
		}
		
		if(normalAlertFilterUsersList == null || normalAlertFilterUsersList.size() == 0){
			normalAlertFilterUsersList = new ArrayList<BashUsers_Class>();
		}
		
		if(originalnormalUsersList == null || originalnormalUsersList.size() == 0){
			originalnormalUsersList = new ArrayList<BashUsers_Class>();
		}
		
		bashUsersList = DataBaseManager.getInstance().getBashUsersList();
		normalUsersList = DataBaseManager.getInstance().getNormalUsersList();
		normalAlertOriginalUsersList = DataBaseManager.getInstance().getNormalUsersList();
		normalAlertFilterUsersList = DataBaseManager.getInstance().getNormalUsersList();
		originalnormalUsersList = DataBaseManager.getInstance().getNormalUsersList();
		
		/*bashUsersList = DataBaseManager.getInstance().getBashUsersList();
		normalUsersList = DataBaseManager.getInstance().getNormalUsersList();
		
		normalAlertOriginalUsersList = DataBaseManager.getInstance().getNormalUsersList();
		normalAlertFilterUsersList = DataBaseManager.getInstance().getNormalUsersList();
		originalnormalUsersList = DataBaseManager.getInstance().getNormalUsersList();*/
		
		
		if(bashUsersList.size() == 0)
			bashUsersList.add(new BashUsers_Class("0", "0", " ", " ", "FALSE", "FALSE", "0"));
		if(normalUsersList.size() == 0)	
			normalUsersList.add(new BashUsers_Class("0", "0", " ", " ", "FALSE", "FALSE", "0"));
		
	 	//bashuserAdapter.notifyDataSetChanged();
		
		//bashuserAdapter.notifyWithDataSet(bashUsersList);
		bashuserAdapter.notifyWithDataSet(DataBaseManager.getInstance().getBashUsersList());
		normalAdapter.notifyDataSetChanged();
	}
	 
	public void getBashUserIds() {

		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
    	myAsyncTask.setupParamsAndUrl("getBashUserIds",getActivity(),AppUrlList.ACTION_URL, 
    			new String[]{
    			"module",
    			"action",
    			"iduser",
    			},
    			new String[]{
    			"user",
    			"allbashusers",
    			PreferenceManager.getInstance().getUserId(),
    			});
    	
    	myAsyncTask.execute();
    	 
    }
	
	public void reloadBashUserIds() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
myAsyncTask.setupParamsAndUrl("reloadBashUserIds",getActivity(),AppUrlList.ACTION_URL, 
    			new String[]{
    			"module",
    			"action",
    			"iduser",
    			},
    			new String[]{
    			"user",
    			"allbashusers",
    			PreferenceManager.getInstance().getUserId(),
    			});
myAsyncTask.execute();
    	
    	 
    }

	class ReloadContactTacble extends AsyncTask<Void, Void, Boolean>{
		ProgressDialog pd;
		@Override
		protected void onPreExecute() {
			pd = new ProgressDialog(getActivity());
			pd.setMessage("Reloading...");
			pd.setCancelable(false);
			pd.show();
			super.onPreExecute();
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				ContactsProvider.ReloadContactsTable(getActivity(), responseForLogin);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			pd.dismiss();
			notifyAdapters();
		}
	}
	
	class InviteUsersAdapter extends BaseAdapter implements Filterable{

		Filter filerInstance;
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return normalAlertFilterUsersList.size();
		}

		@Override
		public BashUsers_Class getItem(int position) {
			// TODO Auto-generated method stub
			return normalAlertFilterUsersList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
	    public View getView(final int position, View convertView, ViewGroup parent) {
	        ViewHolder holder = null;
	        final BashUsers_Class listItem = getItem(position);
	        if (convertView == null) {
	        	LayoutInflater inflater = LayoutInflater.from(getActivity());
	        	if(listItem.getphone_no().equals("0") && getCount() == 1){
	        		convertView = inflater.inflate(R.layout.error_listview, null);
	        	}
	        	else{
	        	convertView = inflater.inflate(R.layout.custom_inviteall_listview, null);
	            holder = new ViewHolder();
	            holder.userImage = (ImageView) convertView.findViewById(R.id.userImage);
	            holder.userName = (TextView) convertView.findViewById(R.id.userName);
	            holder.mobileNumberText = (TextView) convertView.findViewById(R.id.mobileNumberText);
	            holder.messageButton = (ImageView) convertView.findViewById(R.id.messageButton);
	            holder.mailButton = (ImageView) convertView.findViewById(R.id.mailButton);
	            holder.selectedCheckBox = (CheckBox) convertView.findViewById(R.id.selectedCheckBox);
	            }
	            convertView.setTag(holder);
	        } else {
	            holder = (ViewHolder) convertView.getTag();
	        }
	        
	        if(!listItem.getphone_no().equals("0")) {
		        holder.userName.setText(listItem.getname());
		        holder.mobileNumberText.setText(listItem.getphone_no());
		        //ImageLoader.getInstance().displayImage(listItem.imagepath, holder.userImage);
		        holder.messageButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent smsIntent = new Intent(Intent.ACTION_VIEW); 
						smsIntent.setType("vnd.android-dir/mms-sms");
						smsIntent.putExtra("address", listItem.getphone_no());
						smsIntent.putExtra("sms_body","Hi, Download Bash App from Google Play store.. http://google.com");
						startActivity(smsIntent);
					}
				});
		        
		        if(listItem.getEmailId() != null && listItem.getEmailId().length() != 0)
		        	holder.mailButton.setVisibility(View.VISIBLE);
		        else
		        	holder.mailButton.setVisibility(View.GONE);
		        
		        holder.mailButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(Intent.ACTION_SEND);
						intent.setType("text/plain");
						intent.putExtra(Intent.EXTRA_EMAIL, new String[]{listItem.getEmailId()});
						intent.putExtra(Intent.EXTRA_SUBJECT, "Add Bash App Man!");
						intent.putExtra(Intent.EXTRA_TEXT, "Hi, Download Bash App from Google Play store.. http://google.com");
						startActivity(Intent.createChooser(intent, "Send Email"));
					}
				});
		        
		        holder.selectedCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						if(isChecked)
							getItem(position).setIsSelected(true);
						else
							getItem(position).setIsSelected(false);
					}
				});
		        
		        if(listItem.getIsSelected())
		        	holder.selectedCheckBox.setChecked(true);
		        else
		        	holder.selectedCheckBox.setChecked(false);
	        }
	        return convertView;
	    }

		
		private class ViewHolder {
	    	TextView userName;
	    	TextView mobileNumberText;
	    	ImageView messageButton;
	    	ImageView userImage, mailButton;
	    	CheckBox selectedCheckBox;
	     }

	    public void resetDatas(){
	    	normalAlertFilterUsersList = normalAlertOriginalUsersList;
	    	notifyDataSetChanged();
	    }

		@Override
		public Filter getFilter() {
			if(filerInstance == null){
				filerInstance= new NonFriendFilter();
			}
			return filerInstance;
		}
		
		//filter Class... 
		private class NonFriendFilter extends Filter {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
					// We implement here the filter logic
				if (constraint == null || constraint.length() == 0) {
						// No filter implemented we return all the list
							results.values = normalAlertOriginalUsersList;
							results.count = normalAlertOriginalUsersList.size();
					} else {
							// We perform filtering operation
							List<BashUsers_Class> tempList = new ArrayList<BashUsers_Class>();
							for (BashUsers_Class p : normalAlertOriginalUsersList) {
								if (p.getname().toUpperCase()
										.startsWith(constraint.toString().toUpperCase()))
									tempList.add(p);
							}
							results.values = tempList;
							results.count = tempList.size();
						}
						return results;
					}

			@Override
			protected void publishResults(CharSequence constraint,	FilterResults results) {
				if (results.count == 0){
					normalAlertFilterUsersList = (ArrayList<BashUsers_Class>) results.values;
					notifyDataSetInvalidated();
					}
				else {
					normalAlertFilterUsersList = (ArrayList<BashUsers_Class>) results.values;
					notifyDataSetChanged();
				}
			}
		}
	}
	
	class NormalUsersAdapter extends BaseAdapter implements Filterable{

		Filter filerInstance;
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return normalUsersList.size();
		}

		@Override
		public BashUsers_Class getItem(int position) {
			// TODO Auto-generated method stub
			return normalUsersList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        ViewHolder holder = null;
	        final BashUsers_Class listItem = getItem(position);
	        if (convertView == null) {
	        	LayoutInflater inflater = LayoutInflater.from(getActivity());
	        	if(listItem.getphone_no() == null || listItem.getphone_no().equals("0") && getCount() == 1){
	        		convertView = inflater.inflate(R.layout.error_listview, null);
	        	}
	        	else{
	        	convertView = inflater.inflate(R.layout.custom_invitation_normaluser_listview, null);
	            holder = new ViewHolder();
	            holder.userImage = (ImageView) convertView.findViewById(R.id.userImage);
	            holder.userName = (TextView) convertView.findViewById(R.id.userName);
	            holder.mobileNumberText = (TextView) convertView.findViewById(R.id.mobileNumberText);
	            holder.messageButton = (ImageView) convertView.findViewById(R.id.messageButton);
	            holder.mailButton = (ImageView) convertView.findViewById(R.id.mailButton);
	            }
	            convertView.setTag(holder);
	        } else {
	            holder = (ViewHolder) convertView.getTag();
	        }
	        
	        if(listItem.getphone_no() != null && !listItem.getphone_no().equals("0")) {
	        	if(listItem.getname()!=null)
	        		holder.userName.setText(listItem.getname());
		        if(listItem.getphone_no()!=null)
		        	holder.mobileNumberText.setText(listItem.getphone_no());
		        //ImageLoader.getInstance().displayImage(listItem.imagepath, holder.userImage);
		        holder.messageButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent smsIntent = new Intent(Intent.ACTION_VIEW); 
						smsIntent.setType("vnd.android-dir/mms-sms");
						smsIntent.putExtra("address", listItem.getphone_no());
						smsIntent.putExtra("sms_body","Hi, Download Bash App from Google Play store.. http://google.com");
						startActivity(smsIntent);
					}
				});
		        
		        if(listItem.getEmailId() != null && listItem.getEmailId().length() != 0)
		        	holder.mailButton.setVisibility(View.VISIBLE);
		        else
		        	holder.mailButton.setVisibility(View.GONE);
		        
		        holder.mailButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(Intent.ACTION_SEND);
						intent.setType("text/plain");
						intent.putExtra(Intent.EXTRA_EMAIL, new String[]{listItem.getEmailId()});
						intent.putExtra(Intent.EXTRA_SUBJECT, "Add Bash App Man!");
						intent.putExtra(Intent.EXTRA_TEXT, "Hi, Download Bash App from Google Play store.. http://google.com");
						startActivity(Intent.createChooser(intent, "Send Email"));
					}
				});
	        }
	        return convertView;
	    }

		
		private class ViewHolder {
	    	TextView userName;
	    	TextView mobileNumberText;
	    	ImageView messageButton;
	    	ImageView userImage, mailButton;
	     }

	    public void resetDatas(){
	    	normalUsersList = originalnormalUsersList;
	    	notifyDataSetChanged();
	    }

		@Override
		public Filter getFilter() {
			if(filerInstance == null){
				filerInstance= new NonFriendFilter();
			}
			return filerInstance;
		}
		
		//filter Class... 
		private class NonFriendFilter extends Filter {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
					// We implement here the filter logic
				if (constraint == null || constraint.length() == 0) {
						// No filter implemented we return all the list
							results.values = originalnormalUsersList;
							results.count = originalnormalUsersList.size();
					} else {
							// We perform filtering operation
							List<BashUsers_Class> tempList = new ArrayList<BashUsers_Class>();
							for (BashUsers_Class p : originalnormalUsersList) {
								if (p.getname().toUpperCase()
										.startsWith(constraint.toString().toUpperCase()))
									tempList.add(p);
							}
							results.values = tempList;
							results.count = tempList.size();
						}
						return results;
					}

			@Override
			protected void publishResults(CharSequence constraint,	FilterResults results) {
				if (results.count == 0){
					normalUsersList = (ArrayList<BashUsers_Class>) results.values;
					notifyDataSetInvalidated();
					}
				else {
					normalUsersList = (ArrayList<BashUsers_Class>) results.values;
					notifyDataSetChanged();
				}
			}
		}
	}

    class CheckOutProcess extends AsyncTask<Void, Void, Void> {
	ProgressDialog pd;
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		Log.e("Preexecute Called", "");
		pd = new ProgressDialog(getActivity());
		pd.setMessage("Loading CheckOut Process...");
		pd.show();
		super.onPreExecute();
	}
	@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
		try {
			DataBaseManager.getInstance().checkOutContactTable(responseForLogin.bashusers);	
		} catch (Exception e) {
			// TODO: handle exception
			pd.dismiss();
		}
		return null;
	}
	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		pd.dismiss();
		notifyAdapters();
	}
}
    public class BashUsersAdapter extends BaseAdapter
    {
    	 	
    	 	
    	 	
    	 	public FriendFilter filter;
    	 	
    	 	
    	 	
    	    public BashUsersAdapter(Activity contx, ArrayList<BashUsers_Class> feedlist) {
    	        context = contx;
    	    	feedList = feedlist;
    	    	originalList = feedlist;
    	    }

    	    public void notifyWithDataSet(ArrayList<BashUsers_Class> feedlist){
    	    	feedList.clear();
    	    	feedList = feedlist;
    	    	originalList = feedlist;
    	    	Log.e("Size of Adapter", String.valueOf(feedlist.size()));
    	    	this.notifyDataSetChanged();
    	    }
    	    
    	    /*public void notifyWithDataSet(ArrayList<BashUsers_Class> feedList) {
    	    	this.feedList = feedList;
    	    	this.notifyDataSetChanged();
    	    }
    	    */
    	    
    	    @Override
    	    public View getView(final int position, View convertView, ViewGroup parent) {
    	        ViewHolder holder = null;
    	        final BashUsers_Class listItem = getItem(position);
    	        if (convertView == null) {
    	        	LayoutInflater inflater = LayoutInflater.from(context);
    	        	if(listItem.getphone_no().equals("0")){
    	        		convertView = inflater.inflate(R.layout.error_listview, null);
    	        	}
    	        	else{
    	        		convertView = inflater.inflate(R.layout.custom_invitation_bashuser_listview, null);
    		            holder = new ViewHolder();
    		            holder.userImage = (ImageView) convertView.findViewById(R.id.userImage);
    		            holder.userName = (TextView) convertView.findViewById(R.id.userName);
    		            holder.mobileNumberText = (TextView) convertView.findViewById(R.id.mobileNumberText);
    		            holder.sendRequestButton = (Button) convertView.findViewById(R.id.sendRequestButton);
    		            /*holder.acceptFriendButton = (Button) convertView.findViewById(R.id.acceptFriendButton);
    		            holder.rejectFriendButton = (Button) convertView.findViewById(R.id.rejectFriendButton);
    		            holder.cancelRequestButton = (Button) convertView.findViewById(R.id.cancelRequestButton);*/
    	        	}
    	            convertView.setTag(holder);
    	        } else {
    	            holder = (ViewHolder) convertView.getTag();
    	        }
    	        
    	        if(!listItem.getphone_no().equals("0") && listItem.name != null && holder != null) {
    	        	if(holder.userName != null)
    	        		holder.userName.setText(listItem.name);
    	        	if(holder.mobileNumberText != null)
    	        		holder.mobileNumberText.setText(listItem.phone_no);
    	     /*   if(listItem.is_send_request.equals("TRUE")){
    	        	holder.acceptFriendButton.setVisibility(View.GONE);
    	        	holder.rejectFriendButton.setVisibility(View.GONE);
    	        	holder.sendRequestButton.setVisibility(View.GONE);
    	        	holder.cancelRequestButton.setVisibility(View.VISIBLE);
    	        }else if(listItem.is_request_received.equals("TRUE")){
    	        	holder.acceptFriendButton.setVisibility(View.VISIBLE);
    	        	holder.rejectFriendButton.setVisibility(View.VISIBLE);
    	        	holder.sendRequestButton.setVisibility(View.GONE);
    	        	holder.cancelRequestButton.setVisibility(View.GONE);
    	        }else{
    	        	holder.acceptFriendButton.setVisibility(View.GONE);
    	        	holder.rejectFriendButton.setVisibility(View.GONE);
    	        	holder.sendRequestButton.setVisibility(View.VISIBLE);
    	        	holder.cancelRequestButton.setVisibility(View.GONE);
    	        }
    	        */
    	       // holder.mobileNumberText.setText(listItem.phone_no);
    	        
    	        final ViewHolder viewsHolder = holder;
    	        
    	        /*holder.acceptFriendButton.setOnClickListener(new OnClickListener() {
    				@Override
    				public void onClick(View v) {
    					// TODO Auto-generated method stub
    					ProcessAsyntask(PreferenceManager.getInstance().getUserId(), 
    							listItem.idfriend, "acceptrequest", viewsHolder, position);
    				}
    			});
    	        
    	        holder.rejectFriendButton.setOnClickListener(new OnClickListener() {
    				@Override
    				public void onClick(View v) {
    					// TODO Auto-generated method stub
    					ProcessAsyntask(PreferenceManager.getInstance().getUserId(), 
    							listItem.idfriend, "rejectfriend", viewsHolder, position);
    				}
    			});
    	        */
    	        holder.sendRequestButton.setOnClickListener(new OnClickListener() {
    				@Override
    				public void onClick(View v) {
    					current_position = position;
    					ProcessAsyntask(PreferenceManager.getInstance().getUserId(), 
    							listItem.idfriend, "addfriend", viewsHolder, position);
    				}
    			});
    	        
    	       /* holder.cancelRequestButton.setOnClickListener(new OnClickListener() {
    				@Override
    				public void onClick(View v) {
    					// TODO Auto-generated method stub
    					ProcessAsyntask(PreferenceManager.getInstance().getUserId(), 
    							listItem.idfriend, "cancelrequest", viewsHolder, position);
    				}
    			});
    	        */
    	        //ImageLoader.getInstance().displayImage(listItem.imagepath, holder.userImage);
    	        }
    	        return convertView;
    	    }

    	    

    	    public void removeItemFromList(int position)
    		{
    			feedList.remove(position);
    			this.notifyDataSetInvalidated();
    			this.notifyDataSetChanged();
    	    }
    	    
    	    
    	    @Override
    	    public int getCount() {
    	        return feedList.size();
    	    }

    	    @Override
    	    public BashUsers_Class getItem(int position) {
    	        return feedList.get(position);
    	    }

    	    @Override
    	    public long getItemId(int position) {
    	        return position;
    	    }

    	    
    	    
    	    public void resetData() {
    			feedList = originalList;
    			notifyDataSetChanged();
    		}
    	    
    	    public Filter getFilter() {
    	  			// TODO Auto-generated method stub
    	  			  if (filter == null)
    	  				  filter = new FriendFilter();
    	  			    return filter;
    	    }
    	    
    		//filter Class... 
    		private class FriendFilter extends Filter {
    			@Override
    			protected FilterResults performFiltering(CharSequence constraint) {
    				FilterResults results = new FilterResults();
    				// We implement here the filter logic
    				if (constraint == null || constraint.length() == 0) {
    					// No filter implemented we return all the list
    					results.values = originalList;
    					results.count = originalList.size();
    				} else {
    					// We perform filtering operation
    					List<BashUsers_Class> tempList = new ArrayList<BashUsers_Class>();
    					for (BashUsers_Class p : feedList) {
    						if (p.getname().toUpperCase()
    								.startsWith(constraint.toString().toUpperCase()))
    							tempList.add(p);
    					}
    					results.values = tempList;
    					results.count = tempList.size();
    				}
    				return results;
    			}

    			@Override
    			protected void publishResults(CharSequence constraint,
    					FilterResults results) {
    				// Now we have to inform the adapter about the new list filtered
    				/*if (results.count == 0){
    				}
    				else {
    					feedList = (ArrayList<BashUsers_Class>) results.values;
    					notifyDataSetChanged();	
    				}*/
    				feedList = (ArrayList<BashUsers_Class>) results.values;
    				notifyDataSetChanged();
    			}
    		}
        
    		public void notifyDataSet(){
    			this.notifyDataSetChanged();
    		}
    	     
    	    
    }
    public class ViewHolder {
    	TextView userName;
    	TextView mobileNumberText;
    	/*Button acceptFriendButton;
    	Button rejectFriendButton;
    	Button cancelRequestButton;
    	*/
    	Button sendRequestButton;
    	ImageView userImage;
     }
    public void ProcessAsyntask(String iduser, final String idfriend, final String action, final ViewHolder holder,
    		final int position) {
    	myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
    	myAsyncTask.setupParamsAndUrl("ProcessAsyntask",getActivity(),AppUrlList.ACTION_URL, 
    			new String[]{
    			"module",
    			"action",
    			"iduser",
    			"idfriend"
    			},
    			new String[]{
    			"user",
    			action,
    			iduser,
    			idfriend
    			});
    	myAsyncTask.execute();
    	
    	 
    }
	@Override
	public void backgroundProcessFinish(String from, String output) {
		if(from.equalsIgnoreCase("getBashUserIds"))
		{
			if(output!=null)
			{
				try {
					if(BuildConfig.DEBUG)
						Log.e("Json Response", output);
					Gson gson = new Gson();

					responseForLogin = gson.fromJson(output, Login_Register_Class.class);

					if(responseForLogin.result) {

						new CheckOutProcess().execute();

					}else{
						DialogManager.showDialog(getActivity(), "Error Get Users Details! Try Again!");
					}
				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
				}	
			}
			else
			{
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
				
			}
		}
		else if(from.equalsIgnoreCase("reloadBashUserIds"))
		{
			if(output!=null)
			{

				try {
				if(BuildConfig.DEBUG)
					Log.e("Json Response", output);
				Gson gson = new Gson();
				
				responseForLogin = gson.fromJson(output, Login_Register_Class.class);
					
				if(responseForLogin.result) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							new ReloadContactTacble().execute();		
						}
					});
					//new CheckOutProcess().execute();
				}else{
					DialogManager.showDialog(getActivity(), "Error Get Users Details! Try Again!");
				}
					} catch (Exception e) {
						// TODO: handle exception
						DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
					}
			
			}
			else
			{
				// TODO: handle exception
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			}
		}
		else if(from.equalsIgnoreCase("ProcessAsyntask"))
		{
			if(output!=null)
			{

				// TODO Auto-generated method stub
				try {
					JSONObject rootObj = new JSONObject(output);
					if(rootObj.getBoolean("result")){
						feedList.remove(current_position);
						//block code
						
						bashuserAdapter.notifyDataSet();
						Toast.makeText(context, "Friend Added To Bash Friedns Successfully!", Toast.LENGTH_SHORT).show();
						/*
						if(action.equals("addfriend")) {
							holder.sendRequestButton.setVisibility(View.GONE);
				        	holder.cancelRequestButton.setVisibility(View.VISIBLE);
				        	holder.acceptFriendButton.setVisibility(View.GONE);
				        	holder.rejectFriendButton.setVisibility(View.GONE);
							DialogManager.showDialog(context, "Friend Request Sent Successfully!");
						}else if(action.equals("acceptrequest")) {
							DataBaseManager.getInstance().makeMyFriend(idfriend);
							removeItemFromList(position);
							//InvitationsFragment.notifyAdapters();
							DialogManager.showDialog(context, "Friend Added Successfully!");
						}else if(action.equals("rejectfriend")) {
							holder.sendRequestButton.setVisibility(View.VISIBLE);
				        	holder.cancelRequestButton.setVisibility(View.GONE);
				        	holder.acceptFriendButton.setVisibility(View.GONE);
				        	holder.rejectFriendButton.setVisibility(View.GONE);
							DialogManager.showDialog(context, "Friend Request Canceled Successfully!");
						} else if(action.equals("cancelrequest")) {
							holder.sendRequestButton.setVisibility(View.VISIBLE);
				        	holder.cancelRequestButton.setVisibility(View.GONE);
				        	holder.acceptFriendButton.setVisibility(View.GONE);
				        	holder.rejectFriendButton.setVisibility(View.GONE);
							DialogManager.showDialog(context, "Request Canceled Successfully!");
						}*/
					}else{
							DialogManager.showDialog(context, "Error Occured To Request! Try Again!");
					}
					
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			
			}
			else
			{
				DialogManager.showDialog(context, "Server Error Occured! Try Again!");	
			}
		}
	}
	
}
