package com.bash.Fragments;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.GsonClasses.Friends_Class;
import com.bash.GsonClasses.MyFriend_Transaction_Class;
import com.bash.ListModels.BashUsers_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.bash.Utils.MyLocation;
import com.nostra13.universalimageloader.core.ImageLoader;

@SuppressLint("ValidFragment")
public class MyFriends_Transaction_Pay_Fragment extends Fragment implements AsyncResponse{
	
	View mRootView;
	MyAsynTaskManager  myAsyncTask;
	View camera_gallery, pin_webview;
	Dialog camaeraDialog, pinWebDialog;
	Uri mCapturedImageURI;
	public ImageView fileImage;
	public Friends_Class responseForFriends;
	public String selectedFriendId, locationPath=" ";
	public WebView alertWebView;
	public BashUsers_Class userInfo;
	public MyFriend_Transaction_Class transactionDetails;
	public ProgressDialog pd;
	
	public MyFriends_Transaction_Pay_Fragment(BashUsers_Class userInfo, MyFriend_Transaction_Class responseForTransaction) {
		this.userInfo = userInfo;
		transactionDetails=responseForTransaction;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		mRootView = inflater.inflate(R.layout.fragment_myfriends_trans_pay_page, null);
		//mRootView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		return mRootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initializeView();
		initializeAlertView();
		initializeWebAlertView();
		((HomeActivity) getActivity()).currentFragment = this;
	}
	
	private void initializeAlertView() {

		camera_gallery = View.inflate(getActivity(),
				R.layout.alert_gallerycamera_mode_view, null);
		camaeraDialog = new Dialog(getActivity());
		camaeraDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		camaeraDialog.setContentView(camera_gallery);
 
		((Button) camera_gallery.findViewById(R.id.openGalleryBtn))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						camaeraDialog.dismiss();
						Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						getActivity().startActivityForResult(i,	AppConstants.CODE_GALLERY);
					}
		});

		((Button) camera_gallery.findViewById(R.id.openCameraBtn)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						camaeraDialog.dismiss();
						String fileName = "temp.jpg";
						ContentValues values = new ContentValues();
						values.put(MediaStore.Images.Media.TITLE, fileName);
						mCapturedImageURI = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
										values);
						Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
						intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
						getActivity().startActivityForResult(intent, AppConstants.CODE_CAMERA);
					}
				});

	}
	
	
	private void initializeWebAlertView(){
		
		pin_webview = View.inflate(getActivity(),
				R.layout.alert_showpinalert_dialog, null);
		pinWebDialog = new Dialog(getActivity());
		pinWebDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		pinWebDialog.setContentView(pin_webview);
 
		alertWebView = ((WebView) pin_webview.findViewById(R.id.webView));
		
		WebSettings settings = alertWebView.getSettings();
        /*settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);*/
        settings.setJavaScriptEnabled(true);
        //alertWebView.setVerticalScrollBarEnabled(true);
        alertWebView.setHorizontalScrollBarEnabled(true);
        
     /*   alertWebView.setWebViewClient(new WebViewClient(){
        	@Override
        	public void onPageFinished(WebView view, String url) {
        		// TODO Auto-generated method stub
        		super.onPageFinished(view, url);
        		view.loadUrl(url);
        	}
        });*/
	}
	
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case AppConstants.CODE_CAMERA:
			if (resultCode == getActivity().RESULT_OK) {
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(
						mCapturedImageURI, projection, null, null, null);
				int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				String picturePath = cursor.getString(column_index_data);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,	fileImage);
				Log.e("Camera Path", picturePath);
			}
			break;
		case AppConstants.CODE_GALLERY:
			if (resultCode == getActivity().RESULT_OK) {

				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String picturePath = cursor.getString(columnIndex);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,	fileImage);
			}
			break;
		default:
			break;
		}
	}
	
	private void initializeView() {
		
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "Pay Friend", 0);
    	
		((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((BaseFragment)getParentFragment()).popFragment();
			}
		});
		
		pd = new ProgressDialog(getActivity());
		
		((EditText) mRootView.findViewById(R.id.dateText)).setText(AppConstants.simpleDateFormat.format(new Date()));
		
		((EditText) mRootView.findViewById(R.id.amountText)).setText((Integer.parseInt(transactionDetails.totalbalance) * -1)+"");
		
		((EditText) mRootView.findViewById(R.id.friendName)).setText(userInfo.getname());
		
		((RelativeLayout) mRootView.findViewById(R.id.paynowButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(ValidateForm()){
					ProcessForPayment(transactionDetails.iduser);
				}
			}
		});
		
		((RelativeLayout) mRootView.findViewById(R.id.payByCashButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(ValidateForm()){
					ProcessForByCash();
				}
			}
		});
		
		((ImageView) mRootView.findViewById(R.id.locationButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				pd.setMessage("Tracking Your Location...");
				pd.setCancelable(false);
				MyLocation locationPicker = new MyLocation();
				pd.show();
				locationPicker.getLocation(getActivity(), new MyLocation.LocationResult() {
					@Override
					public void gotLocation(final Location location) {
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								pd.dismiss();
								if(location == null){
									Toast.makeText(getActivity(), "Unable to Get Your Location! Try Again!", Toast.LENGTH_SHORT).show();
								}else{
									try {
										Geocoder geocoder;
										List<Address> addresses;
										geocoder = new Geocoder(getActivity(), Locale.getDefault());
										addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
										locationPath = addresses.get(0).getSubLocality()+", "+addresses.get(0).getLocality();
										((TextView)mRootView.findViewById(R.id.locationInfo)).setText(locationPath);
										
								 	} catch (Exception e) {
										e.printStackTrace();
									}
									
								}
								
							}
						});
						
					}
				});
			}
		});
		
	}
	
	public void ProcessForPayment(String payeeNumber){

		String transactionId = String.valueOf("MGGP"+AppConstants.dateformatforZipCash.format(new Date())+PreferenceManager.getInstance().getUserId());
		
	 	Log.e("Transaction Id", transactionId);
	 	myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("ProcessForPayment",getActivity(),AppConstants.URL_ZIP_CASH, new String[] {
				"partnercode",
				"username",
				"password",
				"partnertransactionid",
				"servicetype",
				"merchantCode",
				"amount",
				"usernumber",
				"returl"
				}, 
				new String[]{
				PreferenceManager.getInstance().getPartnerCode(),
				PreferenceManager.getInstance().getPartnerUserName(),
				PreferenceManager.getInstance().getPartnerPassword(),
				transactionId,
				"PP",
				payeeNumber,
				((EditText) mRootView.findViewById(R.id.amountText)).getText().toString(),
				PreferenceManager.getInstance().getUserMobileNumber(),
				""
				});
		myAsyncTask.execute();
		
	}
	
	public void ProcessForByCash() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
	 	myAsyncTask.setupParamsAndUrl("ProcessForByCash",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser",
				"idfriend",
				"date",
				"location",
				"pay_mode",
				"comment",
				"amount"
				}, 
				new String[]{
				"feed",
				"peartopear",
				PreferenceManager.getInstance().getUserId(),
				transactionDetails.iduser,
				((EditText) mRootView.findViewById(R.id.dateText)).getText().toString(),
				"Chennai",
				"cash",
				((EditText) mRootView.findViewById(R.id.feedCommentText)).getText().toString(),
				((EditText) mRootView.findViewById(R.id.amountText)).getText().toString()
				});
	 	myAsyncTask.execute();
		
	 
	}
	
	 
	public boolean ValidateForm(){

		try {
			if(((EditText) mRootView.findViewById(R.id.amountText)).getText() == null || 
					((EditText) mRootView.findViewById(R.id.amountText)).getText().toString().length() == 0){
				 	Toast.makeText(getActivity(), "Please Your Amout to Pay!", Toast.LENGTH_SHORT).show();
				return false;
			}else if(((EditText) mRootView.findViewById(R.id.dateText)).getText() == null || 
					((EditText) mRootView.findViewById(R.id.dateText)).getText().toString().length() == 0){
			 	Toast.makeText(getActivity(), "Please Select Date Details!", Toast.LENGTH_SHORT).show();
			 	return false;
			}else if(((EditText) mRootView.findViewById(R.id.feedCommentText)).getText() == null || 
					((EditText) mRootView.findViewById(R.id.feedCommentText)).getText().toString().length() == 0){
			 	Toast.makeText(getActivity(), "Please Enter Comment!", Toast.LENGTH_SHORT).show();
			 	return false;
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Toast.makeText(getActivity(), "Please Select Friend Name!", Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}
	
	Calendar cal = Calendar.getInstance();
	 
	public DatePickerDialog getDatePickerDialog() {
			DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
				 
	            @Override
	            public void onDateSet(DatePicker view, int year,
	                    int monthOfYear, int dayOfMonth) {
	            	((EditText) mRootView.findViewById(R.id.dateText)).setText(dayOfMonth + "/"
	                        + (monthOfYear + 1) + "/" + year);
	            }
	        }, cal.get(Calendar.YEAR),  cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			
		 return datePickerDialog;
	 }

		
	 
	 
	class Transactions_Class {

		String transactionid, amount, feedcomment;
		
		public Transactions_Class(String transactionid, String feedcomment, String amount){
			this.transactionid = transactionid;
			this.feedcomment = feedcomment;
			this.amount = amount;
		}
		
		public String getTransactionId(){
			return transactionid;
		}
		public String getFeedComment(){
			return feedcomment;
		}
		public String getAmount(){
			return amount;
		}
		
	}




	@Override
	public void backgroundProcessFinish(String from, String output) {
		if(from.equalsIgnoreCase("ProcessForPayment"))
		{
			if(output!=null)
			{
				alertWebView.loadDataWithBaseURL("", output, "", "", "");
				pinWebDialog.show();
			}
			else
			{


				// TODO: handle exception
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			
			
			}
		}
		else if(from.equalsIgnoreCase("ProcessForPayment"))
		{
			if(output!=null)
			{

				try {
					JSONObject rootObj = new JSONObject(output);
					if(rootObj.getBoolean("result")){
						Toast.makeText(getActivity(), "Transaction Processed Successfully!", Toast.LENGTH_SHORT).show();
						((BaseFragment)getParentFragment()).popFragment();
					}else{
						DialogManager.showDialog(getActivity(), "Failed To Proceed Transaction! Try Again!");
					}
				} catch (Exception e) {
						e.printStackTrace();
				}
				
		
			}else
			{

				// TODO: handle exception
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			
			}
		}
	}
}
