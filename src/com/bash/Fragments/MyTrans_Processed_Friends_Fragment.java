package com.bash.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.bash.R;
import com.bash.BaseFragmentClasses.BaseFragment;

public class MyTrans_Processed_Friends_Fragment extends BaseFragment{
	public View mRootView;
	ListView cashInListView;
	//TransactionAdapter adapter;
	//ArrayList<Transaction_Class> feedList = new ArrayList<Transaction_Class>();
	
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		mRootView = inflater.inflate(R.layout.fragment_cashin_page, null);
		initializeView();
		return mRootView;
	}
	
	private void initializeView() {
		// TODO Auto-generated method stub
		//Initialize Activity Views
		/*((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "All Transactions", 0);
		
		((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				HomeFragment.parentFragment.popFragment();
			}
		});*/
		
		cashInListView = (ListView)mRootView.findViewById(R.id.cashInListView);
		
		cashInListView.setAdapter(MyTrans_Processed_Fragment.friendsAdapter);
		
		//getAllTransactionDetails();
	}

		
	
	/*public void getAllTransactionDetails(){
		 
		MyAsynTaskManager.setupParamsAndUrl(AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser"
			 	}, 
				new String[]{
				"feed",
				"alltransaction",
				PreferenceManager.getInstance().getUserId()
				});

		new MyAsynTaskManager(getActivity(), new LoadListener() {
			@Override
			public void onLoadComplete(final String jsonResponse) {
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						try{
							JSONObject rootObj = new JSONObject(jsonResponse);
							//new Transaction_Class(paidByName, paidForName, paymentComment, paymentDate, payerImage, transactionId, status, paymentLocation)
							if(rootObj.getBoolean("result")){
								
								JSONArray jsonArray = rootObj.getJSONArray("feedlist");
								JSONObject jObj;
								for(int i = 0; i<jsonArray.length(); i++){
									jObj = jsonArray.getJSONObject(i);
									feedList.add(new Transaction_Class(
											jObj.getString("paid_by"),
											jObj.getString("paid_for"),
											jObj.getString("feed_comment"),
											jObj.getString("recorded_on"),
											jObj.getString("imagepath"),
											jObj.getString("idtrans"),
											jObj.getString("status"),
											jObj.getString("location"),
											jObj.getString("amount")));
								}
								
								adapter.notifyDataSetChanged();
							}
						} catch (Exception e) {
							// TODO: handle exception
							DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
						}
				}
				});
			}
			@Override
			public void onError(final String errorMessage)  {
				getActivity().runOnUiThread(new Runnable() {
					public void run() {
						DialogManager.showDialog(getActivity(), errorMessage);		
					}
				});
				
			}
		}).execute();
	 
}
	
	*/
	 
			
	}
	
	
	

