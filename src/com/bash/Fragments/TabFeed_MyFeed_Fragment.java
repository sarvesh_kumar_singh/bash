package com.bash.Fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bash.R;
import com.bash.Application.BashApplication;
import com.bash.ListModels.BashUsers_Class;
import com.bash.ListModels.MyGroups_Class;
import com.bash.ListModels.NewsFeed_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;
import com.bash.Utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

public class TabFeed_MyFeed_Fragment extends Fragment implements AsyncResponse{
	View view;
	int pos=0;
	String commentaction="";
	MyAsynTaskManager  myAsyncTask;
	View RootView;
	ListView myfeedListView;
	MyFeedAdapter adapter;
	ArrayList<NewsFeed_Class> feedList = new ArrayList<NewsFeed_Class>();
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		// TODO Auto-generated method stub
		RootView = inflater.inflate(R.layout.fragment_feed_page, null);
		//RootView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		return RootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initializeView();
	}
	
	// empty constructor
	public TabFeed_MyFeed_Fragment(){
		
	}
	
	private void initializeView() {

		myfeedListView = (ListView) RootView.findViewById(R.id.generalListView);
 		
 		switch (TabFeedFragment.mFeedTabhost.getCurrentTab()+1) {
		case 1:
			getNewsFeedListByType("getlatest");
			break;
		case 2:
			getNewsFeedListByType("getmyfeed");
			break;
		case 3:
			getNewsFeedListByType("gettrending");
			break;
		default:
			break;
		}
 		
		adapter = new MyFeedAdapter(getActivity(), feedList);
		myfeedListView.setAdapter(adapter);
	}

 	public void getNewsFeedListByType(String type) {
		 //getlatest
 		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("getNewsFeedListByType",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser"
			 	}, 
				new String[]{
				"feed",
				type,
				PreferenceManager.getInstance().getUserId()
				});
		myAsyncTask.execute();
		
	 
 	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		if(from.equalsIgnoreCase("getNewsFeedListByType"))
				{
					if(output!=null)
					{

						try{
							JSONObject rootObj = new JSONObject(output);
							//new Transaction_Class(paidByName, paidForName, paymentComment, paymentDate, payerImage, transactionId, status, paymentLocation)
							if(rootObj.getBoolean("result")){

								JSONArray jsonArray = rootObj.getJSONArray("feedlist");
								JSONObject jObj;
								ArrayList<NewsFeed_Class> newList = new ArrayList<NewsFeed_Class>();
								for(int i = 0; i<jsonArray.length(); i++){
									jObj = jsonArray.getJSONObject(i);

									newList.add(new NewsFeed_Class(
											jObj.getString("idtrans"),
											jObj.getString("feedpath"),
											jObj.getString("imagepath"),
											jObj.getString("paid_for"),
											jObj.getString("comment_count"),
											jObj.getString("recorded_on"),
											jObj.getString("feed_comment"),
											jObj.getString("location"),
											jObj.getString("likes_count"),
											jObj.getString("paid_by"),
											jObj.getString("isliked"),
											jObj.getString("type"),
											jObj.getString("idpaid_by"),
											jObj.getString("idpaid_for"),
											jObj.getString("status")
											));
								}
								adapter.notifyWithDataSet(newList);
								//adapter.notifyDataSetChanged();
							}
						} catch (Exception e) {
							// TODO: handle exception
							DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
						}

					}
					else
					{

						// TODO: handle exception
						DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
					
					}
				}
	}
 
 	public class MyFeedAdapter extends BaseAdapter
 	{
 			public ArrayList<NewsFeed_Class> feedList = new ArrayList<NewsFeed_Class>();
 			public Activity context;
 			public int msPerHour = 1000*60*60;
 		    
 		    public MyFeedAdapter(Activity context, ArrayList<NewsFeed_Class> feedList) {
 		        this.context = context;
 		    	this.feedList = feedList;
 		    }

 		    public void notifyWithDataSet(ArrayList<NewsFeed_Class> newList) {
 		    	this.feedList = newList;
 		    	this.notifyDataSetChanged();
 		    }
 		    
 		    @Override
 		    public View getView(final int position, View convertView, ViewGroup parent) {
 		        ViewHolder holder = null;
 		        final NewsFeed_Class listItem = getItem(position);
 		        if (convertView == null) {
 		        	LayoutInflater inflater = LayoutInflater.from(context);
 		        	convertView = inflater.inflate(R.layout.custom_feed_myfeed_listviewnew, null);
 		        	holder = new ViewHolder();
 		        	holder.numberofLikes = (TextView) convertView.findViewById(R.id.numberofLikes);
 		        	holder.numberofComments = (TextView) convertView.findViewById(R.id.numberofComments);
 		        	holder.resonforPayment = (TextView) convertView.findViewById(R.id.resonforPayment);
 		        	holder.amoutPaidBy = (TextView) convertView.findViewById(R.id.amoutPaidBy);
 		        	holder.amoutPaidFor = (TextView) convertView.findViewById(R.id.amoutPaidFor);
 		        	holder.placeofPayment = (TextView) convertView.findViewById(R.id.placesofPayment);
 		            holder.timeofComment = (TextView) convertView.findViewById(R.id.timeofComment);
 		            holder.paidText = (TextView) convertView.findViewById(R.id.paidText);
 		            holder.isLikeButton  = (ImageView) convertView.findViewById(R.id.likesButton);
 		            holder.userImage = (ImageView) convertView.findViewById(R.id.userImage);
 		            holder.feedImage = (ImageView) convertView.findViewById(R.id.feedImage);
 		            holder.commentBox = (LinearLayout) convertView.findViewById(R.id.commentBox);
 		            holder.likesBox = (LinearLayout) convertView.findViewById(R.id.likesBox);
 		            convertView.setTag(holder);
 		        } else {
 		            holder = (ViewHolder) convertView.getTag();
 		        }

 		        holder.resonforPayment.setText("for "+ (listItem.getFeedComment().length() < 25 ?  listItem.getFeedComment() : listItem.getFeedComment().substring(0, 24)+"..."));
 		        holder.amoutPaidBy.setText(listItem.getPaidBy().length() < 7 ?  listItem.getPaidBy() : listItem.getPaidBy().substring(0, 6)+".. ");
 		        holder.amoutPaidFor.setText(listItem.getPaidFor().length() < 7 ?  listItem.getPaidFor() : listItem.getPaidFor().substring(0, 6)+".. ");
 		        		
 	        	/*holder.resonforPayment.setText(Html.fromHtml(
 	        			"<font color='#00BDCC' size='16'>"+ listItem.getPaidBy() + "</font>" + " paid "
 	        			+"<font color='#00BDCC' size='16'>"+ listItem.getPaidFor() + "</font> for "
 	        			+ listItem.getFeedComment()
 		        		//+ " for the delicious cheese sandwitch. Cheers to the good times."
 		        		));*/
 		        if(listItem.getStatus().equals("pending")){
 		        	holder.paidText.setText(" charged ");
 		        }else{
 		        	holder.paidText.setText(" paid ");
 		        }
 		        
 		        holder.amoutPaidBy.setOnClickListener(new OnClickListener() {
 					@Override
 					public void onClick(View v) {
 						if(!listItem.getIdPaidBy().equals(PreferenceManager.getInstance().getUserId()))
 							HomeFragment.parentFragment.replaceFragment(new MyFriends_Transaction_Fragment(
 									new BashUsers_Class(listItem.getIdPaidBy(), 
 											" ",
 											listItem.getPaidBy(), 
 											"FALSE", 
 											"FALSE", 
 											"FALSE",
 											"TRUE")
 									),true);
 						}
 				});
 		        
 		        holder.amoutPaidFor.setOnClickListener(new OnClickListener() {
 					@Override
 					public void onClick(View v) {
 						
 						if(listItem.getType().equals("friend") && !listItem.getIdPaidFor().equals(PreferenceManager.getInstance().getUserId()))
 							HomeFragment.parentFragment.replaceFragment(new MyFriends_Transaction_Fragment(
 									new BashUsers_Class(listItem.getIdPaidFor(), 
 											" ",
 											listItem.getPaidFor(), 
 											"FALSE", 
 											"FALSE", 
 											"FALSE",
 											"TRUE")),true);
 						
 						else if(listItem.getType().equals("group") && !listItem.getIdPaidFor().equals(PreferenceManager.getInstance().getUserId()))
 							HomeFragment.parentFragment.replaceFragment(new MyGroups_Transaction_Fragment(
 									new MyGroups_Class(listItem.getPaidFor(), 
 											listItem.getIdPaidFor(), 
 											listItem.getImagepath())),true);
 					}
 				});
 		        
 		        holder.numberofLikes.setText(listItem.getLikeCount()+" Likes");
 		        
 		        holder.numberofComments.setText(listItem.getCommentsCount()+" Comments");
 		        
 		        if(listItem.getLocation() != null && listItem.getLocation().length() != 0)
 		        	holder.placeofPayment.setText("At "+listItem.getLocation());
 		        else
 		        	holder.placeofPayment.setVisibility(View.INVISIBLE);
 		        
 		        Log.e("Timer Text ", listItem.getRecordedOn());
 		        
 		        holder.timeofComment.setText(Utils.getFormattedTimerText(listItem.getRecordedOn()));
 		        
 		        holder.commentBox.setOnClickListener(new OnClickListener() {
 					
 					@Override
 					public void onClick(View v) {
 						// TODO Auto-generated method stub
 						HomeFragment.parentFragment.replaceFragment(new TabFeed_MyFeed_Comment_Fragment(listItem), true);
 					}
 				});
 	 

 		        
 		        /*MyFriends_Transaction_Fragment
 		        MyFriends_Transaction_Fragment*/
 		        
 		        if(listItem.getIsLiked().equals("1"))
 		        	holder.isLikeButton.setImageResource(R.drawable.like_icon_selected);
 		        else
 		        	holder.isLikeButton.setImageResource(R.drawable.like_icon_unselected);
 		        
 		        final TextView numberOfLikeView = holder.numberofLikes;
 		        
 		     view = convertView;
 		        
 	 	        holder.likesBox.setOnClickListener(new OnClickListener() {
 					@Override
 					public void onClick(View v) {
 						if(listItem.getIsLiked().equals("1")){
 							commentaction="unlikefeed";
 							pos=position;
 							makeAnLike(listItem.getIdTrans(), view, pos, commentaction);
 						}else{
 							commentaction="feedlike";
 							pos=position;
 							makeAnLike(listItem.getIdTrans(), view, pos, commentaction);	
 						}
 					}
 				});
 		        
 	 	        final ImageView myuserImage = holder.userImage;
 		        
 		        if(listItem.getImagepath() != null && listItem.getImagepath().length() != 0){
 		        	ImageLoader.getInstance().displayImage(listItem.getImagepath(), 
 		        			holder.userImage, BashApplication.options, BashApplication.animateFirstListener);
 		        }else{
 		        	holder.userImage.setImageResource(R.drawable.addphoto_img_block);
 		        }
 		  
 		        if(listItem.getFeedPath() != null && listItem.getFeedPath().length() != 0){
 		        	holder.feedImage.setVisibility(View.VISIBLE);
 		        	ImageLoader.getInstance().displayImage(listItem.getFeedPath(), holder.feedImage);
 		        }else{
 		        	holder.feedImage.setVisibility(View.GONE);
 		        }
 	 	        
 		        return convertView;
 		    }

 		    @Override
 		    public int getCount() {
 		        return feedList.size();
 		    }

 		    @Override
 		    public NewsFeed_Class getItem(int position) {
 		        return feedList.get(position);
 		    }

 		    @Override
 		    public long getItemId(int position) {
 		        return position;
 		    }
 		    
 		    public class ViewHolder {
 		        TextView numberofLikes;
 		        TextView numberofComments;
 		        TextView resonforPayment;
 		        TextView amoutPaidBy;
 		        TextView amoutPaidFor;
 		        TextView placeofPayment;
 		        TextView timeofComment;
 		        TextView paidText;
 		        ImageView isLikeButton;
 		        ImageView userImage, feedImage;
 		        LinearLayout commentBox, likesBox;
 		    }
 		    
 		    
 	}
 	public void makeAnLike(String commetnId, final View view, final int position, final String commentAction){
	    	//commentaction=commentAction;
	    	myAsyncTask=new MyAsynTaskManager();
			myAsyncTask.delegate=this;
	    	myAsyncTask.setupParamsAndUrl("makeAnLike",getActivity(),AppUrlList.ACTION_URL, new String[] {
					"module",
					"action",
					"idtrans",
					"iduser"
				 	}, 
					new String[]{
					"feed",
					commentAction,
					commetnId,
					PreferenceManager.getInstance().getUserId()
					});
myAsyncTask.execute();

	    }
}
