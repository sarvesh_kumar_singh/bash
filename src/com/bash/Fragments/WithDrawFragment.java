package com.bash.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bash.R;
import com.bash.Activities.HomeActivity;

public class WithDrawFragment extends Fragment{

	View mRootView;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_mywallet_withdraw_page, null);
		initializeView();
		return mRootView;
	}
	
	private void initializeView() {
		// TODO Auto-generated method stub
		// ((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setBackgroundResource(0);
		 ((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "Withdraw", 0);
		 ((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					WalletFragment.parentFragmentClass.popFragment();
				}
		 });
	
		 ((RelativeLayout) mRootView.findViewById(R.id.bankOptionView)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(((RadioGroup) mRootView.findViewById(R.id.bankGroup)).getVisibility() == View.VISIBLE){
						((RadioGroup) mRootView.findViewById(R.id.bankGroup)).setVisibility(View.GONE);
						((ImageView) mRootView.findViewById(R.id.bankIndicator)).setImageResource(R.drawable.downarrow);
						
					}else{
						((RadioGroup) mRootView.findViewById(R.id.bankGroup)).setVisibility(View.VISIBLE);
						((ImageView) mRootView.findViewById(R.id.bankIndicator)).setImageResource(R.drawable.uparrow);
					}
				}
		 });
	
		 ((RelativeLayout) mRootView.findViewById(R.id.cardOptionView)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(((RadioGroup) mRootView.findViewById(R.id.cardGroup)).getVisibility() == View.VISIBLE){
						((RadioGroup) mRootView.findViewById(R.id.cardGroup)).setVisibility(View.GONE);
						((ImageView) mRootView.findViewById(R.id.cardIndicator)).setImageResource(R.drawable.downarrow);
					}else{
						((RadioGroup) mRootView.findViewById(R.id.cardGroup)).setVisibility(View.VISIBLE);
						((ImageView) mRootView.findViewById(R.id.cardIndicator)).setImageResource(R.drawable.uparrow);
					}
				}
		 });
		 
		 
		 ((RadioGroup) mRootView.findViewById(R.id.cardGroup)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				switch (checkedId) {
				case R.id.cardButton1:
					setupFieldforCard("My Personal Card", "4441-2121-2121-3333", "MYNAME", "VISA");
					break;
				case R.id.cardButton2:
					setupFieldforCard("My Office Card", "4441-2121-2121-4444", "MYOFFICE", "MESTRO");
					break;
				case R.id.cardButton3:
					setupFieldforCard("My home Card", "4441-2377-2121-5555", "MYHOME", "MESTRO");
					break;
				case R.id.cardButton4:
					setupFieldforCard("My friends Card", "4441-2121-2121-6666", "MYFRIENDS", "VISA");
					break;
				case R.id.cardButton5:
					setupFieldforCard("My OWN Card", "4441-2121-2121-7777", "MYOWN", "VISA");
					break;
					
				default:
					break;
				}
				
				((LinearLayout) mRootView.findViewById(R.id.bankView)).setVisibility(View.GONE);		
				((LinearLayout) mRootView.findViewById(R.id.cardView)).setVisibility(View.VISIBLE);
			}
		 });
		 
		 
		 ((RadioGroup) mRootView.findViewById(R.id.bankGroup)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					// TODO Auto-generated method stub
					switch (checkedId) {
					case R.id.bankButton1:
						setupFieldforBank("INDIAN BANK", "1112457", "MY INDIAN BANK", "SAIDAPTET");
						break;
					case R.id.bankButton2:
						setupFieldforBank("HDFC BANK", "221545", "MY HDFC BANK", "THEYNAMPET");
						break;
					case R.id.bankButton3:
						setupFieldforBank("INDIAN OVERSEAS BANK", "4546464641", "MY INDIAN OVERSEAS BANK", "KATPADI");
						break;
					case R.id.bankButton4:
						setupFieldforBank("IDBI BANK", "78797856456", "MY IDBI BANK", "SAIDAPTET");
						break;
					case R.id.bankButton5:
						setupFieldforBank("SBI BANK", "313271465", "MY SBI BANK", "SAIDAPTET");
						break;
					default:
						break;
					}
					
					((LinearLayout) mRootView.findViewById(R.id.bankView)).setVisibility(View.VISIBLE);		
					((LinearLayout) mRootView.findViewById(R.id.cardView)).setVisibility(View.GONE);
				}
			 });
		 
		 ((RadioButton) mRootView.findViewById(R.id.bankButton1)).setChecked(true);
		 ((LinearLayout) mRootView.findViewById(R.id.bankView)).setVisibility(View.VISIBLE);		
		 ((LinearLayout) mRootView.findViewById(R.id.cardView)).setVisibility(View.GONE);
		 setupFieldforBank("INDIAN BANK", "1112457", "MY INDIAN BANK", "SAIDAPTET");
	}
	
	public void setupFieldforCard(String cardNickName, String cardNumberText, String nameOnCardText, String cardTypeText) {
		((TextView) mRootView.findViewById(R.id.cardNumberText)).setText(cardNumberText);
		((TextView) mRootView.findViewById(R.id.nameOnCardText)).setText(nameOnCardText);
		((TextView) mRootView.findViewById(R.id.cardNickName)).setText(cardNickName);
		((TextView) mRootView.findViewById(R.id.cardTypeText)).setText(cardTypeText);
	}
	
	public void setupFieldforBank(String bankNameText, String bankAccountNumberText,
			String bankNickNameText, String bankBranchNameText) {
		((TextView) mRootView.findViewById(R.id.bankNameText)).setText(bankNameText);
		((TextView) mRootView.findViewById(R.id.bankAccountNumberText)).setText(bankAccountNumberText);
		((TextView) mRootView.findViewById(R.id.bankNickNameText)).setText(bankNickNameText);
		((TextView) mRootView.findViewById(R.id.bankBranchNameText)).setText(bankBranchNameText);
	}
	
}
