package com.bash.Fragments;

import android.annotation.SuppressLint;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.bash.R;
import com.bash.Providers.ContainerProvider;
import com.bash.Utils.AppConstants;

@SuppressLint("ValidFragment")
public class TabFeedFragment extends Fragment{
	
	private View mRootView;
	public static FragmentTabHost mFeedTabhost;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_home, null);
		mFeedTabhost = (FragmentTabHost)mRootView.findViewById(R.id.fragmenttabhost);
		mFeedTabhost.setup(getActivity(), getChildFragmentManager(), R.id.fragmentframe_holder);
		initializeTabHost();
		return mRootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
	}
	
	private void initializeTabHost() {
		// TODO Auto-generated method stub
		View MyLastetView = LayoutInflater.from(getActivity()).inflate(R.layout.cell_feed_myfeed_bg, null);
		View MyActivityView = LayoutInflater.from(getActivity()).inflate(R.layout.cell_feed_myactivity_bg, null);
		View MyTrendingView = LayoutInflater.from(getActivity()).inflate(R.layout.cell_feed_trending_bg, null);
		
		mFeedTabhost.setBackgroundColor(Color.parseColor("#FFFFFF"));
		
		mFeedTabhost.addTab(mFeedTabhost.newTabSpec(AppConstants.TAB_FEED_LATEST_TAG).setIndicator(MyLastetView),
				ContainerProvider.Tab_Feed_MyFeed_Container.class, null);
		
		mFeedTabhost.addTab(mFeedTabhost.newTabSpec(AppConstants.TAB_FEED_MYACTIVITY_TAG).setIndicator(MyActivityView),
				ContainerProvider.Tab_Feed_MyFeed_Container.class, null);
		
		mFeedTabhost.addTab(mFeedTabhost.newTabSpec(AppConstants.TAB_FEED_TRENDING_TAG).setIndicator(MyTrendingView),
				ContainerProvider.Tab_Feed_MyFeed_Container.class, null);
		
	}
	
	 
	
}
