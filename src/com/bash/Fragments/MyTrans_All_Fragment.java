package com.bash.Fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Adapters.TransactionAdapter;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.ListModels.Transaction_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;

public class MyTrans_All_Fragment extends BaseFragment implements AsyncResponse{
	MyAsynTaskManager  myAsyncTask;
	public View mRootView;
	ListView cashInListView;
	TransactionAdapter adapter;
	ArrayList<Transaction_Class> feedList = new ArrayList<Transaction_Class>();
	
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_cashin_page, null);
		initializeView();
		return mRootView;
	}
	
	private void initializeView() {
		// TODO Auto-generated method stub
		//Initialize Activity Views
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "All Transactions", 0);
		
		((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				HomeFragment.parentFragment.popFragment();
			}
		});
		
		cashInListView = (ListView)mRootView.findViewById(R.id.cashInListView);
		
		adapter = new TransactionAdapter(getActivity(), feedList);
		cashInListView.setAdapter(adapter);
		
		cashInListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if(feedList.get(position).paidForName.equals(PreferenceManager.getInstance().getUserFullName()))
					((BaseFragment)getParentFragment()).replaceFragment(new MyTrans_All_PayFriendFragment(feedList.get(position)),
							true);
			}
		});
		
		getAllTransactionDetails();
	}
	
	
	public void getAllTransactionDetails(){
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this; 
		myAsyncTask.setupParamsAndUrl("getAllTransactionDetails",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser"
			 	}, 
				new String[]{
				"feed",
				"alltransaction",
				PreferenceManager.getInstance().getUserId()
				});
myAsyncTask.execute();
//		new MyAsynTaskManager(getActivity(), new LoadListener() {
//			@Override
//			public void onLoadComplete(final String jsonResponse) {
//				getActivity().runOnUiThread(new Runnable() {
//					@Override
//					public void run() {}
//				});
//			}
//			@Override
//			public void onError(final String errorMessage)  {
//				getActivity().runOnUiThread(new Runnable() {
//					public void run() {
//						DialogManager.showDialog(getActivity(), errorMessage);		
//					}
//				});
//				
//			}
//		}).execute();
	 
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		if(from.equalsIgnoreCase("getAllTransactionDetails"))
		{
			if(output!=null)
			{

				try{
					JSONObject rootObj = new JSONObject(output);
					//new Transaction_Class(paidByName, paidForName, paymentComment, paymentDate, payerImage, transactionId, status, paymentLocation)
					if(rootObj.getBoolean("result")){
						
						JSONArray jsonArray = rootObj.getJSONArray("feedlist");
						JSONObject jObj;
						for(int i = 0; i<jsonArray.length(); i++){
							jObj = jsonArray.getJSONObject(i);
							feedList.add(new Transaction_Class(
									jObj.getString("paid_by"),
									jObj.getString("paid_for"),
									jObj.getString("feed_comment"),
									jObj.getString("recorded_on"),
									jObj.getString("imagepath"),
									jObj.getString("idtrans"),
									jObj.getString("status"),
									jObj.getString("location"),
									jObj.getString("amount")));
							}
						
						adapter.notifyDataSetChanged();
					}
				} catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
				}
		
			}
			else
			{
				// TODO: handle exception
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			}
		}
		
	}
	
	
	 
			
	}
	
	
	

