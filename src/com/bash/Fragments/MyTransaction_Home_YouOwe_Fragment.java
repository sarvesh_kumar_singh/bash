package com.bash.Fragments;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Providers.ContainerProvider;
import com.bash.Utils.AppConstants;

@SuppressLint("ValidFragment")
public class MyTransaction_Home_YouOwe_Fragment extends Fragment {
	
	View mRootView;
	public static ExpandableListView generalView;
	
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.fragment_friendgroup_page, null);
		initializeTabHost();
		return mRootView;
		
		/*mRootView = inflater.inflate(R.layout.fragment_home, null);
		mTabhost = (FragmentTabHost) mRootView.findViewById(R.id.fragmenttabhost);
		mTabhost.setup(getActivity(), getChildFragmentManager(), R.id.fragmentframe_holder);
		initializeTabHost();
		return mRootView;*/
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}
	
	private void initializeTabHost() {
		generalView = (ExpandableListView) mRootView.findViewById(R.id.generalView);
		//generalView.setAdapter(MyTrans_Pending_Fragment.youowe_friendadapter);
		generalView.setAdapter(HomeActivity.youowe_friendadapter);
		//generalView.setAdapter(MyTrans_Pending_Fragment.getYouOweAdapter());
	}
	
}