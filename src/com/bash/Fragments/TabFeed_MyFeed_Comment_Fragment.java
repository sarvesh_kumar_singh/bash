package com.bash.Fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Application.BashApplication;
import com.bash.ListModels.MyFeedComment_Class;
import com.bash.ListModels.NewsFeed_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;
import com.bash.Utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

@SuppressLint("ValidFragment")
public class TabFeed_MyFeed_Comment_Fragment extends Fragment implements AsyncResponse{
	String commentactionview="";
	View view;
	int pos=0;
	MyAsynTaskManager  myAsyncTask;
	View mRootView;
	ListView myfeedListView;
	MyFeedCommentAdapter adapter;
	ArrayList<MyFeedComment_Class> feedList = new ArrayList<MyFeedComment_Class>();
	public NewsFeed_Class transactionDetails;
	ImageView commentLikeButton;
	String commentaction="";
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		mRootView = inflater.inflate(R.layout.fragment_feed_comment_page, null);
		return mRootView;
	}
	
	public TabFeed_MyFeed_Comment_Fragment(){
		
	}
	
	public TabFeed_MyFeed_Comment_Fragment(NewsFeed_Class transactionDetails) {
		this.transactionDetails = transactionDetails;
		Log.e("Transaction Id", transactionDetails.getIdTrans());
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initializeView();
	}
	
	private void initializeView() {
		
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "Add Comment", 0);
		
		((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				HomeFragment.parentFragment.popFragment();
			}
		});
		
		//ImageLoader.getInstance().displayImage( transactionDetails.getImagepath(), ((ImageView)mRootView.findViewById(R.id.userImage)));
		
		ImageLoader.getInstance().displayImage(transactionDetails.getImagepath(), 
				((ImageView)mRootView.findViewById(R.id.userImage)), BashApplication.options, BashApplication.animateFirstListener);
				
		/*ImageLoader.getInstance().displayImage(transactionDetails.getImagepath(), 
				((ImageView)mRootView.findViewById(R.id.userImage)), BashApplication.options, BashApplication.animateFirstListener);*/
		
		/*ImageLoader.getInstance().loadImage(transactionDetails.getImagepath(), new ImageLoadingListener() {
			
			@Override
			public void onLoadingStarted(String arg0, View arg1) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
				((ImageView)mRootView.findViewById(R.id.userImage)).setImageBitmap(CurvedImageBitmapProvider.getRoundedImageBitmap(arg2));
			}
			
			@Override
			public void onLoadingCancelled(String arg0, View arg1) {
				// TODO Auto-generated method stub
				
			}
		});
		
		*/
		
/*		ImageLoader.getInstance().loadImage(transactionDetails.getImagepath(), new SimpleImageLoadingListener() {
    	    @Override
    	    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
    	    	((ImageView)mRootView.findViewById(R.id.userImage)).setImageBitmap(CurvedImageBitmapProvider.getRoundedImageBitmap(loadedImage));
    	    }
    	});
		*/
		/*((ImageView)mRootView.findViewById(R.id.userImage)).setImageBitmap(CurvedImageBitmapProvider.getRoundedCornerBitmap(
        		(BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.addphoto_img_block)), 10));*/
		
		((TextView) mRootView.findViewById(R.id.resonforPayment)).setText(Html.fromHtml(
       			"<font color='#00BDCC' size='16'>"+ "JohnDoe" + "</font>" + " paid "
       			+"<font color='#00BDCC' size='16'>"+ "Jane White" + "</font>"
	        		+ " for the delicious cheese sandwitch. Cheers to the good times."
	        		));
		   
		commentLikeButton =  (ImageView)mRootView.findViewById(R.id.commentLikeButton);
		
		if(transactionDetails.getIsLiked().equals("1"))
			commentLikeButton.setImageResource(R.drawable.like_btn_select);
		else
			commentLikeButton.setImageResource(R.drawable.like_btn_deselect);
		
		
		commentLikeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(transactionDetails.getIsLiked().equals("1")){
					commentaction="unlikefeed";
					makeAnLike(transactionDetails.getIdTrans(), "unlikefeed");
				}else{
					commentaction="feedlike";
					makeAnLike(transactionDetails.getIdTrans(), "feedlike");	
				}
				
			}
		});
		
		myfeedListView = (ListView)mRootView.findViewById(R.id.commentsListView);
		
		adapter = new MyFeedCommentAdapter(getActivity(), feedList);
		
		myfeedListView.setAdapter(adapter);
		
		//((TextView) mRootView.findViewById(R.id.placeofPayment)).setText(transactionDetails.getLocation());
		
		((TextView) mRootView.findViewById(R.id.resonforPayment)).
				  setText(Html.fromHtml(
        			"<font color='#00BDCC' size='16'>"+ transactionDetails.getPaidBy() + "</font>" + " paid "
        			+"<font color='#00BDCC' size='16'>"+ transactionDetails.getPaidFor() + "</font> for "
        			+ transactionDetails.getFeedComment()));
		
		((TextView) mRootView.findViewById(R.id.likesCountText)).setText(transactionDetails.getLikeCount());	
		//((TextView) mRootView.findViewById(R.id.commentsCount)).setText(transactionDetails.getCommentsCount());
		
		((ImageView) getActivity().findViewById(R.id.makeCommentButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(((EditText) mRootView.findViewById(R.id.commentEditText)).getText() != null && 
						((EditText) mRootView.findViewById(R.id.commentEditText)).getText().toString().length() != 0)
					makeAnComment(((EditText) mRootView.findViewById(R.id.commentEditText)).getText().toString()
							);
				else
					Toast.makeText(getActivity(), "Please Enter Any Comment In Box!", Toast.LENGTH_SHORT).show();
			}
		});
		
		/*if(PreferenceManager.getInstance().getUserImagePath() != null && 
				PreferenceManager.getInstance().getUserImagePath().length() != 0)
			ImageLoader.getInstance().displayImage(PreferenceManager.getInstance().getUserImagePath(), ((ImageView) mRootView.findViewById(R.id.userImage)));
		*/
		getCommentList();
	}
	
	public void makeAnLike(String commetnId, final String commentAction) {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
    	myAsyncTask.setupParamsAndUrl("makeAnLike",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"idtrans",
				"iduser"
			 	}, 
				new String[]{
				"feed",
				commentAction,
				commetnId,
				PreferenceManager.getInstance().getUserId()
				});

    	myAsyncTask.execute();
    }
	
	
	
	public void makeAnComment(String commentText) {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("makeAnComment",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"idtrans",
				"iduser",
				"comment"
			 	}, 
				new String[]{
				"feed",
				"addcomment",
				 transactionDetails.getIdTrans(),
				 PreferenceManager.getInstance().getUserId(),
				 commentText
				});

	myAsyncTask.execute();
	 
	}
	
	public void getCommentList() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		 //getlatest
		myAsyncTask.setupParamsAndUrl("getCommentList",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"idtrans",
				"iduser"
				}, 
				new String[]{
				"feed",
				"getcomments",
				 transactionDetails.getIdTrans(),
				 PreferenceManager.getInstance().getUserId()
				});
myAsyncTask.execute();
	 
	}
	public class MyFeedCommentAdapter extends BaseAdapter
	{
		 	private ArrayList<MyFeedComment_Class> feedList = new ArrayList<MyFeedComment_Class>();
			private Activity context;
		    
		    public MyFeedCommentAdapter(Activity context, ArrayList<MyFeedComment_Class> feedList) {
		        this.context = context;
		    	this.feedList = feedList;
		    }
		    
		    public void notifyDataSet(){
		    	this.notifyDataSetChanged();
		    }
		    
		    public void notifyWithDataSet(ArrayList<MyFeedComment_Class> newList){
		    	this.feedList = newList;
		    	this.notifyDataSetChanged();
		    }

		    @Override
		    public View getView(final int position, View convertView, ViewGroup parent) {
		        ViewHolder holder = null;
		        final MyFeedComment_Class listItem = getItem(position);
		        if (convertView == null) {
		        	LayoutInflater inflater = LayoutInflater.from(context);
		        	convertView = inflater.inflate(R.layout.custom_feed_myfeed_comments_new, null);
		        	holder = new ViewHolder();
		        	
		           	holder.commentantName = (TextView) convertView.findViewById(R.id.commentantName);
		        	holder.commentText = (TextView) convertView.findViewById(R.id.commentText);
		        	holder.commentLikeCount = (TextView) convertView.findViewById(R.id.commentLikeCount);
		        	holder.timeofComment = (TextView) convertView.findViewById(R.id.timeofComment);
		        	holder.commentLikeButton  = (ImageView) convertView.findViewById(R.id.commentLikeButton);
		        	holder.commentantImage  = (ImageView) convertView.findViewById(R.id.commentantImage);
		        	holder.commentLikeBox  = (LinearLayout) convertView.findViewById(R.id.commentLikeBox);
		            convertView.setTag(holder);
		            
		        } else {
		            holder = (ViewHolder) convertView.getTag();
		        }

		        holder.commentantName.setText(listItem.getcommentantName());
		        holder.commentText.setText(listItem.getcommentText());
		        holder.commentLikeCount.setText(listItem.getcommentLikeCount()+" Likes");
		        //holder.commentantImage.setImageResource(listItem.getcommentantImage());
		        
		        final ImageView commentantImageView = holder.commentantImage; 
		        
		        ImageLoader.getInstance().displayImage(listItem.getcommentantImage(), 
	        			holder.commentantImage, BashApplication.options, BashApplication.animateFirstListener);
		        
		        if(listItem.getcommentantImage() != null && listItem.getcommentantImage().length() != 0){
		        	ImageLoader.getInstance().displayImage(listItem.getcommentantImage(), 
		        			holder.commentantImage, BashApplication.options, BashApplication.animateFirstListener);
		        	}
		        else
		        	holder.commentantImage.setImageResource(R.drawable.addphoto_img_block);
		        
		        holder.timeofComment.setText(listItem.gettimeofComment());
		        
		        
		        if(listItem.getisLiked().equals("1"))
		        	holder.commentLikeButton.setImageResource(R.drawable.like_icon_selected);
		        else
		        	holder.commentLikeButton.setImageResource(R.drawable.like_icon_unselected);
		     
		       // final View view = convertView;
		        view=convertView;
		        
		        holder.commentLikeBox.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if(listItem.getisLiked().equals("1")){
							pos=position;
							commentactionview="unlikecomment";
							makeAnLikeView(listItem.getcommentId(), view, commentactionview, pos);
						}else{
							pos=position;
							commentactionview="commentlike";
							makeAnLikeView(listItem.getcommentId(), view, commentactionview, pos);
						}
						
					}
				});
		        
		         
		        
		        
		     /*   holder.commentantImage.setImageBitmap(CurvedImageBitmapProvider.getRoundedCornerBitmap(
		        		(BitmapFactory.decodeResource(context.getResources(), R.drawable.addphoto_img_block)), 10));*/
		        
		       /* if(listItem.isLike)
		        	holder.isLikeButton.setChecked(true);
		        else
		        	holder.isLikeButton.setChecked(false);
		        
		        if(listItem.isComment)
		        	holder.isCommentButton.setChecked(true);
		        else
		        	holder.isCommentButton.setChecked(false);*/
		        return convertView;
		    }
	 
		    @Override
		    public int getCount() {
		        return feedList.size();
		    }

		    @Override
		    public MyFeedComment_Class getItem(int position) {
		        return feedList.get(position);
		    }

		    @Override
		    public long getItemId(int position) {
		        return position;
		    }
		    
		    public class ViewHolder {
		        TextView commentantName;
		        TextView commentText;
		        TextView commentLikeCount;
		        TextView timeofComment;
		        ImageView commentantImage;
		        ImageView commentLikeButton;
		        LinearLayout commentLikeBox;
		    }
		    
		    
		    
		    
		    
		    
	 
		    
		    
	}
	public void makeAnLikeView(String commetnId, final View view, final String commentAction, final int position){
    	myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
    	myAsyncTask.setupParamsAndUrl("makeAnLikeView",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"idcomment",
				"iduser"
			 	}, 
				new String[]{
				"feed",
				commentAction,
				commetnId,
				PreferenceManager.getInstance().getUserId()
				});
    	myAsyncTask.execute();
	}
	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		if(from.equalsIgnoreCase("makeAnLike"))
		{
			if(output!=null)
			{

				try{
					JSONObject rootObj = new JSONObject(output);
					if(rootObj.getBoolean("result")){
						
						//likeTextView.setText(rootObj.getString("likes_count"));
						((TextView) mRootView.findViewById(R.id.likesCountText)).setText(rootObj.getString("likes_count"));
						
						transactionDetails.setLikeCount(((TextView) mRootView.findViewById(R.id.likesCountText)).getText().toString());
						
						if(commentaction.equals("unlikefeed")){
							transactionDetails.setIsLiked("0");
							commentLikeButton.setImageResource(R.drawable.like_btn_deselect);
						}else{
							transactionDetails.setIsLiked("1");
							commentLikeButton.setImageResource(R.drawable.like_btn_select);	
						}
						
						}
					}
				 catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
				}
				
			}
			else
			{
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			}
		}
		else if(from.equalsIgnoreCase("makeAnComment"))
		{
			if(output!=null)
			{
				try{
					
					JSONObject rootObj = new JSONObject(output);
					//new Transaction_Class(paidByName, paidForName, paymentComment, paymentDate, payerImage, transactionId, status, paymentLocation)
					if(rootObj.getBoolean("result")){
						JSONObject detailObject = rootObj.getJSONObject("detail");
						feedList.add(new MyFeedComment_Class(
								detailObject.getString("imagepath"), 
								detailObject.getString("firstname"), 
								detailObject.getString("likes_count"),
								detailObject.getString("comment"),
								Utils.getFormattedTimerText(detailObject.getString("recorded_on")),
								detailObject.getString("id"),
								"0"
								));
						}
					
				//		((TextView) mRootView.findViewById(R.id.commentsCount)).setText(String.valueOf(
				//				Integer.parseInt(transactionDetails.getCommentsCount()) + 1));
				//		transactionDetails.setCommentsCount(((TextView) mRootView.findViewById(R.id.commentsCount)).getText().toString());
						adapter.notifyWithDataSet(feedList);
						((EditText) mRootView.findViewById(R.id.commentEditText)).setText("");
						myfeedListView.setSelection(adapter.getCount());
						Toast.makeText(getActivity(), "Your comment has been posted!", Toast.LENGTH_SHORT).show();
						Utils.closeSoftInputBoard(((EditText) mRootView.findViewById(R.id.commentEditText)));
						
//						if(((LinearLayout)mRootView.findViewById(R.id.noImageWindow)).getVisibility() == View.VISIBLE)
//						{
//							((LinearLayout)mRootView.findViewById(R.id.noImageWindow)).setVisibility(View.GONE);
//						}
					}
				 catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
				}
			}
			else
			{
				
			}
		}
		else if(from.equalsIgnoreCase("getCommentList"))
		{
			if(output!=null)
			{

				try{
					JSONObject rootObj = new JSONObject(output);
					//new Transaction_Class(paidByName, paidForName, paymentComment, paymentDate, payerImage, transactionId, status, paymentLocation)
					if(rootObj.getBoolean("result")){
						
						JSONArray jsonArray = rootObj.getJSONArray("commentlist");
						JSONObject jObj;
						ArrayList<MyFeedComment_Class> newList = new ArrayList<MyFeedComment_Class>();
						for(int i = 0; i<jsonArray.length(); i++)
						{
							jObj = jsonArray.getJSONObject(i);
							newList.add(new MyFeedComment_Class
									(
									jObj.getString("imagepath"), 
									jObj.getString("firstname"), 
									jObj.getString("likes_count"),
									jObj.getString("comment"),
									Utils.getFormattedTimerText(jObj.getString("recorded_on")),
									jObj.getString("id"),
									jObj.getString("isliked")
									)
							);
							}
						feedList = newList;
						adapter.notifyWithDataSet(newList);
						
//						if(feedList.size() == 0)
//							((LinearLayout)mRootView.findViewById(R.id.noImageWindow)).setVisibility(View.VISIBLE);
//						else
//							((LinearLayout)mRootView.findViewById(R.id.noImageWindow)).setVisibility(View.GONE);
//						
					}
				} catch (Exception e) {
					e.printStackTrace();
					DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
				}
			
			}
			else
			{DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");}
		}
		else if(from.equalsIgnoreCase("makeAnLikeView"))
		{
			if(output!=null)
			{

				try{
					
					JSONObject rootObj = new JSONObject(output);
					if(rootObj.getBoolean("result")){

				        ((TextView) view.findViewById(R.id.commentLikeCount)).setText(rootObj.getString("likes_count"));
				        
				        feedList.get(pos).setcommentLikeCount(rootObj.getString("likes_count"));
				        
						if(commentactionview.equals("unlikecomment")){
							feedList.get(pos).setisLiked("0");
							((ImageView) view.findViewById(R.id.commentLikeButton)).setImageResource(R.drawable.like_btn_blue_deselect);
						}else{
							feedList.get(pos).setisLiked("1");
							((ImageView) view.findViewById(R.id.commentLikeButton)).setImageResource(R.drawable.like_btn_select);
						}
						adapter.notifyDataSetChanged();
						}else{
							Toast.makeText(getActivity(), "Error In Makeing Like/Unlike! Try Again!", Toast.LENGTH_SHORT).show();
						}
					}
				 catch (Exception e) {
					// TODO: handle exception
					DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
				}
			
			}
			else
			{
				// TODO: handle exception
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			}
		}
	}

	
	 
	
}
