package com.bash.Fragments;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Application.BashApplication;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.ListModels.MyCards_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;
public class MyCards_Fragment extends Fragment implements AsyncResponse{
	int deletecardPosition=0;
	MyAsynTaskManager  myAsyncTask;
	public static BaseFragment parentFragmentClass;
	public ArrayList<MyCards_Class>  feedList = new ArrayList<MyCards_Class>();
	public CardAdapter adapter;
	public ListView cardsListView;
	public int current_poisition = 0;
	View mRootView, deleteAlertView, changepasswordView, editView, addCardView;
	Dialog deleteDialog, changePasswordDialog, editDialog, addCardDialog;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		mRootView = inflater.inflate(R.layout.fragment_mycards, null);
		return mRootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initializeAlertView();
		initializeView();
		initializeEditAlertView();
		initializeAddCardAlertView();
	}
	
	private void initializeView()
	{
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn, "My Cards", 0);
		
		((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) 
			{
				//Sarvesh
				//((HomeActivity)getActivity()).menu.toggle();
				((HomeActivity)getActivity()).slidingmenu_layout.toggleMenu();
			}
		});
		((ImageView)getActivity().findViewById(R.id.toprightsideImage)).setImageResource(R.drawable.add_btn);
		((ImageView)getActivity().findViewById(R.id.toprightsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				addCardDialog.show();
			}
		});
		cardsListView  = (ListView)	mRootView.findViewById(R.id.cardsListView); 
		parentFragmentClass = ((BaseFragment)getParentFragment());
		feedList.clear();
		adapter = new CardAdapter();
		cardsListView.setAdapter(adapter);
		getMyCardDetails();
	}

 

public void getMyCardDetails() {
	myAsyncTask=new MyAsynTaskManager();
	myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("getMyCardDetails",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser"
				}, 
				new String[]{
				"user",
				"carddetails",
				PreferenceManager.getInstance().getUserId()
				}
		);
		myAsyncTask.execute();
		
	}



class CardAdapter extends BaseAdapter
{
	//public ArrayList<MyCards_Class>  adatperList = new ArrayList<MyCards_Class>();
	
	/*public CardAdapter(ArrayList<MyCards_Class> feedList) {
		super();
		this.adatperList = feedList;
	}
	
	public void notifyWithNewDataSet(ArrayList<MyCards_Class> newList){
		this.adatperList.clear();
		this.adatperList = newList;
		this.notifyDataSetInvalidated();
		this.notifyDataSetChanged();
	}
	*/
	  @Override
	    public int getCount() {
	        return feedList.size();
	    }

	    @Override
	    public MyCards_Class getItem(int position) {
	        return feedList.get(position);
	    }

	    @Override
	    public long getItemId(int position) {
	        return position;
	    }
	    
		public void removeObject(ArrayList<MyCards_Class> newlist){
			feedList = newlist;
			//feedList.remove(current_poisition);
			notifyDataSetChanged();
		}
		
	 	@Override
	    public View getView(final int position, View convertView, ViewGroup parent) {
	        ViewHolder holder = null;
	        final MyCards_Class listItem = getItem(position);
	        
	        if (convertView == null) {
	        	LayoutInflater inflater = LayoutInflater.from(getActivity());
	        	convertView = inflater.inflate(R.layout.custom_mycards_view, null);
	            holder = new ViewHolder();
	            holder.deleteButton = (ImageView) convertView.findViewById(R.id.deleteButton);
	            holder.editCardButton = (ImageView) convertView.findViewById(R.id.editCardButton);
	            holder.cardNumberText = (TextView) convertView.findViewById(R.id.cardNumberText);
	            holder.cardTypeText = (TextView) convertView.findViewById(R.id.cardTypeText);
	            holder.defaultCardText = (TextView) convertView.findViewById(R.id.defaultCardText);
	            convertView.setTag(holder);
	        } else {
	            holder = (ViewHolder) convertView.getTag();
	        }
	        holder.cardNumberText.setText(listItem.getCardNumber());
	        holder.cardTypeText.setText(listItem.getCardType());
	        if(listItem.getIsDefault())
	        	holder.defaultCardText.setVisibility(View.VISIBLE);
	        else
	        	holder.defaultCardText.setVisibility(View.GONE);
	        
	        holder.deleteButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					current_poisition = position;
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							deleteDialog.show();
						}
					});
					
				}
			});
	        
	        
	        holder.editCardButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					current_poisition = position;
					((EditText)editView.findViewById(R.id.cardNumberText)).setText(feedList.get(current_poisition).getCardNumber());
					if(feedList.get(current_poisition).getIsDefault())
						((CheckBox) editView.findViewById(R.id.defaultCheckBox)).setChecked(true);
					else
						((CheckBox) editView.findViewById(R.id.defaultCheckBox)).setChecked(false);
					editDialog.show();
				}
			});
	        
	        ((ImageView)deleteAlertView.findViewById(R.id.deleteImageButton)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					getActivity().runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							deletecardPosition=position;
							deleteUserCardDetails(position);
							deleteDialog.dismiss();
						}
					});
				}
			});
	        
	        return convertView;
	    }

	     private class ViewHolder {
	    	
	    	TextView cardNumberText;
	    	TextView cardTypeText;
	    	TextView defaultCardText;
	    	ImageView deleteButton, editCardButton;
	     
	     }
} 
	

private void initializeEditAlertView() {
	
	editDialog = new Dialog(getActivity());
	editDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	
	editView = View.inflate(getActivity(), R.layout.alert_editcarddetails, null);
	editDialog.setContentView(editView);
	
	((Button)editView.findViewById(R.id.updateCardDetails)).setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			if(((EditText)editView.findViewById(R.id.cardNumberText)).getText() != null && 
					((EditText)editView.findViewById(R.id.cardNumberText)).getText().length() != 0){
				if(((EditText)editView.findViewById(R.id.cardNumberText)).getText().length() != 16){
					Toast.makeText(getActivity(), "Please Enter 16 digit valid Card Details!", Toast.LENGTH_SHORT).show();
				}else{
					editDialog.dismiss();	
					updateCardNumberDetails();	
				}
				
			}else{
				Toast.makeText(getActivity(), "Please Enter Card Details!", Toast.LENGTH_SHORT).show();
			}
		}
	});
	
	dateSpinner = ((Spinner)editView.findViewById(R.id.dateSpinner)); 
	dateSpinner.setAdapter(BashApplication.daysAdapter);
	
	yearSpinner = ((Spinner)editView.findViewById(R.id.yearSpinner));
	yearSpinner.setAdapter(BashApplication.yearsAdapter);
	
	((ImageView)editView.findViewById(R.id.closeDialogImage)).setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			editDialog.dismiss();
		}
	});
	
}

public Spinner dateSpinner, yearSpinner, addCarddateSpinner, addCardyearSpinner;

private void initializeAlertView() {
	// TODO Auto-generated method stub
	deleteDialog = new Dialog(getActivity());
	deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	
	deleteAlertView = View.inflate(getActivity(), R.layout.alert_deleteaccount_dialog, null);
	deleteDialog.setContentView(deleteAlertView);
	
	/*((ImageView)deleteAlertView.findViewById(R.id.deleteImageButton)).setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			getActivity().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					deleteUserCardDetails();
					deleteDialog.dismiss();
				}
			});
		}
	});*/
	
	((ImageView)deleteAlertView.findViewById(R.id.cancelImageButton)).setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			deleteDialog.dismiss();
		}
	});
	
	((ImageView)deleteAlertView.findViewById(R.id.cancelDialog)).setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			deleteDialog.dismiss();
		}
	});
	
}


private void initializeAddCardAlertView() {
	 
	addCardDialog = new Dialog(getActivity());
	addCardDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	
	//addCardView = View.inflate(getActivity(), R.layout.alert_add_carddetails, null);
	addCardDialog.setContentView(addCardView);
	
	((Button)addCardView.findViewById(R.id.updateCardDetails)).setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			if(((EditText)addCardView.findViewById(R.id.cardNumberText)).getText() != null && 
					((EditText)addCardView.findViewById(R.id.cardNumberText)).getText().length() != 0){
				if(((EditText)addCardView.findViewById(R.id.cardNumberText)).getText().length() != 16){
					Toast.makeText(getActivity(), "Please Enter 16 digit valid Card Details!", Toast.LENGTH_SHORT).show();
				}else{
					addCardDialog.dismiss();	
					saveCardNumberDetails();	
				}
				
			}else{
				Toast.makeText(getActivity(), "Please Enter Card Details!", Toast.LENGTH_SHORT).show();
			}
		}
	});
	
	addCarddateSpinner = ((Spinner)addCardView.findViewById(R.id.dateSpinner)); 
	addCarddateSpinner.setAdapter(BashApplication.daysAdapter);
	
	addCardyearSpinner = ((Spinner)addCardView.findViewById(R.id.yearSpinner));
	addCardyearSpinner.setAdapter(BashApplication.yearsAdapter);
	
	((ImageView)addCardView.findViewById(R.id.closeDialogImage)).setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			addCardDialog.dismiss();
		}
	});
	
}


public void saveCardNumberDetails() {
	myAsyncTask=new MyAsynTaskManager();
	myAsyncTask.delegate=this;
	myAsyncTask.setupParamsAndUrl("saveCardNumberDetails",getActivity(),AppUrlList.ACTION_URL, new String[]{
			"module",
			"action",
			"iduser",
			"card_type",
			"card_number",
			"valid_upto",
			"default"
			},
		new String[]{
			"user",
			"addcarddetail",
			PreferenceManager.getInstance().getUserId(),
			(((RadioButton) addCardView.findViewById(R.id.creditButton)).isChecked()) ? "credit" : "debit",
			((EditText) addCardView.findViewById(R.id.cardNumberText)).getText().toString(),
			"01/"+addCardyearSpinner.getSelectedItem().toString()+"/"+addCardyearSpinner.getSelectedItem().toString(),
		 	(((CheckBox) addCardView.findViewById(R.id.defaultCheckBox)).isChecked()) ? "1" : "0" 
		 		}
		 	);
	
	myAsyncTask.execute();
	
}

public void updateCardNumberDetails() {
	myAsyncTask=new MyAsynTaskManager();
	myAsyncTask.delegate=this;
	myAsyncTask.setupParamsAndUrl("updateCardNumberDetails",getActivity(),AppUrlList.ACTION_URL, new String[]{
			"module",
			"action",
			"iduser",
			"card_type",
			"card_number",
			"valid_upto",
			"default",
			"idcard"
			},
		new String[]{
			"user",
			"editcard",
			PreferenceManager.getInstance().getUserId(),
			(((RadioButton) editView.findViewById(R.id.creditButton)).isChecked()) ? "credit" : "debit",
			((EditText) editView.findViewById(R.id.cardNumberText)).getText().toString(),
			"01/"+dateSpinner.getSelectedItem().toString()+"/"+yearSpinner.getSelectedItem().toString(),
		 	(((CheckBox) editView.findViewById(R.id.defaultCheckBox)).isChecked()) ? "1" : "0" ,
		 	feedList.get(current_poisition).getCardId()}
		 	);
	myAsyncTask.execute();

}

public void deleteUserCardDetails(final int position) {
	myAsyncTask=new MyAsynTaskManager();
	myAsyncTask.delegate=this;
	myAsyncTask.setupParamsAndUrl("deleteUserCardDetails",getActivity(),AppUrlList.ACTION_URL, new String[] {
			"module",
			"action",
			"iduser",
			"idcard"
			}, 
			new String[]{
			"user",
			"deletecard",
			PreferenceManager.getInstance().getUserId(),
			feedList.get(current_poisition).getCardId()
		}
	);
	myAsyncTask.execute();
	
}

@Override
public void backgroundProcessFinish(String from, String output) {
	if(from.equalsIgnoreCase("getMyCardDetails"))
	{
		if(output!=null)
		{
			try {
				Log.e("Response", output);
				JSONObject rootObj = new JSONObject(output);

				if(rootObj.getBoolean("result")) { 
					feedList.clear();
					JSONArray rootArray = rootObj.getJSONArray("details");

					for(int i =0; i < rootArray.length(); i++){
						JSONObject object = rootArray.getJSONObject(i);
						feedList.add(new MyCards_Class(
								object.getString("id"),
								object.getString("card_number"),
								object.getString("card_type"), 
								object.getString("valid_upto"),
								object.getString("default_type").equals("0") ? false : true));
					}

					//cardsListView.setAdapter(adapter);
					adapter.notifyDataSetChanged();

				}else{
					Toast.makeText(getActivity(), "No Card Details Found!", Toast.LENGTH_SHORT).show();
				}

			} catch (Exception e) {
				DialogManager.showDialog(getActivity(), "Error Occured in Getting Reults!");
				e.printStackTrace();
			}

		}
		else
		{
			DialogManager.showDialog(getActivity(), "Error Occured in Getting Reults!");
			//e.printStackTrace();
		}
	}
	else if(from.equalsIgnoreCase("saveCardNumberDetails"))
	{
		if(output!=null)
		{

			try {
				Log.e("respon", output);
				JSONObject rootObj = new JSONObject(output);
				if(rootObj.getBoolean("result")){
					JSONObject subrootObj = rootObj.getJSONObject("details");
					
				 	((EditText) addCardView.findViewById(R.id.cardNumberText)).setText("");
				 	
				 	if(((CheckBox) editView.findViewById(R.id.defaultCheckBox)).isChecked()){
						Log.e("test", "done");
						for (int i = 0; i < feedList.size(); i++) {
							if(feedList.get(i).getIsDefault())
								feedList.get(i).setIsDefault(false);
						}
					}
					
					((CheckBox) addCardView.findViewById(R.id.defaultCheckBox)).setChecked(false);
					
					feedList.add(new MyCards_Class(
							subrootObj.getString("id"), 
							subrootObj.getString("card_number"),
							subrootObj.getString("card_type"),
							subrootObj.getString("valid_upto"),
							(subrootObj.getString("default_type")).equals("1") ? true : false));
					
					/*feedList.get(current_poisition).setCardId(subrootObj.getString("id"));
					feedList.get(current_poisition).setCardNumber(subrootObj.getString("card_number"));
					feedList.get(current_poisition).setCardVaildPeriod(subrootObj.getString("valid_upto"));
					feedList.get(current_poisition).setIsDefault(
							(subrootObj.getString("default_type")).equals("1") ? true : false);
					feedList.get(current_poisition).setCardType(subrootObj.getString("card_type"));*/
					adapter.notifyDataSetChanged();
					
					DialogManager.showDialog(getActivity(), "Card Details Updated Successfully!");
				}else{
					DialogManager.showDialog(getActivity(), "Error In Adding Card Details!");
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			}
		
		}
		else
		{
			
			DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
		}
	}
	else if(from.equalsIgnoreCase("updateCardNumberDetails"))
	{
		if(output!=null)
		{

			try {
				Log.e("respon", output);
				JSONObject rootObj = new JSONObject(output);
				if(rootObj.getBoolean("result")){
					JSONObject subrootObj = rootObj.getJSONObject("details");
					
				 	((EditText) editView.findViewById(R.id.cardNumberText)).setText("");
					DialogManager.showDialog(getActivity(), "Card Details Updated Successfully!");
					
					if(((CheckBox) editView.findViewById(R.id.defaultCheckBox)).isChecked()){
						Log.e("test", "done");
						for (int i = 0; i < feedList.size(); i++) {
							if(feedList.get(i).getIsDefault())
								feedList.get(i).setIsDefault(false);
						}
					}
					((CheckBox) editView.findViewById(R.id.defaultCheckBox)).setChecked(false);
					feedList.get(current_poisition).setCardId(subrootObj.getString("id"));
					feedList.get(current_poisition).setCardNumber(subrootObj.getString("card_number"));
					feedList.get(current_poisition).setCardVaildPeriod(subrootObj.getString("valid_upto"));
					feedList.get(current_poisition).setIsDefault(
							(subrootObj.getString("default_type")).equals("1") ? true : false);
					feedList.get(current_poisition).setCardType(subrootObj.getString("card_type"));
					
					adapter.notifyDataSetChanged();
					
				}else{
					DialogManager.showDialog(getActivity(), "Error In Adding Card Details!");
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
			}
		
		}
		else
		{DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");}
	}
	else if(from.equalsIgnoreCase("deleteUserCardDetails"))
	{
		if(output!=null)
		{

			try {
				Log.e("Response", output);
				final JSONObject rootObj = new JSONObject(output);
				
				if(rootObj.getBoolean("result")) {
					Log.e("Current Position", current_poisition+"");
					//int i = current_poisition;
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							feedList.remove(deletecardPosition);
							//adapter.removeObject(feedList);
							adapter.notifyDataSetChanged();
						}
					});
					
					Toast.makeText(getActivity(), "Card Deleted Successfully!", Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(getActivity(), "Error In Deleting Account Details! Try Again!", Toast.LENGTH_SHORT).show();
				}
				
			} catch (Exception e) {
				DialogManager.showDialog(getActivity(), "Error In Deleting Account Details! Try Again!");
				e.printStackTrace();
			}
		
		}
	}
}

}
