package com.bash.Fragments;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.AddTabActivity;
import com.bash.Activities.AddTransactionActivity;
import com.bash.Activities.HomeActivity;
import com.bash.Adapters.MyTabsAdapter;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.GsonClasses.Pending_New_Class;
import com.bash.ListModels.MyTabs_Model;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.ContainerProvider;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.github.clans.fab.FloatingActionMenu.OnMenuToggleListener;
import com.google.gson.Gson;

@SuppressLint("ValidFragment")
public class MyTrans_Pending_Fragment extends BaseFragment implements
		AsyncResponse{
	MyAsynTaskManager myAsyncTask;
	public Pending_New_Class pendingNewClass;
	public ArrayList<MyTabs_Model> feedList;
	View mRootView;
	public static FragmentTabHost mTabhost;
	public ListView lviMyTabs;
	public MyTabs_Model tabModel;
	public MyTabsAdapter myTabsAdapter;
	
	//Gizmeon edit
	//for list scroll listener
	private int mPreviousVisibleItem;
	//floating button declare
	private FloatingActionMenu fab;
	 private FloatingActionButton fab1;
	    private FloatingActionButton fab2;
	    private FloatingActionButton fab3;
	    
	 //end   

	/*
	 * //datas public static FriendGroupAdapter total_friendadapter,
	 * youowe_friendadapter, youareowed_friendadapter; public ArrayList<String>
	 * totallistDataHeader, youowlsitDataHeader, youareowedDataHeader; public
	 * static SparseArray<ArrayList<MyTrans_FriendsGroups_Class>> totalfglist;
	 * public static SparseArray<ArrayList<MyTrans_FriendsGroups_Class>>
	 * youowefglist; public static
	 * SparseArray<ArrayList<MyTrans_FriendsGroups_Class>> youareowfglist;
	 */

	public static View totalbalanceView, youOweView, youAreOweView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// mRootView = inflater.inflate(R.layout.fragment_home, null);
		mRootView = inflater.inflate(R.layout.fragment_transaction_home, null);
		mTabhost = (FragmentTabHost) mRootView
				.findViewById(R.id.fragmenttabhost);
		mTabhost.setup(getActivity(), getChildFragmentManager(),
				R.id.fragmentframe_holder);
		lviMyTabs = (ListView) mRootView.findViewById(R.id.lviMyTabs);
		feedList = new ArrayList<MyTabs_Model>();

		// initializeListValues();
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn, "My Tabs", 0);
		initializeTabHost();
		// initializeRangeSeekBar();
		
		
		//Gizmeon Edit
		
		 	fab = (FloatingActionMenu) mRootView.findViewById(R.id.menu1);
		 	fab1 = (FloatingActionButton) mRootView.findViewById(R.id.fab1);
	        fab2 = (FloatingActionButton) mRootView.findViewById(R.id.fab2);
	        fab3 = (FloatingActionButton) mRootView.findViewById(R.id.fab3);
	        
	        
//	        ContextThemeWrapper context = new ContextThemeWrapper(getActivity(), R.style.MenuButtonsStyleNormal);
//	        FloatingActionButton programFab2 = new FloatingActionButton(context);
//	        programFab2.setLabelText("Aphrodite");
//	        programFab2.setImageResource(R.drawable.imgfour);
//	        programFab2.setTag("imagfour");
//	        programFab2.setPadding(20, 20, 20, 20);
//	        fab.addMenuButton(programFab2);
	        
//	        programFab2.setOnClickListener(clickListener);
	        fab1.setOnClickListener(clickListener);
	        fab2.setOnClickListener(clickListener);
	        fab3.setOnClickListener(clickListener);  
	        
	        fab.hideMenuButton(false);
	        fab.setClosedOnTouchOutside(true);
	        fab.showMenuButton(true);
	        fab.setIconAnimated(true);

	        lviMyTabs.setOnScrollListener(new AbsListView.OnScrollListener() {
	            @Override
	            public void onScrollStateChanged(AbsListView view, int scrollState) {
	            }

	            @Override
	            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
	                if (firstVisibleItem > mPreviousVisibleItem) {
	                    fab.hideMenuButton(true);
	                } else if (firstVisibleItem < mPreviousVisibleItem) {
	                    fab.hideMenuButton(true);
	                }
	                mPreviousVisibleItem = firstVisibleItem;
	            }
	        });
		
	       
		//end
		
		
		
		
		return mRootView;
	}

	/*
	 * public static FriendGroupAdapter getTotalAdapter(){ if(totalfglist ==
	 * null || totalfglist.size() == 0){ totalfglist = new
	 * SparseArray<ArrayList<MyTrans_FriendsGroups_Class>>(); } return
	 * total_friendadapter; }
	 * 
	 * public static FriendGroupAdapter getYouOweAdapter(){ if(youowefglist ==
	 * null || youowefglist.size() == 0){ youowefglist = new
	 * SparseArray<ArrayList<MyTrans_FriendsGroups_Class>>(); } return
	 * youowe_friendadapter; }
	 * 
	 * public static FriendGroupAdapter getYouAreOweAdapter(){ if(youareowfglist
	 * == null || youareowfglist.size() == 0){ youareowfglist = new
	 * SparseArray<ArrayList<MyTrans_FriendsGroups_Class>>(); } return
	 * youareowed_friendadapter; }
	 */

	/*
	 * public void initializeListValues() {
	 * 
	 * totallistDataHeader = new ArrayList<String>(); youowlsitDataHeader= new
	 * ArrayList<String>(); youareowedDataHeader= new ArrayList<String>();
	 * 
	 * totallistDataHeader.clear(); totallistDataHeader.add("Friends");
	 * totallistDataHeader.add("Groups");
	 * 
	 * youowlsitDataHeader.clear(); youowlsitDataHeader.add("Friends");
	 * youowlsitDataHeader.add("Groups");
	 * 
	 * youareowedDataHeader.clear(); youareowedDataHeader.add("Friends");
	 * youareowedDataHeader.add("Groups");
	 * 
	 * totalfglist = new SparseArray<ArrayList<MyTrans_FriendsGroups_Class>>();
	 * youowefglist = new SparseArray<ArrayList<MyTrans_FriendsGroups_Class>>();
	 * youareowfglist = new
	 * SparseArray<ArrayList<MyTrans_FriendsGroups_Class>>();
	 * 
	 * totalfglist.clear(); youowefglist.clear(); youareowfglist.clear();
	 * 
	 * total_friendadapter = new FriendGroupAdapter(getActivity(),
	 * totallistDataHeader, totalfglist); youowe_friendadapter = new
	 * FriendGroupAdapter(getActivity(), youowlsitDataHeader, youowefglist);
	 * youareowed_friendadapter = new FriendGroupAdapter(getActivity(),
	 * youareowedDataHeader, youareowfglist);
	 * 
	 * }
	 */

	private void initializeTabHost() {

		totalbalanceView = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab_mytransaction_selector_bg, null);
		youOweView = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab_mytransaction_selector_bg, null);
		youAreOweView = LayoutInflater.from(getActivity()).inflate(
				R.layout.tab_mytransaction_selector_bg, null);

		mTabhost.setBackgroundColor(Color.parseColor("#FFFFFF"));

		Spannable totalbaltoSpan = new SpannableString("Total Balance\n"
				+ AppConstants.RASYMBOL + " 0");
		totalbaltoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 12,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		totalbaltoSpan.setSpan(new ForegroundColorSpan(Color.GREEN), 13,
				totalbaltoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) totalbalanceView.findViewById(R.id.tabText))
				.setText(totalbaltoSpan);

		Spannable toreceivetoSpan = new SpannableString("To Receive\n"
				+ AppConstants.RASYMBOL + " 0");
		toreceivetoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 9,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		toreceivetoSpan.setSpan(new ForegroundColorSpan(Color.GREEN), 10,
				toreceivetoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) youOweView.findViewById(R.id.tabText))
				.setText(toreceivetoSpan);

		Spannable topaytoSpan = new SpannableString("To Pay\n"
				+ AppConstants.RASYMBOL + " 0");
		topaytoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 5,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		topaytoSpan.setSpan(new ForegroundColorSpan(Color.RED), 6,
				topaytoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		((TextView) youAreOweView.findViewById(R.id.tabText))
				.setText(topaytoSpan);

		mTabhost.addTab(
				mTabhost.newTabSpec(AppConstants.TAB_HOME_TOTALBALANCE_TAG)
						.setIndicator(totalbalanceView),
				ContainerProvider.Tab_MyTransactions_TotalBalacne_Container.class,
				null);

		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_HOME_YOU_OWE_TAG)
				.setIndicator(youOweView),
				ContainerProvider.Tab_MyTransactions_YouOwe_Container.class,
				null);

		mTabhost.addTab(
				mTabhost.newTabSpec(AppConstants.TAB_HOME_YOU_ARE_OWED_TAG)
						.setIndicator(youAreOweView),
				ContainerProvider.Tab_MyTransactions_YouAreOwed_Container.class,
				null);

		mTabhost.setOnTabChangedListener(new OnTabChangeListener() {
			@Override
			public void onTabChanged(String tabId) {

			}
		});

		getPendingTransactionDetails();
	}

	public void getPendingTransactionDetails() {
		myAsyncTask = new MyAsynTaskManager();
		myAsyncTask.delegate = this;
		myAsyncTask.setupParamsAndUrl("getPendingTransactionDetails",
				getActivity(), AppUrlList.ACTION_URL, new String[] { "module",
						"action", "iduser" }, new String[] { "feed",
						"pendingtransaction",
						PreferenceManager.getInstance().getUserId() });
		myAsyncTask.execute();
		// new MyAsynTaskManager(getActivity(), new LoadListener() {
		// @Override
		// public void onLoadComplete(final String jsonResponse) {
		// getActivity().runOnUiThread(new Runnable() {
		// @Override
		// public void run() {
		// try{
		// Gson gson = new Gson();
		// pendingNewClass = gson.fromJson(jsonResponse,
		// Pending_New_Class.class);
		// if(pendingNewClass.result){
		//
		// /*totalfglist = pendingNewClass.total_bal.getTotalBalValues();
		// youowefglist = pendingNewClass.you_owe.getYouAreOweValues();
		// youareowfglist = pendingNewClass.you_are_owe.getYouAreOweValues();*/
		//
		// if(pendingNewClass.total_bal.getTotalBalValues() == null)
		// Log.e("NULL88888888888888", "**********************");
		//
		// HomeActivity.total_friendadapter.notifyWithDataSet(pendingNewClass.total_bal.getTotalBalValues());
		// HomeActivity.youowe_friendadapter.notifyWithDataSet(pendingNewClass.you_owe.getYouAreOweValues());
		// HomeActivity.youareowed_friendadapter.notifyWithDataSet(pendingNewClass.you_are_owe.getYouAreOweValues());
		//
		// ((TextView)
		// totalbalanceView.findViewById(R.id.tabText)).setText("Total Balance\n"+AppConstants.RASYMBOL+" "+pendingNewClass.total_bal.total);
		// ((TextView)
		// youOweView.findViewById(R.id.tabText)).setText("You Owe\n"+AppConstants.RASYMBOL+" "+pendingNewClass.you_owe.total);
		// ((TextView)
		// youAreOweView.findViewById(R.id.tabText)).setText("You Are Owed\n"+AppConstants.RASYMBOL+" "+pendingNewClass.you_are_owe.total);
		//
		// /*MyTransaction_Home_YouOwe_Fragment.generalView.setAdapter(youowe_friendadapter);
		// MyTransaction_Home_YouAreOwed_Fragment.generalView.setAdapter(youareowed_friendadapter);
		// MyTransaction_Home_Total_Fragment.generalView.setAdapter(total_friendadapter);*/
		//
		// MyTransaction_Home_YouOwe_Fragment.generalView.expandGroup(0);
		// MyTransaction_Home_YouOwe_Fragment.generalView.expandGroup(1);
		// MyTransaction_Home_YouAreOwed_Fragment.generalView.expandGroup(0);
		// MyTransaction_Home_YouAreOwed_Fragment.generalView.expandGroup(1);
		// MyTransaction_Home_Total_Fragment.generalView.expandGroup(0);
		// MyTransaction_Home_Total_Fragment.generalView.expandGroup(1);
		//
		//
		// /*if(MyTransaction_Home_YouOwe_Fragment.generalView.getAdapter() !=
		// null){
		// total_friendadapter = new FriendGroupAdapter(getActivity(),
		// totallistDataHeader, totalfglist);
		// youowe_friendadapter = new FriendGroupAdapter(getActivity(),
		// youowlsitDataHeader, youowefglist);
		// youareowed_friendadapter = new FriendGroupAdapter(getActivity(),
		// youareowedDataHeader, youareowfglist);
		// MyTransaction_Home_YouOwe_Fragment.generalView.setAdapter(total_friendadapter);
		// MyTransaction_Home_YouAreOwed_Fragment.generalView.setAdapter(youowe_friendadapter);
		// MyTransaction_Home_Total_Fragment.generalView.setAdapter(youareowed_friendadapter);
		// }else{
		// total_friendadapter.notifyWithDataSet(pendingNewClass.total_bal.getTotalBalValues());
		// youowe_friendadapter.notifyWithDataSet(pendingNewClass.you_owe.getYouAreOweValues());
		// youareowed_friendadapter.notifyWithDataSet(pendingNewClass.you_are_owe.getYouAreOweValues());
		// }
		// */
		//
		//
		// /*total_friendadapter.notifyDataSetChanged();
		// youowe_friendadapter.notifyDataSetChanged();
		// youareowed_friendadapter.notifyDataSetChanged();*/
		//
		//
		//
		// /*total_friendadapter = new FriendGroupAdapter(getActivity(),
		// totallistDataHeader, totalfglist);
		// youowe_friendadapter = new FriendGroupAdapter(getActivity(),
		// youowlsitDataHeader, youowefglist);
		// youareowed_friendadapter = new FriendGroupAdapter(getActivity(),
		// youareowedDataHeader, youareowfglist);*/
		//
		// /*total_friendadapter = new FriendGroupAdapter(getActivity(),
		// totallistDataHeader, pendingNewClass.total_bal.getTotalBalValues());
		// youowe_friendadapter = new FriendGroupAdapter(getActivity(),
		// youowlsitDataHeader, pendingNewClass.you_owe.getYouAreOweValues());
		// youareowed_friendadapter = new FriendGroupAdapter(getActivity(),
		// youareowedDataHeader,
		// pendingNewClass.you_are_owe.getYouAreOweValues());*/
		//
		// /*total_friendadapter.notifyWithDataSet(pendingNewClass.total_bal.getTotalBalValues());
		// youowe_friendadapter.notifyWithDataSet(pendingNewClass.you_owe.getYouAreOweValues());
		// youareowed_friendadapter.notifyWithDataSet(pendingNewClass.you_are_owe.getYouAreOweValues());*/
		//
		// /*MyTransaction_Home_YouOwe_Fragment.generalView.setAdapter(total_friendadapter);
		// MyTransaction_Home_YouAreOwed_Fragment.generalView.setAdapter(youowe_friendadapter);
		// MyTransaction_Home_Total_Fragment.generalView.setAdapter(youareowed_friendadapter);*/
		//
		// } else{
		// Toast.makeText(getActivity(), "No Transactions Found!",
		// Toast.LENGTH_SHORT).show();
		// }
		// //new storeTransactionDetailsInDb().execute();
		//
		// } catch (Exception e) {
		// e.printStackTrace();
		// //DialogManager.showDialog(getActivity(),
		// "Server Error Occured! Try Again!");
		// }
		// }
		// });
		// }
		//
		// @Override
		// public void onError(final String errorMessage) {
		// getActivity().runOnUiThread(new Runnable() {
		// public void run() {
		// DialogManager.showDialog(getActivity(), errorMessage);
		// }
		// });
		//
		// }
		// }).execute();

	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		tabModel = new MyTabs_Model("Aphrodit", "", "300.00", "yes", "no");
		feedList.add(tabModel);
		tabModel.setFieldValues("Athena", "", "30.00", "no", "yes");
		feedList.add(tabModel);
		tabModel.setFieldValues("Hera", "", "50.00", "yes", "no");
		feedList.add(tabModel);
		tabModel.setFieldValues("Jeus", "", "150.00", "no", "yes");
		feedList.add(tabModel);
		myTabsAdapter = new MyTabsAdapter(getActivity(), feedList);

		lviMyTabs.setAdapter(myTabsAdapter);
		if (from.equalsIgnoreCase("getPendingTransactionDetails")) {
			if (output != null) {

				try {
					Gson gson = new Gson();
					pendingNewClass = gson.fromJson(output,
							Pending_New_Class.class);
					if (pendingNewClass.result) {

						/*
						 * totalfglist =
						 * pendingNewClass.total_bal.getTotalBalValues();
						 * youowefglist =
						 * pendingNewClass.you_owe.getYouAreOweValues();
						 * youareowfglist =
						 * pendingNewClass.you_are_owe.getYouAreOweValues();
						 */

						if (pendingNewClass.total_bal.getTotalBalValues() == null)
							Log.e("NULL88888888888888",
									"**********************");

						HomeActivity.total_friendadapter
								.notifyWithDataSet(pendingNewClass.total_bal
										.getTotalBalValues());
						HomeActivity.youowe_friendadapter
								.notifyWithDataSet(pendingNewClass.you_owe
										.getYouAreOweValues());
						HomeActivity.youareowed_friendadapter
								.notifyWithDataSet(pendingNewClass.you_are_owe
										.getYouAreOweValues());

						((TextView) totalbalanceView.findViewById(R.id.tabText))
								.setText("Total Balance\n"
										+ AppConstants.RASYMBOL + " "
										+ pendingNewClass.total_bal.total);
						((TextView) youOweView.findViewById(R.id.tabText))
								.setText("You Owe\n" + AppConstants.RASYMBOL
										+ " " + pendingNewClass.you_owe.total);
						((TextView) youAreOweView.findViewById(R.id.tabText))
								.setText("You Are Owed\n"
										+ AppConstants.RASYMBOL + " "
										+ pendingNewClass.you_are_owe.total);

						/*
						 * MyTransaction_Home_YouOwe_Fragment.generalView.setAdapter
						 * (youowe_friendadapter);
						 * MyTransaction_Home_YouAreOwed_Fragment
						 * .generalView.setAdapter(youareowed_friendadapter);
						 * MyTransaction_Home_Total_Fragment
						 * .generalView.setAdapter(total_friendadapter);
						 */

						MyTransaction_Home_YouOwe_Fragment.generalView
								.expandGroup(0);
						MyTransaction_Home_YouOwe_Fragment.generalView
								.expandGroup(1);
						MyTransaction_Home_YouAreOwed_Fragment.generalView
								.expandGroup(0);
						MyTransaction_Home_YouAreOwed_Fragment.generalView
								.expandGroup(1);
						MyTransaction_Home_Total_Fragment.generalView
								.expandGroup(0);
						MyTransaction_Home_Total_Fragment.generalView
								.expandGroup(1);

						/*
						 * if(MyTransaction_Home_YouOwe_Fragment.generalView.
						 * getAdapter() != null){ total_friendadapter = new
						 * FriendGroupAdapter(getActivity(),
						 * totallistDataHeader, totalfglist);
						 * youowe_friendadapter = new
						 * FriendGroupAdapter(getActivity(),
						 * youowlsitDataHeader, youowefglist);
						 * youareowed_friendadapter = new
						 * FriendGroupAdapter(getActivity(),
						 * youareowedDataHeader, youareowfglist);
						 * MyTransaction_Home_YouOwe_Fragment
						 * .generalView.setAdapter(total_friendadapter);
						 * MyTransaction_Home_YouAreOwed_Fragment
						 * .generalView.setAdapter(youowe_friendadapter);
						 * MyTransaction_Home_Total_Fragment
						 * .generalView.setAdapter(youareowed_friendadapter);
						 * }else{
						 * total_friendadapter.notifyWithDataSet(pendingNewClass
						 * .total_bal.getTotalBalValues());
						 * youowe_friendadapter.
						 * notifyWithDataSet(pendingNewClass
						 * .you_owe.getYouAreOweValues());
						 * youareowed_friendadapter
						 * .notifyWithDataSet(pendingNewClass
						 * .you_are_owe.getYouAreOweValues()); }
						 */

						/*
						 * total_friendadapter.notifyDataSetChanged();
						 * youowe_friendadapter.notifyDataSetChanged();
						 * youareowed_friendadapter.notifyDataSetChanged();
						 */

						/*
						 * total_friendadapter = new
						 * FriendGroupAdapter(getActivity(),
						 * totallistDataHeader, totalfglist);
						 * youowe_friendadapter = new
						 * FriendGroupAdapter(getActivity(),
						 * youowlsitDataHeader, youowefglist);
						 * youareowed_friendadapter = new
						 * FriendGroupAdapter(getActivity(),
						 * youareowedDataHeader, youareowfglist);
						 */

						/*
						 * total_friendadapter = new
						 * FriendGroupAdapter(getActivity(),
						 * totallistDataHeader,
						 * pendingNewClass.total_bal.getTotalBalValues());
						 * youowe_friendadapter = new
						 * FriendGroupAdapter(getActivity(),
						 * youowlsitDataHeader,
						 * pendingNewClass.you_owe.getYouAreOweValues());
						 * youareowed_friendadapter = new
						 * FriendGroupAdapter(getActivity(),
						 * youareowedDataHeader,
						 * pendingNewClass.you_are_owe.getYouAreOweValues());
						 */

						/*
						 * total_friendadapter.notifyWithDataSet(pendingNewClass.
						 * total_bal.getTotalBalValues());
						 * youowe_friendadapter.notifyWithDataSet
						 * (pendingNewClass.you_owe.getYouAreOweValues());
						 * youareowed_friendadapter
						 * .notifyWithDataSet(pendingNewClass
						 * .you_are_owe.getYouAreOweValues());
						 */

						/*
						 * MyTransaction_Home_YouOwe_Fragment.generalView.setAdapter
						 * (total_friendadapter);
						 * MyTransaction_Home_YouAreOwed_Fragment
						 * .generalView.setAdapter(youowe_friendadapter);
						 * MyTransaction_Home_Total_Fragment
						 * .generalView.setAdapter(youareowed_friendadapter);
						 */

					} else {
						Toast.makeText(getActivity(), "No Transactions Found!",
								Toast.LENGTH_SHORT).show();
					}
					// new storeTransactionDetailsInDb().execute();

				} catch (Exception e) {
					e.printStackTrace();
					// DialogManager.showDialog(getActivity(),
					// "Server Error Occured! Try Again!");
				}

			} else {
				DialogManager.showDialog(getActivity(),
						"Server Error Occured! Try Again!");
			}

		}

	}
	
	//Gizmeon edit
	
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String text = "";
        
            switch (v.getId()) {
            
            case R.id.menu1:         	
                text = fab1.getLabelText();
                break;
                case R.id.fab1:
                    text = fab1.getLabelText();
                    break;
                case R.id.fab2:
                    text = fab2.getLabelText();
                    break;
                case R.id.fab3:
                    text = fab3.getLabelText();
                    startActivity(new Intent(getActivity(),AddTabActivity.class));
                    break;
            }  	
           Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();            	                  
        }
    };
	
	//end

}
