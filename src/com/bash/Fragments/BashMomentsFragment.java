package com.bash.Fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Adapters.BashMomentsAdapter;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.ListModels.BashMoments_Class;

public class BashMomentsFragment extends Fragment{
	
	View mRootView;
	public static ListView bashmomentsListview;
	BashMomentsAdapter adapter;
	ArrayList<BashMoments_Class> feedList = new ArrayList<BashMoments_Class>();
	public static BaseFragment parentFragment;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_bashmoments_page, null);
		return mRootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initializeView();
	}
	
	private void initializeView() {
		// TODO Auto-generated method stub
		 ((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn, "Bash Moments", 0);
		 
		 ((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//Sarvesh
					((HomeActivity) getActivity()).slidingmenu_layout.toggleMenu();
				}
		 });
		 
		 ((ImageView)getActivity().findViewById(R.id.toprightsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
				}
		 });
		 
		bashmomentsListview = (ListView)mRootView.findViewById(R.id.bashmomentsListview);
		
		if(feedList.size() == 0)
			setDatas();
		adapter = new BashMomentsAdapter(getActivity(), feedList);
		bashmomentsListview.setAdapter(adapter);
		
		parentFragment = ((BaseFragment)getParentFragment());
		
	}
 

	private void setDatas() {
		// TODO Auto-generated method stub
		for(int i = 0; i< 6; i++)
			feedList.add(new BashMoments_Class(new int []{0, 1, 2, 3, 4}));
	}
}
