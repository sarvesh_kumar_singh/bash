package com.bash.Fragments;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.BuildConfig;
import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.GsonClasses.Friends_Class;
import com.bash.GsonClasses.MyFriend_Transaction_Class;
import com.bash.ListModels.BashUsers_Class;
import com.bash.ListModels.CashInOutFriend_Class;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynImageTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;
import com.bash.Utils.MyLocation;
import com.nostra13.universalimageloader.core.ImageLoader;

@SuppressLint("ValidFragment")
public class MyFriends_Transactions_Charge_Fragment extends Fragment{
	
	View mRootView;
	View camera_gallery;
	Dialog camaeraDialog;
	Uri mCapturedImageURI;
	public ImageView fileImage;
	public Friends_Class responseForFriends;
	public ProgressDialog pd;
	/*public ArrayList<String> friendNameList = new ArrayList<String>();
	public ArrayList<BashUsers_Class> friendsList = new ArrayList<BashUsers_Class>();
*/	
	public String picturePath, locationPath=" ";
	public CashInOutFriend_Class itemClass;
	MyFriend_Transaction_Class userDetails;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_myfriends_trans_charge, null);
		//mRootView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		return mRootView;
	}
	
	@SuppressLint("ValidFragment")
	public MyFriends_Transactions_Charge_Fragment(CashInOutFriend_Class itemClass, MyFriend_Transaction_Class userDetails){
		this.itemClass = itemClass;	
		this.userDetails = userDetails;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initializeView();
		initializeAlertView();
		((HomeActivity) getActivity()).currentFragment = this;
	}
	
	private void initializeAlertView() {
		// TODO Auto-generated method stub
		camera_gallery = View.inflate(getActivity(),
				R.layout.alert_gallerycamera_mode_view, null);
		camaeraDialog = new Dialog(getActivity());
		camaeraDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		camaeraDialog.setContentView(camera_gallery);
 
		((Button) camera_gallery.findViewById(R.id.openGalleryBtn))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						camaeraDialog.dismiss();
						Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						getActivity().startActivityForResult(i,	AppConstants.CODE_GALLERY);
					}
		});

		((Button) camera_gallery.findViewById(R.id.openCameraBtn)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						camaeraDialog.dismiss();
						String fileName = "temp.jpg";
						ContentValues values = new ContentValues();
						values.put(MediaStore.Images.Media.TITLE, fileName);
						mCapturedImageURI = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
										values);
						Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
						intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
						getActivity().startActivityForResult(intent, AppConstants.CODE_CAMERA);
					}
		});
		
		pd = new ProgressDialog(getActivity());
		
		((ImageView) mRootView.findViewById(R.id.locationButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				pd.setMessage("Tracking Your Location...");
				pd.setCancelable(false);
				MyLocation locationPicker = new MyLocation();
				pd.show();
				locationPicker.getLocation(getActivity(), new MyLocation.LocationResult() {
					@Override
					public void gotLocation(Location location) {
						pd.dismiss();
						if(location == null){
							Toast.makeText(getActivity(), "Unable to Get Your Location! Try Again!", Toast.LENGTH_SHORT).show();
						}else{
							try {
								Geocoder geocoder;
								List<Address> addresses;
								geocoder = new Geocoder(getActivity(), Locale.getDefault());
								addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
								locationPath = addresses.get(0).getSubLocality()+", "+addresses.get(0).getLocality();
								((TextView)mRootView.findViewById(R.id.locationInfo)).setText(locationPath);
								
							/*	String address = addresses.get(0).getAddressLine(0);
								String city = addresses.get(0).getAddressLine(1);
								String country = addresses.get(0).getAddressLine(2);
								Log.e("Address", address);
								Log.e("City", city);
								Log.e("Country", country);
								Log.e("Locality", addresses.get(0).getLocality());
								Log.e("Admin Area", addresses.get(0).getAdminArea());
								Log.e("Sub Admin Area", addresses.get(0).getSubAdminArea());
								Log.e("Sub Locality Area", addresses.get(0).getSubLocality());
								
								Log.e("Country",addresses.get(0).getAddressLine(3));*/
								
								
							} catch (Exception e) {
								e.printStackTrace();
							}
							
						}
					}
				});
			}
		});
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case AppConstants.CODE_CAMERA:
			if (resultCode == getActivity().RESULT_OK) {
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(
						mCapturedImageURI, projection, null, null, null);
				int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				picturePath = cursor.getString(column_index_data);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,	fileImage);
				Log.e("Camera Path", picturePath);
			}
			break;
		case AppConstants.CODE_GALLERY:
			if (resultCode == getActivity().RESULT_OK) {

				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				picturePath = cursor.getString(columnIndex);
				cursor.close();
				ImageLoader.getInstance().displayImage("file://" + picturePath,	fileImage);
			}
			break;
		default:
			break;
		}
	}
	
	private void initializeView() {
		// TODO Auto-generated method stub
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "Charge Friend", 0);
    	
		((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((BaseFragment)getParentFragment()).popFragment();
			}
		});
		
		fileImage = (ImageView) mRootView.findViewById(R.id.addPhotoButton);
		
		((ImageView) mRootView.findViewById(R.id.addPhotoButton)).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						camaeraDialog.show();
					}
		});
		 
		((EditText) mRootView.findViewById(R.id.dateText)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getDatePickerDialog().show();
			}
		});
		
		((RelativeLayout) mRootView.findViewById(R.id.paynowButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(ValidateForm()){
					EditTransaction();
				}
			 
			}
		});
		
		((RelativeLayout) mRootView.findViewById(R.id.payByCashButton)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				DeleteTransaction();
			}
		});
		
		((EditText) mRootView.findViewById(R.id.friendName)).setText(userDetails.name);
		((EditText) mRootView.findViewById(R.id.amountText)).setText(itemClass.getsubTotalAmount());
		((EditText) mRootView.findViewById(R.id.dateText)).setText(itemClass.getDate());
		((EditText) mRootView.findViewById(R.id.feedCommentText)).setText(itemClass.getFeedComment());
		((TextView) mRootView.findViewById(R.id.locationInfo)).setText(itemClass.getLocation());
		
		((ImageView) mRootView.findViewById(R.id.facebookButton)).setVisibility(View.GONE);
		((ImageView) mRootView.findViewById(R.id.twitterButton)).setVisibility(View.GONE);
		
		//((ImageView) mRootView.findViewById(R.id.addPhotoButton)).setImageResource(R.id.);
		
		
	}
	
	public boolean ValidateForm() {
		try {
			if(((EditText) mRootView.findViewById(R.id.amountText)).getText() == null || 
					((EditText) mRootView.findViewById(R.id.amountText)).getText().toString().length() == 0){
				Toast.makeText(getActivity(), "Please Enter Amout!", Toast.LENGTH_SHORT).show();
				return false;
			}else if(((EditText) mRootView.findViewById(R.id.dateText)).getText() == null || 
					((EditText) mRootView.findViewById(R.id.dateText)).getText().toString().length() == 0){
				Toast.makeText(getActivity(), "Please Select Date!", Toast.LENGTH_SHORT).show();
				return false;
			}else if(((EditText) mRootView.findViewById(R.id.feedCommentText)).getText() == null || 
					((EditText) mRootView.findViewById(R.id.feedCommentText)).getText().toString().length() == 0){
				Toast.makeText(getActivity(), "Please Enter Feed Comment!", Toast.LENGTH_SHORT).show();
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Toast.makeText(getActivity(), "Please Select Friend Name!", Toast.LENGTH_SHORT).show();
			return false;
		}
	
		return true;
	}
	
	Calendar cal = Calendar.getInstance();
	 
	public DatePickerDialog getDatePickerDialog() {
			DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
				 
	            @Override
	            public void onDateSet(DatePicker view, int year,
	                    int monthOfYear, int dayOfMonth) {
	            	((EditText) mRootView.findViewById(R.id.dateText)).setText(dayOfMonth + "/"
	                        + (monthOfYear + 1) + "/" + year);
	            }
	        }, cal.get(Calendar.YEAR),  cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			
		 return datePickerDialog;
	 }
	
	
	public void DeleteTransaction() {

		MyAsynImageTaskManager.setupParamsAndUrl(AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"idtrans",
				"paid_by",
				"paid_for"
				}, 
				new String[]{
				"feed",
				"delete",
				itemClass.getIdTrans(),
				PreferenceManager.getInstance().getUserId(),
				userDetails.iduser
				}, 
				new String[]{
				"0", "0", "0", "0" , "0"
				});

		new MyAsynImageTaskManager(getActivity(), new MyAsynImageTaskManager.LoadListener() {
			@Override
			public void onLoadComplete(final String jsonResponse) {
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						try {
						if(BuildConfig.DEBUG)
								Log.e("Json Response", jsonResponse);
						JSONObject rootObj = new JSONObject(jsonResponse);
						if(rootObj.getBoolean("result")){
								((BaseFragment)getParentFragment()).popFragment();
								Toast.makeText(getActivity(), "Transaction Deleted Successfully!", Toast.LENGTH_SHORT).show();
						}else{
							DialogManager.showDialog(getActivity(), "Error To Charge Friend! Try Again!");
						}
						} catch (Exception e) {
							// TODO: handle exception
							DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
						}
				}
				});
			}
			@Override
			public void onError(final String errorMessage)  {
				getActivity().runOnUiThread(new Runnable() {
					public void run() {
						DialogManager.showDialog(getActivity(), errorMessage);		
					}
				});
				
			}
		}).execute();
	}
 

	public void EditTransaction() {

		 
		 	MyAsynImageTaskManager.setupParamsAndUrl(AppUrlList.ACTION_URL, new String[] {
					"module",
					"action",
					"idtrans",
					"paid_by",
					"paid_for",
					"amount",
					"feed_content",
					"location",
					"feedimage",
					"date"
					}, 
					new String[]{
					"feed",
					"edit",
					itemClass.getIdTrans(),
					PreferenceManager.getInstance().getUserId(),
					userDetails.iduser,
					((EditText)mRootView.findViewById(R.id.amountText)).getText().toString(),
					((EditText)mRootView.findViewById(R.id.feedCommentText)).getText().toString(),
					locationPath,
					picturePath,
					((EditText)mRootView.findViewById(R.id.dateText)).getText().toString()
					}, 
					new String[]{
					"0", "0", "0", "0", "0", "0", "0", "0", "1", "0" 
					});

			new MyAsynImageTaskManager(getActivity(), new MyAsynImageTaskManager.LoadListener() {
				@Override
				public void onLoadComplete(final String jsonResponse) {
					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							try {
							if(BuildConfig.DEBUG)
									Log.e("Json Response", jsonResponse);
							JSONObject rootObj = new JSONObject(jsonResponse);
							if(rootObj.getBoolean("result")){
									((BaseFragment)getParentFragment()).popFragment();
									Toast.makeText(getActivity(), "Transaction Edited Successfully!", Toast.LENGTH_SHORT).show();
							}else{
								DialogManager.showDialog(getActivity(), "Error To Charge Friend! Try Again!");
							}
							} catch (Exception e) {
								// TODO: handle exception
								DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
							}
					}
					});
				}
				@Override
				public void onError(final String errorMessage)  {
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							DialogManager.showDialog(getActivity(), errorMessage);		
						}
					});
					
				}
			}).execute();
		}
	 
	 		 
	 
	
	 
	
}

