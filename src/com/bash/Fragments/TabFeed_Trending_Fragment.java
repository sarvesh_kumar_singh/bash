package com.bash.Fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.bash.R;
import com.bash.ListModels.MyFeed_Class;

public class TabFeed_Trending_Fragment extends Fragment {
	
	View mRootView;
	ListView myfeedListView;
	//MyFeedAdapter adapter;
	ArrayList<MyFeed_Class> feedList = new ArrayList<MyFeed_Class>();
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_feed_page, null);
		//mRootView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		return mRootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		initializeView();
	}
	
 	private void initializeView() {
		// TODO Auto-generated method stub
		myfeedListView = (ListView)mRootView.findViewById(R.id.generalListView);
		if(feedList.size() == 0)
			setDatas();
	/*	adapter = new MyFeedAdapter(getActivity(), feedList);
		myfeedListView.setAdapter(adapter);*/
	}

	private void setDatas() {
		// TODO Auto-generated method stub
		for(int i = 0; i<5; i++){
			switch (i) {
			case 0:
				feedList.add(new MyFeed_Class(R.drawable.addphoto_img_block, 
						"Ramesh", "Suresh", "Payment for dinner and Lunch", "Smoke House Deli", String.valueOf("Feb "+(1+i)+", 2014")));
				break;
			case 1:
				feedList.add(new MyFeed_Class(R.drawable.addphoto_img_block, 
						"Ramesh", "Suresh", "Payment for dinner and Lunch", "Smoke House Deli", String.valueOf("Feb "+(1+i)+", 2014")));
				break;
			case 2:
				feedList.add(new MyFeed_Class(R.drawable.addphoto_img_block, 
						"Ramesh", "Suresh", "Payment for dinner and Lunch", "Smoke House Deli", String.valueOf("Feb "+(1+i)+", 2014")));
				break;
			case 3:
				feedList.add(new MyFeed_Class(R.drawable.addphoto_img_block, 
						"Ramesh", "Suresh", "Payment for dinner and Lunch", "Smoke House Deli",String.valueOf("Feb "+(1+i)+", 2014")));
				break;
			case 4:
				feedList.add(new MyFeed_Class(R.drawable.addphoto_img_block, 
						"Ramesh", "Suresh", "Payment for dinner and Lunch", "Smoke House Deli", String.valueOf("Feb "+(1+i)+", 2014")));
				break;
			case 5:
				feedList.add(new MyFeed_Class(R.drawable.addphoto_img_block, 
						"Ramesh", "Suresh", "Payment for dinner and Lunch", "Smoke House Deli", String.valueOf("Feb "+(1+i)+", 2014")));
			default:
				break;
			}
		}
			
	}
	
}
