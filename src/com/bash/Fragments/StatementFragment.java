package com.bash.Fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Adapters.StatementAdapter;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.ListModels.Statement_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppUrlList;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class StatementFragment extends BaseFragment implements AsyncResponse{
	MyAsynTaskManager  myAsyncTask;
	public View mRootView;
	ListView statementListview;
	StatementAdapter adapter;
	ArrayList<Statement_Class> feedList = new ArrayList<Statement_Class>();
	
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_mywallet_stament_page, null);
		initializeView();
		return mRootView;
	}
	
	private void initializeView() {
		// TODO Auto-generated method stub
		//Initialize Activity Views
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "Statement", 0);
		
		
		((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((BaseFragment)getParentFragment()).popFragment();
			}
		});
		
		statementListview = (ListView)mRootView.findViewById(R.id.statementListview);
		
		adapter = new StatementAdapter(getActivity(), feedList);
		statementListview.setAdapter(adapter);
		
		getStatementList();
	}
	

	public void getStatementList() {
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask
				.setupParamsAndUrl("getStatementList",getActivity(),
						AppUrlList.ACTION_URL,
						new String[] { 
								"module", 
								"action", 
								"iduser"
								},
						new String[] {
								"user",
								"statement",
								PreferenceManager.getInstance().getUserId()
								});
myAsyncTask.execute();
	}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		if(from.equalsIgnoreCase("getStatementList"))
		{
			if(output!=null)
			{

				try {
				Log.e("Response", output);
				JSONObject rootObj = new JSONObject(output);
				if(rootObj.getBoolean("result")){
					JSONArray statementList = rootObj.getJSONArray("statement");
					feedList.clear();
					for(int i =0; i < statementList.length(); i++){
						JSONObject subRoot = statementList.getJSONObject(i);
						feedList.add(new Statement_Class(subRoot.getString("date_of_transaction"), 
								subRoot.getString("transaction_type"),
								subRoot.getString("amount")));
					}
					adapter.notifyDataSetChanged();
				}else{
					Toast.makeText(getActivity(), "No Transaction Found!", Toast.LENGTH_SHORT).show();
				}
				
			} catch (Exception e) {
				Toast.makeText(getActivity(), "Error Occured to Get Datas! Try Again!", Toast.LENGTH_SHORT).show();
				e.printStackTrace();
			}

			
			}else
			{
				Toast.makeText(getActivity(), "Error Occured to Get Datas! Try Again!", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	
	
}