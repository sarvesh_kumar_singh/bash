package com.bash.Fragments;

import java.util.ArrayList;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bash.R;
import com.bash.Adapters.MyTrasaction_PendingFriendsAdapter;
import com.bash.ListModels.Pending_Friends_Class;
import com.bash.Utils.AppConstants;

public class MyTransaction_Pending_Fragment extends Fragment{
	
	View mRootView;
	ListView groupListView;
	MyTrasaction_PendingFriendsAdapter adapter;
	ArrayList<Pending_Friends_Class> feedList = new ArrayList<Pending_Friends_Class>();
	View sortByView, filterByView;
	Dialog sortByDialog, filterByDialog;
	RadioGroup sortbyRadioGroup, filterbyRadioGroup;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_pendings_friends, null);
		initializeView();
		return mRootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
	}
	
	private void initializeView() {
		// TODO Auto-generated method stub
		groupListView = (ListView)mRootView.findViewById(R.id.groupListView);
		if(feedList.size() == 0)
			setDatas();
		adapter = new MyTrasaction_PendingFriendsAdapter(getActivity(), feedList);
		groupListView.setAdapter(adapter);
		
		initializeAlterViews();
		
		((CheckBox)mRootView.findViewById(R.id.sortButton)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked){
					sortByDialog.show();
					((CheckBox)mRootView.findViewById(R.id.sortButton)).setChecked(false);
				}
						
					
			}
		});
		
		((CheckBox)mRootView.findViewById(R.id.filterButton)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked){
					filterByDialog.show();
					((CheckBox)mRootView.findViewById(R.id.filterButton)).setChecked(false);
				}
			}
		});
		
	}

	private void initializeAlterViews() {
		// TODO Auto-generated method stub
		sortByDialog = new Dialog(getActivity());
		filterByDialog = new Dialog(getActivity());
		
		sortByDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		filterByDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		sortByView = View.inflate(getActivity(), R.layout.alert_sortingdialog, null);
		filterByView = View.inflate(getActivity(), R.layout.alert_sortingdialog, null);
		
		((TextView)filterByView.findViewById(R.id.titleText)).setText("Filter By");
		
		sortByDialog.setContentView(sortByView);
		filterByDialog.setContentView(filterByView);

		sortbyRadioGroup = ((RadioGroup)sortByView.findViewById(R.id.sortGroup));
		filterbyRadioGroup = ((RadioGroup)filterByView.findViewById(R.id.sortGroup));
		
		((ImageView)filterByView.findViewById(R.id.cancelDialog)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				filterByDialog.dismiss();
			}
		});
		
		((ImageView)sortByView.findViewById(R.id.cancelDialog)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sortByDialog.dismiss();
			}
		});
		
		sortbyRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				switch (checkedId) {
				case R.id.byFriendButton:
					sortByDialog.dismiss();
					break;
				case R.id.byGroupButton:
					sortByDialog.dismiss();
					break;
				case R.id.byDateButton:
					sortByDialog.dismiss();
					break;
				default:
					break;
				}
			}
		});
		
		filterbyRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				switch (checkedId) {
				case R.id.byFriendButton:
					filterByDialog.dismiss();
					break;
				case R.id.byGroupButton:
					filterByDialog.dismiss();
					break;
				case R.id.byDateButton:
					filterByDialog.dismiss();
					break;
				default:
					break;
				}
			}
		});
		
	}

	private void setDatas() {
		// TODO Auto-generated method stub
		for(int i = 0; i<5; i++)
			feedList.add(new Pending_Friends_Class("Group"+String.valueOf(i), AppConstants.RASYMBOL+". "+String.valueOf(i+50)));
	}
	 
	
}
