package com.bash.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.Providers.ContainerProvider;
import com.bash.Utils.AppConstants;

public class ProfileFragment extends Fragment{
	
	public FragmentTabHost mTabhost;
	public static View mRootView;
	public static BaseFragment parentFragmentClass;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_home, null);
		initializeView();
		return mRootView;
	}
	
	private void initializeView() {
		// TODO Auto-generated method stub
		 ((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn, "My Bash", 0);
		 
		 ((ImageView)getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//Sarvesh
					//((HomeActivity) getActivity()).menu.toggle();
					((HomeActivity)getActivity()).slidingmenu_layout.toggleMenu();
				}
		 });
		 
		 
		View myBashView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_widget_bg_for_mybash, null);
		View myTransactionView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_widget_bg_for_mytransaction, null);
		
		mTabhost = (FragmentTabHost) mRootView.findViewById(R.id.fragmenttabhost);
		mTabhost.setup(getActivity(), getChildFragmentManager(), R.id.fragmentframe_holder);
		
 		// Fragment Tab Host Initialization... 
		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_MYBASH_TAG).setIndicator(myBashView),
				ContainerProvider.Tab_Mybash_Container.class, null);
		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_MYTRANSACTION_TAG).setIndicator(myTransactionView),
				ContainerProvider.Tab_MyTransaction_Container.class, null);
		
		parentFragmentClass = ((BaseFragment)getParentFragment());
	}
	
}
