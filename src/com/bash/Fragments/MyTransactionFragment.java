package com.bash.Fragments;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.BaseFragmentClasses.FragmentBaseFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class MyTransactionFragment extends Fragment {
	
	View mRootView;
	public static FragmentBaseFragment baseFragment;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {
    	mRootView = inflater.inflate(R.layout.fragment_mytransaction_page, null);
    	initializeFragmentTabHost();
    	return mRootView;
    }

    private void initializeFragmentTabHost() {
    	
    	((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn, "Bash", R.drawable.add_btn);
		
		((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//Sarvesh
				//((HomeActivity)getActivity()).menu.toggle();
				((HomeActivity)getActivity()).slidingmenu_layout.toggleMenu();
			}
		});
		
		((ImageView) getActivity().findViewById(R.id.toprightsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//ActivityManager.startActivity(getActivity(), AddTransactionActivity.class);
				HomeFragment.parentFragment.replaceFragment(new AddTransactionFragment(), true);
			}
		});
		
		baseFragment = (FragmentBaseFragment)getParentFragment();
		
		((RelativeLayout) mRootView.findViewById(R.id.allIcon)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				HomeFragment.parentFragment.replaceFragment(new MyTrans_All_Fragment(), true);
			}
		});
		
		((RelativeLayout) mRootView.findViewById(R.id.pendingIcon)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				HomeFragment.parentFragment.replaceFragment(new MyTrans_Pending_Fragment(), true);
			}
		});
		
		((RelativeLayout) mRootView.findViewById(R.id.processedIcon)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				HomeFragment.parentFragment.replaceFragment(new MyTrans_Processed_Fragment(), true);
			}
		});
  	}
    
    
}