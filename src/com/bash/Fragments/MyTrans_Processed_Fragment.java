package com.bash.Fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bash.R;
import com.bash.Activities.HomeActivity;
import com.bash.Adapters.TransactionAdapter;
import com.bash.ListModels.Transaction_Class;
import com.bash.Managers.AsyncResponse;
import com.bash.Managers.DialogManager;
import com.bash.Managers.MyAsynTaskManager;
import com.bash.Managers.PreferenceManager;
import com.bash.Providers.ContainerProvider;
import com.bash.Utils.AppConstants;
import com.bash.Utils.AppUrlList;

@SuppressLint("ValidFragment")
public class MyTrans_Processed_Fragment extends Fragment implements AsyncResponse{
	MyAsynTaskManager  myAsyncTask;
	View mRootView;
	FragmentTabHost mTabhost;
	public static ArrayList<Transaction_Class> friendsList = new ArrayList<Transaction_Class>();
	public static ArrayList<Transaction_Class> groupList = new ArrayList<Transaction_Class>();
	
	public static TransactionAdapter friendsAdapter, groupAdapter;
	
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_home, null);
		mTabhost = (FragmentTabHost) mRootView.findViewById(R.id.fragmenttabhost);
		mTabhost.setup(getActivity(), getChildFragmentManager(), R.id.fragmentframe_holder);
		initializeTabHost();
		return mRootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
	}
	
	private void initializeTabHost() {
		// TODO Auto-generated method stub
		((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.back_btn, "Processed", 0);
		
		((ImageView) getActivity().findViewById(R.id.topleftsideImage)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				HomeFragment.parentFragment.popFragment();
			}
		});
		
		View friendsView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_widget_bg_for_friends, null);
		View groupsView = LayoutInflater.from(getActivity()).inflate(R.layout.tab_widget_bg_for_groups, null);
		
		mTabhost.setBackgroundColor(Color.parseColor("#FFFFFF"));
		
		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_HOME_TOTALBALANCE_FRIENDS_TAG).setIndicator(friendsView),
				ContainerProvider.Tab_Home_MyTransaction_Processed_Friends_Container.class, null);
		
		mTabhost.addTab(mTabhost.newTabSpec(AppConstants.TAB_HOME_TOTALBALANCE_GROUP_TAG).setIndicator(groupsView),
				ContainerProvider.Tab_Home_MyTransaction_Processed_Groups_Container.class, null);
		
		friendsAdapter = new TransactionAdapter(getActivity(), friendsList);
		groupAdapter = new TransactionAdapter(getActivity(), groupList);
		
		getAllTransactionDetails();
	}
	
	public void getAllTransactionDetails(){
		myAsyncTask=new MyAsynTaskManager();
		myAsyncTask.delegate=this;
		myAsyncTask.setupParamsAndUrl("getAllTransactionDetails",getActivity(),AppUrlList.ACTION_URL, new String[] {
				"module",
				"action",
				"iduser"
			 	}, 
				new String[]{
				"feed",
				"processedtransaction",
				PreferenceManager.getInstance().getUserId()
				});

		myAsyncTask.execute();
	 
}

	@Override
	public void backgroundProcessFinish(String from, String output) {
		// TODO Auto-generated method stub
		if(from.equalsIgnoreCase("getAllTransactionDetails"))
		{
			if(output!=null)
			{

				try{
					JSONObject rootObj = new JSONObject(output);
					//new Transaction_Class(paidByName, paidForName, paymentComment, paymentDate, payerImage, transactionId, status, paymentLocation)
					friendsList.clear();
					groupList.clear();

					if(rootObj.getBoolean("result")){

						JSONArray jsonArray = rootObj.getJSONArray("friends");
						JSONObject jObj;
						for(int i = 0; i<jsonArray.length(); i++){
							jObj = jsonArray.getJSONObject(i);
							friendsList.add(new Transaction_Class(
									jObj.getString("paid_by"),
									jObj.getString("paid_for"),
									jObj.getString("feed_comment"),
									jObj.getString("recorded_on"),
									jObj.getString("imagepath"),
									jObj.getString("idtrans"),
									jObj.getString("status"),
									jObj.getString("location"),
									jObj.getString("amount")));
						}

						jsonArray = rootObj.getJSONArray("group");
						for(int i = 0; i<jsonArray.length(); i++){
							jObj = jsonArray.getJSONObject(i);
							groupList.add(new Transaction_Class(
									jObj.getString("paid_by"),
									jObj.getString("paid_for"),
									jObj.getString("feed_comment"),
									jObj.getString("recorded_on"),
									jObj.getString("imagepath"),
									jObj.getString("idtrans"),
									jObj.getString("status"),
									jObj.getString("location"),
									jObj.getString("amount")));
						}

						friendsAdapter.notifyDataSetChanged();
						groupAdapter.notifyDataSetChanged();

					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");
				}

			}
			else
			{

				DialogManager.showDialog(getActivity(), "Server Error Occured! Try Again!");

			}
		}

	}
	
}
