package com.bash.Adapters;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.ItemWiseSplitActivity;
import com.bash.ListModels.ItemWiseListModel;
import com.bash.ListModels.PhotosListModel;
import com.bash.ListModels.TwoWayPersonModel;

public class ItemWiseListAdapter extends ArrayAdapter<ItemWiseListModel>{
	Activity activity;
	LayoutInflater inflater;
	int resource;
	ItemWiseTwoWayViewHolder holder = null;
	ArrayList<ItemWiseListModel> itemWiseListModelList;
	Toast mtoast;
	ImageView imageNext;
	public int itemSelectPossition = -1;

	ArrayList<TwoWayPersonModel> twowayPersonModelList = null;
	
	public ItemWiseListAdapter(Activity activity, int resource,
			ArrayList<ItemWiseListModel> itemWiseListModelList) {
		super(activity, resource, itemWiseListModelList);
		this.activity = activity;
		this.inflater = (LayoutInflater) this.activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.itemWiseListModelList = itemWiseListModelList;
		this.resource = resource;
		twowayPersonModelList = new ArrayList<TwoWayPersonModel>();
	}
	


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return itemWiseListModelList.size();
	}

	@Override
	public ItemWiseListModel getItem(int position) {
		// TODO Auto-generated method stub
		return itemWiseListModelList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {

			LayoutInflater inflater = LayoutInflater.from(activity);
			convertView = inflater.inflate(resource, null);
			holder = new ItemWiseTwoWayViewHolder();
			holder.itemRemoveimg = (ImageView) convertView
					.findViewById(R.id.removeitemicon);
			holder.itemSharedName = (TextView) convertView
					.findViewById(R.id.shareditemwise);
			holder.itemName =(EditText) convertView
					.findViewById(R.id.nameitemwise);
			holder.itemAmount =(EditText) convertView
					.findViewById(R.id.amountitemwise);
			
			
			convertView.setTag(holder);
		} else {
			holder = (ItemWiseTwoWayViewHolder) convertView.getTag();
		}
		ItemWiseTwoWayViewHolder holder = (ItemWiseTwoWayViewHolder) convertView.getTag();
    	ItemWiseListModel itemWiseListModel = getItem(position);
	
		holder.itemName.setText(itemWiseListModel.getPrsnName());
		holder.itemAmount.setText(itemWiseListModel.getItemAmount());
		holder.itemName.setText(itemWiseListModel.getPrsnName());
		
		
		if ((itemWiseListModel.getTwowayPersonList().size() == 1) && 
				(itemWiseListModel.getTwowayPersonList().get(0).getId().length() != 0)){
				
			holder.itemSharedName.setText((itemWiseListModel.getTwowayPersonList().get(0).getPersonName())+" got this");
		}
		else if((itemWiseListModel.getTwowayPersonList().size() > 1)){
			StringBuilder sb = new StringBuilder();
			sb.append("Shared by ");
			
			for(TwoWayPersonModel twoWayPersonModel : (itemWiseListModel.getTwowayPersonList())){
				
				sb.append((twoWayPersonModel.getPersonName())+", ");	
			}
			holder.itemSharedName.setText(sb.toString());
		}else{
			holder.itemSharedName.setText("");
		}
	
		
		if(getItem(position).isItemSelect()){
			LinearLayout linearitemwise = (LinearLayout) convertView.findViewById(R.id.itemwiselinear);
			linearitemwise.setBackgroundColor(activity.getResources().getColor(R.color.app_color));	
		}else{
			LinearLayout linearitemwise = (LinearLayout) convertView.findViewById(R.id.itemwiselinear);
			linearitemwise.setBackgroundColor(activity.getResources().getColor(android.R.color.white));
		}
		
		
		
		convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
            	ItemWiseTwoWayViewAdapter itemWiseTwoWayViewAdapter = ((ItemWiseSplitActivity) activity).getItemWiseTwoWayViewAdapter();
            	
                setListClick(position);
                itemWiseTwoWayViewAdapter.showSharedPerson(getItem(position).getTwowayPersonList());
            }
        });		
		return convertView;
	}

	private class ItemWiseTwoWayViewHolder {
		ImageView itemRemoveimg;
		TextView itemSharedName;
		EditText itemName, itemAmount;
	}
	public boolean isLastFilled() {
		int lastCount = 0;
		if(getCount() != 0){
			lastCount = (getCount() - 1);
		}else{
			lastCount = 0;
		}	
		if(lastCount >= 0){			
			String itemName = "";
			String itemamount = "";
			
			View view=ItemWiseSplitActivity.lvitemwiselist.getChildAt(lastCount);
			itemamount = ((ItemWiseTwoWayViewHolder) view.getTag()).itemAmount.getText().toString();
			itemName = ((ItemWiseTwoWayViewHolder) view.getTag()).itemName.getText().toString();
			if((itemName.length() > 0) && (itemamount.length() > 0)){
				
				itemWiseListModelList.remove(lastCount);
				twowayPersonModelList.clear();
				twowayPersonModelList.add(new TwoWayPersonModel("","",""));
				itemWiseListModelList.add(new ItemWiseListModel(itemName,itemamount,twowayPersonModelList));
				
				return true;		
			}else{
				return false;
			}	
		}
		
		return false;
	}
	private void setListClick(int position){
		
		if(itemSelectPossition == position){
			itemSelectPossition = -1;
		}else{
			itemSelectPossition = position;
		}
		
		Toast.makeText(activity, "item clicked---"+itemSelectPossition, Toast.LENGTH_SHORT).show();

		for(int i = 0; i < getCount(); i++){
			if(i == position){
				if(getItem(i).isItemSelect()){
					
					getItem(i).setItemSelect(false);	
				}else{
					
					getItem(i).setItemSelect(true);	
				}	
				
			}else{		
				
				getItem(i).setItemSelect(false);	
			}
		}
		this.notifyDataSetChanged();
	}
	
public void setSharedPerson(PhotosListModel photosListModel, boolean isImageSelect){

		ArrayList<TwoWayPersonModel> twowayPersonList = new ArrayList<TwoWayPersonModel>();
		
		for(TwoWayPersonModel twoWayPersonModel : getItem(itemSelectPossition).getTwowayPersonList()){
			
			twowayPersonList.add(twoWayPersonModel);
			
		}
		
		int size = twowayPersonList.size();
		
		if(size > 0){
		
		if((twowayPersonList.get(size-1).getId().length() > 0)){
			
			if(isImageSelect){
				
				twowayPersonList.add(new TwoWayPersonModel(photosListModel.getID(), photosListModel.getName(), photosListModel.getUserType()));
			}
			else{
				if(twowayPersonList.size() == 1){
					
					twowayPersonList.remove(size-1);
					twowayPersonList.add(new TwoWayPersonModel("","",""));
					
				}else if(twowayPersonList.size() > 1){
			
					for(int i = 0;i < twowayPersonList.size(); i++){
						
						if(twowayPersonList.get(i).getId().equals(photosListModel.getID())){
							
							twowayPersonList.remove(i);	
						}	
					}	
				}
			}
		} else if((twowayPersonList.get(size-1).getId().length() == 0)){
			
			if(isImageSelect){
				twowayPersonList.remove(size-1);
			
				TwoWayPersonModel twowayPersonObj  = new TwoWayPersonModel(photosListModel.getID(),photosListModel.getName(),photosListModel.getUserType());
				
				twowayPersonList.add(twowayPersonObj);
					
			}
			
		}
		itemWiseListModelList.get(itemSelectPossition).setTwowayPersonList(twowayPersonList);
		notifyDataSetChanged();
		}
		
	}
	public  int getItemSelectPossition() {
		return itemSelectPossition;
	}

	public  void setItemSelectPossition(int itemSelectPossition) {
		this.itemSelectPossition = itemSelectPossition;
	}

}

