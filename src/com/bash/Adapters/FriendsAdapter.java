package com.bash.Adapters;

import java.util.ArrayList;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Fragments.HomeFragment;
import com.bash.Fragments.MyFriends_Transaction_Fragment;
import com.bash.Fragments.MyTrans_All_PayFriendFragment;
import com.bash.ListModels.BashUsers_Class;
import com.bash.ListModels.MyTrans_Friends_Class;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.bash.Utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

public class FriendsAdapter extends BaseAdapter
{
	 	private ArrayList<MyTrans_Friends_Class>  feedList = new ArrayList<MyTrans_Friends_Class>();
		private Context context;
	    
	    public FriendsAdapter(Context context, ArrayList<MyTrans_Friends_Class> feedList) {
	        this.context = context;
	    	this.feedList = feedList;
	    }

	    public void notifyWithDataSet(ArrayList<MyTrans_Friends_Class> feedList){
	    	
	    	this.feedList = feedList;
	    	this.notifyDataSetChanged();
	    }
	    
	    @Override
	    public View getView(final int position, View convertView, ViewGroup parent) {
	        ViewHolder holder = null;
	        final MyTrans_Friends_Class listItem = getItem(position);
	        if (convertView == null) {
	        	LayoutInflater inflater = LayoutInflater.from(context);
	        	//convertView = inflater.inflate(R.layout.custom_cashin_listview, null);
	        	convertView = inflater.inflate(R.layout.custom_cash_in_out_for_friendgroup_listview, null);
	            holder = new ViewHolder();
	            holder.userImage = (ImageView) convertView.findViewById(R.id.userImage);
	            holder.transactionText = (TextView) convertView.findViewById(R.id.transactionText);
	            holder.cellLayout = (RelativeLayout) convertView.findViewById(R.id.cellLayout);
	            
	            if(listItem.getImagepath() != null && listItem.getImagepath().length() != 0)
	            	ImageLoader.getInstance().displayImage(listItem.getImagepath(), holder.userImage);
	            
	            
	            convertView.setTag(holder);
	            
	        } else {
	            holder = (ViewHolder) convertView.getTag();
	        }

	      //  holder.userImage.setImageResource(listItem.getpayerImage());
	        /*holder.transactionText.setText(Html.fromHtml(
        			"<font color='#00BDCC' size='16'>"+ listItem.getFristName() + "</font>" + " has paid"
        			+"<font color='#00BDCC' size='16'> You. </font>"
	        		));
	        		holder.transactionText.setText(Html.fromHtml(
	        			"<font color='#00BDCC' size='16'>"+ listItem.getFristName() + "</font>" + " has paid "
	        			+"<font color='#00BDCC' size='16'> You </font>"+" </font>"+AppConstants.RASYMBOL+" "+listItem.getCharge()+"."
		        		));
	        */
	        
	        /*holder.transactionText.setText(Html.fromHtml(
        			"<font color='#00BDCC' size='16'> You </font> have paid "
        			+"<font color='#00BDCC' size='16'>"+listItem.getFristName()+"</font> for "+
        					AppConstants.RASYMBOL+" "+listItem.getCharge()+"."
	        		));*/
	        
	        if(listItem.getIdPaidBy().equals(PreferenceManager.getInstance().getUserId()))
	        	holder.transactionText.setText(Html.fromHtml(
	        			Utils.getAppColorText(listItem.getFristName())+" owes "+Utils.getAppColorText("you")+" "+ AppConstants.RASYMBOL+" "+
	        									listItem.getCharge()));
	        else
	        	holder.transactionText.setText(Html.fromHtml(
	        			Utils.getAppColorText("You")+" owe "+Utils.getAppColorText(listItem.getFristName())+" "
	        				+AppConstants.RASYMBOL+" "+listItem.getCharge()+"."));
	        
	        /*holder.dateofPayment.setText(Utils.getFormattedTimerText(listItem.getRecordedOn()).toString());
            holder.locationText.setText(listItem.getLocation());*/
           
	        return convertView;
	    }

	    @Override
	    public int getCount() {
	        return feedList.size();
	    }

	    @Override
	    public MyTrans_Friends_Class getItem(int position) {
	        return feedList.get(position);
	    }

	    @Override
	    public long getItemId(int position) {
	        return position;
	    }

	    private class ViewHolder {
	    	
	    	TextView resonforPayment;
	    	TextView transactionText;
	    	TextView dateofPayment;
	    	TextView locationText;
	    	ImageView userImage;
	    	RelativeLayout cellLayout;
	     }

}
