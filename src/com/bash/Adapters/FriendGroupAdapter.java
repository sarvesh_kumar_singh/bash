package com.bash.Adapters;
	
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.Html;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bash.R;
import com.bash.Application.BashApplication;
import com.bash.Fragments.HomeFragment;
import com.bash.Fragments.MyFriends_Transaction_Fragment;
import com.bash.Fragments.MyGroups_Transaction_Fragment;
import com.bash.ListModels.BashUsers_Class;
import com.bash.ListModels.MyGroups_Class;
import com.bash.ListModels.MyTrans_FriendsGroups_Class;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.bash.Utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
	
	public class FriendGroupAdapter extends BaseExpandableListAdapter{
	
		public Context _context;
		public List<String> _listDataHeader; // header titles
		public SparseArray<ArrayList<MyTrans_FriendsGroups_Class>> _listDataChild = new SparseArray<ArrayList<MyTrans_FriendsGroups_Class>>();
		
		public FriendGroupAdapter(Context context, List<String> listDataHeader,
				SparseArray<ArrayList<MyTrans_FriendsGroups_Class>> listChildData) {
			this._context = context;
			this._listDataHeader = listDataHeader;
			if(listChildData != null && listChildData.size() != 0)
				this._listDataChild = listChildData;
				
		}
		
		public void notifyWithDataSet(SparseArray<ArrayList<MyTrans_FriendsGroups_Class>> _listDataChild){
	    	this._listDataChild = _listDataChild;
	    	this.notifyDataSetChanged();
	    }
		
	
		@Override
		public Object getChild(int groupPosition, int childPosititon) {
			/*return this._listDataChild.get(this._listDataHeader.get(groupPosition))
					.get(childPosititon);*/
			return this._listDataChild.get(groupPosition).get(childPosititon);
			
		}
	
		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}
	
		@Override
		public View getChildView(int groupPosition, final int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
	
			final MyTrans_FriendsGroups_Class childDatas = (MyTrans_FriendsGroups_Class) getChild(groupPosition, childPosition);
	
			if (convertView == null) {
				LayoutInflater infalInflater = (LayoutInflater) this._context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater.inflate(R.layout.custom_cash_in_out_for_friendgroup_listview, null);
			}
			
			ImageView userImage = (ImageView) convertView.findViewById(R.id.userImage);
			
			TextView secondName = (TextView) convertView.findViewById(R.id.secondName);
			TextView transactionType = (TextView) convertView.findViewById(R.id.transactionType);
			TextView firstName = (TextView) convertView.findViewById(R.id.firstName);
			TextView amountText = (TextView) convertView.findViewById(R.id.amountText);
			
			RelativeLayout cellLayout = (RelativeLayout) convertView.findViewById(R.id.cellLayout);
	            
			if(childDatas.getImagepath()!=null && childDatas.getImagepath().length() != 0)
				ImageLoader.getInstance().displayImage(childDatas.getImagepath(), 
	        			userImage, BashApplication.options, BashApplication.animateFirstListener);
					//ImageLoader.getInstance().displayImage(childDatas.getImagepath(), userImage);
			
			if(childDatas.getType().equals("friend")) {
				if(childDatas.getIdPaidBy().equals(PreferenceManager.getInstance().getUserId())){
					firstName.setText((childDatas.getFristName().length() < 7 ?  childDatas.getFristName() : childDatas.getFristName().substring(0, 6)+".."));
					secondName.setText("you");
					transactionType.setText(" owes ");
					amountText.setText(" "+AppConstants.RASYMBOL+" "+(childDatas.getCharge().length() < 7 ?  childDatas.getCharge() : childDatas.getCharge().substring(0, 6)+".."));
					
					firstName.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
								HomeFragment.parentFragment.replaceFragment(new MyFriends_Transaction_Fragment(
									new BashUsers_Class(
											childDatas.getidPaidFor(),
											" ", 
											childDatas.getFristName(), 
											" ",
											" ",
											" ", 
											" ")),
										true);		
						}
					});
					/*transactionText.setText(Html.fromHtml(
		        			Utils.getAppColorText(childDatas.getFristName())+" owes "+Utils.getAppColorText("you")+" "+ AppConstants.RASYMBOL+" "+
		        					childDatas.getCharge()));*/
				} else {
					
					firstName.setText("You");
					secondName.setText((childDatas.getFristName().length() < 7 ?  childDatas.getFristName() : childDatas.getFristName().substring(0, 6)+".."));
					transactionType.setText(" owe ");
					amountText.setText(" "+AppConstants.RASYMBOL+" "+(childDatas.getCharge().length() < 7 ?  childDatas.getCharge() : childDatas.getCharge().substring(0, 6)+".."));
					secondName.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
								HomeFragment.parentFragment.replaceFragment(new MyFriends_Transaction_Fragment(
									new BashUsers_Class(
											childDatas.getIdPaidBy(),
											" ", 
											childDatas.getFristName(), 
											" ",
											" ",
											" ", 
											" ")),
										true);		
						}
					});
					
					/*transactionText.setText(Html.fromHtml(
		        			Utils.getAppColorText("You")+" owe "+Utils.getAppColorText(childDatas.getFristName())+" "
		        				+AppConstants.RASYMBOL+" "+childDatas.getCharge()+"."));*/
				}
				
			}else if(childDatas.getType().equals("group")){
				
				firstName.setText(childDatas.getGroupName());
				secondName.setText("");
				transactionType.setText("");
				amountText.setText("");
				
				/*transactionText.setText(Html.fromHtml(
	        			Utils.getAppColorText(childDatas.getGroupName())));*/
				cellLayout.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							HomeFragment.parentFragment.replaceFragment(new MyGroups_Transaction_Fragment(
									new MyGroups_Class(childDatas.getGroupName(), 
											childDatas.getidPaidFor(),
											childDatas.getImagepath())),
										true);		
						}
				});
			}
			
		 	return convertView;
		}
	
		@Override
		public int getChildrenCount(int groupPosition) {
			/*return this._listDataChild.get(this._listDataHeader.get(groupPosition))
					.size();*/
			if(_listDataChild.size() != 0)
				return this._listDataChild.get(groupPosition).size();
			else
				return 0;
		}
	
		@Override
		public Object getGroup(int groupPosition) {
			return this._listDataHeader.get(groupPosition);
		}
	
		@Override
		public int getGroupCount() {
			return this._listDataHeader.size();
		}
	
		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}
	
		@Override
		public View getGroupView(final int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			String headerTitle = (String) getGroup(groupPosition);
			if (convertView == null) {
				LayoutInflater infalInflater = (LayoutInflater) this._context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater.inflate(R.layout.custom_myfriends_transactions_parentview, null);
			}
			
			TextView monthText = (TextView) convertView.findViewById(R.id.monthText);
			monthText.setText(headerTitle);
			
			return convertView;
		}
	
	 
		@Override
		public boolean hasStableIds() {
			return false;
		}
		
		@Override
		public void onGroupExpanded(int groupPosition) {
			// TODO Auto-generated method stub
			super.onGroupExpanded(groupPosition);
		}
	
		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}
		
		 
	 
}
		 
	
	
	
