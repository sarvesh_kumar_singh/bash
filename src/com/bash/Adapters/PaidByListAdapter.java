package com.bash.Adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.PaidbySplitActivity;
import com.bash.CustomViews.CircularImageView;
import com.bash.ListModels.Person;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

public class PaidByListAdapter extends ArrayAdapter<Person>{
	Activity activity;
	LayoutInflater inflater;
	int resource;
	PersonItemViewHolder holder = null;
	public ArrayList<Person> addedPersonArrayList;
	Toast mtoast;
	ImageView imageNext;
	boolean isEditable;
	private static EditText editText[];
	
	public PaidByListAdapter(Activity activity, int resource,
			ArrayList<Person> addedPersonArrayList) {
		super(activity, resource, addedPersonArrayList);
		this.activity = activity;
		this.inflater = (LayoutInflater) this.activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.addedPersonArrayList = addedPersonArrayList;
		this.resource = resource;
		this.isEditable = true;
		editText = new EditText[addedPersonArrayList.size()];
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return addedPersonArrayList.size();
	}

	@Override
	public Person getItem(int position) {
		// TODO Auto-generated method stub
		return addedPersonArrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {

			LayoutInflater inflater = LayoutInflater.from(activity);
			convertView = inflater.inflate(resource, null);
			holder = new PersonItemViewHolder();
			holder.paidbyimageview = (CircularImageView) convertView
					.findViewById(R.id.paidbyimageview);
			holder.paidbyamount = (EditText) convertView
					.findViewById(R.id.paidbyamount);
			holder.paidbyname = (TextView) convertView
					.findViewById(R.id.paidbyname);
			holder.paidbyckbx =(CheckBox) convertView
					.findViewById(R.id.paidbyckbx);
			holder.paidbyamount.setTag("edit"+position);
			
			editText[position] = holder.paidbyamount;
			
			convertView.setTag(holder);
		} else {
			holder = (PersonItemViewHolder) convertView.getTag();
		}
		
		final PersonItemViewHolder holder = (PersonItemViewHolder) convertView.getTag();
		final Person personObj = getItem(position);
	
		
		if((personObj.getImage_location() != null) && (personObj.getImage_location().length() > 0)){
			UrlImageViewHelper
			.setUrlDrawable(
					holder.paidbyimageview,personObj.getImage_location());
			//Toast.makeText(activity, photosListObj.getName(), Toast.LENGTH_SHORT).show();
		}
		
		holder.paidbyname.setText(personObj.getName());
		
		holder.paidbyckbx.setChecked(personObj.isChechbxbool());
		
		holder.paidbyckbx.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
					if(isChecked){	
						isEditable = false;	
						for (Person person : addedPersonArrayList) {
							if(person == personObj){							
								person.setChechbxbool(true);
								person.setPaidamount(PaidbySplitActivity.totalAmount);	
								person.setPaidpersonbool(true);
							}else{
								person.setPaidamount(0.0f);	
								person.setChechbxbool(false);
								person.setPaidpersonbool(false);
								
							}		
							
						}
						
						
					}else{
						personObj.setPaidpersonbool(false);
						personObj.setChechbxbool(false);
						personObj.setPaidamount(0.0f);	
						isEditable = true;
					}
		
					
					notifyDataSetChanged();
			}
		});
	
		if(isEditable){
			holder.paidbyamount.setFocusable(true);
			holder.paidbyamount.setFocusableInTouchMode(true);
			holder.paidbyamount.setClickable(true);
		}else{
			holder.paidbyamount.setFocusable(false);
			holder.paidbyamount.setClickable(false);
		}
		
		return convertView;
	}

	private class PersonItemViewHolder {
		CircularImageView paidbyimageview;
		TextView paidbyname;
		EditText paidbyamount;
		CheckBox paidbyckbx;
	}
	
	public float findTotalAmount(){
		float total = 0;
		
		for(int i = 0 ; i < getCount() ; i++){
			
			if(addedPersonArrayList.get(i).isChechbxbool()){
				
				return PaidbySplitActivity.totalAmount;
				
			}else{
				
				String enterAmount= editText[i].getText().toString();
				if(enterAmount.length() > 0)
				{
					addedPersonArrayList.get(i).setPaidamount(Float.parseFloat(enterAmount));
					addedPersonArrayList.get(i).setPaidpersonbool(true);
					total += Float.parseFloat(enterAmount);
				}else{
					
				}
			}
			
			
			
		}
		
		
		return total;
	}

}
