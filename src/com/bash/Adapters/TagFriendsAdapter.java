package com.bash.Adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bash.R;
import com.bash.ListModels.MyFriendOnGroup_Class;

public class TagFriendsAdapter extends BaseAdapter {
	private ArrayList<MyFriendOnGroup_Class> feedList = new ArrayList<MyFriendOnGroup_Class>();
	private ArrayList<MyFriendOnGroup_Class> originalList = new ArrayList<MyFriendOnGroup_Class>();
	private Context context;
	private Filter tagfilter;

	public TagFriendsAdapter(Context context, ArrayList<MyFriendOnGroup_Class> feedList) {
		this.context = context;
		this.feedList = feedList;
		this.originalList = feedList;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		MyFriendOnGroup_Class listItem = getItem(position);
		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			convertView = inflater.inflate(R.layout.custom_tagphotofriends_listview, null);
			holder = new ViewHolder();
			holder.friendName = (TextView) convertView.findViewById(R.id.friendName);
			holder.friendImageSource = (ImageView) convertView.findViewById(R.id.friendImageSource);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
/*
		holder.friendImageSource.setImageResource(listItem
				.getfriendImageSource());*/
		holder.friendName.setText(listItem.getfriendName());

		return convertView;
	}

	@Override
	public int getCount() {
		// checkEmptyList();
		return feedList.size();
	}

	@Override
	public MyFriendOnGroup_Class getItem(int position) {
		return feedList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	private class ViewHolder {
		TextView friendName;
		ImageView friendImageSource;
	}

	public void resetData() {
		feedList = originalList;
		notifyDataSetChanged();
	}

	public Filter getFilter() {
		if (tagfilter == null)
			tagfilter = new TagFilter();
		return tagfilter;
	}

	//filter Class... 
	private class TagFilter extends Filter {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			FilterResults results = new FilterResults();
			// We implement here the filter logic
			if (constraint == null || constraint.length() == 0) {
				// No filter implemented we return all the list
				results.values = originalList;
				results.count = originalList.size();
			} else {
				// We perform filtering operation
				List<MyFriendOnGroup_Class> tempList = new ArrayList<MyFriendOnGroup_Class>();
				for (MyFriendOnGroup_Class p : feedList) {
					if (p.getfriendName().toUpperCase()
							.startsWith(constraint.toString().toUpperCase()))
						tempList.add(p);
				}
				results.values = tempList;
				results.count = tempList.size();
			}
			return results;
		}

		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			// Now we have to inform the adapter about the new list filtered
			if (results.count == 0)
				notifyDataSetInvalidated();
			else {
				feedList = (ArrayList<MyFriendOnGroup_Class>) results.values;
				notifyDataSetChanged();
			}
		}
	}

}
