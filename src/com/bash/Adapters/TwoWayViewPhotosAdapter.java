package com.bash.Adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.CustomViews.CircularImageView;
import com.bash.ListModels.PhotosListModel;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

public class TwoWayViewPhotosAdapter extends ArrayAdapter<PhotosListModel>{
	Activity activity;
	LayoutInflater inflater;
	int resource;
	PhotosItemViewHolder holder = null;
	ArrayList<PhotosListModel> photosList;
	Toast mtoast;
	ImageView imageNext;
	public TwoWayViewPhotosAdapter(Activity activity, int resource,
			ArrayList<PhotosListModel> photosList) {
		super(activity, resource, photosList);
		this.activity = activity;
		this.inflater = (LayoutInflater) this.activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.photosList = photosList;
		this.resource = resource;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return photosList.size();
	}

	@Override
	public PhotosListModel getItem(int position) {
		// TODO Auto-generated method stub
		return photosList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {

			LayoutInflater inflater = LayoutInflater.from(activity);
			convertView = inflater.inflate(resource, null);
			holder = new PhotosItemViewHolder();
			holder.itemPhotos = (CircularImageView) convertView
					.findViewById(R.id.tworeceiveuserimageView);
			holder.itemtick = (ImageView) convertView
					.findViewById(R.id.tickimageview);
			holder.itemName = (TextView) convertView
					.findViewById(R.id.tworeceiveusernametext);
			holder.twowaymainlinear =(LinearLayout) convertView
					.findViewById(R.id.twowaymainlinear);
			
			convertView.setTag(holder);
		} else {
			holder = (PhotosItemViewHolder) convertView.getTag();
		}
		final PhotosItemViewHolder holder = (PhotosItemViewHolder) convertView.getTag();
		final PhotosListModel photosListObj = getItem(position);
	
		
		if((photosListObj.getImageLocation() != null) && (photosListObj.getImageLocation().length() > 0)){
			UrlImageViewHelper
			.setUrlDrawable(
					holder.itemPhotos,photosListObj.getImageLocation());
		}else {
			holder.itemPhotos.setImageResource(R.drawable.profile_small);
		}
		
		holder.itemName.setText(photosListObj.getName());
		holder.itemtick.setVisibility(View.VISIBLE);
		
		holder.twowaymainlinear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(photosListObj.isImageSelect()){
				holder.itemtick.setVisibility(View.GONE);
				photosListObj.setImageSelect(false);
				}else{
					holder.itemtick.setVisibility(View.VISIBLE);
					photosListObj.setImageSelect(true);
				}
			}
		});
		
		
		return convertView;
	}

	private class PhotosItemViewHolder {
		LinearLayout twowaymainlinear;
		CircularImageView itemPhotos;
		TextView itemName;
		ImageView itemtick;
	}
	public ArrayList<PhotosListModel> getAllPhotosList() {
		return photosList;
	}
}
