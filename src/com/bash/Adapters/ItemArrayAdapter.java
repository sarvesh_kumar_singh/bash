package com.bash.Adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bash.R;
import com.bash.ListModels.CurrencyModel;

public class ItemArrayAdapter extends ArrayAdapter<CurrencyModel> {
	private ArrayList<CurrencyModel> scoreList ;

    static class ItemViewHolder {
        TextView currencyCode;
        TextView currencyText;
    }

    public ItemArrayAdapter(Context context, int textViewResourceId, ArrayList<CurrencyModel> scoreList) {
        super(context, textViewResourceId);
        this.scoreList = scoreList;
    }


    @Override
	public int getCount() {
		return this.scoreList.size();
	}

    @Override
	public CurrencyModel getItem(int index) {
		return scoreList.get(index);
	}

    @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
        ItemViewHolder viewHolder;
		if (row == null) {
			LayoutInflater inflater = (LayoutInflater) this.getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.currency_item_layout, parent, false);
            viewHolder = new ItemViewHolder();
            viewHolder.currencyCode = (TextView) row.findViewById(R.id.currencycode);
            viewHolder.currencyText = (TextView) row.findViewById(R.id.currencytext);
            row.setTag(viewHolder);
		} else {
            viewHolder = (ItemViewHolder)row.getTag();
        }
        CurrencyModel currencyObj = getItem(position);
        viewHolder.currencyCode.setText(currencyObj.getCurrencyCode());
        viewHolder.currencyText.setText(currencyObj.getCurrencyText());
		return row;
	}
}