package com.bash.Adapters;

import java.util.ArrayList;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Fragments.HomeFragment;
import com.bash.Fragments.MyFriends_Transaction_Fragment;
import com.bash.Fragments.MyTrans_All_PayFriendFragment;
import com.bash.ListModels.BashUsers_Class;
import com.bash.ListModels.MyTabs_Model;
import com.bash.ListModels.MyTrans_Friends_Class;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.bash.Utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MyTabsAdapter extends BaseAdapter {
	private ArrayList<MyTabs_Model> myTabsList = new ArrayList<MyTabs_Model>();
	private Context context;

	public MyTabsAdapter(Context context, ArrayList<MyTabs_Model> myTabsList) {
		this.context = context;
		this.myTabsList = myTabsList;
	}

	public void notifyWithDataSet(ArrayList<MyTabs_Model> myTabsList) {

		this.myTabsList = myTabsList;
		this.notifyDataSetChanged();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		final MyTabs_Model listItem = getItem(position);
		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			// convertView = inflater.inflate(R.layout.custom_cashin_listview,
			// null);
			convertView = inflater.inflate(R.layout.custom_mytabs_listview,
					null);
			holder = new ViewHolder();
			holder.iviImage = (ImageView) convertView.findViewById(R.id.iviImage);
			holder.iviOption = (ImageView) convertView.findViewById(R.id.iviOption);
			holder.tviName = (TextView) convertView.findViewById(R.id.tviName);
			holder.tviAmount = (TextView) convertView.findViewById(R.id.tviAmount);

			holder.iviOption.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Toast.makeText(context, "It will come soon", Toast.LENGTH_LONG).show();
				}
			});
			holder.tviName.setText(listItem.getFristName());
			holder.tviAmount.setText(listItem.getCharge());
			
			if (listItem.getImagepath() != null
					&& listItem.getImagepath().length() != 0)
				ImageLoader.getInstance().displayImage(listItem.getImagepath(),
						holder.iviImage);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// holder.userImage.setImageResource(listItem.getpayerImage());
		/*
		 * holder.transactionText.setText(Html.fromHtml(
		 * "<font color='#00BDCC' size='16'>"+ listItem.getFristName() +
		 * "</font>" + " has paid"
		 * +"<font color='#00BDCC' size='16'> You. </font>" ));
		 * holder.transactionText.setText(Html.fromHtml(
		 * "<font color='#00BDCC' size='16'>"+ listItem.getFristName() +
		 * "</font>" + " has paid "
		 * +"<font color='#00BDCC' size='16'> You </font>"
		 * +" </font>"+AppConstants.RASYMBOL+" "+listItem.getCharge()+"." ));
		 */

		/*
		 * holder.transactionText.setText(Html.fromHtml(
		 * "<font color='#00BDCC' size='16'> You </font> have paid "
		 * +"<font color='#00BDCC' size='16'>"
		 * +listItem.getFristName()+"</font> for "+
		 * AppConstants.RASYMBOL+" "+listItem.getCharge()+"." ));
		 */

		/*if (listItem.getIdPaidBy().equals(
				PreferenceManager.getInstance().getUserId()))
			holder.transactionText.setText(Html.fromHtml(Utils
					.getAppColorText(listItem.getFristName())
					+ " owes "
					+ Utils.getAppColorText("you")
					+ " "
					+ AppConstants.RASYMBOL + " " + listItem.getCharge()));
		else
			holder.transactionText
					.setText(Html.fromHtml(Utils.getAppColorText("You")
							+ " owe "
							+ Utils.getAppColorText(listItem.getFristName())
							+ " " + AppConstants.RASYMBOL + " "
							+ listItem.getCharge() + "."));*/

		/*
		 * holder.dateofPayment.setText(Utils.getFormattedTimerText(listItem.
		 * getRecordedOn()).toString());
		 * holder.locationText.setText(listItem.getLocation());
		 */

		return convertView;
	}

	@Override
	public int getCount() {
		return myTabsList.size();
	}

	@Override
	public MyTabs_Model getItem(int position) {
		return myTabsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	private class ViewHolder {
		TextView tviName;
		TextView tviAmount;
		ImageView iviImage;
		ImageView iviOption;

	}

}
