package com.bash.Adapters;

import java.util.ArrayList;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bash.R;
import com.bash.ListModels.Statement_Class;
import com.bash.ListModels.Statement_Class;

public class StatementAdapter extends BaseAdapter
{
 	private ArrayList<Statement_Class>  feedList = new ArrayList<Statement_Class>();
	private Context context;
    
    public StatementAdapter(Context context, ArrayList<Statement_Class> feedList) {
        this.context = context;
    	this.feedList = feedList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        Statement_Class listItem = getItem(position);
        if (convertView == null) {
        	LayoutInflater inflater = LayoutInflater.from(context);
        	//convertView = inflater.inflate(R.layout.custom_cashin_listview, null);
        	convertView = inflater.inflate(R.layout.custom_mywallet_statement_listview, null);
            holder = new ViewHolder();
            holder.dateText = (TextView) convertView.findViewById(R.id.dateText);
            holder.amountText = (TextView) convertView.findViewById(R.id.amountText);
            holder.transactionTypeText = (TextView) convertView.findViewById(R.id.transactionTypeText);
            convertView.setTag(holder);
            
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.dateText.setText(listItem.getDateText());
        holder.amountText.setText(listItem.getAmountText());
        holder.transactionTypeText.setText(listItem.getTransactionTypeText());
        return convertView;
    }

    @Override
    public int getCount() {
        return feedList.size();
    }

    @Override
    public Statement_Class getItem(int position) {
        return feedList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
    	
    	TextView transactionTypeText;
    	TextView amountText;
    	TextView dateText;
    	
     }

}
