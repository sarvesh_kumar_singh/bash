package com.bash.Adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bash.R;
import com.bash.Fragments.BashMomentsFragment;
import com.bash.Fragments.TagPhotoFragment;
import com.bash.ListModels.BashMoments_Class;
import com.bash.Managers.PreferenceManager;

public class BashMomentsAdapter extends BaseAdapter
{
	 	private ArrayList<BashMoments_Class>  feedList = new ArrayList<BashMoments_Class>();
		private Context context;
	    public LinearLayout.LayoutParams imageParams 
	    					= new LinearLayout.LayoutParams(PreferenceManager.getInstance().getPercentageFromWidth(33),
	    							PreferenceManager.getInstance().getPercentageFromHeight(20));
	    
	    public BashMomentsAdapter(Context context, ArrayList<BashMoments_Class> feedList) {
	        this.context = context;
	    	this.feedList = feedList;
	    }

	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        ViewHolder holder = null;
	        BashMoments_Class listItem = getItem(position);
	        if (convertView == null) {
	        	LayoutInflater inflater = LayoutInflater.from(context);
	        	convertView = inflater.inflate(R.layout.custom_bashmoments_listview, null);
	            holder = new ViewHolder();
	            holder.firstImage = (ImageView) convertView.findViewById(R.id.firstImage);
	            holder.secondImage = (ImageView) convertView.findViewById(R.id.secondImage);
	            holder.thirdImage = (ImageView) convertView.findViewById(R.id.thirdImage);
	            
	            holder.firstImage.setLayoutParams(imageParams);
	            holder.secondImage.setLayoutParams(imageParams);
	            holder.thirdImage.setLayoutParams(imageParams);
	            
	        //    holder.fourthImage = (ImageView) convertView.findViewById(R.id.fourthImage);
	            convertView.setTag(holder);	
	        } else {
	            holder = (ViewHolder) convertView.getTag();
	        }
 
	        holder.firstImage.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					BashMomentsFragment.parentFragment.replaceFragment(new TagPhotoFragment(R.drawable.addphoto_img_block), true);
				}
			});
	        holder.secondImage.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					BashMomentsFragment.parentFragment.replaceFragment(new TagPhotoFragment(R.drawable.addphoto_img_block), true);
				}
			});
	        holder.thirdImage.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					BashMomentsFragment.parentFragment.replaceFragment(new TagPhotoFragment(R.drawable.addphoto_img_block), true);
				}
			});
	        
	     /*   holder.fourthImage.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					BashMomentsFragment.parentFragment.replaceFragment(new TagPhotoFragment(R.drawable.profiletwo), true);
				}
			});*/
	        
	        
	        
	        return convertView;
	    }

	    @Override
	    public int getCount() {
	        return feedList.size();
	    }

	    @Override
	    public BashMoments_Class getItem(int position) {
	        return feedList.get(position);
	    }

	    @Override
	    public long getItemId(int position) {
	        return position;
	    }

	    private class ViewHolder {
	        ImageView firstImage;
	        ImageView secondImage;
	        ImageView thirdImage;
	     //   ImageView fourthImage;
	    }

}
