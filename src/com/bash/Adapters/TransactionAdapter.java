package com.bash.Adapters;

import java.util.ArrayList;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bash.R;
import com.bash.ListModels.Transaction_Class;
import com.bash.Managers.PreferenceManager;
import com.bash.Utils.AppConstants;
import com.bash.Utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

public class TransactionAdapter extends BaseAdapter
{
	 	private ArrayList<Transaction_Class>  feedList = new ArrayList<Transaction_Class>();
		private Context context;
	    
	    public TransactionAdapter(Context context, ArrayList<Transaction_Class> feedList) {
	        this.context = context;
	    	this.feedList = feedList;
	    }

	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        ViewHolder holder = null;
	        Transaction_Class listItem = getItem(position);
	        if (convertView == null) {
	        	LayoutInflater inflater = LayoutInflater.from(context);
	        	//convertView = inflater.inflate(R.layout.custom_cashin_listview, null);
	        	convertView = inflater.inflate(R.layout.custom_cash_in_out_listview, null);
	            holder = new ViewHolder();
	            
	            holder.userImage = (ImageView) convertView.findViewById(R.id.userImage);
	            holder.transactionText = (TextView) convertView.findViewById(R.id.transactionText);
	            holder.resonforPayment = (TextView) convertView.findViewById(R.id.resonforPayment);
	            holder.dateofPayment = (TextView) convertView.findViewById(R.id.dateofPayment);
	            holder.locationText = (TextView) convertView.findViewById(R.id.locationText);
	            convertView.setTag(holder);
	            
	        } else {
	            holder = (ViewHolder) convertView.getTag();
	        }

	        // holder.userImage.setImageResource(listItem.getpayerImage());
	        if(!listItem.getpayerImage().equals(""))
	        	ImageLoader.getInstance().displayImage(listItem.getpayerImage(), holder.userImage);
	        
	        Log.e("UserName", PreferenceManager.getInstance().getUserFullName());
	        
	        if(listItem.getpaidByName().equals(PreferenceManager.getInstance().getUserFullName()))
		        holder.transactionText.setText(Html.fromHtml(
	        			"<font color='#00BDCC' size='16'> You </font>" + " have paid to "
	        			+"<font color='#00BDCC' size='16'>"+listItem.getpaidForName()+" </font>"
		        		));
	        else
	        	holder.transactionText.setText(Html.fromHtml(
	        			"<font color='#00BDCC' size='16'>"+ listItem.getpaidByName() + "</font>" + " has paid"
	        			+"<font color='#00BDCC' size='16'> You. </font>"
		        		));
	        
            holder.resonforPayment.setText(AppConstants.RASYMBOL+ " "+ listItem.getAmount()+" for "+listItem.getpaymentComment());
            holder.dateofPayment.setText(Utils.getFormattedTimerText(listItem.getpaymentDate()));
            holder.locationText.setText(listItem.getpaymentLocation());
            return convertView;
	    }

	    @Override
	    public int getCount() {
	        return feedList.size();
	    }

	    @Override
	    public Transaction_Class getItem(int position) {
	        return feedList.get(position);
	    }

	    @Override
	    public long getItemId(int position) {
	        return position;
	    }

	    private class ViewHolder {
	    	
	    	TextView resonforPayment;
	    	TextView transactionText;
	    	TextView dateofPayment;
	    	TextView locationText;
	    	ImageView userImage;
	     }

}
