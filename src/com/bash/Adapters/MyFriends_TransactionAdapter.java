package com.bash.Adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;

import com.bash.R;
import com.bash.Fragments.MyFriendsFragment;
import com.bash.Fragments.MyGroupsFragment;
import com.bash.ListModels.MyFriends_Transactions_Class;
import com.bash.Managers.PreferenceManager;

public class MyFriends_TransactionAdapter extends BaseAdapter
{
	 	private ArrayList<MyFriends_Transactions_Class>  feedList = new ArrayList<MyFriends_Transactions_Class>();
		private Context context;
	    private LinearLayout.LayoutParams backViewParams;
	    //private MyFriends_Transactions_Class emptyList = new MyFriends_Transactions_Class("empty", 0);
	    
	    public MyFriends_TransactionAdapter(Context context, ArrayList<MyFriends_Transactions_Class> feedList) {
	        this.context = context;
	    	this.feedList = feedList;
	    	backViewParams = new LinearLayout.LayoutParams(PreferenceManager.getInstance().getPercentageFromWidth(20), 
	    			LayoutParams.MATCH_PARENT);
	    }

	    @Override
	    public View getView(final int position, View convertView, ViewGroup parent) {
	        ViewHolder holder = null;
	        MyFriends_Transactions_Class listItem = getItem(position);
	        if (convertView == null) {
	        	LayoutInflater inflater = LayoutInflater.from(context);
	        	convertView = inflater.inflate(R.layout.custom_myfriends_transaction, null);
	            holder = new ViewHolder();
	            holder.transText = (TextView) convertView.findViewById(R.id.transText);
	            holder.transLocation = (TextView) convertView.findViewById(R.id.transLocation);
	            holder.transTime = (TextView) convertView.findViewById(R.id.transTime);
	            convertView.setTag(holder);
	        } else {
	            holder = (ViewHolder) convertView.getTag();
	        }
	        
	        holder.transText.setText(listItem.gettransText());
	        holder.transLocation.setText(listItem.gettransLocation());
	        holder.transTime.setText(listItem.gettransTime());
	        return convertView;
	        
	    }

	    public void removeItemFromList(int position)
		{
			this.feedList.remove(position);
			//checkEmptyList();
			this.notifyDataSetInvalidated();
			this.notifyDataSetChanged();
	    }
	    
	   /* public void checkEmptyList(){
	    	if(feedList.size() == 0){
				this.feedList.add(emptyList);
			}
	    }
	    */
	    @Override
	    public int getCount() {
	    	//	checkEmptyList();
	        return feedList.size();
	    }

	    @Override
	    public MyFriends_Transactions_Class getItem(int position) {
	        return feedList.get(position);
	    }

	    @Override
	    public long getItemId(int position) {
	        return position;
	    }

	    private class ViewHolder {
	        TextView transText, transLocation, transTime;
	    }

}
