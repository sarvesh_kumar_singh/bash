package com.bash.Adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bash.R;
import com.bash.ListModels.Pending_Friends_Class;
import com.bash.Utils.AppConstants;

public class MyTrasaction_PendingFriendsAdapter extends BaseAdapter
{
	 	private ArrayList<Pending_Friends_Class>  feedList = new ArrayList<Pending_Friends_Class>();
		private Context context;
	    
	    public MyTrasaction_PendingFriendsAdapter(Context context, ArrayList<Pending_Friends_Class> feedList) {
	        this.context = context;
	    	this.feedList = feedList;
	    }

	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        ViewHolder holder = null;
	        Pending_Friends_Class listItem = getItem(position);
	        if (convertView == null) {
	        	LayoutInflater inflater = LayoutInflater.from(context);
	        	convertView = inflater.inflate(R.layout.custom_mytransaction_listview, null);
	            holder = new ViewHolder();
	            holder.groupName = (TextView) convertView.findViewById(R.id.groupName);
	            holder.groupAmount = (TextView) convertView.findViewById(R.id.groupAmount);
	            convertView.setTag(holder);
	        } else {
	            holder = (ViewHolder) convertView.getTag();
	        }

	        holder.groupAmount.setText(listItem.getgroupAmount());
	        holder.groupName.setText(listItem.getgroupName());
	        return convertView;
	    }

	    @Override
	    public int getCount() {
	        return feedList.size();
	    }

	    @Override
	    public Pending_Friends_Class getItem(int position) {
	        return feedList.get(position);
	    }

	    @Override
	    public long getItemId(int position) {
	        return position;
	    }

	    private class ViewHolder {
	        TextView groupName;
	        TextView groupAmount;
	    }

}
