package com.bash.Adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.bash.R;
import com.bash.ListModels.SlidingMenu_Class;
import com.bash.ListModels.SlidingMenu_Class;

public class SlidingMenuAdapter extends BaseAdapter implements Filterable
{
	 	/*private ArrayList<String>  iconName = new ArrayList<String>();
		private ArrayList<Integer>  iconPath = new ArrayList<Integer>();*/
		private ArrayList<SlidingMenu_Class>  menuList = new ArrayList<SlidingMenu_Class>();
		private ArrayList<SlidingMenu_Class>  originalmenuList = new ArrayList<SlidingMenu_Class>();
		public Filter myFilter;
		private Context context;
	    
	    public SlidingMenuAdapter(Context context, ArrayList<SlidingMenu_Class> menuList) {
	        this.context = context;
	        this.menuList = menuList;
	        this.originalmenuList = menuList;
	    }

	    @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	        ViewHolder holder = null;
	        if (convertView == null) {
	        	LayoutInflater inflater = LayoutInflater.from(context);
	        	convertView = inflater.inflate(R.layout.custom_slidingmenu_listview, null);
	            holder = new ViewHolder();
	            holder.txtTitle = (TextView) convertView.findViewById(R.id.iconText);
	            holder.imageView = (ImageView) convertView.findViewById(R.id.iconImage);
	            convertView.setTag(holder);
	        } else {
	            holder = (ViewHolder) convertView.getTag();
	        }

	        holder.txtTitle.setText(menuList.get(position).getMenuName());
	        holder.imageView.setImageResource(menuList.get(position).getMenuId());
	        return convertView;
	    }

	    @Override
	    public int getCount() {
	        return menuList.size();
	    }

	    @Override
	    public SlidingMenu_Class getItem(int position) {
	        return menuList.get(position);
	    }

	    @Override
	    public long getItemId(int position) {
	        return position;
	    }

	    private class ViewHolder {
	        ImageView imageView;
	        TextView txtTitle;
	    }

	 // Filter Class
	    public void filter(String charText) {
	        charText = charText.toLowerCase(Locale.getDefault());
	        Log.e("charText", charText+"");
	        Log.e("Originallengh", String.valueOf(originalmenuList.size()));
	        menuList.clear();
	        if (charText.length() == 0) {
	        	menuList.addAll(originalmenuList);
	        }else
	        {
	            for (SlidingMenu_Class wp : originalmenuList)
	            {
	                if (wp.getMenuName().startsWith(charText))
	                {
	                	Log.e("Groupname", wp.getMenuName());
	                	menuList.add(wp);
	                }
	            }
	        }
	        this.notifyDataSetChanged();
	        this.notifyDataSetInvalidated();
	    }

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		  if (myFilter == null)
			  myFilter = new GroupFilter();
		    return myFilter;
	}
	 
	//filter Class... 
		private class GroupFilter extends Filter {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				// We implement here the filter logic
				if (constraint == null || constraint.length() == 0) {
					// No filter implemented we return all the list
					results.values = originalmenuList;
					results.count = originalmenuList.size();
				} else {
					// We perform filtering operation
					List<SlidingMenu_Class> tempList = new ArrayList<SlidingMenu_Class>();
					for (SlidingMenu_Class p : menuList) {
						if (p.getMenuName().toUpperCase()
								.startsWith(constraint.toString().toUpperCase()))
							tempList.add(p);
					}
					results.values = tempList;
					results.count = tempList.size();
				}
				return results;
			}

			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				// Now we have to inform the adapter about the new list filtered
				/*menuList = (ArrayList<SlidingMenu_Class>) results.values;
				notifyDataSetChanged();*/
				
				/*menuList = (ArrayList<SlidingMenu_Class>) results.values;
				notifyDataSetChanged();*/
				
				if (results.count == 0)
					notifyDataSetInvalidated();
				else {
					menuList = (ArrayList<SlidingMenu_Class>) results.values;
					notifyDataSetChanged();
				}
			}
		}
    	
		

		public void resetData() {
			menuList = originalmenuList;
			notifyDataSetChanged();
		}
    		
		 
	    
	    
}
