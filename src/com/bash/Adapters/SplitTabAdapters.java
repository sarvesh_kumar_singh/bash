package com.bash.Adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.util.Progress;
import com.bash.R;
import com.bash.Activities.AddTabSplitActivity;
import com.bash.CustomViews.CircularImageView;
import com.bash.Fragments.AddTabSplitFragment;
import com.bash.ListModels.PhotosListModel;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

public class SplitTabAdapters extends ArrayAdapter<PhotosListModel>{
	Activity activity;
	LayoutInflater inflater;
	int resource;
	PersonItemViewHolder holder = null;
	public ArrayList<PhotosListModel> photoListArrayList;
	Toast mtoast;
	ImageView imageNext;
	private static EditText editText[];
	private static TextView textView[];
	private int fragType;
	private float totalvalue = 0.0f;
	
	public SplitTabAdapters(Activity activity, int resource,
			ArrayList<PhotosListModel> photoListArrayList,int fragType, float totalvalue) {
		super(activity, resource, photoListArrayList);
		this.activity = activity;
		this.inflater = (LayoutInflater) this.activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.photoListArrayList = photoListArrayList;
		this.resource = resource;
		this.fragType = fragType;
		this.totalvalue = totalvalue;
		editText = new EditText[photoListArrayList.size()];
		textView = new TextView[photoListArrayList.size()];
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return photoListArrayList.size();
	}

	@Override
	public PhotosListModel getItem(int position) {
		// TODO Auto-generated method stub
		return photoListArrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {

			LayoutInflater inflater = LayoutInflater.from(activity);
			convertView = inflater.inflate(resource, null);
			holder = new PersonItemViewHolder();
			holder.splituserimage = (CircularImageView) convertView
					.findViewById(R.id.splituserimage);
			holder.splitadapteramoutpart = (EditText) convertView
					.findViewById(R.id.splitadapteramoutpart);
			holder.splitadapteramount = (TextView) convertView
					.findViewById(R.id.splitadapteramount);
			holder.splitnametext =(TextView) convertView
					.findViewById(R.id.splitnametext);
			holder.splitprogressbar =(ProgressBar) convertView
					.findViewById(R.id.splitprogressbar);
					
			holder.splitadapteramoutpart.setTag("edit"+position);
			holder.splitprogressbar.setTag("progress"+position);
			
			editText[position] = holder.splitadapteramoutpart;
			textView[position] = holder.splitadapteramount;
			
			convertView.setTag(holder);
		} else {
			holder = (PersonItemViewHolder) convertView.getTag();
			holder.splitadapteramoutpart.setTag("edit"+position);
			holder.splitprogressbar.setTag("progress"+position);
		}
		final PersonItemViewHolder holder = (PersonItemViewHolder) convertView.getTag();

		final PhotosListModel photoListObj = getItem(position);
	
		
		if((photoListObj.getImageLocation() != null) && (photoListObj.getImageLocation().length() > 0)){
			UrlImageViewHelper
			.setUrlDrawable(
					holder.splituserimage,photoListObj.getImageLocation());
		}
		holder.splitnametext.setText(photoListObj.getName());
		
		
		if(fragType == 1){
			setRatioAmountValue(holder.splitprogressbar, holder.splitadapteramoutpart,holder.splitadapteramount,photoListObj);
			
			holder.splitadapteramoutpart.setFocusable(true);
		}
		else if(fragType == 2){
			setAmountValue(holder.splitprogressbar,holder.splitadapteramoutpart,photoListObj);
			
			holder.splitadapteramoutpart.setFocusable(true);
		}
		else if(fragType == 3){
			setAmountValue(holder.splitprogressbar,holder.splitadapteramoutpart,photoListObj);
			
			holder.splitadapteramoutpart.setFocusable(false);
		}
		else if(fragType == 4){
			setPercentAmountValue(holder.splitprogressbar,holder.splitadapteramoutpart,holder.splitadapteramount,photoListObj);
			
			holder.splitadapteramoutpart.setFocusable(true);
		}
		
		
		holder.splitadapteramoutpart.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				float changeValue = 0.0f;
				if(s.length() > 0){
				changeValue = Float.parseFloat(s.toString());
				} else {
					changeValue = 0.0f;
				}
				
				if(fragType == 1){
					photoListObj.setPaidamount((int)changeValue);
					findRationAmount(holder.splitprogressbar,holder.splitadapteramount,(int)changeValue);
				}
				else if(fragType == 2){
					progressChange(holder.splitprogressbar,changeValue);

					findUnEqualAmount(changeValue);
				
				}
				else if(fragType == 3){
					progressChange(holder.splitprogressbar,changeValue);
				}
				else if(fragType == 4){
					photoListObj.setPaidamount(changeValue);
					findPercentAmount(holder.splitprogressbar,holder.splitadapteramount,changeValue);
					findPercentPending(changeValue);
				}
 			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});
		
		return convertView;
	}

	private class PersonItemViewHolder {
		CircularImageView splituserimage;
		TextView splitadapteramount,splitnametext;
		EditText splitadapteramoutpart;
		ProgressBar splitprogressbar;
	}
	
	public float findTotalAmount(){
		float total = 0;
		
		for(int i = 0 ; i < getCount() ; i++){		
				String enterAmount= editText[i].getText().toString();
				if(enterAmount.length() > 0)
				{
					photoListArrayList.get(i).setReceiveamount(Float.parseFloat(enterAmount));
					total += Float.parseFloat(enterAmount);
				}	
			
		}		
		
		return total;
	}
	private void progressChange(ProgressBar pgrsBar, float changeValue){
		int percent = (int) ((changeValue/totalvalue)*100);
		pgrsBar.setProgress(percent);
	}
	private void setAmountValue(ProgressBar splitprogressbar,EditText splitadapteramount,PhotosListModel photoListObj){
		
		splitadapteramount.setText(""+photoListObj.getPaidamount());
		progressChange(splitprogressbar,photoListObj.getPaidamount());
		
	}
private void setPercentAmountValue(ProgressBar splitprogressbar,EditText splitadapteramount,TextView splitamount, PhotosListModel photoListObj){	
	float percentage = 0.0f;
	
		splitadapteramount.setText(""+photoListObj.getPaidamount());
		
		percentage = findPercent(photoListObj.getPaidamount());	
		splitamount.setText(String.format("%.2f", percentage));
		progressChange(splitprogressbar,percentage);
		
	}
	
private void setRatioAmountValue(ProgressBar splitprogressbar, EditText splitadapteramount,TextView splitamount,PhotosListModel photoListObj){
		
		splitadapteramount.setText(""+(int)photoListObj.getPaidamount());
		findRationAmount(splitprogressbar,splitamount, (int)photoListObj.getPaidamount());
		
	}
	private void findUnEqualAmount(float changeValue){
		
		float pendingAmt = 0.0f;
		float splitAmt = 0.0f;
		for(int i = 0 ; i < getCount() ; i++){	
		
			String enterAmount= editText[i].getText().toString();
			if(enterAmount.length() > 0)
			{
				photoListArrayList.get(i).setReceiveamount(Float.parseFloat(enterAmount));
				splitAmt += Float.parseFloat(enterAmount);
			}	
			pendingAmt = totalvalue - splitAmt;
			
	}
		
		AddTabSplitFragment.pendingunequalamount.setText(""+pendingAmt);
		AddTabSplitFragment.totalsplitunequalamount.setText(""+splitAmt);
		
	}
	private void findRationAmount(ProgressBar splitprogressbar,TextView splitText,int changeValue){
		
		int totalRt = 0;
		float changeRatioAmt = 0.0f;
		
		for(PhotosListModel photoList : photoListArrayList){		
			
			float enterAmount= photoList.getPaidamount();
			
			photoList.setReceiveamount(enterAmount);
				totalRt += (int)(enterAmount);
				
			}
	
		try{
			float divisor =((float)changeValue/(float)totalRt);
			changeRatioAmt = (totalvalue*divisor);
		} catch(ArithmeticException ae){
			changeRatioAmt = 0.0f;
		}
		
		splitText.setText(""+changeRatioAmt);
		progressChange(splitprogressbar,changeRatioAmt);
	}
	private void findPercentAmount(ProgressBar splitprogressbar,TextView splitadapteramount,float changeValue){
		float percentage = 0.0f; 
		percentage = findPercent(changeValue);	
		splitadapteramount.setText(String.format("%.2f", percentage));
		progressChange(splitprogressbar,percentage);
		
	}
	private void findPercentPending(float changeValue)
	{
		
		float pendingAmt = 0.0f;
		float splitAmt = 0.0f;
		
		for(int i = 0 ; i < getCount() ; i++){	
		
			String enterAmount= editText[i].getText().toString();
			if(enterAmount.length() > 0)
			{
				photoListArrayList.get(i).setReceiveamount(Float.parseFloat(enterAmount));
				splitAmt += Float.parseFloat(enterAmount);
			}	
			pendingAmt = 100 - splitAmt;
			
	}
		if(pendingAmt >= 0 && pendingAmt <= 100){
		AddTabSplitFragment.pendingpercent.setText(""+pendingAmt);
		AddTabSplitFragment.totalsplitpercent.setText(""+splitAmt);
		}else{
			
		}
	}
	private float findPercent(float perValue){
		float percentValue = 0.0f;
		
		percentValue = (totalvalue*((perValue)/100));
		
		return percentValue;
	}
	public float findTotalPercent(){
		float total = 0;
		
		for(int i = 0 ; i < getCount() ; i++){		
				String enterAmount= textView[i].getText().toString();
				if(enterAmount.length() > 0)
				{
					photoListArrayList.get(i).setReceiveamount(Float.parseFloat(enterAmount));
					total += Float.parseFloat(enterAmount);
				}	
			
		}		
		
		return total;
	}

}

