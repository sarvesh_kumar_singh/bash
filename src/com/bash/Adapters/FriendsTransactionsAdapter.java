package com.bash.Adapters;
	
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bash.R;
import com.bash.ListModels.CashInOutFriend_Class;
import com.bash.Utils.AppConstants;
	
	public class FriendsTransactionsAdapter extends BaseExpandableListAdapter{
	
		public Context _context;
		private List<String> _listDataHeader; // header titles
	 	private SparseArray<ArrayList<CashInOutFriend_Class>> _listDataChild;
		
		public FriendsTransactionsAdapter(Context context, List<String> listDataHeader,
				SparseArray<ArrayList<CashInOutFriend_Class>> listChildData) {
			this._context = context;
			this._listDataHeader = listDataHeader;
			this._listDataChild = listChildData;
		}
	
		@Override
		public Object getChild(int groupPosition, int childPosititon) {
			
			return this._listDataChild.get(groupPosition).get(childPosititon);
		}
	
		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}
	
		@Override
		public View getChildView(int groupPosition, final int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
	
			final CashInOutFriend_Class childDatas = (CashInOutFriend_Class) getChild(groupPosition, childPosition);
	
			if (convertView == null) {
				LayoutInflater infalInflater = (LayoutInflater) this._context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater.inflate(R.layout.custom_myfriends_transactions_childview, null);
			}
			
			TextView eventName = (TextView) convertView.findViewById(R.id.eventName);
			TextView youpaidText = (TextView) convertView.findViewById(R.id.youpaidText);
			ImageView statusImage = (ImageView) convertView.findViewById(R.id.statusImage);
			
			eventName.setText(childDatas.geteventName());
			
			if(childDatas.getHaveYouPaid()){
				youpaidText.setText("You lent"+"\n"+AppConstants.RASYMBOL+". "+childDatas.getsubTotalAmount());
				youpaidText.setTextColor(Color.parseColor("#378E34"));
				statusImage.setImageResource(R.drawable.dollor_paid);
			}else{
				youpaidText.setText("You borrowed"+"\n"+AppConstants.RASYMBOL+". "+childDatas.getsubTotalAmount());
				youpaidText.setTextColor(Color.RED);
				statusImage.setImageResource(R.drawable.dollor_notpaid);				
			}/*else{
				youpaidText.setText("You  are not Involded");
				youpaidText.setTextColor(Color.DKGRAY);
				statusImage.setImageResource(R.drawable.dollor_paid);	
			}
			*/
			return convertView;
		}
	
		@Override
		public int getChildrenCount(int groupPosition) {
			/*return this._listDataChild.get(this._listDataHeader.get(groupPosition))
					.size();*/
			return this._listDataChild.get(groupPosition).size();
		}
	
		@Override
		public Object getGroup(int groupPosition) {
			return this._listDataHeader.get(groupPosition);
		}
	
		@Override
		public int getGroupCount() {
			return this._listDataHeader.size();
		}
	
		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}
	
		@Override
		public View getGroupView(final int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			String headerTitle = (String) getGroup(groupPosition);
			if (convertView == null) {
				LayoutInflater infalInflater = (LayoutInflater) this._context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater.inflate(R.layout.custom_myfriends_transactions_parentview, null);
			}
			
			TextView monthText = (TextView) convertView.findViewById(R.id.monthText);
			monthText.setText(headerTitle);
		
		   ExpandableListView mExpandableListView = (ExpandableListView) parent;
		   mExpandableListView.expandGroup(groupPosition);
			    
			return convertView;
		}
	
	 
		@Override
		public boolean hasStableIds() {
			return false;
		}
		
		@Override
		public void onGroupExpanded(int groupPosition) {
			// TODO Auto-generated method stub
			super.onGroupExpanded(groupPosition);
		}
	
		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}
		
		 
	 
}
		 
	
	
	
