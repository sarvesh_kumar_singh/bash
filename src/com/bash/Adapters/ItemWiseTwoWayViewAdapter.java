package com.bash.Adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bash.R;
import com.bash.Activities.ItemWiseSplitActivity;
import com.bash.CustomViews.CircularImageView;
import com.bash.ListModels.ItemWiseListModel;
import com.bash.ListModels.PhotosListModel;
import com.bash.ListModels.TwoWayPersonModel;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

public class ItemWiseTwoWayViewAdapter extends ArrayAdapter<PhotosListModel>{
	Activity activity;
	LayoutInflater inflater;
	int resource;
	ItemWiseTwoWayViewHolder holder = null;
	ArrayList<PhotosListModel> photoListArrayList;
	private static ItemWiseTwoWayViewHolder holderArray[];
	Toast mtoast;
	ImageView imageNext;
	
	public ItemWiseTwoWayViewAdapter(Activity activity, int resource,
			ArrayList<PhotosListModel> photoListArrayList) {
		super(activity, resource, photoListArrayList);
		this.activity = activity;
		this.inflater = (LayoutInflater) this.activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.photoListArrayList = photoListArrayList;
		this.resource = resource;
		holderArray = new ItemWiseTwoWayViewHolder[photoListArrayList.size()];
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return photoListArrayList.size();
	}

	@Override
	public PhotosListModel getItem(int position) {
		// TODO Auto-generated method stub
		return photoListArrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {

			LayoutInflater inflater = LayoutInflater.from(activity);
			convertView = inflater.inflate(resource, null);
			holder = new ItemWiseTwoWayViewHolder();
			holder.itemPhotos = (CircularImageView) convertView
					.findViewById(R.id.itemwisereceiveuserimageView);
			holder.itemtick = (ImageView) convertView
					.findViewById(R.id.itemwisetickimageview);
			holder.itemName = (TextView) convertView
					.findViewById(R.id.itemwisereceiveusernametext);
			holder.twowaymainlinear1 =(LinearLayout) convertView
					.findViewById(R.id.itemwiselinear1);
			holder.twowaymainlinear2 =(LinearLayout) convertView
					.findViewById(R.id.itemwiselinear2);
			
			holderArray[position] = holder;
			
			convertView.setTag(holder);
		} else {
			holder = (ItemWiseTwoWayViewHolder) convertView.getTag();
		}
		ItemWiseTwoWayViewHolder holder = (ItemWiseTwoWayViewHolder) convertView.getTag();
		final PhotosListModel photosListModel = getItem(position);
	
		
		if((photosListModel.getImageLocation() != null) && (photosListModel.getImageLocation().length() > 0)){
			UrlImageViewHelper
			.setUrlDrawable(
					holder.itemPhotos,photosListModel.getImageLocation());
			
		} else {
			holder.itemPhotos.setImageResource(R.drawable.profile_small);
		}
		
		holder.itemName.setText(photosListModel.getName());
		
		if(photosListModel.isImageItemWiseSelect()){
			holder.itemtick.setVisibility(View.VISIBLE);
			}else {
				holder.itemtick.setVisibility(View.GONE);
			}
		
		
		
		holder.twowaymainlinear1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				ItemWiseListAdapter itemWiseListadapter = ((ItemWiseSplitActivity) activity).getItemWiseAdater();
				
				
//				if(photosListModel.isImageLongpress()){
//					
//					photosListModel.setImageLongpress(false);
//					
//					for(int i=0;i<getCount();i++){
//						
//						
//							holderArray[i].twowaymainlinear2.setVisibility(View.GONE);
//						
//						
//					}
//					
//				} else {
				if(itemWiseListadapter.getItemSelectPossition() != -1){
					if(photosListModel.isImageItemWiseSelect()){
						photosListModel.setImageItemWiseSelect(false);
						}else {
							photosListModel.setImageItemWiseSelect(true);	
						}
					itemWiseListadapter.setSharedPerson(photosListModel,photosListModel.isImageItemWiseSelect());
					
				}
				//}
				
				notifyDataSetChanged();
			}
		});
		holder.twowaymainlinear1.setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				
//				photosListModel.setImageLongpress(true);
				
				for(int i=0;i<getCount();i++){	
					if(!photoListArrayList.get(i).isImageLongpress()){
							holderArray[i].twowaymainlinear2.setVisibility(View.VISIBLE);
					}else{
						holderArray[i].twowaymainlinear2.setVisibility(View.GONE);
					}	
				}
				return false;
			}
		});
		
		
		
		return convertView;
	}

	private class ItemWiseTwoWayViewHolder {
		LinearLayout twowaymainlinear1, twowaymainlinear2;
		CircularImageView itemPhotos;
		TextView itemName;
		ImageView itemtick;
	}
	
	public void showSharedPerson(ArrayList<TwoWayPersonModel> twoWayPersonList){
		
ArrayList<PhotosListModel> dummyPhotosListModel = new ArrayList<PhotosListModel>();
		
		for(PhotosListModel photosListModelObj1 : photoListArrayList){
			
			photosListModelObj1.setImageItemWiseSelect(false);
			
			dummyPhotosListModel.add(photosListModelObj1);
			
		}
		

		for(PhotosListModel photosListModelObj : dummyPhotosListModel){
			
			for(TwoWayPersonModel twoWayPersonModelobj : twoWayPersonList){
				
		
				if(twoWayPersonModelobj.getId().equals(photosListModelObj.getID()) && (!photosListModelObj.isImageItemWiseSelect())){
				
					photosListModelObj.setImageItemWiseSelect(true);
					
				}
				
			}
			
			
			
			
		}
			photoListArrayList = dummyPhotosListModel;
		notifyDataSetChanged();
	}
}

