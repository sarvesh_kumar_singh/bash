package com.bash.Adapters;
	
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.util.SparseArray;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.bash.R;
import com.bash.Fragments.MyFriendsFragment;
import com.bash.Managers.PreferenceManager;
	
	public class FriendsListAdapter extends BaseExpandableListAdapter {
	
		public Context _context;
		private List<String> _listDataHeader; // header titles
	
		private static final int SWIPE_MIN_DISTANCE = 50;
		private static final int SWIPE_MAX_OFF_PATH = 50;
		private static final int SWIPE_THRESHOLD_VELOCITY = 50;
		
		// child data in format of header title, child title
		private SparseArray<List<String>> _listDataChild;
		RelativeLayout.LayoutParams frontViewParams;
		LinearLayout.LayoutParams backViewParams;
		
		public int offset = 55;
		
		public FriendsListAdapter(Context context, List<String> listDataHeader,
				SparseArray<List<String>> listChildData) {
			this._context = context;
			this._listDataHeader = listDataHeader;
			this._listDataChild = listChildData;
			
			backViewParams = new LinearLayout.LayoutParams(PreferenceManager.getInstance().getPercentageFromWidth(20),
					LayoutParams.MATCH_PARENT);
			
			offset = PreferenceManager.getInstance().getPercentageFromWidth(20);
			
			/*frontViewParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, 
					75);*/
		}
	
		@Override
		public Object getChild(int groupPosition, int childPosititon) {
			/*return this._listDataChild.get(this._listDataHeader.get(groupPosition))
					.get(childPosititon);*/
			return this._listDataChild.get(groupPosition).get(childPosititon);
			
		}
	
		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}
	
		@Override
		public View getChildView(int groupPosition, final int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
	
			final String childText = (String) getChild(groupPosition, childPosition);
	
			if (convertView == null) {
				LayoutInflater infalInflater = (LayoutInflater) this._context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater.inflate(R.layout.custom_myfriendchild_item, null);
			}
			
			TextView txtListChild = (TextView) convertView.findViewById(R.id.titleofChild);
			TextView subtitleItem = (TextView) convertView.findViewById(R.id.subtitleItem);
			
			txtListChild.setText("Raju Bought Money From Kamal...");
			subtitleItem.setText(childText);
			return convertView;
		}
	
		@Override
		public int getChildrenCount(int groupPosition) {
			/*return this._listDataChild.get(this._listDataHeader.get(groupPosition))
					.size();*/
			return this._listDataChild.get(groupPosition).size();
		}
	
		@Override
		public Object getGroup(int groupPosition) {
			return this._listDataHeader.get(groupPosition);
		}
	
		@Override
		public int getGroupCount() {
			return this._listDataHeader.size();
		}
	
		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}
	
		@Override
		public View getGroupView(final int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			String headerTitle = (String) getGroup(groupPosition);
			if (convertView == null) {
				LayoutInflater infalInflater = (LayoutInflater) this._context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = infalInflater.inflate(R.layout.custom_myfriendongroup_listview, null);
			}
			
			
			ImageView friendImageSource = (ImageView) convertView.findViewById(R.id.friendImageSource);
			
			final RelativeLayout frontView = (RelativeLayout) convertView.findViewById(R.id.frontView);
			final RelativeLayout backView = (RelativeLayout) convertView.findViewById(R.id.backView);
			
			backView.setLayoutParams(backViewParams);
			
			switch (groupPosition) {
			case 0:
				friendImageSource.setImageResource(R.drawable.addphoto_img_block);
				break;
			case 1:
				friendImageSource.setImageResource(R.drawable.addphoto_img_block);
				break;
			case 2:
				friendImageSource.setImageResource(R.drawable.addphoto_img_block);
				break;
			default:
				break;
			}
			//MyGestureDetector myDetector = new MyGestureDetector(frontView, groupPosition);
			//final GestureDetector gestureDetector = new GestureDetector(_context, myDetector);
			
			final GestureDetector gestureDetector = new GestureDetector(_context, new MyGestureDetector(frontView, groupPosition));
			
			View.OnTouchListener gestureListener = new View.OnTouchListener() {
	            public boolean onTouch(View v, MotionEvent event) {
	                return gestureDetector.onTouchEvent(event);
	            }
	        };

	        frontView.setOnTouchListener(gestureListener);
	        
	        ImageView deleteFriend = (ImageView) convertView.findViewById(R.id.deleteFriend);
	        TextView lblListHeader = (TextView) convertView.findViewById(R.id.friendName);
			lblListHeader.setTypeface(null, Typeface.BOLD);
			lblListHeader.setText(headerTitle);
	
			deleteFriend.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					closeSlideOption(frontView);
					removeFromList(groupPosition);
				}
			});
			
			return convertView;
		}
	
		@SuppressLint("NewApi")
		public void removeFromList(int groupPosition) {
			//_listDataChild.remove(groupPosition);
		/*	_listDataChild.remove(groupPosition);
			_listDataHeader.remove(groupPosition);
			Log.i("Position",String.valueOf(groupPosition));*/
			notifyDataSetChanged();
		}
		
		@Override
		public boolean hasStableIds() {
			return false;
		}
		
		@Override
		public void onGroupExpanded(int groupPosition) {
			// TODO Auto-generated method stub
			super.onGroupExpanded(groupPosition);
		}
	
		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}
		
		public void openSlideOption(RelativeLayout frontView){
			if (((int)(((RelativeLayout.LayoutParams)frontView.getLayoutParams()).leftMargin - offset)) == -(offset))
			{
			((RelativeLayout.LayoutParams)frontView.getLayoutParams()).setMargins(
            		(int)(((RelativeLayout.LayoutParams)frontView.getLayoutParams()).leftMargin - offset),  // left
            		(int)(((RelativeLayout.LayoutParams)frontView.getLayoutParams()).topMargin),  // top
            		(int)(((RelativeLayout.LayoutParams)frontView.getLayoutParams()).rightMargin + offset),  // right
            		(int)(((RelativeLayout.LayoutParams)frontView.getLayoutParams()).bottomMargin)); // bottom
		     notifyDataSetChanged();
			}
		}
		
		public void closeSlideOption(RelativeLayout frontView) {
			if (((int)(((RelativeLayout.LayoutParams)frontView.getLayoutParams()).leftMargin - offset)) !=  -(offset))
			{
			((RelativeLayout.LayoutParams)frontView.getLayoutParams()).setMargins(
            		(int)(((RelativeLayout.LayoutParams)frontView.getLayoutParams()).leftMargin + offset),  // left
            		(int)(((RelativeLayout.LayoutParams)frontView.getLayoutParams()).topMargin),  // top
            		(int)(((RelativeLayout.LayoutParams)frontView.getLayoutParams()).rightMargin - offset),  // right
            		(int)(((RelativeLayout.LayoutParams)frontView.getLayoutParams()).bottomMargin)); // bottom
		     notifyDataSetChanged();
			}
		}

	 	class MyGestureDetector extends SimpleOnGestureListener {
			
			private RelativeLayout frontView;
			private int groupPosition = 0;
			
			public MyGestureDetector(RelativeLayout frontView, int groupPosition) {
				this.frontView = frontView;
				this.groupPosition = groupPosition;
			}
			
			@Override
			public boolean onScroll(MotionEvent e1, MotionEvent e2,	float distanceX, float distanceY) {
				// TODO Auto-generated method stub
				//  canshowdatas = false;
				   try {
		            	if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE)
		            		{
		            		openSlideOption(frontView);
		            //		canshowdatas = false;
		                }  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE)
		                		{
		                	closeSlideOption(frontView);
		             //   	canshowdatas = false;
		                }
		               }
		            catch (Exception e) {
		                // nothing
		            }
				return false;
			}
			
			/*@Override
	        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {	
	            try {
	            	if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY)
	            			{
	            		openSlideOption(frontView);
	                }  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY)
	                		{
	                	closeSlideOption(frontView);
	                }
	               }
	            catch (Exception e) {
	                // nothing
	            }
	            return false;
	        }
			 */
			
		 
			/*@Override
	        public boolean onSingleTapUp(MotionEvent e) {
	        // TODO Auto-generated method stub
	        	Log.e("Touched", "True");
            	if(MyFriendsFragment.friendsListView.isGroupExpanded(groupPosition)) {
            		MyFriendsFragment.friendsListView.collapseGroup(groupPosition);
				 	}else{
					MyFriendsFragment.friendsListView.expandGroup(groupPosition);
				 	}
            	return super.onSingleTapUp(e);
	        }
	            @Override
	        public boolean onDown(MotionEvent e) {
	              return true;
	        }*/
		}
		 
	
	}
	
