package com.bash.Providers;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bash.R;
import com.bash.Activities.AddTabActivity;
import com.bash.Activities.AddTabSplitActivity;
import com.bash.Activities.HomeActivity;
import com.bash.BaseFragmentClasses.AddSplitTabBaseFragment;
import com.bash.BaseFragmentClasses.AddTabBaseFragment;
import com.bash.BaseFragmentClasses.BaseFragment;
import com.bash.BaseFragmentClasses.FragmentBaseFragment;
import com.bash.Fragments.AddDescription_Charge_Fragment;
import com.bash.Fragments.AddDescription_Group_Fragment;
import com.bash.Fragments.AddDescription_Pay_Fragment;
import com.bash.Fragments.AddTabPayFragment;
import com.bash.Fragments.AddTabSplitFragment;
import com.bash.Fragments.Banking_NewAccount;
import com.bash.Fragments.Banking_SavedAccount;
import com.bash.Fragments.Banking_SavedCard_Fragment;
import com.bash.Fragments.BashMomentsFragment;
import com.bash.Fragments.Deposit_DebitCard_Fragment;
import com.bash.Fragments.Deposit_SavedCard_Fragment;
import com.bash.Fragments.FriendsFragment;
import com.bash.Fragments.GroupsFragment;
import com.bash.Fragments.InvitationsFragment;
import com.bash.Fragments.MyBash_Fragment;
import com.bash.Fragments.MyCards_Fragment;
import com.bash.Fragments.MyFriendsAndGroupsFragment;
import com.bash.Fragments.MyFriendsFragment;
import com.bash.Fragments.MyGroupsFragment;
import com.bash.Fragments.MyTrans_Pending_Fragment;
import com.bash.Fragments.MyTrans_Processed_Friends_Fragment;
import com.bash.Fragments.MyTrans_Processed_Groups_Fragment;
import com.bash.Fragments.MyTransaction_History_Fragment;
import com.bash.Fragments.MyTransaction_Home_Total_Fragment;
import com.bash.Fragments.MyTransaction_Home_YouAreOwed_Fragment;
import com.bash.Fragments.MyTransaction_Home_YouOwe_Fragment;
import com.bash.Fragments.MyTransaction_Pending_Fragment;
import com.bash.Fragments.MyWallet_Banking_SavedCards_Fragment;
import com.bash.Fragments.NewsFeed_Fragment;
import com.bash.Fragments.Notification_Fragment;
import com.bash.Fragments.PrivacyFragment;
import com.bash.Fragments.TabFeed_MyFeed_Fragment;
import com.bash.Fragments.TabFeed_Trending_Fragment;
import com.bash.Fragments.TabUserFragment;
import com.bash.Fragments.Transaction_YouAreOwed_Fragment;
import com.bash.Fragments.Transaction_YouOwe_Fragment;
import com.bash.Fragments.WalletFragment;
import com.bash.Utils.AppConstants;

public class ContainerProvider {

  public static class AddDescription_Group_Container extends BaseFragment
	 {
	 private boolean mIsViewInited;
	 	
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() 
	 	{
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new AddDescription_Group_Fragment(), false);
	 	}
	 }
    
  public static class AddDescription_Pay_Container extends BaseFragment
	 {
	 private boolean mIsViewInited;
	 	
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() 
	 	{
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new AddDescription_Pay_Fragment(), false);
	 	}
	 	 
	 }
  
  public static class AddDescription_Charge_Container extends BaseFragment
	 {
	 private boolean mIsViewInited;
	 	
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() 
	 	{
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new AddDescription_Charge_Fragment(), false);
	 	}
	 	 
	 }

  public static class Home_Container extends BaseFragment
	 {
	 private boolean mIsViewInited;
	 	
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 Home oncreateview");
	 		return inflater.inflate(R.layout.fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 Home container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn, "My Tabs", 0);
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() 
	 	{
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new MyTrans_Pending_Fragment(), false);
	 	}
	 	 

	 }
  
  public static class MyProfile_Container extends BaseFragment
	 {
	 private boolean mIsViewInited;
	 	
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() 
	 	{
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new MyBash_Fragment(), false);
	 	}
	 	 
	 }
  
  public static class MyCards_Container extends BaseFragment
	 {
	 private boolean mIsViewInited;
	 	
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() 
	 	{
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new MyCards_Fragment(), false);
	 	}
	 	 
	 }

  
  public static class MyWallet_Container extends BaseFragment
	 {
	 private boolean mIsViewInited;
	 	
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() 
	 	{
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new WalletFragment(), false);
	 	}
	 	 
	 }

  
  public static class Privacy_Container extends BaseFragment
	 {
	 private boolean mIsViewInited;
	 	
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() 
	 	{
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new PrivacyFragment(), false);
	 	}
	 	 
	 }
  
  public static class MyGroups_Container extends BaseFragment
	 {
	 private boolean mIsViewInited;
	 	
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn, "My Groups", 0);
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() 
	 	{
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new MyGroupsFragment(), false);
	 	}
	 	 
	 }
  
  public static class MyFriends_Container extends BaseFragment
	 {
	 private boolean mIsViewInited;
	 	
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn, "My Friends", 0);
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() 
	 	{
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new MyFriendsFragment(), false);
	 	}
	 	 
	 }
  public static class Newsfeed_Container extends BaseFragment
	 {
	 private boolean mIsViewInited;
	 	
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn, "Newsfeed", 0);
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() 
	 	{
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new NewsFeed_Fragment(), false);
	 	}
	 	 
	 }
  public static class Notification_Container extends BaseFragment
	 {
	 private boolean mIsViewInited;
	 	
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			((HomeActivity)getActivity()).setUpTopBarFields(R.drawable.menu_btn, "Notifications", 0);
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() 
	 	{
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new Notification_Fragment(), false);
	 	}
	 	 
	 }
  
  public static class MyFriendsAndGroups_Container extends BaseFragment
	 {
	 private boolean mIsViewInited;
	 	
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() 
	 	{
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new MyFriendsAndGroupsFragment(), false);
	 	}
	 	 
	 }
  
  
  public static class BashMoments_Container extends BaseFragment
	 {
	 private boolean mIsViewInited;
	 	
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() 
	 	{
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new BashMomentsFragment(), false);
	 	}
	 	 
	 }
  
  public static class InvitePeople_Container extends BaseFragment
	 {
	 private boolean mIsViewInited;
	 	
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() 
	 	{
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new InvitationsFragment(), false);
	 	}
	 	 
	 }
  
  
 
   public static class Tab_Feed_Container extends FragmentBaseFragment
	 {
		private boolean mIsViewInited;
		
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 2 oncreateview");
	 		return inflater.inflate(R.layout.tab_fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 2 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() {
	 		Log.e("SessionView", "Called");
	 		//replaceFragment(new Transactions_Fragment(), false);
	 		//	replaceFragment(new TabFeedFragment(), false);
	 		replaceFragment(new NewsFeed_Fragment(), false);

	 	}
	 }
//   public static class Tab_Feed_MyFeed_Container extends FragmentBaseFragment{
  public static class Tab_Feed_MyFeed_Container extends BaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new TabFeed_MyFeed_Fragment(), false);
			 	}
	}

  public static class Tab_Feed_Trending_Container extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new TabFeed_Trending_Fragment(), false);
			 	}
	}

  public static class Tab_MyTransaction_Container extends FragmentBaseFragment
	 {
	 private boolean mIsViewInited;
	 	
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.tab_fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			initView();
	 		}
	 	}
	 	
	 	private void initView() 
	 	{
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new MyTrans_Pending_Fragment(), false);
	 		//replaceFragment(new MyTransactionFragment(), false);
	 	}
	 }
  
  public static class Tab_User_Container extends FragmentBaseFragment
	 {
	 private boolean mIsViewInited;
	 	
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.tab_fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() 
	 	{
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new TabUserFragment(), false);
	 	}

	 }

  public static class Tab_Mybash_Container extends FragmentBaseFragment
	 {
		private boolean mIsViewInited;
		
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.tab_fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() {
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new MyBash_Fragment(), false);
	 		//replaceFragment(new TabUserFragment(), false);
	 	}
	 }
  
  
  /*public static class Tab_Mytransaction_Container extends FragmentBaseFragment
	 {
		private boolean mIsViewInited;
		
	 	@Override
	 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	 		Log.e("test", "tab 1 oncreateview");
	 		return inflater.inflate(R.layout.tab_fragment_holder, null);
	 	}
	 	
	 	@Override
	 	public void onActivityCreated(Bundle savedInstanceState) {
	 		super.onActivityCreated(savedInstanceState);
	 		Log.e("test", "tab 1 container on activity created");
	 		if (!mIsViewInited) 
	 		{
	 			mIsViewInited = true;
	 			initView();
	 		} 
	 	}
	 	
	 	private void initView() {
	 		Log.e("SessionView", "Called");
	 		replaceFragment(new MyTransaction_Fragment(), false);
	 		//replaceFragment(new TabUserFragment(), false);
	 	}
	 }
  */
  
  public static class Tab_MyProfile_MyTransaction_Pending_Container extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new MyTransaction_Pending_Fragment(), false);
			 	}
	}
  
  public static class Tab_Home_YouAreOwed_MyTransaction_Friends_Container extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new FriendsFragment(3), false);
			 		//replaceFragment(new MyTrans_Processed_Friends_Fragment(), false);
			 	}
	}
  
  
  public static class Tab_Home_YouOwe_MyTransaction_Friends_Container extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new FriendsFragment(2), false);
			 		//replaceFragment(new MyTrans_Processed_Friends_Fragment(), false);
			 	}
	}
  
  public static class Tab_Home_Total_MyTransaction_Friends_Container extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new FriendsFragment(1), false);
			 		//replaceFragment(new MyTrans_Processed_Friends_Fragment(), false);
			 	}
	}
  
  
  
  public static class Tab_Home_MyTransaction_Processed_Friends_Container extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		//replaceFragment(new FriendsFragment(), false);
			 		replaceFragment(new MyTrans_Processed_Friends_Fragment(), false);
			 	}
	}
  
  
  public static class Tab_Home_YouAreOwed_MyTransaction_Groups_Container extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new GroupsFragment(3), false);
			 		//replaceFragment(new MyTrans_Processed_Groups_Fragment(), false);
			 	}
	}
  
  public static class Tab_Home_YouOwe_MyTransaction_Groups_Container extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new GroupsFragment(2), false);
			 		//replaceFragment(new MyTrans_Processed_Groups_Fragment(), false);
			 	}
	}
  
  
  public static class Tab_Home_Total_MyTransaction_Groups_Container extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new GroupsFragment(1), false);
			 		//replaceFragment(new MyTrans_Processed_Groups_Fragment(), false);
			 	}
	}
  
  
  public static class Tab_Home_MyTransaction_Processed_Groups_Container extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		//replaceFragment(new GroupsFragment(), false);
			 		replaceFragment(new MyTrans_Processed_Groups_Fragment(), false);
			 	}
	}
  
  public static class Tab_MyWallet_Banking_SavedCards extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new MyWallet_Banking_SavedCards_Fragment(), false);
			 	}
	}
  
  
  public static class Tab_MyWallet_Deposit_SavedCards extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new Deposit_SavedCard_Fragment(), false);
			 	}
	}

  public static class Tab_MyWallet_Banking_SavedCard extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new Banking_SavedCard_Fragment(), false);
			 	}
	}
  
  public static class Tab_MyWallet_Banking_SavedAccount extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new Banking_SavedAccount(), false);
			 	}
	}

  public static class Tab_MyWallet_Banking_NewAccount extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new Banking_NewAccount(), false);
			 	}
	}

  
  
  public static class Tab_MyWallet_Deposit_DebitCards extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new Deposit_DebitCard_Fragment(), false);
			 	}
	}


  
  public static class Tab_MyWallet_Credit_DebitCards extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new Deposit_DebitCard_Fragment(), false);
			 	}
	}


  
  
  
  public static class Tab_Transactions_YouOwe_Container extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new Transaction_YouOwe_Fragment(), false);
			 	}
			 	
	}
  
  
  public static class Tab_MyTransactions_TotalBalacne_Container extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new MyTransaction_Home_Total_Fragment(), false);
			 	}
	}
  
  public static class Tab_MyTransactions_YouOwe_Container extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new MyTransaction_Home_YouOwe_Fragment(), false);
			 	}
	}
  
  public static class Tab_MyTransactions_YouAreOwed_Container extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new MyTransaction_Home_YouAreOwed_Fragment(), false);
			 	}
	}
  
  
  public static class Tab_MyProfile_MyTransaction_History_Container extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new MyTransaction_History_Fragment(), false);
			 	}
	}
  
  public static class Tab_MyProfile_YouAreOwed_Container extends FragmentBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.tab_fragment_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		replaceFragment(new Transaction_YouAreOwed_Fragment(), false);
			 	}
	}
  public static class AddTab_Pay_Container extends AddTabBaseFragment{

		private boolean mIsViewInited;
		private String fragTagstr;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.addtab_frame_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			
			 			if(AddTabActivity.mTabhost.getCurrentTabTag() != null){
			 				
			 				fragTagstr = AddTabActivity.mTabhost.getCurrentTabTag().toString();
			 			
			 			}
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		//if(fragTagstr.equals(AppConstants.TAB_PAY_TAG))
			 		replaceFragment(new AddTabPayFragment(), false,fragTagstr);
			 	}
	}
  public static class AddTab_Split_Container extends AddSplitTabBaseFragment{

		private boolean mIsViewInited;
		private String fragTagstr;
		private Bundle bundle;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.addsplittab_frame_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			bundle = getArguments();
			 			
			 			if(AddTabSplitActivity.mTabhost.getCurrentTabTag() != null){
			 				
			 				fragTagstr = AddTabSplitActivity.mTabhost.getCurrentTabTag().toString();
			 				bundle.putString(AppConstants.FRAGMENT_TAG, fragTagstr);
			 			}
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		if(!fragTagstr.equals(AppConstants.SPLIT_TAB_ITEMWISE_TAG))
			 		replaceFragment(new AddTabSplitFragment(), false, bundle);
			 	}
	}
  public static class AddTab_Receive_Container extends AddTabBaseFragment{

		private boolean mIsViewInited;
			 	
			 	@Override
			 	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			 		Log.e("test", "tab 1 oncreateview");
			 		return inflater.inflate(R.layout.addtab_frame_holder, null);
			 		//return inflater.inflate(R.layout.fragment_holder, null);
			 		
			 	}
			 	
			 	@Override
			 	public void onActivityCreated(Bundle savedInstanceState) {
			 		super.onActivityCreated(savedInstanceState);
			 		Log.e("test", "tab 1 container on activity created");
			 		if (!mIsViewInited) 
			 		{
			 			mIsViewInited = true;
			 			initView();
			 		} 
			 	}
			 	
			 	private void initView() 
			 	{
			 		Log.e("SessionView", "Called");
			 		//replaceFragment(new AddTabReceiveFragment(), false);
			 	}
	}
  
  
}
